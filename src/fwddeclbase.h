/*
    Copyright 2020-2022 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef FWDDECLBASE_H
#define FWDDECLBASE_H

#include <QMetaType>
#include <QDataStream>
#include <src/util/strongnum.h>

template <typename T> struct is_variant_savable;

// Helper macro to declare a StrongerNum, and register it with Qt for use with variants,
// register it with our preferences saving system, and register it for save/load with QDataStream;
#define REGISTER_STRONGNUM_BASE(BASETYPE, T, OPTS) \
    using T = Strong::Num<class T##_KEY, BASETYPE, OPTS>; \
    Q_DECLARE_METATYPE(T) \
    inline QDataStream& operator<<(QDataStream& stream, const T& t) { return stream << T::base_type(t); } \
    inline QDataStream& operator>>(QDataStream& stream, T& t) { return stream >> reinterpret_cast<T::base_type&>(t); } \
    template <> struct is_variant_savable<T> { static constexpr bool value = true; }

#define REGISTER_STRONGNUM(BASETYPE, T) \
    REGISTER_STRONGNUM_BASE(BASETYPE, T, Strong::Options::None)

#define REGISTER_STRONGNUM_EXP(BASETYPE, T) \
    REGISTER_STRONGNUM_BASE(BASETYPE, T, Strong::Options::ExplicitConstruction)

// For types which do not need QDataStream support
#define REGISTER_STRONGNUM_INTERNAL(BASETYPE, T) \
    using T = Strong::Num<class T##_KEY, BASETYPE>

#define REGISTER_STRONGNUM_INT_EXP(BASETYPE, T) \
    using T = Strong::Num<class T##_KEY, BASETYPE, Strong::Options::ExplicitConstruction>

REGISTER_STRONGNUM(int32_t, PaneId_t);
REGISTER_STRONGNUM(int32_t, PaneClass_t);
REGISTER_STRONGNUM(int32_t, ModelId_t);

#endif // FWDDECLBASE_H
