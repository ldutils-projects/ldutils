/*
    Copyright 2020-2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef SEEKABLESTREAM_H
#define SEEKABLESTREAM_H

#include <QIODevice>
#include <QTemporaryFile>

// Some file format generation requires seeking to the beginning of a file after
// generating the file's contents.  That isn't possible on stdout, so to support
// such streams, this class uses a temporary file, and copies the temp file to
// the stream upon close.
//
// Streams opened in WriteOnly mode shortcircuit the temp file.
//
// Currently this is only implemented for output streams, but could be extended
// for input stream arbitrary seeking as well.
class SeekableStream final : public QIODevice
{
public:
    SeekableStream(int fd);
    ~SeekableStream() override;

    void close() override;
    bool open(QIODevice::OpenMode) override;
    qint64 pos() const override;
    bool seek(qint64 pos) override;
    qint64 size() const override;

private:
    qint64 readData(char *data, qint64 len) override;
    qint64 writeData(const char *data, qint64 len) override;

    QTemporaryFile m_tmpFile;    // temp file to use
    QFile          m_directOut;  // short ciruit the temp file
    int            m_fd;         // FD to copy to
};

#endif // SEEKABLESTREAM_H
