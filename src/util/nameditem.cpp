/*
    Copyright 2020-2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "nameditem.h"

const NamedItemInterface::name_t& NamedItemInterface::getItemName(const QModelIndexList& selections) const
{
    if (selections.isEmpty())
        return getItemName();

    return getItemName(selections.front());
}

NamedItem::NamedItem(const name_t& name) :
    m_name(name)
{
}

NamedItem::NamedItem(const QString& singular, const QString& plural) :
    NamedItem(std::make_tuple(singular, plural))
{
}

NamedItem::name_t NamedItemInterface::getItemNameLower() const
{
    const auto names = getItemName();
    return std::make_tuple(std::get<0>(names).toLower(),
                           std::get<1>(names).toLower());
}
