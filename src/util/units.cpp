/*
    Copyright 2019-2023 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <cassert>
#include <tuple>
#include <array>

#include <QGuiApplication>
#include <QMutexLocker>
#include <QDateTime>
#include <QDate>
#include <QTime>
#include <QComboBox>
#include <QRegularExpression>
#include <QLocale>

#include "units.h"
#include "maybe_unused.h"

decltype(Units::m_suffixes)   Units::m_suffixes;
decltype(Units::m_formats)    Units::m_formats;
decltype(Units::m_durFormats) Units::m_durFormats;
decltype(Units::parseError)   Units::parseError(QVariant(), Format::_Invalid, -1);

QtCompat::RecursiveMutex Units::m_cacheLock;

// QRegularExpression::OptimizeOnFirstUsageOption is depreciated since Qt 5.12
#   if (QT_VERSION < QT_VERSION_CHECK(5, 12, 0))
#      define REGEXOPTS QRegularExpression::OptimizeOnFirstUsageOption
#   else
#      define REGEXOPTS QRegularExpression::NoPatternOption
#   endif

namespace {
std::tuple<int, int, double> DegreesToDMS(double ang)
{
    const double sgn = (ang < 0.0) ? -1.0 : 1.0;
    ang = std::abs(ang);

    const int deg = int(ang);
    double frac = ang - double(deg);

    frac *= 60.0;
    int min = int(frac);
    frac -= double(min);

    double sec = std::round(frac * 600000.0) / 10000.0;

    if (sec >= 60.0) {
        ++min;
        sec -= 60.0;
    }

    return std::make_tuple(sgn * deg, min, sec);
}

} // anonymous namespace

const QString& Units::findFmtStr(const QHash<Format, QString>& formats, Format f)
{
    static const QString empty("");

    if (const auto it = formats.find(f); it != formats.end())
        return it.value();

    return empty;
}

const QString& Units::dateFormat() const
{
    static const QString empty;

    switch (rangeBegin()) {
    case Format::_DateTimeBegin:  [[fallthrough]];
    case Format::_TimeBegin:      [[fallthrough]];
    case Format::_DateBegin:      return fmtStr(format());
    case Format::_DurBegin:       return durFmtStr(format());
    default:
        return empty;
    }
}

bool Units::isDateFormat() const
{
    switch (rangeBegin()) {
    case Format::_DateTimeBegin:  [[fallthrough]];
    case Format::_TimeBegin:      [[fallthrough]];
    case Format::_DateBegin:      [[fallthrough]];
    case Format::_DurBegin:       return true;
    default:                      return false;
    }
}

void Units::setupFormats()
{
    if (!m_formats.isEmpty())
        return;

    QMutexLocker lock(&m_cacheLock);

    m_formats[Format::DurS]                     = "%1s";
    m_formats[Format::DurM]                     = "%1m";
    m_formats[Format::DurMS]                    = "%1m %2s";
    m_formats[Format::DurHMS]                   = "%1h %2m %3s";
    m_formats[Format::DurH]                     = "%1h";
    m_formats[Format::DurDHMS]                  = "%1d %2h %3m %4s";
    m_formats[Format::DurD]                     = "%1d";

    // The QDateTimeAxis can't display durations, so for charts, use a QValueAxis displaying fractional hours
    m_durFormats[Format::DurS]                  =
    m_durFormats[Format::DurM]                  =
    m_durFormats[Format::DurMS]                 =
    m_durFormats[Format::DurHMS]                =
    m_durFormats[Format::DurH]                  =
    m_durFormats[Format::DurDHMS]               =
    m_durFormats[Format::DurD]                  = "%2.2f h";

    m_formats[Format::GeoPosDMS]                = "%1° %2' %3\"%4";

    const QLocale locale = QLocale();

    m_formats[Format::DateTimeISO]              = "YYYY-MM-DDTHH:mm:ss";
    m_formats[Format::DateTimeLocaleShort]      = locale.dateTimeFormat(QLocale::ShortFormat);
    m_formats[Format::DateTimeLocaleLong]       = locale.dateTimeFormat(QLocale::LongFormat);
    m_formats[Format::DateTimeyyyyMMMdd_hhmmss] = "yyyy-MMM-dd hh:mm:ss";
    m_formats[Format::DateTimeyyyyMMdd_hhmmss]  = "yyyy-MM-dd hh:mm:ss";
    m_formats[Format::DateTimeyyMMMdd_hhmmss]   = "yy-MMM-dd hh:mm:ss";
    m_formats[Format::DateTimeyyMMdd_hhmmss]    = "yy-MM-dd hh:mm:ss";
    m_formats[Format::DateTimeddMMMyyyy_hhmmss] = "dd-MMM-yyyy hh:mm:ss";
    m_formats[Format::DateTimeddMMyyyy_hhmmss]  = "dd-MM-yyyy hh:mm:ss";
    m_formats[Format::DateTimeddMMMyy_hhmmss]   = "dd-MMM-yy hh:mm:ss";
    m_formats[Format::DateTimeddMMyy_hhmmss]    = "dd-MM-yy hh:mm:ss";
    m_formats[Format::DateTimeyyyyMMMdd]        = "yyyy-MMM-dd";
    m_formats[Format::DateTimeyyyyMMdd]         = "yyyy-MM-dd";
    m_formats[Format::DateTimeyyMMMdd]          = "yy-MMM-dd";
    m_formats[Format::DateTimeyyMMdd]           = "yy-MM-dd";
    m_formats[Format::DateTimeddMMMyyyy]        = "dd-MMM-yyyy";
    m_formats[Format::DateTimeddMMyyyy]         = "dd-MM-yyyy";
    m_formats[Format::DateTimeddMMMyy]          = "dd-MMM-yy";
    m_formats[Format::DateTimeddMMyy]           = "dd-MM-yy";
    m_formats[Format::DateTimehhmmss]           = "hh:mm:ss";
    m_formats[Format::DateTimehhmm]             = "hh:mm";
    m_formats[Format::DateTimehh_mm_ss]         = "hh'h' mm'm' ss's'";
    m_formats[Format::DateTimehh_mm]            = "hh'h' mm'm'";

    m_formats[Format::Timehhmmss]               = "hh:mm:ss";
    m_formats[Format::Timehhmm]                 = "hh:mm";
    m_formats[Format::Timehh_mm_ss]             = "hh'h' mm'm' ss's'";
    m_formats[Format::Timehh_mm]                = "hh'h' mm'm'";

    m_formats[Format::DateLocaleShort]          = locale.dateFormat(QLocale::ShortFormat);
    m_formats[Format::DateLocaleLong]           = locale.dateFormat(QLocale::LongFormat);
    m_formats[Format::DateyyyyMMMdd]            = "yyyy-MMM-dd";
    m_formats[Format::DateyyyyMMdd]             = "yyyy-MM-dd";
    m_formats[Format::DateyyMMMdd]              = "yy-MMM-dd";
    m_formats[Format::DateyyMMdd]               = "yy-MM-dd";
    m_formats[Format::DateddMMMyyyy]            = "dd-MMM-yyyy";
    m_formats[Format::DateddMMyyyy]             = "dd-MM-yyyy";
    m_formats[Format::DateddMMMyy]              = "dd-MMM-yy";
    m_formats[Format::DateddMMyy]               = "dd-MM-yy";
    m_formats[Format::Dateyyyy]                 = "yyyy";
    m_formats[Format::Dateyy]                   = "yy";
    m_formats[Format::DateMMMyyyy]              = "MMM-yyyy";
    m_formats[Format::DateMMyyyy]               = "MM-yyyy";
    m_formats[Format::DateMMMyy]                = "MMM-yy";
    m_formats[Format::DateMMyy]                 = "MM-yy";
    m_formats[Format::DateyyyyMMM]              = "yyyy-MMM";
    m_formats[Format::DateyyyyMM]               = "yyyy-MM";
    m_formats[Format::DateyyMMM]                = "yy-MMM";
    m_formats[Format::DateyyMM]                 = "yyyy-MM";

    m_formats[Format::PctFloat]  =
    m_formats[Format::RiseRun]   =
    m_formats[Format::GeoPosDeg] =
    m_formats[Format::GeoPosRad] =
    m_formats[Format::Float]     =
    m_formats[Format::Int]       = "%1";

    for (const auto f : {
            Format::Bytes,
            Format::KiB,
            Format::MiB,
            Format::GiB,
            Format::TiB,
            Format::PiB,
            Format::EiB,
            Format::KB,
            Format::MB,
            Format::GB,
            Format::TB,
            Format::PB,
            Format::EB,

            Format::Distmm,
            Format::Distm,
            Format::Distkm,
            Format::DistAU,
            Format::Distft,
            Format::Distmi,
            Format::DistSmoot,

            Format::SpeedMPS,
            Format::SpeedKPH,
            Format::SpeedMPH,
            Format::SpeedFPS,
            Format::SpeedMPKm,
            Format::SpeedMPMi,

            Format::Aream2,
            Format::Areakm2,
            Format::Areaft2,
            Format::Areami2,

            Format::TempC,
            Format::TempK,
            Format::TempF,

            Format::Percent,
            Format::Degrees,
            Format::Radians,
            Format::SlopePct,

            Format::Watt,
            Format::kW,
            Format::hp,
            Format::kcalperhr,

            Format::g,
            Format::kg,
            Format::oz,
            Format::lb,
            Format::stone,
            Format::ton,

            Format::Wh,
            Format::kWh,
            Format::cal,
            Format::kcal,
            Format::J,
            Format::kJ,
            Format::MJ,

            Format::AccelMPS2,
            Format::AccelFPS2,

            Format::BeatsPerS,
            Format::BeatsPerM,
            Format::BeatsPerH,
            Format::BeatsPerD,

            Format::RevsPerS,
            Format::RevsPerM,
            Format::RevsPerH,
            Format::RevsPerD,
        } )
        m_formats[f] = "%L1 " + suffix(f);
}

template <typename... T>
char* Units::fmtBuffer(char* buf, int buflen, const char* fmt, const T&... param)
 {
    snprintf(buf, buflen, fmt, param...);
    return buf;
}

QString Units::operator()(const QVariant& value, int forcePrecision) const
{
    return convert(value, m_format, forcePrecision >= 0 ? forcePrecision : m_precision);
}

QString Units::operator()(const QTimeZone& value, const QDateTime& at) const
{
    switch (m_format) {
    case Format::TzShort:  return value.displayName(at, QTimeZone::ShortName);
    case Format::TzOffset: return value.displayName(at, QTimeZone::OffsetName);
    case Format::TzLong:   return value.displayName(at, QTimeZone::LongName);
    case Format::TzIANA:   return value.id();
    default:               return "Unsupported format";
    }
}

QVariant Units::to(const QVariant& value, Format fmt, bool utc)
{
    switch (rangeBegin(fmt)) {
    case Format::_StringBegin: return value;
    case Format::_DateTimeBegin:
        return utc ? value.toDateTime() : value.toDateTime().toTimeZone(QTimeZone::systemTimeZone());
    case Format::_TimeBegin: return value.toTime();
    case Format::_DateBegin: return value.toDate();
    default:
        break;
    }

    return toDouble(value, fmt);
}

QVariant Units::from(const QVariant& value, Format fmt, bool utc)
{
    if (!value.isValid())
        return value;

    switch (rangeBegin(fmt)) {
    case Format::_StringBegin: return value;
    case Format::_DateTimeBegin:
        return utc ? value : value.toDateTime().toTimeZone(QTimeZone::utc());
    case Format::_TimeBegin:   return value;
    case Format::_DateBegin:   return value;
    default:
        break;
    }

    const double valueD = value.toDouble();

    switch (fmt) {
    case Format::String:    return value;
    case Format::TempK:     return valueD - 273.15;
    case Format::TempF:     return (valueD - 32.0) * 5.0/9.0;
    case Format::Degrees:   return std::tan(Math::toRad(valueD));
    case Format::Radians:   return std::tan(valueD);
    case Format::GeoPosRad: return Math::toRad(valueD);
    case Format::SpeedMPKm: return 1.0_SpeedMPKm / valueD;
    case Format::SpeedMPMi: return 1.0_SpeedMPMi / valueD;
    default:                return valueD * multiplier(fmt);
    }
}

Units& Units::setFormat(Format f)
{
    // m_maxFormat is one past last valid format, so subtract 1 for the clamp.
    m_format = Format(std::clamp(int(f), int(rangeBegin()), int(rangeEnd()) - 1));
    return *this;
}

Units& Units::setFormat(const QString& name)
{
    return setFormat(findSuffix(name));
}

Format Units::findSuffix(const QString& suffixStr) const
{
    return findSuffix(QStringRef(&suffixStr));
}

Format Units::findSuffix(const QStringRef& suffixStr) const
{
    if (suffixStr.isEmpty())
        return Format::_Invalid;

    for (Format f = rangeBegin(); f != rangeEnd(); Util::inc(f))
        for (const QString& suffix : suffixes(f))
            if (suffix.compare(suffixStr, Qt::CaseInsensitive) == 0)
                return f;

    return Format::_Invalid;
}

auto Units::parseNumSuffix(const QStringRef& text)
{
    // Regexps: emulating line noise since 1972
    // Capture 1 = number, capture 3 = unit suffix
    static const QRegularExpression
            //                       spaces+number+spaces                     (alpha | spec chars | exponent)*
            numberAndSuffix(R"REGEX(^[\s]*(-?([\d]*[.][\d]+|[\d]+[.]?))[\s]*(([[:alpha:]/.%°]|(\^[[:digit:]]))*))REGEX",
                            REGEXOPTS);

    const auto match = numberAndSuffix.match(text);

    bool isNumber = false;
    const bool hasMatch = match.hasMatch();
    const double number = hasMatch ? match.capturedRef(1).toDouble(&isNumber) : 0.0;

    return std::make_tuple(hasMatch && isNumber, number,
                           match.capturedRef(3),   // unit suffix
                           match.capturedEnd(0),   // entire match length
                           match.capturedEnd(1));  // whitespace+number match length
}

Units::ParseCode Units::parseDuration(const QStringRef& text) const
{
    QStringRef parsePos = text;
    Format fmt = Format::_Invalid;
    qreal d = -1.0, h = -1.0, m = -1.0, s = -1.0;
    int len = 0;
    bool bad = false; // set on error

    while (!bad) {
        [[maybe_unused]] const auto [isValid, value, suffix, length, numLength] = parseNumSuffix(parsePos);
        Util::maybe_unused(numLength); // see comment in maybe_unused.h

        if (!isValid || suffix.isEmpty())
            break;

        switch (findSuffix(suffix)) {
        case Format::DurD: bad |= (d >= 0); [[fallthrough]];
        case Format::DurH: bad |= (h >= 0); [[fallthrough]];
        case Format::DurM: bad |= (m >= 0); [[fallthrough]];
        case Format::DurS: bad |= (s >= 0); [[fallthrough]];
        default: bad |= (value < 0.0);
        }

        switch (findSuffix(suffix)) {
        case Format::DurS: s = value; break;
        case Format::DurM: m = value; break;
        case Format::DurH: h = value; break;
        case Format::DurD: d = value; break;
        default: bad = true; break;
        }

        if (bad)
            break;

        len += length;
        parsePos = parsePos.mid(length);
    }

    if (bad || len == 0)
        return parseError;

    if (d >= 0 && h >= 0 && m >= 0 && s >= 0) fmt = Format::DurDHMS;
    else if (h >= 0 && m >= 0 && s >= 0) fmt = Format::DurHMS;
    else if (m >= 0 && s >= 0) fmt = Format::DurMS;
    else if (s >= 0) fmt = Format::DurS;
    else if (m >= 0) fmt = Format::DurM;
    else if (h >= 0) fmt = Format::DurH;
    else if (d >= 0) fmt = Format::DurD;

    s = std::fmax(s, 0.0);
    m = std::fmax(m, 0.0);
    h = std::fmax(h, 0.0);
    d = std::fmax(d, 0.0);

    // Convert to nanoseconds for duration
    const auto nanoSec = quint64((s + (m + (h + d * 24.0) * 60.0) * 60.0) * 1e9);
    return { nanoSec, fmt, len };
}

Units::ParseCode Units::parseString(const QStringRef& text) const
{
    // This matches leading space, followed by either:
    //    1. A quote plus zero or more non-escaped-quote characters plus a quote
    //    2. One or more non-quote and non-space
    // That isn't perfect, but it's good enough for our bargain-bin query language.
    static const QRegularExpression stringRe(R"REGEX(^\s*("(([^"]|[\]["])*)"|[^"\s]+))REGEX", REGEXOPTS);

    const auto match = stringRe.match(text);
    if (!match.hasMatch())
        return parseError;

    const int cap = match.capturedLength(2) > 0 ? 2 : 1;

    return { match.captured(cap), format(), match.capturedEnd(0) };
}

template <class T>
Units::ParseCode Units::parseDateStamp(const QStringRef& text, Format begin, Format end) const
{
    static const QRegularExpression token3(R"REGEX(^\s*(\S+\s+\S+\s+\S+))REGEX", REGEXOPTS);
    static const QRegularExpression token2(R"REGEX(^\s*(\S+\s+\S+))REGEX", REGEXOPTS);
    static const QRegularExpression token1(R"REGEX(^\s*(\S+))REGEX", REGEXOPTS);

    // check next two words, then one word, then tree.
    for (const auto& regex : { token2, token1, token3 }) {
        if (const auto match = regex.match(text); match.hasMatch()) {
            const QStringRef dateStr = match.capturedRef(1);

            for (Format fmt = begin; fmt != end; Util::inc(fmt)) {
                const QString& dateFmt = fmtStr(fmt);
                const T date = T::fromString(dateStr.toString(), dateFmt);
                if (date.isValid())
                    return ParseCode(date, fmt, match.capturedEnd(0));
            }
        }
    }

    return parseError;
}

Units::ParseCode Units::parseValueSuffix(const QStringRef& text) const
{
    if (const auto [isValid, value, suffix, length, numLength] = parseNumSuffix(text); isValid) {
        if (!suffix.isEmpty()) {
            if (const Format fmt = findSuffix(suffix); fmt != Format::_Invalid)
                return std::make_tuple(from(value, fmt, isUTC()), fmt, length);
            return { from(value), format(), numLength };
        }

        return { from(value), format(), length };
    }

    return parseError;
}

Units::ParseCode Units::parse(const QString& text) const
{
    return Units::parse(QStringRef(&text));
}

Units::ParseCode Units::parse(const QStringRef& text) const
{
    if (format() == Format::String)
        return parseString(text);

    switch (rangeBegin()) {
    case Format::_DurBegin:      return parseDuration(text);
    case Format::_DateTimeBegin: return parseDateStamp<QDateTime>(text, Format::_DateTimeBegin, Format::_DateTimeEnd);
    case Format::_TimeBegin:     return parseDateStamp<QTime>(text, Format::_TimeBegin, Format::_TimeEnd);
    case Format::_DateBegin:     return parseDateStamp<QDate>(text, Format::_DateBegin, Format::_DateEnd);
    default:                     return parseValueSuffix(text);
    }
}

QString Units::convert(const QVariant& value, Format fmt, int precision) const
{
    fmt = autoUnit(value, fmt);

    const QString& fmtStr = Units::fmtStr(fmt);

    switch (fmt) {
    // It's a little crazy there isn't an std:: gizmo to do these.  std::chrono
    // didn't seem to help, but maybe it's just a matter of finding it...
    case Format::DurS:    [[fallthrough]];
    case Format::DurMS:   [[fallthrough]];
    case Format::DurHMS:  [[fallthrough]];
    case Format::DurDHMS: {
        // Incoming time is in nanoseconds
        lldiv_t t = lldiv(value.toLongLong(), 1e9);
        const int64_t nano = t.rem;
        if (fmt == Format::DurS)
            return fmtStr.arg(double(t.quot) + double(nano) / 1e9, 2, 'f', precision, '0');

        t = lldiv(t.quot, 60);
        const double s = double(t.rem) + double(nano) / 1e9;
        if (fmt == Format::DurMS) {
            if (!m_leadingZeros && t.quot == 0)
                return convert(value, Format::DurS, precision);
            return fmtStr.arg(t.quot, 0, 10, QChar('0'))
                         .arg(s, 2, 'f', precision, QChar('0'));
        }

        t = lldiv(t.quot, 60);
        const auto m = uint32_t(t.rem);
        if (fmt == Format::DurHMS) {
            if (!m_leadingZeros && t.quot == 0)
                return convert(value, Format::DurMS, precision);

            return fmtStr.arg(t.quot, 0, 10, QChar('0'))
                         .arg(m,      2, 10, QChar('0'))
                         .arg(s, 2, 'f', precision, QChar('0'));
        }

        t = lldiv(t.quot, 24);
        const int64_t h = t.rem;
        {
            if (!m_leadingZeros && t.quot == 0)
                return convert(value, Format::DurHMS, precision);

            return fmtStr.arg(t.quot, 0, 10, QChar('0'))
                         .arg(h,      2, 10, QChar('0'))
                         .arg(m,      2, 10, QChar('0'))
                         .arg(s, 2, 'f', precision, QChar('0'));
        }
    }

    case Format::Float:
        return fmtStr.arg(value.toDouble(), 2, 'f', precision, QChar('0'));

    case Format::Int:
        return fmtStr.arg(value.toLongLong(), m_leadingZeros ? precision : 0, 10, QChar('0'));

    case Format::String:
        return value.toString();

    case Format::Bytes:
        return fmtStr.arg(value.toULongLong());

    case Format::PctFloat:   [[fallthrough]];
    case Format::Percent:    [[fallthrough]];
    case Format::DurM:       [[fallthrough]];
    case Format::DurH:       [[fallthrough]];
    case Format::DurD:       [[fallthrough]];
    case Format::KiB:        [[fallthrough]];
    case Format::MiB:        [[fallthrough]];
    case Format::GiB:        [[fallthrough]];
    case Format::TiB:        [[fallthrough]];
    case Format::PiB:        [[fallthrough]];
    case Format::EiB:        [[fallthrough]];
    case Format::KB:         [[fallthrough]];
    case Format::MB:         [[fallthrough]];
    case Format::GB:         [[fallthrough]];
    case Format::TB:         [[fallthrough]];
    case Format::PB:         [[fallthrough]];
    case Format::EB:         [[fallthrough]];

    case Format::Distmm:     [[fallthrough]];
    case Format::Distm:      [[fallthrough]];
    case Format::Distkm:     [[fallthrough]];
    case Format::DistAU:     [[fallthrough]];
    case Format::Distft:     [[fallthrough]];
    case Format::Distmi:     [[fallthrough]];
    case Format::DistSmoot:  [[fallthrough]];

    case Format::SpeedMPS:   [[fallthrough]];
    case Format::SpeedKPH:   [[fallthrough]];
    case Format::SpeedMPH:   [[fallthrough]];
    case Format::SpeedFPS:   [[fallthrough]];
    case Format::SpeedMPKm:  [[fallthrough]];
    case Format::SpeedMPMi:  [[fallthrough]];

    case Format::Aream2:     [[fallthrough]];
    case Format::Areakm2:    [[fallthrough]];
    case Format::Areaft2:    [[fallthrough]];
    case Format::Areami2:    [[fallthrough]];

    case Format::SlopePct:   [[fallthrough]];
    case Format::RiseRun:    [[fallthrough]];
    case Format::GeoPosDeg:  [[fallthrough]];

    case Format::Watt:       [[fallthrough]];
    case Format::kW:         [[fallthrough]];
    case Format::hp:         [[fallthrough]];
    case Format::kcalperhr:  [[fallthrough]];

    case Format::g:          [[fallthrough]];
    case Format::kg:         [[fallthrough]];
    case Format::oz:         [[fallthrough]];
    case Format::lb:         [[fallthrough]];
    case Format::stone:      [[fallthrough]];
    case Format::ton:        [[fallthrough]];

    case Format::Wh:         [[fallthrough]];
    case Format::kWh:        [[fallthrough]];
    case Format::cal:        [[fallthrough]];
    case Format::kcal:       [[fallthrough]];
    case Format::J:          [[fallthrough]];
    case Format::kJ:         [[fallthrough]];
    case Format::MJ:         [[fallthrough]];

    case Format::AccelMPS2:  [[fallthrough]];
    case Format::AccelFPS2:  [[fallthrough]];

    case Format::TempC:      [[fallthrough]];
    case Format::TempK:      [[fallthrough]];
    case Format::TempF:      [[fallthrough]];

    case Format::Degrees:    [[fallthrough]];
    case Format::Radians:    [[fallthrough]];
    case Format::GeoPosRad:  [[fallthrough]];

    case Format::RevsPerS:   [[fallthrough]];
    case Format::RevsPerM:   [[fallthrough]];
    case Format::RevsPerH:   [[fallthrough]];
    case Format::RevsPerD:   [[fallthrough]];

    case Format::BeatsPerS:  [[fallthrough]];
    case Format::BeatsPerM:  [[fallthrough]];
    case Format::BeatsPerH:  [[fallthrough]];
    case Format::BeatsPerD:
        return fmtStr.arg(toDouble(value, fmt), 0, 'f', precision, '0');

    case Format::GeoPosDMS: {
        const double dmsVal = value.toDouble();
        const auto& [deg, min, sec] = DegreesToDMS(std::abs(dmsVal));
        return fmtStr.arg(deg, m_leadingZeros ? 3 : 0, 10, QChar('0'))
                     .arg(min, 2, 10, QChar('0'))
                     .arg(sec, precision+3, 'f', precision, QChar('0')) // +3 -> 1 for '.', 2 for leading 0's
                     .arg((dmsVal < 0.0) ? m_neg : m_pos);
    }

    case Format::DateTimeISO:
        return to(value).toDateTime().toString(Qt::ISODate);
    case Format::DateTimeLocaleLong:
        return QLocale().toString(to(value).toDateTime(), QLocale::LongFormat);
    case Format::DateTimeLocaleShort:
        return QLocale().toString(to(value).toDateTime(), QLocale::ShortFormat);
    case Format::DateTimeyyyyMMMdd_hhmmss:  [[fallthrough]];
    case Format::DateTimeyyMMMdd_hhmmss:    [[fallthrough]];
    case Format::DateTimeddMMMyyyy_hhmmss:  [[fallthrough]];
    case Format::DateTimeddMMMyy_hhmmss:    [[fallthrough]];
    case Format::DateTimeyyyyMMdd_hhmmss:   [[fallthrough]];
    case Format::DateTimeyyMMdd_hhmmss:     [[fallthrough]];
    case Format::DateTimeddMMyyyy_hhmmss:   [[fallthrough]];
    case Format::DateTimeddMMyy_hhmmss:     [[fallthrough]];
    case Format::DateTimeyyyyMMMdd:         [[fallthrough]];
    case Format::DateTimeyyyyMMdd:          [[fallthrough]];
    case Format::DateTimeyyMMMdd:           [[fallthrough]];
    case Format::DateTimeyyMMdd:            [[fallthrough]];
    case Format::DateTimeddMMMyyyy:         [[fallthrough]];
    case Format::DateTimeddMMyyyy:          [[fallthrough]];
    case Format::DateTimeddMMMyy:           [[fallthrough]];
    case Format::DateTimeddMMyy:            [[fallthrough]];
    case Format::DateTimehhmmss:            [[fallthrough]];
    case Format::DateTimehhmm:              [[fallthrough]];
    case Format::DateTimehh_mm_ss:          [[fallthrough]];
    case Format::DateTimehh_mm:
        return to(value).toDateTime().toString(fmtStr);

    case Format::Timehhmmss:            [[fallthrough]];
    case Format::Timehhmm:              [[fallthrough]];
    case Format::Timehh_mm_ss:          [[fallthrough]];
    case Format::Timehh_mm:
        return to(value).toTime().toString(fmtStr);

    case Format::DateLocaleShort:       [[fallthrough]];
    case Format::DateLocaleLong:        [[fallthrough]];
    case Format::DateyyyyMMMdd:         [[fallthrough]];
    case Format::DateyyyyMMdd:          [[fallthrough]];
    case Format::DateyyMMMdd:           [[fallthrough]];
    case Format::DateyyMMdd:            [[fallthrough]];
    case Format::DateddMMMyyyy:         [[fallthrough]];
    case Format::DateddMMyyyy:          [[fallthrough]];
    case Format::DateddMMMyy:           [[fallthrough]];
    case Format::DateddMMyy:            [[fallthrough]];
    case Format::Dateyyyy:              [[fallthrough]];
    case Format::Dateyy:                [[fallthrough]];
    case Format::DateMMMyyyy:           [[fallthrough]];
    case Format::DateMMyyyy:            [[fallthrough]];
    case Format::DateMMMyy:             [[fallthrough]];
    case Format::DateMMyy:              [[fallthrough]];
    case Format::DateyyyyMMM:           [[fallthrough]];
    case Format::DateyyyyMM:            [[fallthrough]];
    case Format::DateyyMMM:             [[fallthrough]];
    case Format::DateyyMM:

    return to(value).toDate().toString(fmtStr);

    default: return "Unsupported format";
    }
}

Format Units::autoUnit(const QVariant& value, Format fmt)
{
    const qulonglong size  = value.toULongLong();
    const qreal      sizeF = value.toDouble();

    switch (fmt) {
    case Format::AutoTime:
        if (size >= 1_DateD) return Format::DurD;
        if (size >= 1_DateH) return Format::DurH;
        if (size >= 1_DateM) return Format::DurM;
        return Format::DurS;

    case Format::AutoBinary:
        if (size >= 1_EiB) return Format::EiB;
        if (size >= 1_PiB) return Format::PiB;
        if (size >= 1_TiB) return Format::TiB;
        if (size >= 1_GiB) return Format::GiB;
        if (size >= 1_MiB) return Format::MiB;
        if (size >= 1_KiB) return Format::KiB;
        return Format::Bytes;

    case Format::AutoDecimal:
        if (size >= 1_EB) return Format::EB;
        if (size >= 1_PB) return Format::PB;
        if (size >= 1_TB) return Format::TB;
        if (size >= 1_GB) return Format::GB;
        if (size >= 1_MB) return Format::MB;
        if (size >= 1_KB) return Format::KB;
        return Format::Bytes;

    case Format::AutoDistMetric:
        if (sizeF >= 1.0_DistAU) return Format::DistAU;
        if (sizeF >= 1.0_Distkm) return Format::Distkm;
        if (sizeF >= 1.0_Distm)  return Format::Distm;
        return Format::Distmm;

    case Format::AutoDistImperial:
        if (sizeF >= 1.0_Distmi) return Format::Distmi;
        return Format::Distft;

    default:
        return fmt;
    }
}

const QString& Units::suffix(double value) const
{
    return suffix(autoUnit(value, m_format));
}

template<> QMap<QStringRef, Format> Units::rangeSuffixes() const
{
    decltype(Units::rangeSuffixes()) suffixes;

    for (Format f = rangeBegin(); f != rangeEnd(); Util::inc(f))
        suffixes[&suffix(f)] = f;

    return suffixes;
}

template<> QStringList Units::rangeSuffixes() const
{
    QStringList suffixes;

    for (Format f = rangeBegin(); f != rangeEnd(); Util::inc(f))
        suffixes.append(suffix(f));

    return suffixes;
}

template<> QVector<QVector<QStringRef>> Units::rangeSuffixes() const
{
    QVector<QVector<QStringRef>> suffixes;

    for (Format f = rangeBegin(); f != rangeEnd(); Util::inc(f)) {
        suffixes.append(decltype(suffixes)::value_type());

        if (const auto suffixList = m_suffixes.find(f); suffixList != m_suffixes.end())
            for (const auto& suffix : suffixList.value())
                suffixes.last().append(&suffix);
    }

    return suffixes;
}

double Units::multiplier(double value) const
{
    return multiplier(autoUnit(value, m_format));
}

const QString& Units::suffix(Format f)
{
    static const QString empty("");

    if (const auto it = m_suffixes.find(f); it != m_suffixes.end())
        return it.value().front();

    return empty;
}

const QVector<QString>& Units::suffixes(Format f)
{
    static const QVector<QString> empty;

    if (const auto it = m_suffixes.find(f); it != m_suffixes.end())
        return it.value();

    return empty;
}

void Units::setupSuffixes()
{
    if (!m_suffixes.isEmpty())
        return;

    QMutexLocker lock(&m_cacheLock);

    m_suffixes[Format::DurS]      = { QObject::tr("s"),
                                      QObject::tr("sec"),
                                      QObject::tr("second"),
                                      QObject::tr("seconds") };
    m_suffixes[Format::DurM]      = { QObject::tr("m"),
                                      QObject::tr("min"),
                                      QObject::tr("minute"),
                                      QObject::tr("minutes") };
    m_suffixes[Format::DurH]      = { QObject::tr("h"),
                                      QObject::tr("hr"),
                                      QObject::tr("hour"),
                                      QObject::tr("hours") };
    m_suffixes[Format::DurD]      = { QObject::tr("d"), 
                                      QObject::tr("day"),
                                      QObject::tr("days") };
    
    m_suffixes[Format::Percent]   = { QObject::tr("%"),
                                      QObject::tr("pct") };
    m_suffixes[Format::Bytes]     = { QObject::tr("B") };
    m_suffixes[Format::KiB]       = { QObject::tr("KiB") };
    m_suffixes[Format::MiB]       = { QObject::tr("MiB") };
    m_suffixes[Format::GiB]       = { QObject::tr("GiB") };
    m_suffixes[Format::TiB]       = { QObject::tr("TiB") };
    m_suffixes[Format::PiB]       = { QObject::tr("PiB") };
    m_suffixes[Format::EiB]       = { QObject::tr("EiB") };
    m_suffixes[Format::KB]        = { QObject::tr("KB") };
    m_suffixes[Format::MB]        = { QObject::tr("MB") };
    m_suffixes[Format::GB]        = { QObject::tr("GB") };
    m_suffixes[Format::TB]        = { QObject::tr("TB") };
    m_suffixes[Format::PB]        = { QObject::tr("PB") };
    m_suffixes[Format::EB]        = { QObject::tr("EB") };

    m_suffixes[Format::Distmm]    = { QObject::tr("mm"),
                                      QObject::tr("millimeter"),
                                      QObject::tr("millimeters") };
    m_suffixes[Format::Distm]     = { QObject::tr("m"),
                                      QObject::tr("meter"),
                                      QObject::tr("meters") };
    m_suffixes[Format::Distkm]    = { QObject::tr("Km"),
                                      QObject::tr("kilometer"),
                                      QObject::tr("kilometers") };
    m_suffixes[Format::DistAU]    = { QObject::tr("AU") };
    m_suffixes[Format::Distft]    = { QObject::tr("ft"),
                                      QObject::tr("foot"),
                                      QObject::tr("feet") };
    m_suffixes[Format::Distmi]    = { QObject::tr("mi"),
                                      QObject::tr("mile"),
                                      QObject::tr("miles") };
    m_suffixes[Format::DistSmoot] = { QObject::tr("Smoot"),
                                      QObject::tr("Smoots") };
    
    m_suffixes[Format::SpeedMPS]  = { QObject::tr("m/s"),
                                      QObject::tr("mps"),
                                      QObject::tr("meter/sec") };
    m_suffixes[Format::SpeedKPH]  = { QObject::tr("Km/h"),
                                      QObject::tr("Kph") };
    m_suffixes[Format::SpeedMPH]  = { QObject::tr("mi/h"),
                                      QObject::tr("mph") };
    m_suffixes[Format::SpeedFPS]  = { QObject::tr("ft/s"),
                                      QObject::tr("fps") };
    m_suffixes[Format::SpeedMPKm] = { QObject::tr("min/km"),
                                      QObject::tr("minute/km"),
                                      QObject::tr("minutes/km"),
                                      QObject::tr("minutes/kilometer") };
    m_suffixes[Format::SpeedMPMi] = { QObject::tr("min/mi"),
                                      QObject::tr("minute/mi"),
                                      QObject::tr("minutes/mi"),
                                      QObject::tr("minutes/mile") };

    m_suffixes[Format::Aream2]    = { QObject::tr("m^2") };
    m_suffixes[Format::Areakm2]   = { QObject::tr("Km^2") };
    m_suffixes[Format::Areaft2]   = { QObject::tr("Ft^2") };
    m_suffixes[Format::Areami2]   = { QObject::tr("Mi^2") };

    m_suffixes[Format::TempC]     = { QObject::tr("C"),
                                      QObject::tr("DegC") };
    m_suffixes[Format::TempK]     = { QObject::tr("K"),
                                      QObject::tr("DegK") };
    m_suffixes[Format::TempF]     = { QObject::tr("F"),
                                      QObject::tr("DegF") };

    m_suffixes[Format::SlopePct]  = { QObject::tr("%"),
                                      QObject::tr("pct") };
    m_suffixes[Format::Degrees]   = { QObject::tr("°"),
                                      QObject::tr("deg"),
                                      QObject::tr("degree"),
                                      QObject::tr("degrees") };
    m_suffixes[Format::Radians]   = { QObject::tr("rad"),
                                      QObject::tr("radian"),
                                      QObject::tr("radians")  };

    m_suffixes[Format::GeoPosDeg] = { QObject::tr("°"),
                                      QObject::tr("deg"),
                                      QObject::tr("degree"),
                                      QObject::tr("degrees") };
    m_suffixes[Format::GeoPosRad] = { QObject::tr("rad"),
                                      QObject::tr("radian"),
                                      QObject::tr("radians")  };

    m_suffixes[Format::Watt]      = { QObject::tr("W"),
                                      QObject::tr("watt"),
                                      QObject::tr("watts") };
    m_suffixes[Format::kW]        = { QObject::tr("kW"),
                                      QObject::tr("kilowatt"),
                                      QObject::tr("kilowatts") };
    m_suffixes[Format::hp]        = { QObject::tr("hp"),
                                      QObject::tr("horsepower") };
    m_suffixes[Format::kcalperhr] = { QObject::tr("kcal/hr"),
                                      QObject::tr("kcal/hour"),
                                      QObject::tr("kilocalorie/hr"),
                                      QObject::tr("kilocalories/hr"),
                                      QObject::tr("kilocalorie/hour"),
                                      QObject::tr("kilocalories/hour") };

    m_suffixes[Format::g]         = { QObject::tr("g"),
                                      QObject::tr("gram"),
                                      QObject::tr("grams") };
    m_suffixes[Format::kg]        = { QObject::tr("kg"),
                                      QObject::tr("kilogram"),
                                      QObject::tr("kilograms") };
    m_suffixes[Format::oz]        = { QObject::tr("oz"),
                                      QObject::tr("ounce"),
                                      QObject::tr("ounces") };
    m_suffixes[Format::lb]        = { QObject::tr("lbs"),
                                      QObject::tr("lb"),
                                      QObject::tr("pound"),
                                      QObject::tr("pounds") };
    m_suffixes[Format::stone]     = { QObject::tr("stone") };
    m_suffixes[Format::ton]       = { QObject::tr("ton"),
                                      QObject::tr("tons") };

    m_suffixes[Format::Wh]        = { QObject::tr("Wh"),
                                      QObject::tr("watt.hour") };
    m_suffixes[Format::kWh]       = { QObject::tr("kWh"),
                                      QObject::tr("kW.h") };
    m_suffixes[Format::cal]       = { QObject::tr("cal"),
                                      QObject::tr("calorie"),
                                      QObject::tr("calories") };
    m_suffixes[Format::kcal]      = { QObject::tr("kcal"),
                                      QObject::tr("kilocalorie"),
                                      QObject::tr("kilocalories") };
    m_suffixes[Format::J]         = { QObject::tr("J"),
                                      QObject::tr("joule"),
                                      QObject::tr("joules") };
    m_suffixes[Format::kJ]        = { QObject::tr("kJ"),
                                      QObject::tr("kilojoule"),
                                      QObject::tr("kilojoules") };
    m_suffixes[Format::MJ]        = { QObject::tr("MJ"),
                                      QObject::tr("megajoule"),
                                      QObject::tr("megajoules") };
    m_suffixes[Format::AccelMPS2] = { QObject::tr("m/s^2"),
                                      QObject::tr("m/s/s") };
    m_suffixes[Format::AccelFPS2] = { QObject::tr("ft/s^2"),
                                      QObject::tr("ft/s/s") };

    m_suffixes[Format::RevsPerS]  = { QObject::tr("rps"),
                                      QObject::tr("rev/s"),
                                      QObject::tr("revs/s"),
                                      QObject::tr("rev/sec"),
                                      QObject::tr("revs/sec"),
                                      QObject::tr("rev/second"),
                                      QObject::tr("revs/second") };
    m_suffixes[Format::RevsPerM]  = { QObject::tr("rpm"),
                                      QObject::tr("rev/m"),
                                      QObject::tr("revs/m"),
                                      QObject::tr("rev/min"),
                                      QObject::tr("revs/min"),
                                      QObject::tr("rev/minute"),
                                      QObject::tr("revs/minute") };
    m_suffixes[Format::RevsPerH]  = { QObject::tr("rph"),
                                      QObject::tr("rev/h"),
                                      QObject::tr("revs/h"),
                                      QObject::tr("rev/hr"),
                                      QObject::tr("revs/hr"),
                                      QObject::tr("rev/hour"),
                                      QObject::tr("revs/hour")};
    m_suffixes[Format::RevsPerD]  = { QObject::tr("rev/d"),
                                      QObject::tr("revs/d"),
                                      QObject::tr("rev/day"),
                                      QObject::tr("revs/d") };

    m_suffixes[Format::BeatsPerS] = { QObject::tr("bps"),
                                      QObject::tr("beat/s"),
                                      QObject::tr("beats/s"),
                                      QObject::tr("beat/sec"),
                                      QObject::tr("beats/sec"),
                                      QObject::tr("beat/second"),
                                      QObject::tr("beats/second")};
    m_suffixes[Format::BeatsPerM] = { QObject::tr("bpm"),
                                      QObject::tr("beat/m"),
                                      QObject::tr("beats/m"),
                                      QObject::tr("beat/min"),
                                      QObject::tr("beats/min"),
                                      QObject::tr("beat/minute"),
                                      QObject::tr("beats/minute") };
    m_suffixes[Format::BeatsPerH] = { QObject::tr("bph"),
                                      QObject::tr("beat/h"),
                                      QObject::tr("beats/h"),
                                      QObject::tr("beat/hr"),
                                      QObject::tr("beats/hr"),
                                      QObject::tr("beat/hour"),
                                      QObject::tr("beats/hour") };
    m_suffixes[Format::BeatsPerD] = { QObject::tr("beats/d"),
                                      QObject::tr("beat/d"),
                                      QObject::tr("beat/day"),
                                      QObject::tr("beats/day")};
}

QString Units::name(Format f)
{
    switch (f) {
    case Format::DurS:        return QObject::tr("S");
    case Format::DurM:        return QObject::tr("M");
    case Format::DurH:        return QObject::tr("H");
    case Format::DurD:        return QObject::tr("D");
    case Format::DurMS:       return QObject::tr("MS");
    case Format::DurHMS:      return QObject::tr("HMS");
    case Format::DurDHMS:     return QObject::tr("DHMS");
    case Format::Percent:     return QObject::tr("Percent");
    case Format::PctFloat:    return QObject::tr("Float %");
    case Format::Float:       return QObject::tr("Float");
    case Format::Int:         return QObject::tr("Int");
    case Format::String:      return QObject::tr("String");

    case Format::AutoBinary:  return QObject::tr("Auto Binary");
    case Format::AutoDecimal: return QObject::tr("Auto Decimal");
    case Format::Bytes:       return QObject::tr("Bytes");
    case Format::KiB:         return QObject::tr("KiB (1024^1)");
    case Format::MiB:         return QObject::tr("MiB (1024^2)");
    case Format::GiB:         return QObject::tr("GiB (1024^3)");
    case Format::TiB:         return QObject::tr("TiB (1024^4)");
    case Format::PiB:         return QObject::tr("PiB (1024^5)");
    case Format::EiB:         return QObject::tr("EiB (1024^6)");
    case Format::KB:          return QObject::tr("KB (1000^1)");
    case Format::MB:          return QObject::tr("MB (1000^2)");
    case Format::GB:          return QObject::tr("MB (1000^3)");
    case Format::TB:          return QObject::tr("TB (1000^4)");
    case Format::PB:          return QObject::tr("PB (1000^5)");
    case Format::EB:          return QObject::tr("EB (1000^6)");

    case Format::AutoDistMetric:   return QObject::tr("Auto Metric");
    case Format::AutoDistImperial: return QObject::tr("Auto Imperial");

    case Format::Distmm:      return QObject::tr("mm (millimeters)");
    case Format::Distm:       return QObject::tr("m (meters)");
    case Format::Distkm:      return QObject::tr("km (kilometers)");
    case Format::DistAU:      return QObject::tr("Astronautical Units");
    case Format::Distft:      return QObject::tr("ft (feet)");
    case Format::Distmi:      return QObject::tr("mi (miles)");
    case Format::DistSmoot:   return QObject::tr("Smoots");

    case Format::SpeedMPS:    return QObject::tr("m/s");
    case Format::SpeedKPH:    return QObject::tr("Km/h");
    case Format::SpeedMPH:    return QObject::tr("mi/h");
    case Format::SpeedFPS:    return QObject::tr("ft/s");
    case Format::SpeedMPKm:   return QObject::tr("min/km");
    case Format::SpeedMPMi:   return QObject::tr("min/mi");

    case Format::Aream2:      return QObject::tr("m^2");
    case Format::Areakm2:     return QObject::tr("km^2");
    case Format::Areaft2:     return QObject::tr("ft^2");
    case Format::Areami2:     return QObject::tr("mi^2");

    case Format::TempC:       return QObject::tr("Celcius");
    case Format::TempK:       return QObject::tr("Kelvin");
    case Format::TempF:       return QObject::tr("Fahrenheit");

    case Format::SlopePct:    return QObject::tr("Percent");
    case Format::Degrees:     return QObject::tr("Degrees");
    case Format::Radians:     return QObject::tr("Radians");
    case Format::RiseRun:     return QObject::tr("Rise/run");

    case Format::GeoPosDMS:   return QObject::tr("ddd° mm' ss.s\"");
    case Format::GeoPosDeg:   return QObject::tr("Decimal degrees");
    case Format::GeoPosRad:   return QObject::tr("Radians");

    case Format::Watt:        return QObject::tr("watts");
    case Format::kW:          return QObject::tr("kilowatts");
    case Format::hp:          return QObject::tr("horsepower");
    case Format::kcalperhr:   return QObject::tr("kcal/hr");

    case Format::g:           return QObject::tr("grams");
    case Format::kg:          return QObject::tr("kilograms");
    case Format::oz:          return QObject::tr("ounces");
    case Format::lb:          return QObject::tr("pounds");
    case Format::stone:       return QObject::tr("stone");
    case Format::ton:         return QObject::tr("tons");

    case Format::Wh:          return QObject::tr("watt hours");
    case Format::kWh:         return QObject::tr("kilowatt hours");
    case Format::cal:         return QObject::tr("physics calories");
    case Format::kcal:        return QObject::tr("food calories");
    case Format::J:           return QObject::tr("joules");
    case Format::kJ:          return QObject::tr("kilojoules");
    case Format::MJ:          return QObject::tr("megajoules");

    case Format::AccelMPS2:   return QObject::tr("meters / sec^2");
    case Format::AccelFPS2:   return QObject::tr("feet / sec^2");

    case Format::RevsPerS:    return QObject::tr("rev/sec");
    case Format::RevsPerM:    return QObject::tr("rev/min");
    case Format::RevsPerH:    return QObject::tr("rev/hr");
    case Format::RevsPerD:    return QObject::tr("rev/day");

    case Format::BeatsPerS:   return QObject::tr("beats/sec");
    case Format::BeatsPerM:   return QObject::tr("beats/min");
    case Format::BeatsPerH:   return QObject::tr("beats/hr");
    case Format::BeatsPerD:   return QObject::tr("beats/day");

    case Format::DateTimeISO:               [[fallthrough]];
    case Format::DateTimeLocaleShort:       [[fallthrough]];
    case Format::DateTimeLocaleLong:        [[fallthrough]];
    case Format::DateTimeyyyyMMMdd_hhmmss:  [[fallthrough]];
    case Format::DateTimeyyMMMdd_hhmmss:    [[fallthrough]];
    case Format::DateTimeddMMMyyyy_hhmmss:  [[fallthrough]];
    case Format::DateTimeddMMMyy_hhmmss:    [[fallthrough]];
    case Format::DateTimeyyyyMMdd_hhmmss:   [[fallthrough]];
    case Format::DateTimeyyMMdd_hhmmss:     [[fallthrough]];
    case Format::DateTimeddMMyyyy_hhmmss:   [[fallthrough]];
    case Format::DateTimeddMMyy_hhmmss:     [[fallthrough]];
    case Format::DateTimeyyyyMMMdd:         [[fallthrough]];
    case Format::DateTimeyyyyMMdd:          [[fallthrough]];
    case Format::DateTimeyyMMMdd:           [[fallthrough]];
    case Format::DateTimeyyMMdd:            [[fallthrough]];
    case Format::DateTimeddMMMyyyy:         [[fallthrough]];
    case Format::DateTimeddMMyyyy:          [[fallthrough]];
    case Format::DateTimeddMMMyy:           [[fallthrough]];
    case Format::DateTimeddMMyy:            [[fallthrough]];
    case Format::DateTimehhmmss:            [[fallthrough]];
    case Format::DateTimehhmm:              [[fallthrough]];
    case Format::DateTimehh_mm_ss:          [[fallthrough]];
    case Format::DateTimehh_mm:
    {
        const Units units(f);
        return units(QDateTime::currentDateTime());
    }

    case Format::Timehhmmss:            [[fallthrough]];
    case Format::Timehhmm:              [[fallthrough]];
    case Format::Timehh_mm_ss:          [[fallthrough]];
    case Format::Timehh_mm:
    {
        const Units units(f);
        return units(QTime::currentTime());
    }

    case Format::DateLocaleShort:       [[fallthrough]];
    case Format::DateLocaleLong:        [[fallthrough]];
    case Format::DateyyyyMMMdd:         [[fallthrough]];
    case Format::DateyyyyMMdd:          [[fallthrough]];
    case Format::DateyyMMMdd:           [[fallthrough]];
    case Format::DateyyMMdd:            [[fallthrough]];
    case Format::DateddMMMyyyy:         [[fallthrough]];
    case Format::DateddMMyyyy:          [[fallthrough]];
    case Format::DateddMMMyy:           [[fallthrough]];
    case Format::DateddMMyy:            [[fallthrough]];
    case Format::Dateyyyy:              [[fallthrough]];
    case Format::Dateyy:                [[fallthrough]];
    case Format::DateMMMyyyy:           [[fallthrough]];
    case Format::DateMMyyyy:            [[fallthrough]];
    case Format::DateMMMyy:             [[fallthrough]];
    case Format::DateMMyy:              [[fallthrough]];
    case Format::DateyyyyMMM:           [[fallthrough]];
    case Format::DateyyyyMM:            [[fallthrough]];
    case Format::DateyyMMM:             [[fallthrough]];
    case Format::DateyyMM:
    {
        const Units units(f);
        return units(QDate::currentDate());
    }

    case Format::TzShort:     return QObject::tr("Short/Abbrev");
    case Format::TzOffset:    return QObject::tr("Offset from UTC");
    case Format::TzLong:      return QObject::tr("Long");
    case Format::TzIANA:      return QObject::tr("IANA Id");

    default:                  return "";
    }
}

double Units::multiplier(Format f)
{
    switch (f) {
    case Format::DurS:        return 1_DateS;
    case Format::DurM:        return 1_DateM;
    case Format::DurH:        return 1_DateH;
    case Format::DurD:        return 1_DateD;

    case Format::Percent:     return 1.0/100.0;
    case Format::Bytes:       return 1_B;
    case Format::KiB:         return 1_KiB;
    case Format::MiB:         return 1_MiB;
    case Format::GiB:         return 1_GiB;
    case Format::TiB:         return 1_TiB;
    case Format::PiB:         return 1_PiB;
    case Format::EiB:         return 1_EiB;
    case Format::KB:          return 1_KB;
    case Format::MB:          return 1_MB;
    case Format::GB:          return 1_GB;
    case Format::TB:          return 1_TB;
    case Format::PB:          return 1_PB;
    case Format::EB:          return 1_EB;

    case Format::Distmm:      return 1.0_Distmm;
    case Format::Distm:       return 1.0_Distm;
    case Format::Distkm:      return 1.0_Distkm;
    case Format::DistAU:      return 1.0_DistAU;
    case Format::Distft:      return 1.0_Distft;
    case Format::Distmi:      return 1.0_Distmi;
    case Format::DistSmoot:   return 1.0_DistSmoot;

    case Format::SpeedMPS:    return 1.0_SpeedMPS;
    case Format::SpeedKPH:    return 1.0_SpeedKPH;
    case Format::SpeedMPH:    return 1.0_SpeedMPH;
    case Format::SpeedFPS:    return 1.0_SpeedFPS;

    case Format::Aream2:      return Math::sqr(1.0_Distm);
    case Format::Areakm2:     return Math::sqr(1.0_Distkm);
    case Format::Areaft2:     return Math::sqr(1.0_Distft);
    case Format::Areami2:     return Math::sqr(1.0_Distmi);

    case Format::TempC:       return 1;
    case Format::TempK:       return 1;
    case Format::TempF:       return 1;

    case Format::SlopePct:    return 1.0/100.0;

    case Format::Watt:        return 1.0_Watt;
    case Format::kW:          return 1.0_kW;
    case Format::hp:          return 1.0_hp;
    case Format::kcalperhr:   return 1.0_kcalperhr;

    case Format::g:           return 1.0_g;
    case Format::kg:          return 1.0_kg;
    case Format::oz:          return 1.0_oz;
    case Format::lb:          return 1.0_lb;
    case Format::stone:       return 1.0_stone;
    case Format::ton:         return 1.0_ton;

    case Format::Wh:          return 1.0_Wh;
    case Format::kWh:         return 1.0_kWh;
    case Format::cal:         return 1.0_cal;
    case Format::kcal:        return 1.0_kcal;
    case Format::J:           return 1.0_J;
    case Format::kJ:          return 1.0_kJ;
    case Format::MJ:          return 1.0_MJ;

    case Format::AccelMPS2:   return 1.0_SpeedMPS;
    case Format::AccelFPS2:   return 1.0_SpeedFPS;

    case Format::RevsPerS:    return 1.0 / 1.0_perS;
    case Format::RevsPerM:    return 1.0 / 1.0_perM;
    case Format::RevsPerH:    return 1.0 / 1.0_perH;
    case Format::RevsPerD:    return 1.0 / 1.0_perD;

    case Format::BeatsPerS:   return 1.0 / 1.0_perS;
    case Format::BeatsPerM:   return 1.0 / 1.0_perM;
    case Format::BeatsPerH:   return 1.0 / 1.0_perH;
    case Format::BeatsPerD:   return 1.0 / 1.0_perD;

    default:                  return 1.0;
    }
}

void Units::save(QSettings& settings) const
{
    SL::Save(settings, "format",       int(m_format));
    SL::Save(settings, "precision",    m_precision);
    SL::Save(settings, "leadingZeros", m_leadingZeros);
    SL::Save(settings, "UTC",          m_UTC);
}

void Units::load(QSettings& settings)
{
    if (settings.contains("format"))
        m_format = Format(SL::Load<int>(settings, "format"));

    setFormat(m_format); // to clamp the format to the valid range

    SL::Load(settings, "precision",    m_precision);
    SL::Load(settings, "leadingZeros", m_leadingZeros);
    SL::Load(settings, "UTC", m_UTC);
}

void Units::addToComboBox(QComboBox* comboBox, Format first) const
{
    if (first == Format::_Invalid)
        first = rangeBegin();

    for (Format f = first; f != rangeEnd(); Util::inc(f))
        comboBox->addItem(name(f));
}

// format for use in chart axes
QString Units::chartLabelFormat(double value) const
{
    // Awkward: chart label format doesn't accept the same QString arg() formats as other
    // things, so we kludge-convert the precision and suffix.  This isn't right, but it's
    // the best easily possible at the moment.
    std::array<char, 128> numFmt;
    snprintf(numFmt.data(), numFmt.size(), "%%.%df", precision());

    return QString(numFmt.data()) + " " + suffix(value);
}

Format Units::rangeEnd() const
{
    switch (m_range) {
    case Format::_DurBegin:      return Format::_DurEnd;
    case Format::_PctBegin:      return Format::_PctEnd;
    case Format::_SizeBegin:     return Format::_SizeEnd;
    case Format::_DistBegin:     return Format::_DistEnd;
    case Format::_SpeedBegin:    return Format::_SpeedEnd;
    case Format::_AreaBegin:     return Format::_AreaEnd;
    case Format::_TempBegin:     return Format::_TempEnd;
    case Format::_SlopeBegin:    return Format::_SlopeEnd;
    case Format::_GeoPosBegin:   return Format::_GeoPosEnd;
    case Format::_PowerBegin:    return Format::_PowerEnd;
    case Format::_DateTimeBegin: return Format::_DateTimeEnd;
    case Format::_WeightBegin:   return Format::_WeightEnd;
    case Format::_RawNumBegin:   return Format::_RawNumEnd;
    case Format::_StringBegin:   return Format::_StringEnd;
    case Format::_EnergyBegin:   return Format::_EnergyEnd;
    case Format::_AccelBegin:    return Format::_AccelEnd;
    case Format::_TimeBegin:     return Format::_TimeEnd;
    case Format::_TzBegin:       return Format::_TzEnd;
    case Format::_RevsBegin:     return Format::_RevsEnd;
    case Format::_DateBegin:     return Format::_DateEnd;
    case Format::_BeatsBegin:    return Format::_BeatsEnd;
    default:                     assert(0 && "Unknown units range");
                                 return Format::_Invalid;
    }
}

Units& Units::operator=(const Units& rhs)
{
    m_format       = rhs.m_format;
    m_precision    = rhs.m_precision;
    m_leadingZeros = rhs.m_leadingZeros;
    m_UTC          = rhs.m_UTC;

    // don't copy neg/pos indicator or range

    return *this;
}

Units::Units(const Units& rhs) :
    m_format(rhs.m_format),
    m_precision(rhs.m_precision),
    m_leadingZeros(rhs.m_leadingZeros),
    m_UTC(rhs.m_UTC),
    m_neg(rhs.m_neg),
    m_pos(rhs.m_pos),
    m_range(rhs.m_range)
{
}
