/*
    Copyright 2019-2022 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef CMDLINEBASE_H
#define CMDLINEBASE_H

#include <functional>
#include <QtGlobal>
#include <QObject>
#include <QStringList>
#include <QVector>

// Cmd line argument processing
class CmdLineBase : public QObject
{
    Q_OBJECT

public:
    explicit CmdLineBase(const char* appname, const char* version, const char* build, const QStringList& = QStringList());
    explicit CmdLineBase(const char* appname, const char* version, const char* build, int argc, const char** argv);
    explicit CmdLineBase(const char* appname, const char* version, const char* build, int argc, char** argv);
    explicit CmdLineBase(const CmdLineBase&) = default;
    CmdLineBase& operator=(const CmdLineBase&) = default;

    ~CmdLineBase() override;

    // Process entire command line
    virtual int  processArgs(); // not in constructor, so it can be virtual
    virtual int  processArgsPostApp() const;

    // Obtain arguments
    const QStringList& argv() const { return m_argv; }

    QString      m_initialSettingsFile;
    bool         m_disableFirstRun;     // disable first run processing
    bool         m_privateSession;      // don't save this session's data, or name to the session list

    struct SaveMessageHandler
    {
        SaveMessageHandler(const QtMessageHandler& newhandler) { pushMessageHandler(newhandler); }
        ~SaveMessageHandler() { popMessageHandler(); }
    };

    // Forward message from the given level to level -1
    static void forwardMessage(int level, QtMsgType, const QMessageLogContext&, const QString&);

    static int  pushMessageHandler(const QtMessageHandler&);  // returns prior level
    static void popMessageHandler();

protected:
    static void messageHandler(QtMsgType, const QMessageLogContext&, const QString&);
    static void setupMessageHandler();

    void setArgv(int argc, const char** argv);

    // Process single argument and any options thereto
    virtual bool processArg(int&);

    virtual void setup() { } // post-process, pre-verify setup

    // Verify options
    virtual bool verify() const;

    // Print usage, etc
    virtual void usage() const;
    virtual void batch() const;   // hook for batch (non-GUI) operations

    // Parsing utilities
    const QString& next(int& arg) const;
    void collect(int& arg, QStringList&) const;

    QStringList         m_argv;          // QStringList based version of cmd line argv
    const char*         m_appname;       // app name
    const char*         m_version;       // version string
    const char*         m_build;         // build string
    mutable QString     m_missing;       // missing expected parameter
    mutable QString     m_unknown;       // unknown option
    bool                m_pubkeyRequest; // dump public key

    // void(QtMsgType, const QMessageLogContext&, const QString&)>
    static QVector<QtMessageHandler> m_messageHandlerStack;
};

#endif // CMDLINEBASE_H
