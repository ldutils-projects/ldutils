/*
    Copyright 2019-2021 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <cstdlib>
#include <QApplication>
#include <QPalette>
#include <QString>
#include <QDirIterator>
#include <QAction>
#include <QMenu>
#include <QTabWidget>
#include <QAbstractButton>
#include <QTabBar>

#include "icons.h"

QIcon Icons::get(const QString& name)
{
    // Return theme icon if there is one
    if (QIcon::hasThemeIcon(name))
        return QIcon::fromTheme(name);

    // Get from internal theme
    static const char* backupTheme = ":icons/hicolor";

    QDirIterator it(backupTheme, QDirIterator::Subdirectories);
    QIcon backupIcon;

    // This is unfortunately awkward.  We can't easily supply a backup theme for just the
    // icons not found in the system theme, and we also can't easily use QIcon::setThemeName
    // to point to icons in our local art asset, because that resets ALL icons.  Using a
    // system theme where it supplies an icon, and falling back to an internal icon otherwise
    // seems way harder than it should be.
    while (it.hasNext()) {
        const QString& file = it.next();
        const int lastSlash = file.lastIndexOf('/');
        const int lastDot = file.lastIndexOf('.');

        if (lastDot > 0) {
            if (file.mid(lastSlash+1, lastDot - lastSlash - 1) == name) {
                const int sizeSlash = file.lastIndexOf('/', lastSlash-1);
                if (sizeSlash > 0) {
                    const int size = int(strtol(qUtf8Printable(file) + sizeSlash+1, nullptr, 10));
                    if (size > 0)
                        backupIcon.addFile(file, QSize(size, size));
                    else
                        backupIcon.addFile(file, QSize(24, 24));
                }
            }
        }
    }

    return backupIcon;
}

QIcon Icons::get(const char *name)
{
    return get(QString(name));
}

// Replace action icons with default from internal art assets, if not otherwise found.
void Icons::defaultIcon(QAction *action, const char* iconName)
{
    if (!action->icon().availableSizes().isEmpty())
        return;

    action->setIcon(get(iconName));
}

void Icons::defaultIcon(QMenu* menu, const char* iconName)
{
    if (!menu->icon().availableSizes().isEmpty())
        return;

    menu->setIcon(get(iconName));
}

// Replace tab icons with default from internal art assets, if not otherwise found.
void Icons::defaultIcon(QTabWidget* tabs, int index, const char* iconName)
{
    if (!tabs->tabIcon(index).availableSizes().isEmpty())
        return;
    tabs->setTabIcon(index, get(iconName));
}

void Icons::defaultIcon(QAbstractButton* button, const char* iconName)
{
    if (!button->icon().availableSizes().isEmpty())
        return;

    button->setIcon(get(iconName));
}

