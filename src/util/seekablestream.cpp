/*
    Copyright 2020-2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "seekablestream.h"

SeekableStream::SeekableStream(int fd) :
    m_fd(fd)
{
}

SeekableStream::~SeekableStream()
{
    close();
}

void SeekableStream::close()
{
    if (!isOpen())
        return;

    // Now rewind, and copy the contents to the FD
    if (m_tmpFile.isOpen()) {
        if (m_tmpFile.seek(0)) {
            static const int copyBuffSize = 1<<16;
            while (!m_tmpFile.atEnd())
                m_directOut.write(m_tmpFile.read(copyBuffSize));
        }
        m_tmpFile.close();
    }

    m_directOut.close();
    QIODevice::close();
}

bool SeekableStream::open(QIODevice::OpenMode mode)
{
    if (!QIODevice::open(mode))
        return false;

    if (!m_directOut.open(m_fd, QIODevice::WriteOnly))
        return false;

    // Short-circuit using the temp file if possible.
    if ((mode & (QIODevice::ReadOnly | QIODevice::Unbuffered)) == 0)
        return true;

    return m_tmpFile.open();
}

qint64 SeekableStream::pos() const
{
    if (m_tmpFile.isOpen())
        return m_tmpFile.pos();

    return m_directOut.pos();
}

bool SeekableStream::seek(qint64 pos)
{
    if (!m_tmpFile.isOpen())
        return false;

    QIODevice::seek(pos);
    return m_tmpFile.seek(pos);
}

qint64 SeekableStream::size() const
{
    if (m_tmpFile.isOpen())
        return m_tmpFile.size();

    return m_directOut.size();
}

qint64 SeekableStream::readData(char* data, qint64 len)
{
    return m_tmpFile.read(data, len);
}

qint64 SeekableStream::writeData(const char* data, qint64 len)
{
    if (m_tmpFile.isOpen())
        return m_tmpFile.write(data, len);

    return m_directOut.write(data, len);
}

