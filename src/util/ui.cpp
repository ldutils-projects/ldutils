/*
    Copyright 2019-2022 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "ui.h"

#include <QMessageBox>
#include <QApplication>
#include <QSet>
#include <QString>
#include <QTreeView>
#include <QColor>
#include <QWidget>
#include <QTextDocumentFragment>
#include <QTextDocument>

#include <src/core/uicolormodel.h>
#include <src/ui/windows/mainwindowbase.h>
#include <src/ui/widgets/delegatebase.h>

namespace Util {

int WarningDialog(MainWindowBase& mainWindow, const QString& winTitle, const QString& msg, QWidget* parent)
{
    if (parent == nullptr)
        parent = &mainWindow;

    const QMessageBox::StandardButton result =
            QMessageBox::warning(parent, winTitle, msg,
                                 QMessageBox::Ok | QMessageBox::Cancel,
                                 QMessageBox::Cancel);

    if (result != QMessageBox::Ok)
        mainWindow.statusMessage(UiType::Warning, QObject::tr("Canceled"));

    return result;
}

bool IsLightTheme()
{
    return QApplication::palette().window().color().lightness() > 127;
}

QString LocalThemeIcon(const QString& relPath)
{
    return QString(":icons/hicolor/") + relPath;
}

void SetupWhatsThis(QWidget* root)
{
    if (root == nullptr)
        return;

    root->setWhatsThis(root->toolTip());

    for (QWidget* w : root->findChildren<QWidget*>()) {
        w->setWhatsThis(w->toolTip());

        for (QAction* action : w->actions())
            action->setWhatsThis(action->toolTip());
    }
}

void SetupWhatsThis(QWidget& root)
{
    SetupWhatsThis(&root);
}

void CopyActions(QWidget* to, QWidget* from)
{
    if (to == nullptr || from == nullptr)
        return;

    for (QWidget* w : from->findChildren<QWidget*>(QString(), Qt::FindDirectChildrenOnly))
        for (QAction* action : w->actions())
            to->addAction(action);
}

void InitDelegates(QTreeView& view, const QVector<std::tuple<int, DelegateBase*>>& setup)
{
    for (const auto& delegate : setup) {
        std::get<DelegateBase*>(delegate)->setSelector(view.selectionModel());
        view.setItemDelegateForColumn(std::get<ModelType>(delegate),
                                      std::get<DelegateBase*>(delegate));
    }
}

void InitDelegates(QTreeView& view, UndoMgr& undoMgr, const QVector<std::tuple<int, DelegateBase*>>& setup)
{
    InitDelegates(view, setup);

    for (const auto& delegate : setup)
        std::get<DelegateBase*>(delegate)->setUndoMgr(undoMgr);
}

void SetFocus(QWidget* w)
{
    if (w == nullptr)
        return;

    // Focus events aren't sent unless the widget's window is active, so activate it.
    w->activateWindow();
    w->setFocus();
}

void SetWidgetStyle(QWidget* w, const QColor& color,
                    const QByteArray& fontStyle,
                    const QByteArray& weight)
{
    if (w == nullptr)
        return;

    w->setStyleSheet(QString(w->metaObject()->className()) + " { " +
                     (!color.isValid()    ? QByteArray() : QByteArray("color:") + color.name() + ";") +
                     (fontStyle.isEmpty() ? QByteArray() : QByteArray("font-style:") + fontStyle + ";") +
                     (weight.isEmpty()    ? QByteArray() : QByteArray("font-weight:") + weight + ";") +
                     " }");
}

void UnsetWidgetStyle(QWidget* w)
{
    if (w == nullptr)
        return;

    w->setStyleSheet(QString());
}

void ClearLayout(QLayout* layout, int keepLeft, int keepRight)
{
    QLayoutItem* child;

    while (layout->count() > (keepLeft + keepRight) &&
           (child = layout->takeAt(keepLeft)) != nullptr) {
        delete child->widget();
        delete child;
    }
}

void SetEnabled(QWidget* w, bool enabled)
{
    if (w == nullptr)
        return;

    w->setEnabled(enabled);

    SetEnabled(w->layout(), enabled);
}

void SetEnabled(QLayout* lo, bool enabled)
{
    if (lo == nullptr)
        return;

    for (int w = 0; w < lo->count(); ++w) {
        if (QWidget* widget = lo->itemAt(w)->widget(); widget != nullptr)
            SetEnabled(widget, enabled);
        if (QLayout* subLo = lo->itemAt(w)->layout(); subLo != nullptr)
            SetEnabled(subLo, enabled);
    }
}

QString PlainText(const QLabel* w)
{
    if (w == nullptr)
        return { };

    // Sanitize label text if it looks like HTML
    if (Qt::mightBeRichText(w->text()))
        return QTextDocumentFragment::fromHtml(w->text()).toPlainText();

    return w->text();
}

QString PlainText(const QAbstractButton* w)
{
    return w == nullptr ? QString() : w->text();
}

QString PlainText(const QGroupBox* w)
{
    return w == nullptr ? QString() : w->title();
}

} // namespace Util
