/*
    Copyright 2019-2023 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef UTIL_H
#define UTIL_H
#include <cassert>
#include <cstdarg>
#include <array>
#include <climits>
#include <sys/types.h>
#include <pwd.h>
#include <type_traits>

#include <QVariant>
#include <QColor>
#include <QByteArray>
#include <QFile>
#include <QVector>
#include <QSize>
#include <QModelIndexList>
#include <QRect>
#include <QPoint>
#include <QSet>
#include <QItemSelectionModel>

class QIcon;
class QTreeView;
class QStandardItemModel;
class QAbstractProxyModel;
class QItemSelectionModel;
class QSortFilterProxyModel;
class QToolButton;
class QHeaderView;

namespace Util {

// Expand ~ and $VAR or ${VAR} in pathnames
[[nodiscard]] QByteArray ExpandDirName(const char* in);
[[nodiscard]] QByteArray ExpandDirName(const QByteArray& in);
[[nodiscard]] QByteArray ExpandDirName(const QString &in);

// Icon helper
[[nodiscard]] QIcon ReadIcon(const QString& prefix,
               const QString& suffix = ".png",
               const QVector<QSize>& sizes = {
                  { 16, 16 }, { 32, 32 }, { 64, 64 }, { 128, 128 }
               });

// For deleting C-api style points to arrays of allocated data
template <class T>
void FreeCArray(T*** data, int count)
{
    for (int i = 0; i < count; ++i)
        free((*data)[i]);

    free(*data);
    *data = nullptr;
}

[[nodiscard]] uid_t GetUid(const QByteArray& uid_s);
[[nodiscard]] uid_t GetGid(const QByteArray& gid_s);

template <typename R, typename... T>
[[nodiscard]] R PathCat(const T&... path)
{
    const std::array<R, sizeof...(T)> paths = { path... };

    R out;
    for (const auto& p : paths)
        out += p + "/";

    out.truncate(out.size()-1);

    return out;
}

template <typename T> T& RemoveNewline(T& line, bool removeNL = true)
{
    if (removeNL)
        if (line.size() > 0 && line[line.size()-1] == '\n')
            line.resize(line.size()-1);
    return line;
}

template <typename R>
[[nodiscard]] inline R ReadFile(QFile& file, int size, bool removeNL = true)
{
    R contents = file.read(size);
    return RemoveNewline(contents, removeNL);
}

template <>
[[nodiscard]] inline quint64 ReadFile<quint64>(QFile& file, [[maybe_unused]] int size, bool)
{
    std::array<char, 64> buf;

    assert(unsigned(size) <= sizeof(buf));

    if (file.read(buf.data(), sizeof(buf)) < 0)
        return 0;

    return strtoull(buf.data(), nullptr, 10);
}

template <>
[[nodiscard]] inline qint64 ReadFile<qint64>(QFile& file, [[maybe_unused]] int size, bool)
{
    std::array<char, 64> buf;

    assert(unsigned(size) <= sizeof(buf));

    if (file.read(buf.data(), sizeof(buf)) < 0)
        return 0;

    return strtoll(buf.data(), nullptr, 10);
}

template <>
[[nodiscard]] inline QList<QByteArray> ReadFile<QList<QByteArray>>(QFile& file, int size, bool)
{
    return file.read(size).split('\n');
}

template <typename R>
[[nodiscard]] inline R ReadFile(const QString& path, int maxLen = 1024, bool removeNL = true)
{
    QFile file(path);
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
        return R();

    return ReadFile<R>(file, maxLen, removeNL);
}

template <typename R, typename... T>
[[nodiscard]] inline R ReadFilev(int maxLen, const char* fmt, const T&... data)
{
    std::array<char, PATH_MAX> buf;

    const int rc = snprintf(buf.data(), sizeof(buf), fmt, data...);
    if (rc < 0 || size_t(rc) >= sizeof(buf))
        return R();

    return ReadFile<R>(buf.data(), maxLen);
}

// To facilitate incrementing over enums
template <typename T>
inline T inc(T& x) { return x = T(typename std::underlying_type_t<T>(x) + 1); }

// Resize view to fit all displayed data
void ResizeViewForData(QTreeView& view, bool ignoreHeaders = false,
                       int minColumnWidth = 20, int pad = 0);

void ResizeColumns(QTreeView& view, const QVector<uint>& ratios);

[[nodiscard]] QModelIndex clickPosIndex(const QTreeView*, const QHeaderView&, const QPoint& pos);

template <typename T>
[[nodiscard]] T Clamp(const T& val, const T& min, const T& max)  { return std::max(std::min(val, max), min); }

// Map an index all the way down a filter chain
[[nodiscard]] QModelIndex MapDown(const QModelIndex&);
QModelIndexList& MapDown(QModelIndexList&);
[[nodiscard]] QModelIndexList MapDown(const QModelIndexList&);
const QAbstractItemModel* MapDown(const QAbstractItemModel*);
[[nodiscard]] QAbstractItemModel* MapDown(QAbstractItemModel*);
QItemSelectionModel& MapDown(QItemSelectionModel& dst,
                             const QItemSelectionModel& src,
                             QItemSelectionModel::SelectionFlags = QItemSelectionModel::Select);

// Map an index all the way up a filter chain
[[nodiscard]] QModelIndex MapUp(const QAbstractItemModel*, const QModelIndex&);
QModelIndexList& MapUp(const QAbstractItemModel*, QModelIndexList&);
[[nodiscard]] QModelIndexList MapUp(const QAbstractItemModel*, const QModelIndexList&);

// Build set of indexes for fast lookup
[[nodiscard]] QSet<QModelIndex> IndexSet(const QModelIndexList&);

// Lerp
template <typename T>
[[nodiscard]] inline T Lerp(const T& v0, const T& v1, float factor) {
    return T(v0 + (v1 - v0) * T(factor));
}

template <>
[[nodiscard]] inline QColor Lerp(const QColor& v0, const QColor& v1, float factor)
{
    qreal h0d, s0d, v0d, a0d, h1d, s1d, v1d, a1d;

    v0.getHsvF(&h0d, &s0d, &v0d, &a0d);
    v1.getHsvF(&h1d, &s1d, &v1d, &a1d);

    return QColor::fromHsvF(Lerp(h0d, h1d, factor),
                            Lerp(s0d, s1d, factor),
                            Lerp(v0d, v1d, factor),
                            Lerp(a0d, a1d, factor));
}
    
template <>
[[nodiscard]] inline QVariant Lerp(const QVariant& v0, const QVariant& v1, float factor)
{
    if (!v0.isValid() || !v1.isValid())
        return { };

    assert(v0.type() == v1.type());

    switch (v0.type()) {
    case QVariant::Double:     return Util::Lerp<double>(v0.toDouble(), v1.toDouble(), factor);
    case QVariant::LongLong:   return Util::Lerp<qint64>(v0.toLongLong(), v1.toLongLong(), factor);
    case QVariant::ULongLong:  return Util::Lerp<quint64>(v0.toULongLong(), v1.toULongLong(), factor);
    case QVariant::Int:        return Util::Lerp<int>(v0.toInt(), v1.toInt(), factor);
    case QVariant::UInt:       return Util::Lerp<uint>(v0.toUInt(), v1.toUInt(), factor);
    case QVariant::Char:       return Util::Lerp<char>(v0.toFloat(), v1.toFloat(), factor); // don't interp as chars
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wswitch"
    // For some reason, float doesn't appear in the QVariant::Type enum on Qt 5.9
    case QVariant::Type(QMetaType::Float):
        return Util::Lerp<float>(v0.toFloat(), v1.toFloat(), factor);
#pragma GCC diagnostic pop
    case QVariant::Invalid:    return { };
    default:
        assert(0); return { };
    }
}

// Convenience: recurse through an QAbstractItemModel, calling function.
// Terminate if the functor returns false at any point.
bool Recurse(const QAbstractItemModel& model, const std::function<bool(const QModelIndex&)>&,
             const QModelIndex& parent = QModelIndex(), bool root = false, int column = 0);

// Find next and previous index in tree traversal. Forms with a predicate only
// return pages which satisfy it.
[[nodiscard]] QModelIndex NextIndex(QModelIndex);
[[nodiscard]] QModelIndex NextIndex(QModelIndex, const std::function<bool(const QModelIndex&)>&);
[[nodiscard]] QModelIndex PrevIndex(const QModelIndex&);
[[nodiscard]] QModelIndex PrevIndex(QModelIndex, const std::function<bool(const QModelIndex&)>&);

// Return first index, or first satisfying predicate
[[nodiscard]] QModelIndex FirstIndex(const QAbstractItemModel&, int column = 0, const QModelIndex& parent = QModelIndex());
[[nodiscard]] QModelIndex FirstIndex(const QAbstractItemModel&, const std::function<bool(const QModelIndex&)>&,
                                     int column = 0, const QModelIndex& parent = QModelIndex());

[[nodiscard]] QModelIndex LastIndex(const QAbstractItemModel&, int column = 0, const QModelIndex& parent = QModelIndex());
[[nodiscard]] QModelIndex LastIndex(const QAbstractItemModel&, const std::function<bool(const QModelIndex&)>&,
                                    int column = 0, const QModelIndex& parent = QModelIndex());

// Return all row indexes for a given column, or which satisfy given predicate.
[[nodiscard]] QModelIndexList RowIndexes(const QAbstractItemModel&, int column = 0);
[[nodiscard]] QModelIndexList RowIndexes(const QAbstractItemModel&, const std::function<bool(const QModelIndex&)>&,
                                         int column = 0);

// Create savable reference to given row (doesn't restore column)
using SavableIndex = QVector<int>;

[[nodiscard]] SavableIndex SaveIndex(QModelIndex);

// Restore index from savable reference
[[nodiscard]] QModelIndex RestoreIndex(QAbstractItemModel&, const SavableIndex&, int column = 0);
[[nodiscard]] QModelIndex RestoreIndex(QAbstractItemModel*, const SavableIndex&, int column = 0);

// Convenience: remove rows from a model matching predicate.  This is here because it's shared with
// some models which do not inherit from TreeModel.
void RemoveRows(QAbstractItemModel&,
                const std::function<bool(const QModelIndex&)>& predicate,
                const QModelIndex& parent = QModelIndex(), int column = 0);

// Convenience: remove rows from a model matching selection.  This is here because it's shared with
// some models which do not inherit from TreeModel.
void RemoveRows(QAbstractItemModel&,
                const QItemSelectionModel* selection, const QSortFilterProxyModel* filter = nullptr,
                const QModelIndex& parent = QModelIndex());

// Convenience: remove model rows are are found in a given QModelIndexList
void RemoveRows(QAbstractItemModel&, const QModelIndexList&,
                const QModelIndex& parent = QModelIndex());

// Open the given TreeView nodes to display items matching given predicate.
bool OpenToMatch(const QAbstractItemModel&, QTreeView&, const std::function<bool(const QModelIndex&)>&,
                 const QModelIndex& parent = QModelIndex());

// Map a popup position to glocal coordinates, but keeping it on the screen containing the mouse cursor.
[[nodiscard]] QRect MapOnScreen(QWidget*, const QPoint&, const QSize&);

// Functions to help QToolButtons act like color selector buttons.
[[nodiscard]] QColor GetTBColor(const QToolButton*);
void SetTBColor(QToolButton*, const QColor&);

// Create anchored regex using perl \A and \z anchors
QString AnchoredPattern(const QString&, bool beginAnchor = true, bool endAnchor = true);

// Return text length of de-HTML-ized text.
int PlainTextLen(const QString&);
int PlainTextLen(const QVariant&);

} // namespace Util

// Convenience functions to pull things out of standard item models
namespace SIM {
[[nodiscard]] QVariant data(const QStandardItemModel& m, int row, int col);
[[nodiscard]] bool     isSet(const QStandardItemModel& m, int row, int col);
[[nodiscard]] QColor   bg(const QStandardItemModel& m, int row, int col);
[[nodiscard]] QIcon    icon(const QStandardItemModel& m, int row, int col);
} // end namespace SIM

#endif // UTIL_H
