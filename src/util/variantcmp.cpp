/*
    Copyright 2020-2022 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <cassert>

#include <QtGlobal>
#include <QVariant>
#include <QBitArray>
#include <QBitmap>
#include <QByteArray>
#include <QDate>
#include <QDateTime>
#include <QIcon>
#include <QLine>
#include <QLineF>
#include <QLocale>
#include <QModelIndex>
#include <QPixmap>
#include <QPoint>
#include <QPointF>
#include <QPersistentModelIndex>
#include <QRect>
#include <QRectF>
#include <QRegExp>
#include <QSize>
#include <QSizeF>
#include <QString>
#include <QStringList>
#include <QTime>
#include <QUrl>

#include "variantcmp.h"

namespace QtCompat {

namespace {
inline bool operator<(const QLine& lhs, const QLine& rhs) {
    return (lhs.y1() != rhs.y1()) ? lhs.y1() < rhs.y1() :
           (lhs.y2() != rhs.y2()) ? lhs.y2() < rhs.y2() :
           (lhs.x1() != rhs.x1()) ? lhs.x1() < rhs.x1() : lhs.x2() < rhs.x2();
}

inline bool operator<(const QLineF& lhs, const QLineF& rhs) {
    return (lhs.y1() != rhs.y1()) ? lhs.y1() < rhs.y1() :
           (lhs.y2() != rhs.y2()) ? lhs.y2() < rhs.y2() :
           (lhs.x1() != rhs.x1()) ? lhs.x1() < rhs.x1() : lhs.x2() < rhs.x2();
}

inline bool operator<(const QLocale& lhs, const QLocale& rhs) {
    return lhs.name() < rhs.name();
}

inline bool operator<(const QIcon& lhs, const QIcon& rhs) {
    return lhs.name() < rhs.name();
}

inline bool operator<(const QPoint& lhs, const QPoint& rhs) {
    return lhs.y() != rhs.y() ? lhs.y() < rhs.y() : lhs.x() < rhs.x();
}

inline bool operator<(const QPointF& lhs, const QPointF& rhs) {
    return lhs.y() != rhs.y() ? lhs.y() < rhs.y() : lhs.x() < rhs.x();
}

inline bool operator<(const QRect& lhs, const QRect& rhs) {
    return lhs.topLeft() != rhs.topLeft() ? lhs.topLeft() < rhs.topLeft() :
                                            lhs.bottomRight() < rhs.bottomRight();
}

inline bool operator<(const QRectF& lhs, const QRectF& rhs) {
    return lhs.topLeft() != rhs.topLeft() ? lhs.topLeft() < rhs.topLeft() :
                                            lhs.bottomRight() < rhs.bottomRight();
}

inline bool operator<(const QRegExp& lhs, const QRegExp& rhs) {
    return lhs.pattern() < rhs.pattern();
}

inline bool operator<(const QSize& lhs, const QSize& rhs) {
    return lhs.height() != rhs.height() ? lhs.height() < rhs.height() : lhs.width() < rhs.width();
}

inline bool operator<(const QSizeF& lhs, const QSizeF& rhs) {
    return lhs.height() != rhs.height() ? lhs.height() < rhs.height() : lhs.width() < rhs.width();
}

inline bool operator<(const QStringList& lhs, const QStringList& rhs) {
    for (int i = 0; i < std::min(lhs.size(), rhs.size()); ++i) {
        if (lhs.at(i) < rhs.at(i))
            return true;
        if (lhs.at(i) > rhs.at(i))
            return false;
    }

    return lhs.size() < rhs.size();
}

// QUrl has its own operator<
//inline bool operator<(const QUrl& lhs, const QUrl& rhs) {
//    return lhs.toString() < rhs.toString();
//}

inline bool operator>(const QLine& lhs, const QLine& rhs) {
    return (lhs.y1() != rhs.y1()) ? lhs.y1() > rhs.y1() :
           (lhs.y2() != rhs.y2()) ? lhs.y2() > rhs.y2() :
           (lhs.x1() != rhs.x1()) ? lhs.x1() > rhs.x1() : lhs.x2() < rhs.x2();
}

inline bool operator>(const QLineF& lhs, const QLineF& rhs) {
    return (lhs.y1() != rhs.y1()) ? lhs.y1() > rhs.y1() :
           (lhs.y2() != rhs.y2()) ? lhs.y2() > rhs.y2() :
           (lhs.x1() != rhs.x1()) ? lhs.x1() > rhs.x1() : lhs.x2() < rhs.x2();
}

inline bool operator>(const QLocale& lhs, const QLocale& rhs) {
    return lhs.name() > rhs.name();
}

inline bool operator>(const QIcon& lhs, const QIcon& rhs) {
    return lhs.name() > rhs.name();
}

inline bool operator>(const QPoint& lhs, const QPoint& rhs) {
    return lhs.y() != rhs.y() ? lhs.y() > rhs.y() : lhs.x() > rhs.x();
}

inline bool operator>(const QPointF& lhs, const QPointF& rhs) {
    return lhs.y() != rhs.y() ? lhs.y() > rhs.y() : lhs.x() > rhs.x();
}

inline bool operator>(const QRect& lhs, const QRect& rhs) {
    return lhs.topLeft() != rhs.topLeft() ? lhs.topLeft() > rhs.topLeft() :
                                            lhs.bottomRight() > rhs.bottomRight();
}

inline bool operator>(const QRectF& lhs, const QRectF& rhs) {
    return lhs.topLeft() != rhs.topLeft() ? lhs.topLeft() > rhs.topLeft() :
                                            lhs.bottomRight() > rhs.bottomRight();
}

inline bool operator>(const QRegExp& lhs, const QRegExp& rhs) {
    return lhs.pattern() > rhs.pattern();
}

inline bool operator>(const QSize& lhs, const QSize& rhs) {
    return lhs.height() != rhs.height() ? lhs.height() > rhs.height() : lhs.width() > rhs.width();
}

inline bool operator>(const QSizeF& lhs, const QSizeF& rhs) {
    return lhs.height() != rhs.height() ? lhs.height() > rhs.height() : lhs.width() > rhs.width();
}

inline bool operator>(const QPersistentModelIndex& lhs, const QPersistentModelIndex& rhs) {
    return !(lhs < rhs || lhs == rhs);
}

inline bool operator>(const QUrl& lhs, const QUrl& rhs) {
    return lhs.toString() > rhs.toString();
}

inline bool operator>(const QStringList& lhs, const QStringList& rhs) {
    for (int i = 0; i < std::min(lhs.size(), rhs.size()); ++i) {
        if (lhs.at(i) > rhs.at(i))
            return true;
        if (lhs.at(i) < rhs.at(i))
            return false;
    }

    return lhs.size() > rhs.size();
}

template <typename T> inline bool lt(const QVariant& lhs, const QVariant& rhs) {
    return lhs.value<T>() < rhs.value<T>();
}

template <typename T> inline bool gt(const QVariant& lhs, const QVariant& rhs) {
    return lhs.value<T>() > rhs.value<T>();
}

inline bool isTimeType(const QVariant& v) {
    return v.type() == QVariant::Date ||
           v.type() == QVariant::DateTime ||
           v.type() == QVariant::Time;
}

} // anonymous namespace

bool lt(const QVariant& lhs, const QVariant& rhs)
{
    if (lhs.type() != rhs.type()) {
        if (lhs.canConvert(QVariant::Double) && rhs.canConvert(QVariant::Double))
            return lhs.value<double>() < rhs.value<double>();

        if (lhs.canConvert(QVariant::String) && rhs.canConvert(QVariant::String))
            return lhs.value<QString>() < rhs.value<QString>();
    }

    switch (lhs.type())
    {
    case QVariant::BitArray:              assert(0); return false;
    case QVariant::Bitmap:                assert(0); return false;
    case QVariant::Bool:                  return lt<bool>(lhs, rhs);
    case QVariant::ByteArray:             return lt<QByteArray>(lhs, rhs);
    case QVariant::Char:                  return lt<QChar>(lhs, rhs);
    case QVariant::Date:                  return lt<QDate>(lhs, rhs);
    case QVariant::DateTime:              return lt<QDateTime>(lhs, rhs);
    case QVariant::Double:                return lt<double>(lhs, rhs);
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wswitch"
    // For some reason, float doesn't appear in the QVariant::Type enum on Qt 5.9
    case QMetaType::Float:                return lt<float>(lhs, rhs);
#pragma GCC diagnostic pop
    case QVariant::Icon:                  return lt<QIcon>(lhs, rhs);
    case QVariant::Int:                   return lt<int>(lhs, rhs);
    case QVariant::Invalid:               return false;
    case QVariant::Line:                  return lt<QLine>(lhs, rhs);
    case QVariant::LineF:                 return lt<QLineF>(lhs, rhs);
    case QVariant::Locale:                return lt<QLocale>(lhs, rhs);
    case QVariant::LongLong:              return lt<qlonglong>(lhs, rhs);
    case QVariant::PersistentModelIndex:  return lt<QPersistentModelIndex>(lhs, rhs);
    case QVariant::Point:                 return lt<QPoint>(lhs, rhs);
    case QVariant::Pixmap:                return lt<QPixmap>(lhs, rhs);
    case QVariant::PointF:                return lt<QPointF>(lhs, rhs);
    case QVariant::ModelIndex:            return lt<QModelIndex>(lhs, rhs);
    case QVariant::Rect:                  return lt<QRect>(lhs, rhs);
    case QVariant::RectF:                 return lt<QRectF>(lhs, rhs);
    case QVariant::RegExp:                return lt<QRegExp>(lhs, rhs);
    case QVariant::Size:                  return lt<QSize>(lhs, rhs);
    case QVariant::SizeF:                 return lt<QSizeF>(lhs, rhs);
    case QVariant::String:                return lt<QString>(lhs, rhs);
    case QVariant::StringList:            return lt<QStringList>(lhs, rhs);
    case QVariant::Time:                  return lt<QTime>(lhs, rhs);
    case QVariant::UInt:                  return lt<uint>(lhs, rhs);
    case QVariant::ULongLong:             return lt<qulonglong>(lhs, rhs);
    case QVariant::Url:                   return lt<QUrl>(lhs, rhs);
    default: assert(0 && "unexpected variant type"); return false;
    }
}

bool gt(const QVariant& lhs, const QVariant& rhs)
{
    if (lhs.type() != rhs.type()) {
        if (lhs.canConvert(QVariant::Double) && rhs.canConvert(QVariant::Double))
            return lhs.value<double>() > rhs.value<double>();

        if (lhs.canConvert(QVariant::String) && rhs.canConvert(QVariant::String))
            return lhs.value<QString>() > rhs.value<QString>();
    }

    switch (lhs.type())
    {
    case QVariant::BitArray:              assert(0); return false;
    case QVariant::Bitmap:                assert(0); return false;
    case QVariant::Bool:                  return gt<bool>(lhs, rhs);
    case QVariant::ByteArray:             return gt<QByteArray>(lhs, rhs);
    case QVariant::Char:                  return gt<QChar>(lhs, rhs);
    case QVariant::Date:                  return gt<QDate>(lhs, rhs);
    case QVariant::DateTime:              return gt<QDateTime>(lhs, rhs);
    case QVariant::Double:                return gt<double>(lhs, rhs);
    case QVariant::Icon:                  return gt<QIcon>(lhs, rhs);
    case QVariant::Int:                   return gt<int>(lhs, rhs);
    case QVariant::Invalid:               return false;
    case QVariant::Line:                  return gt<QLine>(lhs, rhs);
    case QVariant::LineF:                 return gt<QLineF>(lhs, rhs);
    case QVariant::Locale:                return gt<QLocale>(lhs, rhs);
    case QVariant::LongLong:              return gt<qlonglong>(lhs, rhs);
    case QVariant::PersistentModelIndex:  return gt<QPersistentModelIndex>(lhs, rhs);
    case QVariant::Point:                 return gt<QPoint>(lhs, rhs);
    case QVariant::Pixmap:                return gt<QPixmap>(lhs, rhs);
    case QVariant::PointF:                return gt<QPointF>(lhs, rhs);
    case QVariant::ModelIndex:            return gt<QModelIndex>(lhs, rhs);
    case QVariant::Rect:                  return gt<QRect>(lhs, rhs);
    case QVariant::RectF:                 return gt<QRectF>(lhs, rhs);
    case QVariant::RegExp:                return gt<QRegExp>(lhs, rhs);
    case QVariant::Size:                  return gt<QSize>(lhs, rhs);
    case QVariant::SizeF:                 return gt<QSizeF>(lhs, rhs);
    case QVariant::String:                return gt<QString>(lhs, rhs);
    case QVariant::StringList:            return gt<QStringList>(lhs, rhs);
    case QVariant::Time:                  return gt<QTime>(lhs, rhs);
    case QVariant::UInt:                  return gt<uint>(lhs, rhs);
    case QVariant::ULongLong:             return gt<qulonglong>(lhs, rhs);
    case QVariant::Url:                   return gt<QUrl>(lhs, rhs);
    default: assert(0 && "unexpected variant type"); return false;
    }
}

QVariant min(const QVariant& lhs, const QVariant& rhs)
{
    return lt(lhs, rhs) ? lhs : rhs;
}

QVariant max(const QVariant& lhs, const QVariant& rhs)
{
    return gt(lhs, rhs) ? lhs : rhs;
}

} // namespace QtCompat

