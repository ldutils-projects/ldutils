/*
    Copyright 2020-2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef NAMEDITEM_H
#define NAMEDITEM_H

#include <tuple>
#include <QString>
#include <QModelIndexList>

class QModelIndex;

class NamedItemInterface
{
public:
    // Singular and plural names of model items.
    using name_t = std::tuple<QString, QString>;

    // Return singular/plural item names
    virtual const name_t& getItemName() const = 0;
    // Similar, if names should vary per index
    virtual const name_t& getItemName(const QModelIndex&) const { return getItemName(); }
    // Create name from selection list
    virtual const name_t& getItemName(const QModelIndexList&) const;

    // Convenience: Lower case form
    name_t getItemNameLower() const;
};

// Interface to query singular and ploral item names
class NamedItem : virtual public NamedItemInterface
{
public:
    NamedItem(const QString& singular, const QString& plural);
    NamedItem(const name_t& name);
    NamedItem(const NamedItem&) = default;

    NamedItem& operator=(const NamedItem&) = default;

    static name_t getItemNameStatic(); // for interface purposes
    const name_t& getItemName() const override { return m_name; }
    const name_t& getItemName(const QModelIndex&) const override { return getItemName(); }

private:
    name_t m_name;  // names
};

#endif // NAMEDITEM_H
