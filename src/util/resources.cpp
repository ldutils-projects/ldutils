/*
    Copyright 2019-2022 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "resources.h"

#include <algorithm>
#include <QString>
#include <QStandardPaths>
#include <QFile>
#include <QByteArray>

#include <src/util/util.h>

namespace Util {

QByteArray pubkey()
{
    if (QFile pubkey(QStandardPaths::locate(QStandardPaths::AppDataLocation, "pubkey.asc")); pubkey.exists())
        if (pubkey.open(QIODevice::ReadOnly | QIODevice::Text))
            return Util::ReadFile<QByteArray>(pubkey, 1<<15);

    return { };
}

} // namespace Util


int Exit::code(int rc)
{
    return std::max(rc, 0);
}
