/*
    Copyright 2020-2023 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef QTCOMPAT_H
#define QTCOMPAT_H

#include <QString>
#include <QtGlobal>
#include <QLabel>
#include <QPixmap>
#include <QMutex>
#include <QMap>

// Handle API changes between Qt versions. We compile across a wide range of Qt versions, and sometimes
// the APIs change in incompatible ways. These functions and types attempt to smooth that over.

namespace QtCompat {

// This changed namespaces from QString:: to Qt:: in QT 5.14
#if (QT_VERSION >= QT_VERSION_CHECK(5, 14, 0))
using SplitBehavior = Qt::SplitBehaviorFlags;
#else
typedef QString::SplitBehavior SplitBehavior;
#endif // QT_VERSION

// Qt 5.15 changed QLabel::pixmap to return by value, from return by pointer.
#if (QT_VERSION >= QT_VERSION_CHECK(5, 15, 0))
inline QPixmap LabelPixmap(const QLabel* label) { return label->pixmap(); }

using RecursiveMutex = QRecursiveMutex;
#else
inline QPixmap LabelPixmap(const QLabel* label) {
    return label->pixmap() != nullptr ? *label->pixmap() : QPixmap();
}

class RecursiveMutex : public QMutex {
public:
    RecursiveMutex() : QMutex(QMutex::Recursive) { }
};
#endif

// Merge QMap: like QMap::unite, which is deprecated in Qt, but overwrites values instead of adding multiple.
template <class KEY, class T> QMap<KEY, T>& Merge(QMap<KEY, T>& lhs, const QMap<KEY, T>& rhs)
{
    for (auto it = rhs.constBegin(); it != rhs.constEnd(); ++it)
        lhs[it.key()] = it.value();

    return lhs;
}

} // namespace QtCompat

#endif // QTCOMPAT_H
