/*
    Copyright 2022 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef UI_INL_H
#define UI_INL_H

namespace Util {
template <class TAB> void NextTab(TAB* tabs, int reserve)
{
    if (const int current = tabs->currentIndex(); current < (tabs->count() - reserve))
        tabs->setCurrentIndex(current + 1);
}

template <class TAB> void PrevTab(TAB* tabs, int reserve)
{
    if (const int current = tabs->currentIndex(); current > reserve)
        tabs->setCurrentIndex(current - 1);
}

} // namespace Util

#endif // UI_INL_H
