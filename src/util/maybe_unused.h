/*
    Copyright 2021 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef MAYBE_UNUSED_H
#define MAYBE_UNUSED_H

// Silence compiler warning: maybe_unused not valid on structured binding variables,
// and doesn't work on the whole binding as of g++ 7.4.0.  TODO: scheduled for improvement
// in future C++ standards: revisit at that time.
namespace Util {
template <typename... T> void maybe_unused([[maybe_unused]] const T&... v) { }
} // namespace Util

#endif // MAYBE_UNUSED_H
