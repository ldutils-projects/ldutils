/*
    Copyright 2019-2020 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef RESOURCES_H
#define RESOURCES_H

class QByteArray;

namespace Util {

QByteArray pubkey(); // return application's public key, if any

} // namespace Util

// To avoid using exit(), which does not unwind the stack, thus potentially leaving
// dangling temp files.
class Exit
{
public:
    static const int Quit = -1;  // signal quit with no error
    Exit(int rc) : m_rc(rc) { }
    int rc() const { return m_rc; }
    static int code(int rc);

private:
    int m_rc;
};

#endif // RESOURCES_H
