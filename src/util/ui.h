/*
    Copyright 2019-2022 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef UI_H
#define UI_H

#include <tuple>
#include <QVector>

class QString;
class QWidget;
class QTreeView;
class QColor;
class QLayout;
class QLabel;
class QAbstractButton;
class QGroupBox;

class MainWindowBase;
class DelegateBase;
class UndoMgr;

namespace Util {

// Navigation in QTabWidgets and QStackedWidgets
template <class TAB> void NextTab(TAB* tabs, int reserve = 0);
template <class TAB> void PrevTab(TAB* tabs, int reserve = 0);

// Helper to display warning dialog and also set status text.
int WarningDialog(MainWindowBase&, const QString& winTitle, const QString& msg, QWidget* parent = nullptr);

// Returns true if the underlying theme is light.
[[nodiscard]] bool IsLightTheme();

// Use light or dark them icon, given path under the icon directory.
[[nodiscard]] QString LocalThemeIcon(const QString& relPath);

// Set WhatsThis to match ToolTips, recursively for widget
void SetupWhatsThis(QWidget*);
void SetupWhatsThis(QWidget&);

// Copy actions from 'from' widget to 'to' widget
void CopyActions(QWidget* to, QWidget* from);

// Delegate setup helper
void InitDelegates(QTreeView&, const QVector<std::tuple<int, DelegateBase*>>&);
void InitDelegates(QTreeView&, UndoMgr&, const QVector<std::tuple<int, DelegateBase*>>&);

// set widget's window as active, and focus the widget
void SetFocus(QWidget*);

// Colorize text in a widget using style sheet
void SetWidgetStyle(QWidget*, const QColor&,
                    const QByteArray& fontStyle = QByteArray(),
                    const QByteArray& weight    = QByteArray());

void UnsetWidgetStyle(QWidget*);

// Clear out layout contents
void ClearLayout(QLayout*, int keepLeft = 0, int keepRight = 0);

// Recursively enable or disable items in a layout or widget
void SetEnabled(QWidget*, bool enabled);
void SetEnabled(QLayout*, bool enabled);

// Obtain plain text from potentially HTML widget text
QString PlainText(const QLabel*);
QString PlainText(const QAbstractButton*);
QString PlainText(const QGroupBox*);

} // namespace Util

#endif // UI_H
