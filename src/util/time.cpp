/*
    Copyright 2018-2022 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "src/util/time.h"

Time Time::operator-(const Time& t0) const
{
    int64_t tv_nsec = time.tv_nsec - t0.time.tv_nsec;
    int64_t tv_sec;

    if (tv_nsec < 0 ) {
        tv_nsec += 1000000000;
        tv_sec = time.tv_sec - t0.time.tv_sec - 1;
    } else {
        tv_sec = time.tv_sec - t0.time.tv_sec;
    }

    return { tv_sec, tv_nsec };
}

bool Time::operator>(const Time &t0) const
{
   return (time.tv_sec != t0.time.tv_sec) ?
               (time.tv_sec > t0.time.tv_sec) :
               (time.tv_nsec > t0.time.tv_nsec);
}

