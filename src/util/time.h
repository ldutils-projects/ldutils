/*
    Copyright 2018-2022 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TIME_H
#define TIME_H

#include <ctime>
#include <cinttypes>

// Subtract timespecs
void TSDiff(timespec *interval, const timespec *begin, const timespec *end);

class Time
{
public:
    Time() { time.tv_nsec = 0; time.tv_sec = 0; }
    Time(timespec ts) : time(ts) { }
    Time(int64_t sec, int64_t nsec) { time.tv_sec = sec; time.tv_nsec = nsec; }

    Time operator-(const Time& t0) const;
    bool operator>(const Time& t0) const;

    static Time now() {
        Time ts;
        clock_gettime(CLOCK_REALTIME, &ts.time);
        return ts;
    }

private:
    timespec time;
};

#endif // TIME_H
