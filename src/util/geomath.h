/*
    Copyright 2020-2022 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef GEOMATH_H
#define GEOMATH_H

#include <cmath>
#include <type_traits>

#include "src/util/math.h"

namespace GeoMath {
    static const constexpr double earthRadiusM = 6371008.8; // radius of earth around 39N

    // great circle distance using haversine formula
    template <class LAT, class LON, class R = double>
    R greatCircleDist(LAT lhsLatRad, LON lhsLonRad, LAT rhsLatRad, LON rhsLonRad)
    {
        const R latDiff = R(rhsLatRad - lhsLatRad);
        const R lonDiff = R(rhsLonRad - lhsLonRad);

        const R a = Math::sqr(std::sin(latDiff*R(0.5))) + std::cos(R(lhsLatRad)) * std::cos(R(rhsLatRad)) * Math::sqr(std::sin(lonDiff*R(0.5)));
        const R c = R(2.0) * atan2(sqrt(a), sqrt(R(1.0)-a));

        return c * R(earthRadiusM);
    }
} // namespace GeoMath

#endif // GEOMATH_H
