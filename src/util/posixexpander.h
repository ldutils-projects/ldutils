/*
    Copyright 2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef POSIXEXPANDER_H
#define POSIXEXPANDER_H

#include <wordexp.h>
#include <QString>
#include <QStringList>

namespace Util {

// Wrapper for wordexp(3). This ensures we clean up no matter how the stack frame is exited.
//
// We use wordexp(3) to parse the command string. This performs some POSIX-shell-like steps:
//   * tilde expansion
//   * variable substitution
//   * command substitution
//   * arithmetic expansion
//   * field splitting
//   * wildcard globbing
//   * quoting
class PosixExpander final {
public:
    PosixExpander(const QString& cmdLine);
    ~PosixExpander(); // free on stack unwind

    [[nodiscard]] int rc()           const { return m_rc; }
    [[nodiscard]] QString cmd()      const { return argc() > 0 ? at(0) : ""; }
    [[nodiscard]] int argc()         const { return m_args.we_wordc; }
    [[nodiscard]] QStringList args() const;

private:
    const char* at(int i) const { return m_args.we_wordv[i]; }

    wordexp_t m_args;
    int       m_rc;
};

} // namespace Util

#endif // POSIXEXPANDER_H
