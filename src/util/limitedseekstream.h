/*
    Copyright 2020 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef LIMITEDSEEKSTREAM_H
#define LIMITEDSEEKSTREAM_H

#include <QIODevice>
#include <QBuffer>
#include <QFile>

// Rather than buffer a whole stdin stream for seek/reopen purposes, requiring potentially unlimited
// amounts of memory, this class buffers just the beginning of the file, which is all that's needed
// for many purposes.  The first bufferSize bytes are buffered.  Reading more than that and then
// attempting to seek or re-open the device will fail.
//
// Note that QIODevice has its own internal 16K buffer.  Our buffer size must be that size of larger,
// to satisify QIODevice's buffered request.
class LimitedSeekStream final : public QIODevice
{
public:
    LimitedSeekStream(int fd, qint64 bufferSize = 1<<16);

    void close() override;
    bool open(QIODevice::OpenMode flags) override;
    qint64 pos() const override;
    bool seek(qint64 pos) override;
    qint64 size() const override;

private:
    qint64 readData(char *data, qint64 len) override;
    qint64 writeData(const char *data, qint64 len) override;

    inline qint64 incPos(qint64 inc);
    inline qint64 setPos(qint64 pos);

    QBuffer m_startOfFile;  // buffers the first bufferSize bytes.
    QFile   m_stream;       // stream to read from
    qint64  m_bufferSize;   // buffered size
    qint64  m_maxPos;       // maximum position so far
    qint64  m_pos;          // current position
    int     m_fd;           // file descriptor to buffer
};

#endif // LIMITEDSEEKSTREAM_H
