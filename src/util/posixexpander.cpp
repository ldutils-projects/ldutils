/*
    Copyright 2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "posixexpander.h"

namespace Util {

PosixExpander::PosixExpander(const QString& cmdLine) :
    m_rc(wordexp(qUtf8Printable(cmdLine), &m_args, 0))
{
}

PosixExpander::~PosixExpander()
{
    if (rc() == 0)  // wordfree will crash if called after a wordexp error.
        wordfree(&m_args);
}

QStringList PosixExpander::args() const {
    QStringList args;
    args.reserve(argc());

    for (int i = 1; i < argc(); ++i)
        args.append(at(i));
    return args;
}

} // namespace Util
