/*
    Copyright 2019-2022 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <QFileDevice>

#include "versionedstream.h"

bool VersionedStream::openWrite(QIODevice& io, unsigned int magic, unsigned int version)
{
    m_error = Error::NoError;

    if (!io.isOpen() && !io.open(QIODevice::WriteOnly | QIODevice::Truncate)) {
        m_error = Error::FileOpenError;
        return false;
    }
    
    setDevice(&io);
    writeMagic(magic);               // write the magic
    writeVersion(version);           // our internal stream version
    setVersion(m_dataStreamVersion); // Qt's stream version

    return true;
}

bool VersionedStream::openRead(QIODevice& io, unsigned int magic, unsigned int minVersion, unsigned int maxVersion)
{
    m_error = Error::NoError;

    if (!io.isOpen() && !io.open(QIODevice::ReadOnly)) {
        m_error = Error::FileOpenError;
        return false;
    }
    
    if (!io.isSequential())
        io.seek(0); // start at beginnings

    setDevice(&io);

    if (readMagic() != magic) {
        m_error = Error::BadMagic;
        return false;
    }

    readVersion();
    if (getVersion() < minVersion || getVersion() > maxVersion) {
        m_error = Error::BadVersion;
        return false;
    }

    setVersion(m_dataStreamVersion);

    return true;
}

const QString& VersionedStream::errorString()
{
    static const QString NoError("");
    static const QString BadMagic(QObject::tr("Bad magic"));
    static const QString BadVersion(QObject::tr("Bad version"));
    static const QString FileOpenError(QObject::tr("File open error"));
    static const QString FileError(QObject::tr("File error"));
    static const QString StreamError(QObject::tr("Stream error"));
    static const QString Unknown(QObject::tr("Unknown error"));

    switch (error()) {
    case Error::NoError:       return NoError;
    case Error::BadMagic:      return BadMagic;
    case Error::BadVersion:    return BadVersion;
    case Error::FileOpenError: return FileOpenError;
    case Error::FileError:     return FileError;
    case Error::StreamError:   return StreamError;
    }

    return Unknown;
}

VersionedStream::Error VersionedStream::error()
{
    if (status() != QDataStream::Ok)
        m_error = Error::StreamError;

    if (auto* file = qobject_cast<QFileDevice*>(device()); file != nullptr)
        if (file->error() != QFileDevice::NoError)
            m_error = Error::FileError;

    return m_error;
}
