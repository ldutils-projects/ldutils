/*
    Copyright 2020-2021 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef VARIANTCMP_H
#define VARIANTCMP_H

class QVariant;

namespace QtCompat {

// QVariant comparisons went away in Qt 5.15.  This is our own implementation thereof.
[[nodiscard]] bool lt(const QVariant&, const QVariant&);
[[nodiscard]] bool gt(const QVariant&, const QVariant&);
[[nodiscard]] inline bool ge(const QVariant& lhs, const QVariant& rhs) { return !lt(lhs, rhs); }
[[nodiscard]] inline bool le(const QVariant& lhs, const QVariant& rhs) { return !gt(lhs, rhs); }
[[nodiscard]] QVariant min(const QVariant& lhs, const QVariant& rhs);
[[nodiscard]] QVariant max(const QVariant& lhs, const QVariant& rhs);

} // namespace QtCompat

#endif // VARIANTCMP_H
