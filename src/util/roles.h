/*
    Copyright 2019-2022 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef ROLES_H
#define ROLES_H

#include <Qt>

namespace Util {
    static const int RawDataRole     = Qt::UserRole + 0;  // non-display, raw data
    static const int CopyRole        = Qt::UserRole + 1;  // format for copying
    static const int PrivateDataRole = Qt::UserRole + 2;  // private data
    static const int PlotRole        = Qt::UserRole + 3;  // format for plotting
    // Qt::UserRole+4 currently unused.
    static const int IconNameRole    = Qt::UserRole + 5;  // icon name (as opposed to icon bitmap data)
    static const int ModelTypeRole   = Qt::UserRole + 6;  // for ModelMetaData::setupComboBox
} // namespace Util

#endif // ROLES_H
