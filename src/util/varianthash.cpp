/*
    Copyright 2019-2022 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <cassert>
#include "varianthash.h"

#include <QVariant>
#include <QBitArray>
#include <QBitmap>
#include <QByteArray>
#include <QDate>
#include <QDateTime>
#include <QIcon>
#include <QLine>
#include <QLineF>
#include <QLocale>
#include <QModelIndex>
#include <QPixmap>
#include <QPoint>
#include <QPointF>
#include <QPersistentModelIndex>
#include <QRect>
#include <QRectF>
#include <QRegExp>
#include <QSize>
#include <QSizeF>
#include <QString>
#include <QStringList>
#include <QTime>
#include <QUrl>

namespace {
    template <typename T> inline uint hash(const QVariant& key, uint seed) {
        return qHash(key.value<T>(), seed);
    }
} // anonymous namespace

uint qHash(const QVariant& key, uint seed)
{
    switch (key.type())
    {
    case QVariant::BitArray:              return hash<QBitArray>(key, seed);
    case QVariant::Bitmap:                return hash<QBitmap>(key, seed);
    case QVariant::Bool:                  return hash<bool>(key, seed);
    case QVariant::ByteArray:             return hash<QByteArray>(key, seed);
    case QVariant::Char:                  return hash<QChar>(key, seed);
    case QVariant::Date:                  return hash<QDate>(key, seed);
    case QVariant::DateTime:              return hash<QDateTime>(key, seed);
    case QVariant::Double:                return hash<double>(key, seed);
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wswitch"
    // For some reason, float doesn't appear in the QVariant::Type enum on Qt 5.9
    case QMetaType::Float:                return hash<float>(key, seed);
#pragma GCC diagnostic pop
    case QVariant::Icon:                  return hash<QIcon>(key, seed);
    case QVariant::Int:                   return hash<int>(key, seed);
    case QVariant::Invalid:               return uint(-1);
    case QVariant::Line:                  return hash<QLine>(key, seed);
    case QVariant::LineF:                 return hash<QLineF>(key, seed);
    case QVariant::Locale:                return hash<QLocale>(key, seed);
    case QVariant::LongLong:              return hash<qlonglong>(key, seed);
    case QVariant::PersistentModelIndex:  return hash<QPersistentModelIndex>(key, seed);
    case QVariant::Point:                 return hash<QPoint>(key, seed);
    case QVariant::Pixmap:                return hash<QPixmap>(key, seed);
    case QVariant::PointF:                return hash<QPointF>(key, seed);
    case QVariant::ModelIndex:            return hash<QModelIndex>(key, seed);
    case QVariant::Rect:                  return hash<QRect>(key, seed);
    case QVariant::RectF:                 return hash<QRectF>(key, seed);
    case QVariant::RegExp:                return hash<QRegExp>(key, seed);
    case QVariant::Size:                  return hash<QSize>(key, seed);
    case QVariant::SizeF:                 return hash<QSizeF>(key, seed);
    case QVariant::String:                return hash<QString>(key, seed);
    case QVariant::StringList:            return hash<QStringList>(key, seed);
    case QVariant::Time:                  return hash<QTime>(key, seed);
    case QVariant::UInt:                  return hash<uint>(key, seed);
    case QVariant::ULongLong:             return hash<qulonglong>(key, seed);
    case QVariant::Url:                   return hash<QUrl>(key, seed);
    default: assert(0 && "unhashable variant type"); return uint(-1);
    }
}

uint qHash(const QVariant& key)
{
    return qHash(key, 0x92045bf4);
}
