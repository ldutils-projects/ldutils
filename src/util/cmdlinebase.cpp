/*
    Copyright 2019-2022 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <cstdio>
#include <cstring>
#include <cstdlib>

#include <QtGlobal>
#include <QByteArray>

#include "cmdlinebase.h"
#include "resources.h"

decltype(CmdLineBase::m_messageHandlerStack) CmdLineBase::m_messageHandlerStack;

CmdLineBase::CmdLineBase(const char* appname, const char* version, const char* build, const QStringList& argv) :
    m_disableFirstRun(false),
    m_privateSession(false),
    m_argv(argv),
    m_appname(appname), m_version(version), m_build(build),
    m_pubkeyRequest(false)
{
    if (m_messageHandlerStack.isEmpty())
        setupMessageHandler();
}

CmdLineBase::CmdLineBase(const char* appname, const char* version, const char* build, int argc, const char** argv) :
    CmdLineBase(appname, version, build)
{
    setArgv(argc, argv);
}

CmdLineBase::CmdLineBase(const char* appname, const char* version, const char* build, int argc, char** argv) :
    CmdLineBase(appname, version, build, argc, const_cast<const char**>(argv))
{
}

void CmdLineBase::setArgv(int argc, const char** argv)
{
    m_argv.clear();
    m_argv.reserve(argc);

    // Set up string array from C style argc/argv
    for (int i = 0; i < argc; ++i)
        m_argv.append(argv[i]);
}

CmdLineBase::~CmdLineBase()
{
}

void CmdLineBase::setupMessageHandler()
{
    pushMessageHandler(messageHandler);
}

void CmdLineBase::messageHandler(QtMsgType type, const QMessageLogContext& context, const QString& msg)
{
    switch (type) {
    case QtInfoMsg:
        fprintf(stdout, "%s\n", qUtf8Printable(msg));
        break;
    case QtDebugMsg:    [[fallthrough]];
    case QtWarningMsg:  [[fallthrough]];
    case QtCriticalMsg:
        fprintf(stderr, "%s\n",  qUtf8Printable(msg));
        break;
    case QtFatalMsg:
        fprintf(stderr, "Fatal: %s (%s:%u, %s)\n",  qUtf8Printable(msg), context.file, context.line, context.function);
        abort();
    }
}

int CmdLineBase::pushMessageHandler(const QtMessageHandler& handler)
{
    m_messageHandlerStack.push_back(qInstallMessageHandler(handler));
    return m_messageHandlerStack.size() - 1;  // our position in the stack
}

void CmdLineBase::popMessageHandler()
{
    qInstallMessageHandler(m_messageHandlerStack.takeLast());
}

void CmdLineBase::forwardMessage(int level, QtMsgType type, const QMessageLogContext& context, const QString& msg)
{
    m_messageHandlerStack.at(level)(type, context, msg);
}

bool CmdLineBase::processArg(int& arg)
{
    const QString& astr = m_argv[arg]; // convenience

    if (astr == "--desktop") {
        qputenv("XDG_CURRENT_DESKTOP", qUtf8Printable(next(arg)));
    } else if (astr == "--conf") {
        m_initialSettingsFile = next(arg);
    } else if (astr == "--xyzzy") {
        qInfo("%s", "nothing happens.");
        throw Exit(Exit::Quit);
    } else if (astr == "--help" || astr == "-?") {
        usage();
        throw Exit(Exit::Quit);
    } else if (astr == "--version") {
        qInfo("%s %s (built: %s)", m_appname, m_version, m_build);
        throw Exit(Exit::Quit);
    } else if (astr == "--pubkey") {
        m_pubkeyRequest = true;
    } else if (astr == "--no-first-run") {
        m_disableFirstRun = true;
    } else if (astr == "--private-session") {
        m_privateSession = true;
    } else {
        return false;
    }

    return true;
}

int CmdLineBase::processArgs()
{
    try {
        for (int arg = 1; arg < m_argv.size(); ++arg) {
            if (!processArg(arg)) {
                m_unknown = m_argv[arg];
                break;
            }
        }
    } catch (const Exit& rc) {
        return rc.rc();
    }

    return 0;
}

int CmdLineBase::processArgsPostApp() const
{
    try {
        // A bit ugly, but arguably synthetic data doesn't alter the
        // properties of the object.
        const_cast<CmdLineBase&>(*this).setup();

        if (!verify()) // check for sanity errors
            throw Exit(5);

        batch(); // handle any batch processesing
    } catch (const Exit& rc) {
        return rc.rc();
    }

    return 0;
}

bool CmdLineBase::verify() const
{
    if (!m_missing.isEmpty()) {
        qCritical("%s %s", qUtf8Printable(tr("Missing parameter to")), qUtf8Printable(m_missing));
        return false;
    }

    if (!m_unknown.isEmpty()) {
        qCritical("%s %s", qUtf8Printable(tr("Unknown option")), qUtf8Printable(m_unknown));
        return false;
    }

    return true;
}

void CmdLineBase::usage() const
{
    qInfo(qUtf8Printable(
              tr("Usage: %s [args...]\n\n"
                 "General Options:\n"
                 "   --help            Print usage information and exit.\n"
                 "   --version         Print program version and exit.\n"
                 "   --pubkey          Dump PGP/GPG public key to stdout and exit.\n"
                 "   --conf FILE       Load session from .conf file named FILE.\n"
                 "   --no-first-run    Disable first run handling, even on the first run.\n"
                 "   --private-session Disallow saves for this session.\n"
                 "   --desktop NAME    Sets the XDG_CURRENT_DESKTOP environment variable to NAME. This can\n"
                 "                     be useful when running the program under sudo, for example as in:\n"
                 "                        sudo -H -u user %s --desktop KDE\n"
                 "                     This will allow KDE to use a native theme under sudo.\n")),
          m_appname, m_appname);
}

void CmdLineBase::batch() const
{
    if (m_pubkeyRequest) {
        if (const QByteArray key = Util::pubkey(); !key.isEmpty()) {
            qInfo("%s", key.constData());
            throw Exit(Exit::Quit);
        }

        qCritical("Public key not found.");
        throw Exit(5);
    }
}

const QString& CmdLineBase::next(int& arg) const
{
    static const QString empty;

    if (arg >= (m_argv.size() - 1)) {
        m_missing = m_argv[arg];
        return empty;
    }

    return m_argv[++arg];
}

void CmdLineBase::collect(int& arg, QStringList& list) const
{
    if (arg >= (m_argv.size() - 1)) {
        m_missing = m_argv[arg];
        return;
    }

    ++arg; // skip option name

    // Collect up to next option, or end of list.  Treat '-' (stdin/stdout) as an argument, not an option.
    while (arg < m_argv.size() && (!m_argv[arg].startsWith("-") || m_argv[arg] == "-"))
        list.append(m_argv[arg++]);

    --arg;  // we must leave this on the last arg consumed, not one past.
}
