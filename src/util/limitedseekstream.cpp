/*
    Copyright 2020-2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <cstring>   // for memcpy
#include <algorithm> // for std::max
#include "limitedseekstream.h"

LimitedSeekStream::LimitedSeekStream(int fd, qint64 bufferSize) :
    m_bufferSize(bufferSize), m_maxPos(0), m_pos(0), m_fd(fd)
{
}

void LimitedSeekStream::close()
{
    m_startOfFile.close();
    m_stream.close();
    QIODevice::close();
}

qint64 LimitedSeekStream::incPos(qint64 inc)
{
    return setPos(m_pos + inc);
}

qint64 LimitedSeekStream::setPos(qint64 pos)
{
    m_pos = pos;
    m_maxPos = std::max(m_maxPos, m_pos);
    return m_pos;
}

// TODO: we could implement large stdout streaming with a temp file, copying it to
// stdout on close.
bool LimitedSeekStream::open(QIODevice::OpenMode flags)
{
    if (m_maxPos >= m_bufferSize) // we read too far; cannot re-open.
        return false;

    if (bool(flags & (QIODevice::Unbuffered)))
        return false;

    if (!m_stream.open(m_fd, flags) || !m_startOfFile.open(flags | QIODevice::WriteOnly))
        return false;

    setPos(0);

    return QIODevice::open(flags);
}

qint64 LimitedSeekStream::pos() const
{
    return m_pos;
}

bool LimitedSeekStream::seek(qint64 pos)
{
    // We cannot seek past beginning of buffer, or at all once we've read beyond.
    // However, we use > rather than >= here, because if we read m_bufferSize bytes
    // but haven't read more past that, it's still OK to seek to the beginning.
    if (pos > m_bufferSize || m_maxPos > m_bufferSize)
        return false;

    QIODevice::seek(pos);
    return m_startOfFile.seek(setPos(pos));
}

qint64 LimitedSeekStream::size() const
{
    return m_stream.bytesAvailable();
}

qint64 LimitedSeekStream::readData(char* data, qint64 len)
{
    if (len == 0)
        return 0;

    qint64 totalRead = 0;

    // Return data from buffer if possible
    if (m_pos < m_bufferSize) {
        // If we have no data, read up to m_bufferSize if available.
        if (m_startOfFile.data().isEmpty() && m_maxPos == 0) {
            m_startOfFile.write(m_stream.read(m_bufferSize));
            m_startOfFile.seek(0);
        }

        if (const qint64 bytesFromBuffer = std::min(len, m_startOfFile.size() - pos()); bytesFromBuffer > 0) {
            memcpy(data, m_startOfFile.data().constData() + m_pos, bytesFromBuffer);
            incPos(bytesFromBuffer);
            len -= bytesFromBuffer;
            data += bytesFromBuffer;
            totalRead += bytesFromBuffer;
        }
    }

    // Satisfy any of rest from the stream
    if (len > 0) {
        totalRead += m_stream.read(data, len);
        incPos(len);
    }

    return totalRead;
}

qint64 LimitedSeekStream::writeData(const char* data, qint64 len)
{
    if ((m_pos + len) > m_bufferSize)
        return -1;

    incPos(len);

    return m_startOfFile.write(data, len);
}
