/*
    Copyright 2020-2023 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef STRONGNUM_H
#define STRONGNUM_H

#if __cplusplus < 201703L
#  error "This requires c++17"
#else

/*******************************************************************************
 * A strongly typed arithmetic.
 *
 * Various web pages recommend using enum classes for this purpose.  While those
 * are nominally strong integers, the approach runs into limitations.
 *
 * StrongNum is templatized on a key type. Values sharing a key can mingle, but
 * mingling with different keys causes a compile error (barring explicit
 * efforts with type conversion or construction: see IsForeignConstructable below).
 *
 * For example:
 *      Strong::int32_t<class KeyA> a1(5), a2(6);   // Key A
 *      Strong::int32_t<class KeyB> b1(1), b2(2);   // Key B...
 *      Strong::int32_t<class KeyB> b3(1);          // ...also Key B
 *
 *      a1 + a2;   a1 == a2;   // OK
 *      b1 + b2;   b1 == b2;   // OK
 *      a1 + 6;    a1 == 6;    // OK
 *   // a1 + b1;   a1 == b2;   // YOU! SHALL! NOT! PASS!
 *
 * The key need not exist.  Merely mentioning it, as above, is sufficient.
 *
 * You can control the base integer type with the 2nd template parameter:
 *    Strong::Num<class MyKey, uint64_t>
 *
 * The template accepts zero or more compile-time options as follows.  These
 * flags can be |'ed together and given as the 3rd template parameter:
 *    Strong::Options::ExplicitConstruction
 *        Disables implicit construction from the base integer type.
 *        This makes the int strongER, at some loss of convenience when
 *        mingling with the base int type in expressions.
 *    Strong::Options::IsForeignConstructable
 *        Allow explicit construction from foreign-keyed strong integers.
 *    Strong::Options::DefaultZeroConstruct
 *        Provides a default constructor to zero.  Otherwise, default
 *        construction is trivial.
 *
 * A convenience template StrongerNum is merely a StrongNum with
 * Strong::Options::ExplicitConstruction set.
 *
 * Goodies:
 *   - Strong::Num::base_type provides the base integer type of a StrongNum type
 *   - Strong::Num::key_type  provides the key type of a StrongNum type
 *   - Strong::Num::is_explicit_constructable() tests explicit constructability.
 *   - Strong::Num::is_default_zero_construct() tests default zero construction
 *   - Strong::Num::is_foreign_constructable()  tests explicit constructability
 *         from foreign keyed StrongNums.
 *
 * StrongNum attempts to do these things transparently:
 *
 *   - Allow all StrongNums with a common key type to mingle, but cross type
 *     mingling requires an explicit request via casting.
 *
 *   - Primary comparison and arithmetic operators (+, ==, !, pre/post inc/decs,
 *     etc) work among all values sharing a key, and with the integer base_type
 *     provided the ExplicitConstruction flag is not enabled.
 *
 *   - std hashing and comparison based containers work, with no need to cast out
 *     to integers to use them.
 *
 *   - std::numeric_limits fns work with a StrongNum as expected.
 *
 *   - Certain but not all type_traits work as expected.  e.g, std::is_signed.
 *
 *   - StrongNum satisfies:
 *        std::is_trivially_copyable
 *        std::is_trivially_copy_constructible
 *        std::is_trivially_move_constructible
 *        std::is_trivially_move_assignable
 *        std::is_standard_layout
 *        std::is_trivially_default_constructable, unless DefaultZeroConstruct is set
 *
 *   - StrongNums supported forward declaration with key compatibility, so a common
 *     typedef for them need not be available everywhere.
 *
 * Limitations:
 *
 *   - Several expected properties of integers are not satisfied, such as
 *     std::is_trivial_v, std::is_pod_v, and std::is_integral_v.  While nothing
 *     prevents specializing those symptoms away, that'd be playing with fire.
 *
 *   - Bitfield syntax cannot be used, ala StrongNum<class KEY> x : 5;
 *
 *   - Due to different signedness warning behavior between variable and literal
 *     comparisons, it is very difficult to achieve warning-for-warning
 *     compatibility with the base integer type.  StrongNum does not even attempt it.
 *
 *   - Although there's an std::swap with the base_type in both directions,
 *     std::is_swappable_with is false.  I haven't yet hunted down the reason.
 *
 *******************************************************************************/

#include <cassert>
#include <cstdint>
#include <ctgmath>
#include <type_traits>
#include <limits>
#include <algorithm>
#include <functional>

static_assert(sizeof(float) == 4, "project requires 4 byte float type");
static_assert(sizeof(double) == 8, "project requires 8 byte double type");

namespace Strong {

enum class Options;

// Detail namespace.  See the external API below for usage.
namespace detail {

template <typename IMPL, typename KEY, typename T, bool EC, bool AFC, bool DZC>
class NumImpl
{
public:
    static_assert(std::is_arithmetic_v<T>); // TODO: use c++20 concepts, when available.

    NumImpl() = default;
    constexpr NumImpl(const NumImpl&) = default;

    // Useful things to provide.  The functions are constexpr and thus can be used in static_asserts
    // and type_traits checks.
    using base_type = T;
    using key_type = KEY;
    static constexpr bool is_explicit_constructable() { return EC; }
    static constexpr bool is_default_zero_construct() { return DZC; }
    static constexpr bool is_foreign_constructable()  { return AFC; }

    // Implicit construction from other values.  The normal method of using
    // std::enable_if_t with SFINAE not work on constructors, which have no return,
    // so we use a fake parameter.
    template <bool IF = is_explicit_constructable()>
    constexpr NumImpl(T v, typename std::enable_if_t<!IF>* = 0) : value(v) { }

    // Explicit construction from other values, if not disabled.
    template <bool IF = is_explicit_constructable()>
    explicit constexpr NumImpl(T v, typename std::enable_if_t<IF>* = 0) : value(v) { }

    // Construct from same key, but different parameters.
    template <typename IMPL2, typename T2, bool TC2, bool AFC2, bool DZ2>
    explicit constexpr NumImpl(NumImpl<IMPL2, KEY, T2, TC2, AFC2, DZ2> other) : value(other.value)
    { }

    // Foreign StrongNm construction, if not disabled.
    template <typename IMPL2, typename KEY2, typename T2, bool TC2, bool AFC2, bool DZ2,
              bool IF = is_foreign_constructable()>
    explicit constexpr NumImpl(NumImpl<IMPL2, KEY2, T2, TC2, AFC2, DZ2> other,
                               typename std::enable_if_t<IF>* = 0) : value(other.value)
    { }

    // Assignment allowed from T2, unless ExplicitConstruct is enabled.
    NumImpl& operator=(const NumImpl&) = default;

    template <typename T2, bool IF = is_foreign_constructable(), typename std::enable_if_t<!IF>>
    IMPL& operator=(T2 rhs) {
        value = rhs;
        return reinterpret_cast<IMPL&>(*this);
    }

    // Other specializations cannot be assigned.
    template <typename IMPL2, typename KEY2, typename T2, bool EC2, bool AFC2, bool DZC2>
    IMPL& operator=(const NumImpl<IMPL2, KEY2, T2, EC2, AFC2, DZC2>& v) = delete;

    // We can assign from the same key, but different parameters.
    template <typename IMPL2, typename T2, bool EC2, bool AFC2, bool DZC2>
    IMPL& operator=(const NumImpl<IMPL2, KEY, T2, EC2, AFC2, DZC2>& rhs) {
        value = rhs.value;
        return reinterpret_cast<IMPL&>(*this);
    }

    // Explicit conversions to other numeric types
#   define CONVERT(T) explicit operator T()  const { return T(value); }
        CONVERT(std::int8_t)  CONVERT(std::int16_t)  CONVERT(std::int32_t)  CONVERT(std::int64_t)
        CONVERT(std::uint8_t) CONVERT(std::uint16_t) CONVERT(std::uint32_t) CONVERT(std::uint64_t)
        CONVERT(float) CONVERT(double)

    // 64 bit Qt int types are defined differently from std::int64_t/uint64_t for 64 bit builds, but not 32.
    // That creates overload issues with QDataStream. We avoid that by providing separate conversion casts
    // in that case.
#   if defined QT_VERSION && QT_POINTER_SIZE == 8
       CONVERT(::qint64)
       CONVERT(::quint64)
#   endif // QT_VERSION

#   undef CONVERT

    // Explicit conversion to our own reference types
    explicit operator T&()             { return value; }

    // These operators create a new StrongNum, so are only allowed if implicit
    // conversions are allowed.
    IMPL operator+(NumImpl v)  const { return IMPL(value + v.value); }
    IMPL operator-(NumImpl v)  const { return IMPL(value - v.value); }
    IMPL operator*(NumImpl v)  const { return IMPL(value * v.value); }
    IMPL operator/(NumImpl v)  const { return IMPL(value / v.value); }
    IMPL operator-()           const { return -value; }
    IMPL operator+()           const { return +value; }

    template<typename T2 = T, typename = std::enable_if_t<std::is_integral_v<T2>>>
    IMPL operator&(NumImpl v)  const { return IMPL(value & v.value); }

    template<typename T2 = T, typename = std::enable_if_t<std::is_integral_v<T2>>>
    IMPL operator|(NumImpl v)  const { return IMPL(value | v.value); }

    template<typename T2 = T, typename = std::enable_if_t<std::is_integral_v<T2>>>
    IMPL operator^(NumImpl v)  const { return IMPL(value ^ v.value); }

    template<typename T2 = T, typename = std::enable_if_t<std::is_integral_v<T2>>>
    IMPL operator%(NumImpl v)  const { return IMPL(value % v.value); }

    template<typename T2 = T, typename = std::enable_if_t<std::is_integral_v<T2>>>
    IMPL operator<<(NumImpl v)  const { return IMPL(value << v.value); }

    template<typename T2 = T, typename = std::enable_if_t<std::is_integral_v<T2>>>
    IMPL operator>>(NumImpl v)  const { return IMPL(value >> v.value); }

    // Since these operators produce booleans, they can't create a StrongNum,
    // so it's safe to always allow them from the underlying type no matter whether
    // implicit constructions are allowed.
    bool  operator==(T v)             const { return value == v;  }
    bool  operator!=(T v)             const { return value != v;  }
    bool  operator<(T v)              const { return value < v;  }
    bool  operator<=(T v)             const { return value <= v;  }
    bool  operator>(T v)              const { return value > v;  }
    bool  operator>=(T v)             const { return value >= v;  }

    bool  operator==(NumImpl v) const { return value == v.value;  }
    bool  operator!=(NumImpl v) const { return value != v.value;  }
    bool  operator<(NumImpl v)  const { return value < v.value;   }
    bool  operator<=(NumImpl v) const { return value <= v.value;  }
    bool  operator>(NumImpl v)  const { return value > v.value;   }
    bool  operator>=(NumImpl v) const { return value >= v.value;  }
    bool  operator!()           const { return !value; }

    template<typename T2 = T, typename = std::enable_if_t<std::is_integral_v<T2>>>
    IMPL  operator~()           const { return IMPL(~value); }

    IMPL& operator+=(NumImpl v) { value += v.value; return reinterpret_cast<IMPL&>(*this); }
    IMPL& operator-=(NumImpl v) { value -= v.value; return reinterpret_cast<IMPL&>(*this); }
    IMPL& operator*=(NumImpl v) { value *= v.value; return reinterpret_cast<IMPL&>(*this); }
    IMPL& operator/=(NumImpl v) { value /= v.value; return reinterpret_cast<IMPL&>(*this); }

    template<typename T2 = T, typename = std::enable_if_t<std::is_integral_v<T2>>>
    IMPL& operator<<=(NumImpl v) { value <<= v.value; return reinterpret_cast<IMPL&>(*this); }
    template<typename T2 = T, typename = std::enable_if_t<std::is_integral_v<T2>>>
    IMPL& operator>>=(NumImpl v) { value >>= v.value; return reinterpret_cast<IMPL&>(*this); }

    template<typename T2 = T, typename = std::enable_if_t<std::is_integral_v<T2>>>
    IMPL& operator&=(NumImpl v) { value &= v.value; return reinterpret_cast<IMPL&>(*this); }
    template<typename T2 = T, typename = std::enable_if_t<std::is_integral_v<T2>>>
    IMPL& operator|=(NumImpl v) { value |= v.value; return reinterpret_cast<IMPL&>(*this); }
    template<typename T2 = T, typename = std::enable_if_t<std::is_integral_v<T2>>>
    IMPL& operator%=(NumImpl v) { value %= v.value; return reinterpret_cast<IMPL&>(*this); }

    IMPL& operator++()    /*pre-inc*/  { ++value; return reinterpret_cast<IMPL&>(*this); }
    IMPL& operator--()    /*pre-inc*/  { --value; return reinterpret_cast<IMPL&>(*this); }
    IMPL  operator++(int) /*post-inc*/ { const T old = value++; return IMPL(old); }
    IMPL  operator--(int) /*post-inc*/ { const T old = value--; return IMPL(old); }

    void swap(NumImpl& rhs) { std::swap(value, rhs.value); }  // swap with this type
    void swap(T& rhs) { std::swap(value, rhs); }  // swap with base_type
    void set(const T& v) { value = v; }           // explicit set, useful if implicit construction is disabled

protected:
    // Other specializations cannot be assigned.
    template <typename IMPL2, typename KEY2, typename T2, bool EC2, bool AFC2, bool DZC2>
    NumImpl& operator=(const NumImpl<IMPL2, KEY2, T2, EC2, AFC2, DZC2>& v) = delete;

    T value;  // all else exists to support this wee little integer.
};

// Since we cannot templatize "= default" style constructors to switch them with std::enable_if,
// we must use specialization to either provide = default, or our own default constructor.
template <typename KEY, typename T, bool EC, bool AFC, bool DZC>
class Num { };

template <typename KEY, typename T, bool EC, bool AFC>
class Num<KEY, T, EC, AFC, true> : public NumImpl<Num<KEY, T, EC, AFC, true>, KEY, T, EC, AFC, true>
{
    using base_t = NumImpl<Num<KEY, T, EC, AFC, true>, KEY, T, EC, AFC, true>;

public:
    using base_t::base_t;

    Num() : base_t(0) { }    // default zero construction

    // We can assign from the same key, but different parameters.
    template <typename T2, bool EC2, bool AFC2, bool DZC2>
    Num& operator=(Num<KEY, T2, EC2, AFC2, DZC2> rhs) { return base_t::operator=(rhs); }

    // The type of an identical StrongNum, but with a new key.
    template <typename KEY2> using change_key = Num<KEY2, T, EC, AFC, true>;
    // The type of an identical StrongNum, but with a new base_type
    template <typename T2> using change_base_type = Num<KEY, T2, EC, AFC, true>;
};

template <typename KEY, typename T, bool EC, bool AFC>
class Num<KEY, T, EC, AFC, false> : public NumImpl<Num<KEY, T, EC, AFC, false>, KEY, T, EC, AFC, false>
{
    using base_t = NumImpl<Num<KEY, T, EC, AFC, false>, KEY, T, EC, AFC, false>;

public:
    using base_t::base_t;

    Num() = default;    // default trivial construct (redundant: given for clarity)

    // We can assign from the same key, but different parameters.
    template <typename T2, bool EC2, bool AFC2, bool DZC2>
    Num& operator=(Num<KEY, T2, EC2, AFC2, DZC2> rhs) { return base_t::operator=(rhs); }

    // The type of an identical StrongNum, but with a new key.
    template <typename KEY2> using change_key = Num<KEY2, T, EC, AFC, false>;
    // The type of an identical StrongNum, but with a new base_type
    template <typename T2> using change_base_type = Num<KEY, T2, EC, AFC, false>;
};

   // Provide for comparison operators with numeric LHS types.
   // This badly needs c++ concepts so we can control the acceptable types to just std::is_integral's
   // Lacking that, will will have to hack this in, because we cannot allow foreign keyed Strong::Nums
   // to match the LHS typename.
#   define MKOP2(LHS, op) \
    template <typename KEY, typename T, bool AFC, bool DZC> \
    Num<KEY, T, false, AFC, DZC> operator op(LHS lhs, Num<KEY, T, false, AFC, DZC> rhs) { \
        return Num<KEY, T, false, AFC, DZC>(T(lhs) op T(rhs)); \
    }

#   define MKOPF(op) MKOP2(float, op) MKOP2(double, op)

#   define MKOPI(op) \
    MKOP2(int8_t, op)  MKOP2(int16_t, op)  MKOP2(int32_t, op)  MKOP2(int64_t, op) \
    MKOP2(uint8_t, op) MKOP2(uint16_t, op) MKOP2(uint32_t, op) MKOP2(uint64_t, op)

#   if defined QT_VERSION && QT_POINTER_SIZE == 8
#      define MKOPQT(op) MKOP2(qint64, op) MKOP2(quint64, op)
#   else
#      define MKOPQT(op)
#   endif // QT_VERSION

#   define MKOP(op) MKOPF(op) MKOPI(op) MKOPQT(op)

    MKOP(+) MKOP(-) MKOP(*) MKOP(/) MKOPI(&) MKOPI(|) MKOPI(^) MKOPI(%) MKOPI(<<) MKOPI(>>)

#   undef MKOP2

#   define MKOP2(LHS, op) \
    template <typename KEY, typename T, bool AFC, bool DZC> \
    bool operator op(LHS lhs, Num<KEY, T, false, AFC, DZC> rhs) { return T(lhs) op T(rhs); }

    MKOP(==) MKOP(!=) MKOP(<) MKOP(<=) MKOP(>) MKOP(>=)
#   undef MKOP2
#   undef MKOP
#   undef MKOPI

} // namespace detail
} // namespace Strong

namespace std {
   using namespace ::Strong::detail;

   // Provide for numeric limits.  AFAICS this can't be done with 'using', so we
   // use inheritance.
   template <typename KEY, typename T, bool EC, bool AFC, bool DZC>
   struct numeric_limits<Num<KEY, T, EC, AFC, DZC>> : public numeric_limits<T> { };

   // Provide for containers that use std::hash. This cannot be done with 'using', given
   // that we're producing the same "hash" name as we're pointing to.
   //
   // Injection of a 'hash' specialization into std:: is explicitly permitted by std.
   // It sure seems like this would be a good use for variadic templates, but param
   // packets seem to break parameter type deduction.
   template <typename KEY, typename T, bool EC, bool AFC, bool DZC>
   struct hash<Num<KEY, T, EC, AFC, DZC>> {
       size_t operator()(const Num<KEY, T, EC, AFC, DZC>& si) const noexcept {
           return hash<T>()(T(si));
       }
   };

   // Provide for std::is_signed/is_unsigned.
   template <typename KEY, typename T, bool EC, bool AFC, bool DZC>
   struct is_signed<Num<KEY, T, EC, AFC, DZC>> : public is_signed<T> { };

   template <typename KEY, typename T, bool EC, bool AFC, bool DZC>
   struct is_unsigned<Num<KEY, T, EC, AFC, DZC>> : public is_unsigned<T> { };

   // Provide for std::swap
   template <typename KEY, typename T, bool EC, bool AFC, bool DZC>
   void swap(Num<KEY, T, EC, AFC, DZC>& lhs, T& rhs) { lhs.swap(rhs); }

   template <typename KEY, typename T, bool EC, bool AFC, bool DZC>
   void swap(T& rhs, Num<KEY, T, EC, AFC, DZC>& lhs) { rhs.swap(lhs); }

   // std::clamp
   template <typename KEY, typename T, typename T2, bool EC, bool AFC, bool DZC>
   Num<KEY, T, EC, AFC, DZC> clamp(const Num<KEY, T, EC, AFC, DZC>& val, T2 low, T2 high) {
       return std::clamp(T(val), T(low), T(high));
   }

   // Create some common floating point math functions in std::

   // macro ugliness to avoid a bunch of boilerplate that isn't amenable to TMP.
#   define MKOP1R(R, op) \
   template <typename KEY, typename T, bool EC, bool AFC, bool DZC, \
             typename T2 = T, typename = std::enable_if_t<std::is_floating_point_v<T2>>> \
    R op(Num<KEY, T, EC, AFC, DZC> v) { \
    return (R)(op(T(v))); \
}

#   define MKOP2R(R, op) \
    template <typename KEY, typename T, bool EC, bool AFC, bool DZC, \
              typename T2 = T, typename = std::enable_if_t<std::is_floating_point_v<T2>>> \
    R op(Num<KEY, T, EC, AFC, DZC> v1, Num<KEY, T, EC, AFC, DZC> v2) { \
        return (R)(op(T(v1), T(v2))); \
    }

   // Provide common floating point ops
#   define NUMTMPL Num<KEY, T, EC, AFC, DZC>
#   define MKOP1(op) MKOP1R(NUMTMPL, op)
#   define MKOP2(op) MKOP2R(NUMTMPL, op)

    MKOP1(cos) MKOP1(sin) MKOP1(tan) MKOP1(acos) MKOP1(asin) MKOP1(atan) MKOP2(atan2)
    MKOP1(cosh) MKOP1(sinh) MKOP1(tanh) MKOP1(acosh) MKOP1(asinh) MKOP1(atanh)
    MKOP1(exp) MKOP1(log) MKOP1(log10) MKOP1(exp2) MKOP1(expm1) MKOP1(log2) MKOP1(logb)
    MKOP2(pow) MKOP1(sqrt) MKOP1(cbrt) MKOP2(hypot) MKOP1(erf) MKOP1(erfc)
    MKOP1(tgamma) MKOP1(lgamma) MKOP1(ceil) MKOP1(floor) MKOP2(fmod) MKOP1(trunc)
    MKOP1(round) MKOP1R(long int, lround) MKOP1R(long long int, llround)
    MKOP1(rint) MKOP1R(long int, lrint) MKOP1R(long long int, llrint) MKOP1R(bool, signbit)
    MKOP1(nearbyint) MKOP2(remainder)
    MKOP2(copysign) MKOP2(nextafter) MKOP2(nexttoward) MKOP2(fdim) MKOP2(fmax) MKOP2(fmin)
    MKOP1(fabs) MKOP1(abs)
    MKOP1R(bool, isfinite) MKOP1R(bool, isinf) MKOP1R(bool, isnan) MKOP1R(bool, isnormal)
    MKOP1R(int, fpclassify)
    MKOP2R(bool, isgreater) MKOP2R(bool, isgreaterequal) MKOP2R(bool, isless) MKOP2R(bool, islessequal)
    MKOP2R(bool, islessgreater) MKOP2R(bool, isunordered)
   // TODO: modf, remquo, nan, fma

#   undef MKOP1R
#   undef MKOP1
#   undef MKOP2R
#   undef MKOP2

} // namespace std

namespace {

   // Allow easy flag combining with | and &
   inline constexpr Strong::Options operator|(Strong::Options lhs, Strong::Options rhs) {
       return Strong::Options(std::underlying_type_t<Strong::Options>(lhs) |
                               std::underlying_type_t<Strong::Options>(rhs));
   }

   inline constexpr Strong::Options operator&(Strong::Options lhs, Strong::Options rhs) {
       return Strong::Options(std::underlying_type_t<Strong::Options>(lhs) &
                               std::underlying_type_t<Strong::Options>(rhs));
   }
} // end anonymous namespace

   // Qt hash function
   template <typename KEY, typename T, bool EC, bool AFC, bool DZC>
   inline uint qHash(Strong::detail::Num<KEY, T, EC, AFC, DZC> v, uint seed = 0) noexcept { return qHash(T(v), seed); }

#define DEFTYPE(T, G) \
    template <typename KEY, Options opts = Options::None> using T = Num<KEY, G, opts>

#ifdef QT_VERSION
#   define NAMESPACE_TYPES_QT \
    ; \
    DEFTYPE(qint64,   ::qint64); \
    DEFTYPE(quint64,   ::quint64)
#else
#   define NAMESPACE_TYPES_QT
#endif // QT_VERSION

// macro to insert helper templates for standard types into a namespace
#define NAMESPACE_TYPES \
    DEFTYPE(int8_t ,   std::int8_t); \
    DEFTYPE(uint8_t,   std::uint8_t); \
    DEFTYPE(int16_t,   std::int16_t); \
    DEFTYPE(uint16_t,  std::uint16_t); \
    DEFTYPE(int32_t,   std::int32_t); \
    DEFTYPE(uint32_t,  std::uint32_t); \
    DEFTYPE(int64_t,   std::int64_t); \
    DEFTYPE(uint64_t,  std::uint64_t); \
    DEFTYPE(float_t,   float); \
    DEFTYPE(double_t,  double) \
    NAMESPACE_TYPES_QT

// ****************************** Public API ******************************
// A strong integer class which acts a lot, but not entirely, like an
// integer.  See comment at top of file for details.
namespace Strong {
   enum class Options {
       None                   = 0,
       ExplicitConstruction   = (1<<0), // even stronger: only explicit construction from T.
       IsForeignConstructable = (1<<1), // constructable from foreign-keyed StrongNums
       DefaultZeroConstruct   = (1<<2), // default construction to 0, rather than trivial initialization.
   };

// StrongNum. Can be given zero or more Options as above.
template <typename KEY, typename T, Options opts = Options::None>
   using Num = detail::Num<KEY, T,
      bool(opts & Options::ExplicitConstruction),
      bool(opts & Options::IsForeignConstructable),
      bool(opts & Options::DefaultZeroConstruct)>;

   // Convenience templates to provide various Strong::int32_t/etc types
   NAMESPACE_TYPES;
} // namespace Strong

namespace Stronger {
   using ::Strong::Options;  // bring options into Stronger namespace

   // A convenience template which is simply a StrongNum with the ExplicitConstruction flag
   template <typename KEY, typename T, Options opts = Options::None>
      using Num = ::Strong::Num<KEY, T, opts | Options::ExplicitConstruction>;

    // Convenience templates to provide various Strong::int32_t/etc types
    NAMESPACE_TYPES;
} // namespace Stronger

#undef DEFTYPE
#undef NAMESPACE_TYPES

#endif // __cplusplus < 201703L

#endif // STRONGNUM_H
