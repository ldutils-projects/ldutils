/*
    Copyright 2019 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef SCOPETIMER_H
#define SCOPETIMER_H

#include <chrono>
#include <QDebug>

namespace Util {

class ScopeTimer
{
public:
    ScopeTimer(const char* msg) :
        start(std::chrono::high_resolution_clock::now()),
        msg(msg)
    { }

    ~ScopeTimer() {
        qDebug() << msg << std::chrono::duration_cast<std::chrono::microseconds>
                    (std::chrono::high_resolution_clock::now() - start).count();
    }
private:
    std::chrono::high_resolution_clock::time_point start;
    const char* msg;
};

} // namespace Util

#endif // SCOPETIMER_H
