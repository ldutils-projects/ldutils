/*
    Copyright 2019-2022 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef MATH_H
#define MATH_H

#include <limits>
#include <ctgmath>
#include <type_traits>
#include <src/util/strongnum.h>

namespace Math {
    // Weirdly, std:: doesn't provide pi!, before c++20, just this nonstandard define.
    template <typename T> [[nodiscard]] inline T toRad(T v) { return v * T(M_PI) / T(180.0); }
    template <typename T> [[nodiscard]] inline T toDeg(T v) { return v * T(180.0) / T(M_PI); }

    template <typename T> [[nodiscard]] inline T sqr(T x)   { return x*x; }
    template <typename T> [[nodiscard]] inline T distSqr(T x, T y)  { return sqr(x) + sqr(y); }

    template <typename T, typename X> [[nodiscard]] inline T mix(T a, T b, X x) { return a+(b-a)*x; }

    template<class T> [[nodiscard]] bool almost_equal(T x, T y)
    {
        return T(std::nextafter(x, T(std::numeric_limits<T>::lowest()))) <= y &&
               T(std::nextafter(x, T(std::numeric_limits<T>::max()))) >= y;
    }
} // namespace Math

#endif // MATH_H
