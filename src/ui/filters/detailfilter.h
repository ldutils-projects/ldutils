/*
    Copyright 2019-2022 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef DETAILFILTER_H
#define DETAILFILTER_H

#include <cassert>
#include <initializer_list>

#include <QSortFilterProxyModel>
#include <QModelIndexList>
#include <QModelIndex>
#include <QVector>
#include <QPersistentModelIndex>

#include <src/core/modelmetadata.h>

class DetailFilter : public QSortFilterProxyModel
{
protected:
    enum class Header : ModelType {
    };

    struct Line {
        explicit Line(const Line& other, const Line* parent) :
            data(other.data), children(other.children), parent(parent), m_isHeader(other.m_isHeader)
        {
            setParentPtr(this);
        }

        Line(ModelType data, const std::initializer_list<Line>& init = std::initializer_list<Line>()) :
            data(data), parent(nullptr), m_isHeader(false)
        {
            for (const auto& i : init)
                children.push_back(new Line(i, this));
        }

        Line(Header header, const std::initializer_list<Line>& init = std::initializer_list<Line>()) :
            header(header), parent(nullptr), m_isHeader(true)
        {
            for (const auto& i : init)
                children.push_back(new Line(i, this));
        }

        bool isHeader() const { return m_isHeader; }
        const Line* child(int n) const { return n < children.size() ? children[n] : nullptr; }

        union {
            ModelType data;
            Header    header;
        };

        QVector<const Line*> children;
        const Line*          parent;

    private:
        bool m_isHeader;

        // Bit of a hack to allow static const construction: fix child pointers as nodes are copied
        void static setParentPtr(const Line* parent) {
            for (const auto& child : parent->children)
                const_cast<Line*>(child)->parent = parent;
        }
    };

public:
    enum {
        Key  = 0,
        Data,
        _Count
    };

    DetailFilter(const DetailFilter::Line& m_rootItem,
                 int m_sourceColumnBegin, int m_sourceColumnEnd, QObject *parent = nullptr);

    int         rowCount(const QModelIndex &parent = QModelIndex()) const override;
    int         columnCount(const QModelIndex &parent = QModelIndex()) const override;
    QModelIndex mapFromSource(const QModelIndex &sourceIndex) const override;
    QModelIndex mapToSource(const QModelIndex &proxyIndex) const override;
    using QSortFilterProxyModel::index;
    QModelIndex index(int row, int column, const QModelIndex &parent) const override;
    QModelIndex parent(const QModelIndex &child) const override;
    QModelIndex sibling(int row, int column, const QModelIndex &idx) const override;
    bool        filterAcceptsRow(int source_row, const QModelIndex &source_parent) const override;
    bool        filterAcceptsColumn(int source_column, const QModelIndex &source_parent) const override;
    bool        setData(const QModelIndex &index, const QVariant &value, int role) override;
    QVariant    data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    Qt::ItemFlags flags(const QModelIndex &index) const override;
    bool        hasChildren(const QModelIndex &parent = QModelIndex()) const override;
    void        setSourceModel(QAbstractItemModel *sourceModel) override;
    
    virtual int         dataRow(ModelType d) const;
    virtual QModelIndex dataIndex(ModelType d, int col) const;
    virtual QModelIndex headerIndex(const QString& header) const;  // map header string to its index
    virtual bool        hasSelection() const;
    virtual QModelIndex getSelection() const;

public slots:
    void currentItemChanged(const QModelIndex &current);

private slots:
    void processDataChanged(const QModelIndex &topLeft, const QModelIndex &bottomRight, const QVector<int> &roles);

protected:
    virtual QString text(Header) const = 0;

    inline const QModelIndex& verify(const QModelIndex& idx) const {
        assert(idx.model() == nullptr || idx.model() == this);
        assert(idx.column() < DetailFilter::_Count);
        assert(containsLine(idx));

        return idx;
    }
    
    [[nodiscard]] const Line* getItem(const QModelIndex &idx) const;
    [[nodiscard]] QModelIndex index(const Line* line, int col) const;

private:
    void setupDataMap(const Line* parent);
    void checkMap(); // verify all entries are present in map
    [[nodiscard]] bool containsLine(const QModelIndex& idx) const; // for sanity checking
    inline QModelIndex createIndex(int row, int col, const Line*) const;

    QPersistentModelIndex m_currentContainer;  // selected row to display
    QVector<const Line*>  m_dataToLineMap;     // map source row to Line
    const Line& m_rootItem;                    // our local index space root item
    const int m_sourceColumnBegin;             // first source column
    const int m_sourceColumnEnd;               // one past last source column
};

#endif // DETAILFILTER_H
