/*
    Copyright 2019-2022 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef SUBTREEFILTER_H
#define SUBTREEFILTER_H

#include <functional>
#include <QSortFilterProxyModel>
#include <QModelIndexList>

#include <src/core/query.h>

class QTreeView;
class QItemSelectionModel;
class CfgData;
class QRegExp;
class QVariant;

// Filter, accepting rows who have sub-items matching filter
class SubTreeFilter : public QSortFilterProxyModel
{
private:
    using ModelIndexPred = std::function<bool(const QModelIndex&)>;

public:
    using QSortFilterProxyModel::QSortFilterProxyModel;
    using PatternList = Query::Base::PatternList;

    SubTreeFilter(QObject *parent = nullptr);
    ~SubTreeFilter() override { }

    // Model index based query
    [[nodiscard]] bool filterAcceptsRow(int row, const QModelIndex& parent) const override;

    // Return true if filter matches given string
    [[nodiscard]] bool filterMatchesData(const QVariant&) const;

    // Return list of regular expressions for the query
    [[nodiscard]] PatternList patterns() const;

    // filter-space sort of selection, so clipboard copy ends up in right order
    [[nodiscard]] QModelIndexList sortedSelection(const QItemSelectionModel* selector,
                                                  const QModelIndex& parent = QModelIndex()) const;

    // First form uses provided row indexes.  Second for uses view selection.
    void copyToClipboard(const QTreeView& view, const QModelIndexList& filterRows,
                         const QString& rowSep, const QString& colSep,
                         int role,
                         const ModelIndexPred& pred = [](const QModelIndex&) { return true; } ) const;

    void setSourceModel(QAbstractItemModel*) override;
    void setSourceModel(QAbstractItemModel*, const QVector<const Units*>&);
    void setCaseSensitivity(Qt::CaseSensitivity cs);

    [[nodiscard]] bool isValid() const { return m_queryRoot->isValid(); }
    [[nodiscard]] bool isEmpty() const { return m_queryRoot->is<Query::All>(); }

    [[nodiscard]] const Query::Context& queryCtx() const { return m_queryCtx; }

public slots:
    void setQueryString(const QString&);

protected:
    using QSortFilterProxyModel::match;
    // Return true if the given row/parent is a match. Subclasses can override to provide other behaviors.
    [[nodiscard]] virtual bool match(int row, const QModelIndex &parent) const;

private:
    [[nodiscard]] bool lessThan(const QModelIndex& left, const QModelIndex& right) const override;
    [[nodiscard]] bool anyChildMatches(const QModelIndex& parent) const;

    void setFilterRegExp(const QString&) = delete;
    void setFilterRegExp(const QRegExp&) = delete;

    Query::Context           m_queryCtx;  // for query language
    Query::query_uniqueptr_t m_queryRoot; // root of node filter tree
};

#endif // SUBTREEFILTER_H
