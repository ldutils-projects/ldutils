/*
    Copyright 2019-2022 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <functional>
#include <cassert>

#include <src/util/roles.h>
#include <src/util/util.h>
#include "flattenfilter.h"

FlattenFilter::FlattenFilter(QObject *parent, const PredFn& pred) :
    QAbstractProxyModel(parent),
    m_dynamic(false),
    predicate(pred)
{
    proxyToSource.reserve(128); // avoid reallocs
}

FlattenFilter::~FlattenFilter()
{
}

QModelIndex FlattenFilter::mapFromSource(const QModelIndex& sourceIndex) const
{
    if (sourceModel() == nullptr || !sourceIndex.isValid())
        return { };

    const QModelIndex sourceRow = sourceModel()->sibling(sourceIndex.row(), 0, sourceIndex);

    const auto it = sourceToProxy.find(sourceRow);
    if (it == sourceToProxy.end())
        return { };

    return index(*it, sourceIndex.column(), QModelIndex());
}

QModelIndex FlattenFilter::mapToSource(const QModelIndex& proxyIndex) const
{
    if (sourceModel() == nullptr || !proxyIndex.isValid())
        return { };

    const QModelIndex& srcIdx = proxyToSource[proxyIndex.row()];

    if (!srcIdx.isValid())
        return { };

    return sourceModel()->index(srcIdx.row(), proxyIndex.column(), srcIdx.parent());
}

void FlattenFilter::clear()
{
    proxyToSource.clear();
    sourceToProxy.clear();
}

// Repopulate from the source model
void FlattenFilter::rebuild()
{
    if (sourceModel() == nullptr)
        return;

    clear();

    Util::Recurse(*sourceModel(), [this](const QModelIndex& idx) {
        if (acceptedRow(idx))
            insert(idx);
        return true;
    });
}

int FlattenFilter::rowCount(const QModelIndex& parent) const
{
    verify(parent);
    if (sourceModel() == nullptr || parent.isValid())
        return 0;

    return proxyToSource.size();
}

int FlattenFilter::columnCount(const QModelIndex& parent) const
{
    if (sourceModel() == nullptr)
        return 0;

    return sourceModel()->columnCount(mapToSource(verify(parent)));
}

QModelIndex FlattenFilter::index(int row, int column, const QModelIndex& /*parent*/) const
{
    // The QSortFilterProxyModel sometimes generates invalid indexes, so we can't use
    // verify() here
    return createIndex(row, column);
}

QModelIndex FlattenFilter::parent(const QModelIndex& child) const
{
    verify(child);
    return { }; // We flatten: all items have root patent
}

QVariant FlattenFilter::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (sourceModel() == nullptr)
        return { };

    return sourceModel()->headerData(section, orientation, role);
}

bool FlattenFilter::hasChildren(const QModelIndex& parent) const
{
    if (sourceModel() == nullptr)
        return false;

    return !verify(parent).isValid();
}

void FlattenFilter::setSourceModel(QAbstractItemModel* newSourceModel)
{
    beginResetModel();

    // We have to translate data change events to our own index space and re-forward
    if (sourceModel() != newSourceModel) {
        if (sourceModel() != nullptr) {
            disconnect(sourceModel(), &QAbstractItemModel::columnsAboutToBeInserted, this, &FlattenFilter::processColumnsAboutToBeInserted);
            disconnect(sourceModel(), &QAbstractItemModel::columnsAboutToBeMoved, this, &FlattenFilter::processColumnsAboutToBeMoved);
            disconnect(sourceModel(), &QAbstractItemModel::columnsAboutToBeRemoved, this, &FlattenFilter::processColumnsAboutToBeRemoved);
            disconnect(sourceModel(), &QAbstractItemModel::columnsInserted, this, &FlattenFilter::processColumnsInserted);
            disconnect(sourceModel(), &QAbstractItemModel::columnsMoved, this, &FlattenFilter::processColumnsMoved);
            disconnect(sourceModel(), &QAbstractItemModel::columnsRemoved, this, &FlattenFilter::processColumnsRemoved);
            disconnect(sourceModel(), &QAbstractItemModel::dataChanged, this, &FlattenFilter::processDataChanged);
            disconnect(sourceModel(), &QAbstractItemModel::rowsAboutToBeInserted, this, &FlattenFilter::processRowsAboutToBeInserted);
            disconnect(sourceModel(), &QAbstractItemModel::rowsAboutToBeMoved, this, &FlattenFilter::processRowsAboutToBeMoved);
            disconnect(sourceModel(), &QAbstractItemModel::rowsAboutToBeRemoved, this, &FlattenFilter::processRowsAboutToBeRemoved);
            disconnect(sourceModel(), &QAbstractItemModel::rowsInserted, this, &FlattenFilter::processRowsInserted);
            disconnect(sourceModel(), &QAbstractItemModel::rowsMoved, this, &FlattenFilter::processRowsMoved);
            disconnect(sourceModel(), &QAbstractItemModel::rowsRemoved, this, &FlattenFilter::processRowsRemoved);
            disconnect(sourceModel(), &QAbstractItemModel::modelAboutToBeReset, this, &FlattenFilter::processModelAboutToBeReset);
            disconnect(sourceModel(), &QAbstractItemModel::modelReset, this, &FlattenFilter::processModelReset);
            disconnect(sourceModel(), &QAbstractItemModel::layoutAboutToBeChanged, this, &FlattenFilter::processLayoutAboutToBeChanged);
            disconnect(sourceModel(), &QAbstractItemModel::layoutChanged, this, &FlattenFilter::processLayoutChanged);

            disconnect(sourceModel(), &QAbstractItemModel::headerDataChanged, this, &FlattenFilter::headerDataChanged);
        }

        QAbstractProxyModel::setSourceModel(newSourceModel);

        if (sourceModel() != nullptr) {
            connect(sourceModel(), &QAbstractItemModel::columnsAboutToBeInserted, this, &FlattenFilter::processColumnsAboutToBeInserted);
            connect(sourceModel(), &QAbstractItemModel::columnsAboutToBeMoved, this, &FlattenFilter::processColumnsAboutToBeMoved);
            connect(sourceModel(), &QAbstractItemModel::columnsAboutToBeRemoved, this, &FlattenFilter::processColumnsAboutToBeRemoved);
            connect(sourceModel(), &QAbstractItemModel::columnsInserted, this, &FlattenFilter::processColumnsInserted);
            connect(sourceModel(), &QAbstractItemModel::columnsMoved, this, &FlattenFilter::processColumnsMoved);
            connect(sourceModel(), &QAbstractItemModel::columnsRemoved, this, &FlattenFilter::processColumnsRemoved);
            connect(sourceModel(), &QAbstractItemModel::dataChanged, this, &FlattenFilter::processDataChanged);
            connect(sourceModel(), &QAbstractItemModel::rowsAboutToBeInserted, this, &FlattenFilter::processRowsAboutToBeInserted);
            connect(sourceModel(), &QAbstractItemModel::rowsAboutToBeMoved, this, &FlattenFilter::processRowsAboutToBeMoved);
            connect(sourceModel(), &QAbstractItemModel::rowsAboutToBeRemoved, this, &FlattenFilter::processRowsAboutToBeRemoved);
            connect(sourceModel(), &QAbstractItemModel::rowsInserted, this, &FlattenFilter::processRowsInserted);
            connect(sourceModel(), &QAbstractItemModel::rowsMoved, this, &FlattenFilter::processRowsMoved);
            connect(sourceModel(), &QAbstractItemModel::rowsRemoved, this, &FlattenFilter::processRowsRemoved);
            connect(sourceModel(), &QAbstractItemModel::modelAboutToBeReset, this, &FlattenFilter::processModelAboutToBeReset);
            connect(sourceModel(), &QAbstractItemModel::modelReset, this, &FlattenFilter::processModelReset);
            connect(sourceModel(), &QAbstractItemModel::layoutAboutToBeChanged, this, &FlattenFilter::processLayoutAboutToBeChanged);
            connect(sourceModel(), &QAbstractItemModel::layoutChanged, this, &FlattenFilter::processLayoutChanged);

            connect(sourceModel(), &QAbstractItemModel::headerDataChanged, this, &FlattenFilter::headerDataChanged);
        }
    }

    rebuild();
    endResetModel();
}

void FlattenFilter::setPredicate(const PredFn& pred)
{
    predicate = pred;
    invalidateFilter();
}

bool FlattenFilter::setDynamic(bool dynamic)
{
    const bool prevDynamic = m_dynamic;
    m_dynamic = dynamic;
    return prevDynamic;
}

void FlattenFilter::invalidateFilter()
{
    beginResetModel();
    rebuild();
    endResetModel();
}

void FlattenFilter::processDataChanged(const QModelIndex &topLeft, const QModelIndex& bottomRight, const QVector<int>& roles)
{
    for (int r = topLeft.row(); r <= bottomRight.row(); ++r) {
        for (int c = topLeft.column(); c <= bottomRight.column(); ++c) {
            const QModelIndex sourceIndex = sourceModel()->index(r, c, topLeft.parent());
            QModelIndex proxyIndex  = mapFromSource(sourceIndex);

            if (predicate != nullptr && (m_dynamic || roles.contains(Util::PrivateDataRole))) {
                if (proxyIndex.isValid()) {
                    // If the proxy no longer likes it, remove it.
                    if (!acceptedRow(sourceIndex)) {
                        processRowsAboutToBeRemoved(QModelIndex(), proxyIndex.row(), proxyIndex.row());
                        processRowsRemoved(QModelIndex(), proxyIndex.row(), proxyIndex.row());
                        proxyIndex = QModelIndex(); // don't change the data
                    }
                } else {
                    // Attempt to add it: maybe the proxy didn't like it before, but now it does.
                    if (acceptedRow(sourceIndex)) {
                        const int newRow = proxyToSource.size();
                        beginInsertRows(QModelIndex(), newRow, newRow);
                        proxyIndex = insert(sourceIndex);
                        endInsertRows();
                    }
                }
            }

            if (proxyIndex.isValid())
                emit dataChanged(proxyIndex, proxyIndex, roles);
        }
    }
}

void FlattenFilter::processRowsAboutToBeInserted(const QModelIndex&, int /*first*/, int /*last*/)
{
}

void FlattenFilter::processRowsInserted(const QModelIndex& parent, int first, int last)
{
    int passing = 0;

    // count how many pass our predicate
    for (int r = first; r <= last; ++r)
        if (acceptedRow(sourceModel()->index(r, 0, parent)))
            ++passing;

    if (passing <= 0)
        return;

    if (useProcessRowsInserted) {

        beginInsertRows(QModelIndex(), proxyToSource.size(), proxyToSource.size() + passing - 1);

        // We always add new entries to the end of our list, which is unsorted.
        for (int r = first; r <= last; ++r) {
            const QModelIndex sourceIndex = sourceModel()->index(r, 0, parent);

            if (acceptedRow(sourceIndex))
                insert(sourceIndex);
        }

        endInsertRows();
    } else {
        beginResetModel();
        rebuild();
        endResetModel();
    }
}

void FlattenFilter::processRowsAboutToBeRemoved(const QModelIndex& parent, int first, int last)
{
    // It's very difficult to react properly to this, so for now we simply rebuild the entire
    // model in processRowsRemoved.
    if (useProcessRowsRemoved) {
        QVector<int> proxyRowsToRemove;

        // This is a little bit tricky.  We can't start removing things straight off because that'll
        // perturb the rest of the mappings, so we collect the set to remove, then do them one at a
        // time.
        for (int r = first; r <= last; ++r) {
            const QModelIndex sourceIndex = sourceModel()->index(r, 0, parent);
            proxyRowsToRemove.push_back(sourceToProxy[sourceIndex]);
        }

        // Process in order in our index space
        std::sort(proxyRowsToRemove.begin(), proxyRowsToRemove.end());

        // TODO: remove in batches for efficiency.
        while (!proxyRowsToRemove.empty()) {
            const int row = proxyRowsToRemove.takeLast();
            beginRemoveRows(QModelIndex(), row, row);
            sourceToProxy.remove(proxyToSource[row]);
            proxyToSource.remove(row);
            endRemoveRows();
        }

        // We must fix the sourceToProxy indexes, since the removal changed them around.
        for (int p2s = 0; p2s < proxyToSource.size(); ++p2s)
            sourceToProxy[proxyToSource[p2s]] = p2s;
    }
}

void FlattenFilter::processRowsRemoved(const QModelIndex& /*parent*/, int /*first*/, int /*last*/)
{
    if (!useProcessRowsRemoved) {
        beginResetModel();
        rebuild();
        endResetModel();
    }
}

void FlattenFilter::processRowsAboutToBeMoved(const QModelIndex& /*sourceParent*/, int /*sourceStart*/, int /*sourceEnd*/,
                                              const QModelIndex& /*destinationParent*/, int /*destinationColumn*/)
{
    (void)this; // Preserve this as a non-static method. This avoids clang-tidy complaints.
    assert(0 && "TODO...");
}

void FlattenFilter::processRowsMoved(const QModelIndex&, int, int, const QModelIndex&, int)
{
    (void)this; // Preserve this as a non-static method. This avoids clang-tidy complaints.
    assert(0 && "TODO...");
}

void FlattenFilter::processColumnsAboutToBeInserted(const QModelIndex& /*parent*/, int first, int last)
{
    beginInsertColumns(QModelIndex(), first, last);
}

void FlattenFilter::processColumnsInserted(const QModelIndex& /*parent*/, int /*first*/, int /*last*/)
{
    endInsertColumns();
}

void FlattenFilter::processColumnsAboutToBeRemoved(const QModelIndex& /*parent*/, int first, int last)
{
    beginRemoveColumns(QModelIndex(), first, last);
}

void FlattenFilter::processColumnsRemoved(const QModelIndex& /*parent*/, int /*first*/, int /*last*/)
{
    endRemoveColumns();
}

void FlattenFilter::processColumnsAboutToBeMoved(const QModelIndex& /*sourceParent*/, int sourceStart, int sourceEnd,
                                                 const QModelIndex& /*destinationParent*/, int destinationColumn)
{
    beginMoveColumns(QModelIndex(), sourceStart, sourceEnd, QModelIndex(), destinationColumn);
}

void FlattenFilter::processColumnsMoved(const QModelIndex& /*parent*/, int /*start*/, int /*end*/,
                                        const QModelIndex& /*destination*/, int /*column*/)
{
    endMoveColumns();
}

void FlattenFilter::processLayoutAboutToBeChanged(const QList<QPersistentModelIndex>& parents, QAbstractItemModel::LayoutChangeHint hint)
{
    emit layoutAboutToBeChanged(parents, hint);
}

void FlattenFilter::processLayoutChanged(const QList<QPersistentModelIndex>& parents, QAbstractItemModel::LayoutChangeHint hint)
{
    emit layoutChanged(parents, hint);
}

void FlattenFilter::processModelAboutToBeReset()
{
    beginResetModel();
}

void FlattenFilter::processModelReset()
{
    rebuild();
    endResetModel();
}

QModelIndex FlattenFilter::insert(const QModelIndex &sourceIndex)
{
    if (!sourceIndex.isValid())
        return { };

    const int newRow = proxyToSource.size();
    sourceToProxy.insert(sourceIndex, newRow);
    proxyToSource.push_back(sourceIndex);

    return createIndex(newRow, 0);
}

bool FlattenFilter::acceptedRow(const QModelIndex &sourceIndex) const
{
    if (sourceModel() == nullptr)
        return false;

    // A null predicate accepts everything.
    return predicate == nullptr || predicate(sourceModel(), sourceIndex);
}
