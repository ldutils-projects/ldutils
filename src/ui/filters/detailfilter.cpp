/*
    Copyright 2019-2022 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <functional>
#include <QItemSelection>

#include <src/util/util.h>

#include <src/ui/filters/detailfilter.h>

DetailFilter::DetailFilter(const Line& rootItem, int sourceColumnBegin, int sourceColumnEnd, QObject *parent) :
    QSortFilterProxyModel(parent),
    m_dataToLineMap(sourceColumnEnd, nullptr),
    m_rootItem(rootItem),
    m_sourceColumnBegin(sourceColumnBegin),
    m_sourceColumnEnd(sourceColumnEnd)
{
    setupDataMap(&rootItem);
    checkMap();  // sanity testing
}

int DetailFilter::rowCount(const QModelIndex& parent) const
{
    if (const Line* line = getItem(verify(parent)))
        return line->children.size();

    return 0;
}

int DetailFilter::columnCount(const QModelIndex& parent) const
{
    verify(parent);

    return DetailFilter::_Count;   // fixed columns
}

inline QModelIndex DetailFilter::createIndex(int row, int col, const Line* line) const
{
    void* voidLine = const_cast<void*>(reinterpret_cast<const void*>(line));

    return QSortFilterProxyModel::createIndex(row, col, voidLine);
}

QModelIndex DetailFilter::index(int row, int column, const QModelIndex &parent) const
{
    const Line* parentItem = getItem(parent);
    const Line* childItem  = parentItem != nullptr ? parentItem->child(row) : nullptr;

    if (childItem == nullptr)
        return { };

    return verify(createIndex(row, column, childItem));
}

QModelIndex DetailFilter::index(const Line* line, int col) const
{
    if (line == nullptr || line == &m_rootItem)
        return { };

    return verify(createIndex(line->parent->children.indexOf(line), col, line));
}

QModelIndex DetailFilter::parent(const QModelIndex& child) const
{
    if (!child.isValid())
        return { };

    const Line* childLine  = getItem(child);

    if (childLine == nullptr || childLine->parent == nullptr || childLine->parent == &m_rootItem)
        return { };

    return verify(index(childLine->parent, 0));
}

QModelIndex DetailFilter::sibling(int row, int column, const QModelIndex &idx) const
{
     return verify(index(row, column, parent(verify(idx))));
}

bool DetailFilter::filterAcceptsRow(int /*source_row*/, const QModelIndex& /*source_parent*/) const
{
    return false;
}

// NOTE!! This is rather wrong, but necessary due to Qt issues.  The QSortFilterProxyModel, who has
// us as a source model, doesn't ask us for indexes in all cases.  Instead it makes them up, but clamps
// them to "accepted" rows and columns.  Trouble is, we are happy to accept any column from OUR source
// model, but that makes the QSortFilterProxyModel give us invalid indexes.  This is a kludge workaround,
// until that is addressed in Qt.
bool DetailFilter::filterAcceptsColumn(int source_column, const QModelIndex& /*source_parent*/) const
{
    return source_column < columnCount();
}

bool DetailFilter::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (sourceModel() == nullptr || index.column() != DetailFilter::Data)
        return false;

    return QSortFilterProxyModel::setData(index, value, role);
}

QVariant DetailFilter::data(const QModelIndex &index, int role) const
{
    const Line* line = getItem(index);

    if (sourceModel() == nullptr || line == nullptr || index.column() >= DetailFilter::_Count)
        return { };

    if (index.column() == DetailFilter::Data && !line->isHeader()) {
        // Override alignment: everything on the left in this view.
        if (role == Qt::TextAlignmentRole)
            return { Qt::AlignLeft | Qt::AlignVCenter };

        return QSortFilterProxyModel::data(index, role);
    }

    if (index.column() == DetailFilter::Key) {
        if ((role == Qt::WhatsThisRole || role == Qt::ToolTipRole) && !line->isHeader())
            if (const Line* line = getItem(index); line != nullptr)
                return sourceModel()->headerData(line->data, Qt::Horizontal, role);

        if (role == Qt::DisplayRole || role >= Qt::UserRole)
            return text(line->header);
    }

    return { };
}

Qt::ItemFlags DetailFilter::flags(const QModelIndex& index) const
{
    if (sourceModel() == nullptr)
        return Qt::NoItemFlags;

    if (index.column() == DetailFilter::Key) // keys are immutable.
        return Qt::ItemIsEnabled | Qt::ItemIsSelectable;

    return sourceModel()->flags(mapToSource(index));
}

bool DetailFilter::hasChildren(const QModelIndex &parent) const
{
    const Line* parentLine = getItem(verify(parent));

    if (parentLine == nullptr)
        return false;

    return !parentLine->children.empty();
}

void DetailFilter::setSourceModel(QAbstractItemModel *newSourceModel)
{
    beginResetModel();

    // We have to translate data change events to our own index space and re-forward
    if (sourceModel() != newSourceModel) {
        if (sourceModel() != nullptr)
            disconnect(sourceModel(), &QAbstractItemModel::dataChanged, this, &DetailFilter::processDataChanged);

        connect(newSourceModel, &QAbstractItemModel::dataChanged, this, &DetailFilter::processDataChanged);
    }

    endResetModel();
    QSortFilterProxyModel::setSourceModel(newSourceModel);
}

// Return the row which displays the given ContainerData, relative to parent
int DetailFilter::dataRow(ModelType d) const
{
    const Line* line = m_dataToLineMap[d];

    return line->parent->children.indexOf(line);
}

// Return the model index corresponding to this model data.
QModelIndex DetailFilter::dataIndex(ModelType d, int col) const
{
    return index(m_dataToLineMap[d], col);
}

QModelIndex DetailFilter::headerIndex(const QString& header) const
{
    // This is slow, but it only happens once on load, so performance isn't important.
    const std::function<const Line*(const Line*)> findHeader = [&](const Line* parent) -> const Line* {
        if (text(parent->header) == header)
            return parent;

        for (const auto& child : parent->children)
            if (const Line* headerLine = findHeader(child); headerLine != nullptr)
                return headerLine;

        return nullptr;
    };

    if (const Line* headerLine = findHeader(&m_rootItem); header != nullptr)
        return index(headerLine, DetailFilter::Key);

    return { }; // not found
}

bool DetailFilter::hasSelection() const
{
    return m_currentContainer.isValid();
}

QModelIndex DetailFilter::getSelection() const
{
    return mapFromSource(m_currentContainer);
}

QModelIndex DetailFilter::mapFromSource(const QModelIndex& sourceIndex) const
{
    assert(sourceIndex.model() == nullptr || sourceIndex.model() == sourceModel());

    if (!m_currentContainer.isValid() || !sourceIndex.isValid())
        return { };

    // If not our display row, it doesn't exist in our view.
    if (sourceIndex.row() != m_currentContainer.row())
        return { };

    const Line* line = m_dataToLineMap[sourceIndex.column()];

    return verify(index(line, DetailFilter::Data));
}

QModelIndex DetailFilter::mapToSource(const QModelIndex& proxyIndex) const
{
    if (!m_currentContainer.isValid() || !proxyIndex.isValid() || proxyIndex.column() != DetailFilter::Data)
        return { };

    assert(sourceModel() == m_currentContainer.model()); // must point at the source model.

    const Line* line = getItem(proxyIndex);
    if (line == nullptr || line->isHeader())    // headers have no analog in source model
        return { };

    return sourceModel()->index(m_currentContainer.row(), int(line->data), m_currentContainer.parent());
}

void DetailFilter::currentItemChanged(const QModelIndex &current)
{
    if (sourceModel() == nullptr)
        return;

    // grab persistant index
    m_currentContainer = sourceModel()->sibling(current.row(), DetailFilter::Data, current);

    // Emit a slew of data change events for all the rows
    const std::function<void(const Line*)> recurse = [&](const Line* line) {
        if (const int rowCount = line->children.size()) {
            // signal for all child data columns, at once.
            emit dataChanged(index(line->child(0), DetailFilter::Data),
                             index(line->child(rowCount - 1), DetailFilter::Data));

            for (const auto& child : line->children)
                recurse(child);
        }
    };

    recurse(&m_rootItem);
}

void DetailFilter::processDataChanged(const QModelIndex &topLeft, const QModelIndex &bottomRight, const QVector<int> &roles)
{
    if (!m_currentContainer.isValid())
        return;

    // Re-emit the signal in our index space.
    if (topLeft.parent() == m_currentContainer.parent() &&
        topLeft.row() <= m_currentContainer.row() &&
        bottomRight.row() >= m_currentContainer.row() &&
        bottomRight.column() < m_sourceColumnEnd) {

        // The contiguous source model data may not be contiguous in our view.  We may have
        // to emit multiple change events.

        // Source left and right (might be different from our left and right)
        const Line* sl = m_dataToLineMap[topLeft.column()];
        const Line* sr = m_dataToLineMap[bottomRight.column()];

        if (sl->parent == sr->parent) {
            // Same parent: emit one event (but sort)
            const Line* l = (sl->data < sr->data) ? sl : sr;
            const Line* r = (sl->data < sr->data) ? sr : sl;

            if (l != nullptr && r != nullptr)
                emit dataChanged(verify(index(l, DetailFilter::Data)),
                                 verify(index(r, DetailFilter::Data)), roles);
        } else {
            for (int d = topLeft.column(); d <= bottomRight.column(); ++d) {
                if (const Line* line = m_dataToLineMap[d]) {
                    const QModelIndex idx = verify(index(line, DetailFilter::Data));

                    emit dataChanged(idx, idx, roles);
                }
            }
        }
    }
}

const DetailFilter::Line* DetailFilter::getItem(const QModelIndex &idx) const
{
    verify(idx);

    if (!idx.isValid() || idx.column() >= DetailFilter::_Count)
        return &m_rootItem;

    if (const Line *item = static_cast<const Line*>(idx.internalPointer()))
        return item;

    return &m_rootItem;
}

// Check that we are accounting for all ContainerData
// TODO: also check Line::text uniqueness
void DetailFilter::checkMap()
{
    for (ModelType d = m_sourceColumnBegin; d < m_sourceColumnEnd; d++)
        assert(m_dataToLineMap[int(d)] != nullptr && m_dataToLineMap[int(d)]->data == d);
}

// Create mapping from source model's column to a Line*
void DetailFilter::setupDataMap(const Line* parent)
{
    if (parent != &m_rootItem)
        if (!parent->isHeader())
            m_dataToLineMap[int(parent->data)] = parent;

    // verify parent pointers
    for (const auto& child : parent->children) {
        setupDataMap(child);
        assert(child->parent == parent);
    }
}

bool DetailFilter::containsLine(const QModelIndex &idx) const
{
    const Line* idxLine = static_cast<const Line*>(idx.internalPointer());

    if (idxLine == nullptr)
        return true;

    // verify the line is in the tree somewhere.
    const std::function<bool(const Line*)> recurse = [&](const Line* line) {
        if (line == idxLine)
            return true;

        for (const auto& child : line->children)
            if (recurse(child))
                return true;

        return false;
    };

    return recurse(&m_rootItem);
}
