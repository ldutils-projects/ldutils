/*
    Copyright 2019-2022 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <QTreeView>
#include <QHeaderView>
#include <QGuiApplication>
#include <QClipboard>
#include <QVariant>

#include <src/util/util.h>
#include <src/util/roles.h>
#include <src/util/variantcmp.h>
#include "src/core/treemodel.h"

#include "subtreefilter.h"

// Override: match if any child matches, and allow multi-column matches
SubTreeFilter::SubTreeFilter(QObject *parent) :
    QSortFilterProxyModel(parent),
    m_queryRoot(new Query::All())
{
    setFilterKeyColumn(-1);
}

bool SubTreeFilter::filterAcceptsRow(int row, const QModelIndex &parent) const
{
    if (sourceModel() == nullptr || m_queryRoot == nullptr)
        return false;

    return match(row, parent) || anyChildMatches(sourceModel()->index(row, 0, parent));
}

bool SubTreeFilter::match(int row, const QModelIndex &parent) const
{
    const int filterColumn = filterKeyColumn() >= 0 ? filterKeyColumn() : Query::Base::NoColumn;

    return queryCtx().match(m_queryRoot, parent, row, filterColumn);
}

bool SubTreeFilter::filterMatchesData(const QVariant& variant) const
{
    return queryCtx().match(m_queryRoot, variant);
}

SubTreeFilter::PatternList SubTreeFilter::patterns() const
{
    return m_queryRoot->patterns();
}

void SubTreeFilter::setQueryString(const QString& query)
{
    m_queryRoot = m_queryCtx.parse(query);

    invalidateFilter();
}

// This has a quite inefficient time complexity.  It doesn't matter much for now, but
// collections ever became large, this should be improved.
bool SubTreeFilter::anyChildMatches(const QModelIndex &parent) const
{
    const int numChildren = sourceModel()->rowCount(parent);

    for (int child = 0; child < numChildren; ++child)
        if (filterAcceptsRow(child, parent))
            return true;

    return false;
}

// Custom iteration to preserve filter order (selectedRows on the selectionmodel is
// unordered).  The indexes are in filter space.
QModelIndexList SubTreeFilter::sortedSelection(const QItemSelectionModel* selector,
                                               const QModelIndex &parent) const
{
    QModelIndexList selection;

    if (selector == nullptr)
        return selection;

    Util::Recurse(*this, [&](const QModelIndex& idx) {
        if (selector->isSelected(idx))
            selection.push_back(idx);
        return true;
    }, parent);

    return selection;
}

void SubTreeFilter::copyToClipboard(const QTreeView& view, const QModelIndexList& filterRows,
                                    const QString& rowSep, const QString& colSep,
                                    int role, const ModelIndexPred& pred) const
{
    QString textForm;
    textForm.reserve(4096);

    // Inefficient, but handy, and this isn't performance sensitive.
    const auto cook = [](const QString& s) {
        QString cooked = s;
        return cooked.replace("\\t", "\t").replace("\\n", "\n");
    };

    const QString cookedRowSep = cook(rowSep);
    const QString cookedColSep = cook(colSep);

    const int cols = sourceModel()->columnCount();

    for (const auto& filterIdx : filterRows) {
        if (!pred(Util::MapDown(filterIdx)))  // skip if doesn't pass predicate
            continue;

        for (int col = 0; col < cols; ++col) {
            const int logicalCol = view.header()->logicalIndex(col);
            if (view.isColumnHidden(logicalCol))
                continue;

            if (col != 0)
                textForm += cookedColSep;

            const QModelIndex entry = sibling(filterIdx.row(), logicalCol, filterIdx);

            textForm += data(entry, role).toString();
        }

        textForm += cookedRowSep;
    }

    QGuiApplication::clipboard()->setText(textForm);
}

void SubTreeFilter::setSourceModel(QAbstractItemModel* sourceModel)
{   
    // Set the Query model before the proxy filter model, in case it's needed during setup.
    m_queryCtx.setModel(Util::MapDown(sourceModel), filterCaseSensitivity());
    QSortFilterProxyModel::setSourceModel(sourceModel);
}

void SubTreeFilter::setSourceModel(QAbstractItemModel* sourceModel, const QVector<const Units*>& columnUnits)
{
    // Set the Query model before the proxy filter model, in case it's needed during setup.
    m_queryCtx.setModel(Util::MapDown(sourceModel), columnUnits, filterCaseSensitivity());
    QSortFilterProxyModel::setSourceModel(sourceModel);
}

void SubTreeFilter::setCaseSensitivity(Qt::CaseSensitivity cs)
{
    setFilterCaseSensitivity(cs);
    m_queryCtx.setCaseSensitivity(cs);
}

// We override to sort on the raw data role, if exists, else the display data.
bool SubTreeFilter::lessThan(const QModelIndex &left, const QModelIndex &right) const
{
    const QVariant& leftDataRaw  = sourceModel()->data(left, Util::RawDataRole);
    const QVariant& rightDataRaw = sourceModel()->data(right, Util::RawDataRole);
    
    if (leftDataRaw.isValid() && rightDataRaw.isValid())
        return QtCompat::lt(leftDataRaw, rightDataRaw);

    if (leftDataRaw.isValid() && !rightDataRaw.isValid())
        return sortOrder() == Qt::AscendingOrder;

    if (!leftDataRaw.isValid() && rightDataRaw.isValid())
        return sortOrder() == Qt::DescendingOrder;

    // If no raw data, fall back on comparing display data
    return QSortFilterProxyModel::lessThan(left, right);
}

