/*
    Copyright 2021 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <src/ui/dialogs/modeltexteditdialog.h>

#include "modeltexteditordelegate.h"

ModelTextEditorDelegate::ModelTextEditorDelegate(const TreeModel& model, QWidget* parent, bool defaultShowVars,
                                                 const QString& winTitle, bool winBorders, int role) :
    TextEditorDelegate(parent, winTitle, winBorders, role),
    m_model(model),
    m_defaultShowVars(defaultShowVars)
{
}

ModelTextEditorDelegate::~ModelTextEditorDelegate()
{
}

TextEditorDialogBase* ModelTextEditorDelegate::dialogFactory(QWidget* parent) const
{
   return new ModelTextEditDialog(m_model, m_defaultShowVars, parent);
}

