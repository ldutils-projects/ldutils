/*
    Copyright 2019-2022 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TEXTEDITOR_H
#define TEXTEDITOR_H

#include <tuple>
#include <QWidget>
#include <QMenu>
#include <QTextListFormat>
#include <QTimer>

#include <src/ui/dialogs/linkdialog.h>
#include <src/ui/dialogs/imagelinkdialog.h>
#include <src/core/settings.h>

class QTextCharFormat;
class QTextCursor;
class QShowEvent;
class QDialogButtonBox;
class QTextEdit;

namespace Ui {
class TextEditor;
} // namespace Ui

// Credit: Some of this code borrows heavily from the Qt "textedit" example shipped with qtcreator.
class TextEditor :
        public QWidget,
        public Settings
{
    Q_OBJECT

public:
    explicit TextEditor(QWidget *parent = nullptr);
    ~TextEditor() override;

    void setHtml(const QString& s);                // set HTML string and remember it for later reset
    [[nodiscard]] QString toHtml() const;          // obtain HTML
    [[nodiscard]] QTextEdit* editor();             // pointer to QTextEdit, or nullptr if none
    [[nodiscard]] const QTextEdit* editor() const; // ..

    void setupButtonBox(QDialogButtonBox*) const;

    // *** begin Settings API
    void save(QSettings&) const override { }
    void load(QSettings&) override { }
    // *** end Settings API

public slots:
    void resetText();

private slots:
    void showContextMenu(const QPoint&);
    void currentCharFormatChanged(const QTextCharFormat &format);
    void cursorPositionChanged();
    void selectionChanged();
    void clipboardDataChanged();
    void textFamily(int index);
    void textSize(int index);
    void focusEditor();

    void on_action_Bold_triggered();
    void on_action_Italic_triggered();
    void on_action_Underline_triggered();
    void on_action_Text_Color_triggered();
    void on_action_Align_Left_triggered();
    void on_action_Align_Right_triggered();
    void on_action_Align_Center_triggered();
    void on_action_Align_Fill_triggered();
    void on_action_Merge_Cells_triggered();
    void on_action_Split_Cells_triggered();
    void on_action_Add_Row_triggered();
    void on_action_Add_Column_triggered();
    void on_action_Delete_Row_triggered();
    void on_action_Delete_Column_triggered();
    void on_action_Size_Columns_triggered();
    void on_action_Cut_triggered();
    void on_action_Copy_triggered();
    void on_action_Copy_Html_triggered();
    void on_action_Paste_triggered();
    void on_action_Paste_Html_triggered();
    void on_action_Undo_triggered();
    void on_action_Redo_triggered();
    void on_action_Insert_Link_triggered();
    void on_action_Insert_Image_triggered();
    void on_action_Insert_Table_triggered();
    void on_action_Format_Ordered_List_triggered(bool checked);
    void on_action_Format_Unordered_List_triggered(bool checked);

private:
    void showEvent(QShowEvent*) override;

    QTextCursor cursor() const;
    std::tuple<int,int,int,int> selectedCells() const;

    void setupActionIcons();
    void setupActions();
    void setupTimers();
    void setupSignals();
    void setupFontSizes();
    void setupUi();
    void setupMenus();

    void textColor();
    void enableActions();
    void mergeFormatOnSelection(const QTextCharFormat &format);
    void alignmentChanged();
    void colorChanged();
    void fontChanged();
    void styleChanged();
    void setListFormat(bool apply, QTextListFormat::Style);

    Ui::TextEditor *ui;
    QString         m_original;
    LinkDialog      m_linkDialog;
    ImageLinkDialog m_imageLinkDialog;
    QMenu           m_editorMenu;
    QTimer          m_focusTimer;
};

#endif // TEXTEDITOR_H
