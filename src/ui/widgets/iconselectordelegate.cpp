/*
    Copyright 2019-2022 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <QAbstractItemModel>

#include <src/ui/dialogs/iconselector.h>
#include <src/util/roles.h>
#include <src/util/util.h>
#include <src/core/treemodel.h>

#include "iconselectordelegate.h"

// Avoid parenting this to two things
IconSelectorDelegate::IconSelectorDelegate(const QStringList& roots, const QString& winTitle,
                                           const IconSelector::namefilter_t& nameFilter,
                                           bool winBorders, int role,
                                           QObject* /*parent*/) :
    DelegateBase(nullptr, true, winTitle, winBorders, role),
    m_roots(roots),
    m_nameFilter(nameFilter)
{
}

QWidget* IconSelectorDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem& /*option*/, const QModelIndex& index) const
{
    IconSelector* editor = IconSelector::iconSelector(m_roots, m_nameFilter, parent);
    if (editor == nullptr)
        return nullptr;

    if (const auto* model = index.model(); model != nullptr) {
        editor->setCurrentPath(model->data(index, Util::IconNameRole).value<QString>());
        setPopup(editor);
    }

    return editor;
}

void IconSelectorDelegate::destroyEditor(QWidget*, const QModelIndex&) const
{
    // We don't delete the icon editor: it's owned statically bt the IconSelector static registry.
}

void IconSelectorDelegate::setModelData(QWidget* editor, QAbstractItemModel* model, const QModelIndex& idx) const
{
    updateResultFromEditor(editor, idx, [&](const QModelIndex& idx) {
        if (auto* dialog = dynamic_cast<IconSelector*>(editor); dialog != nullptr)
            if (dialog->result() == QDialog::Accepted)
                if (auto* tm = qobject_cast<TreeModel*>(Util::MapDown(model)); tm != nullptr)
                    tm->setIcon(Util::MapDown(idx), dialog->iconFile());
    });
}
