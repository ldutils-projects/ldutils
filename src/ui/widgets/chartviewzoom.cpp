/*
    Copyright 2018-2021 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <QMouseEvent>

#include <src/core/appbase.h>

#include "chartviewzoom.h"

ChartViewZoom::ChartViewZoom(QWidget* parent) :
    QtCharts::QChartView(parent),
    m_pressed(false), m_panned(false)
{ }

void ChartViewZoom::mousePressEvent(QMouseEvent *event)
{
    if (chart() != nullptr && chart()->plotArea().contains(event->pos())) {
        m_pressed = true;
        m_panned = false;

        m_prev = m_orig = event->pos();
    }

    emit mousePress(event);
    QtCharts::QChartView::mousePressEvent(event);
}

void ChartViewZoom::mouseReleaseEvent(QMouseEvent* event)
{
    if (m_pressed) {
        if (m_panned)
            emit mouseEndPan();
        else
            emit mouseRelease(event);
        m_pressed = false;
    }

    QtCharts::QChartView::mouseReleaseEvent(event);
}

void ChartViewZoom::mouseMoveEvent(QMouseEvent* event)
{
    if (m_pressed) {
        // Don't start the pan unless the drag has exceed a threshold, to revent clicks being interpreted as dragging
        if (!m_panned && (event->pos() - m_orig).manhattanLength() >= appBase().startDragDistance())
            m_panned = true;

        if (m_panned) {
            emit mousePan(event, event->pos() - m_prev);
            m_prev = event->pos();
        }

        return;
    }

    // Non-drag event in absolute chart coordinates
    emit mouseMove(event);

    QtCharts::QChartView::mouseMoveEvent(event);
}

void ChartViewZoom::contextMenuEvent(QContextMenuEvent* event)
{
    emit customContextMenuRequested(event->pos());
}
