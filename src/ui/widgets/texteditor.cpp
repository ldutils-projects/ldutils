/*
    Copyright 2019-2022 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <cassert>
#include <chrono>

#include <QTextCharFormat>
#include <QClipboard>
#include <QMimeData>
#include <QColorDialog>
#include <QPushButton>
#include <QMessageBox>
#include <QTextCursor>
#include <QTextList>
#include <QTextTable>
#include <QTextTableFormat>
#include <QTextTableCell>
#include <QTextLength>
#include <QDialogButtonBox>

#include <src/ui/dialogs/texteditortableformat.h>
#include <src/ui/dialogs/texteditorcolumnsize.h>
#include <src/util/icons.h>
#include <src/util/ui.h>
#include <src/util/maybe_unused.h>

#include "texteditor.h"
#include "ui_texteditor.h"

// Credit: Some of this code borrows heavily from the Qt "textedit" example shipped with qtcreator.
TextEditor::TextEditor(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::TextEditor),
    m_editorMenu(this)
{
    ui->setupUi(this);

    setupActionIcons();
    setupActions();
    setupFontSizes();
    setupTimers();
    setupSignals();
    setupUi();
    setupMenus();
    Util::SetupWhatsThis(this);
}

TextEditor::~TextEditor()
{
    delete ui;
}

void TextEditor::setupActionIcons()
{
    Icons::defaultIcon(ui->action_Bold,                  "format-text-bold");
    Icons::defaultIcon(ui->action_Italic,                "format-text-italic");
    Icons::defaultIcon(ui->action_Underline,             "format-text-underline");
    Icons::defaultIcon(ui->action_Align_Left,            "format-justify-left");
    Icons::defaultIcon(ui->action_Align_Right,           "format-justify-right");
    Icons::defaultIcon(ui->action_Align_Center,          "format-justify-center");
    Icons::defaultIcon(ui->action_Cut,                   "edit-cut");
    Icons::defaultIcon(ui->action_Copy,                  "edit-copy");
    Icons::defaultIcon(ui->action_Copy_Html,             "viewhtml");
    Icons::defaultIcon(ui->action_Paste_Html,            "xml-element-new");
    Icons::defaultIcon(ui->action_Paste,                 "edit-paste");
    Icons::defaultIcon(ui->action_Undo,                  "edit-undo");
    Icons::defaultIcon(ui->action_Redo,                  "edit-redo");
    Icons::defaultIcon(ui->action_Align_Fill,            "format-justify-fill");
    Icons::defaultIcon(ui->action_Text_Color,            "format-text-color");
    Icons::defaultIcon(ui->action_Insert_Link,           "insert-link");
    Icons::defaultIcon(ui->action_Insert_Image,          "insert-image");
    Icons::defaultIcon(ui->action_Insert_Table,          "insert-table");
    Icons::defaultIcon(ui->action_Format_Ordered_List,   "format-list-ordered");
    Icons::defaultIcon(ui->action_Format_Unordered_List, "format-list-unordered");
    Icons::defaultIcon(ui->action_Merge_Cells,           "gnumeric-cells-merge");
    Icons::defaultIcon(ui->action_Split_Cells,           "gnumeric-cells-split");
    Icons::defaultIcon(ui->action_Add_Row,               "gnumeric-row-add");
    Icons::defaultIcon(ui->action_Add_Column,            "gnumeric-column-add");
    Icons::defaultIcon(ui->action_Delete_Row,            "gnumeric-row-delete");
    Icons::defaultIcon(ui->action_Delete_Column,         "gnumeric-column-delete");
    Icons::defaultIcon(ui->action_Size_Columns,          "gnumeric-column-size");
}

void TextEditor::setupSignals()
{
    if (ui == nullptr)
        return;

    // Selection
    connect(ui->textEdit, &QTextEdit::selectionChanged, this, &TextEditor::selectionChanged);

    // Cursor position
    connect(ui->textEdit, &QTextEdit::cursorPositionChanged, this, &TextEditor::cursorPositionChanged);

    // Character format
    connect(ui->textEdit, &QTextEdit::currentCharFormatChanged, this, &TextEditor::currentCharFormatChanged);

    // Font family
    connect(ui->fontFamily, static_cast<void (QComboBox::*)(int)> (&QComboBox::activated), this, &TextEditor::textFamily);

    // Font size
    connect(ui->fontSize, static_cast<void (QComboBox::*)(int)> (&QComboBox::activated), this, &TextEditor::textSize);

    // Undo/redo:
    connect(ui->textEdit->document(), &QTextDocument::undoAvailable, ui->action_Undo, &QAction::setEnabled);
    connect(ui->textEdit->document(), &QTextDocument::redoAvailable, ui->action_Redo, &QAction::setEnabled);

    connect(QApplication::clipboard(), &QClipboard::dataChanged, this, &TextEditor::clipboardDataChanged);

    // Menu
    connect(this, &TextEditor::customContextMenuRequested, this, &TextEditor::showContextMenu);
}

void TextEditor::setupActions()
{
    if (ui == nullptr)
        return;

    ui->formatBold->setDefaultAction(ui->action_Bold);
    ui->formatItalic->setDefaultAction(ui->action_Italic);
    ui->formatUnderline->setDefaultAction(ui->action_Underline);
    ui->textColor->setDefaultAction(ui->action_Text_Color);
    ui->formatLeft->setDefaultAction(ui->action_Align_Left);
    ui->formatCenter->setDefaultAction(ui->action_Align_Center);
    ui->formatRight->setDefaultAction(ui->action_Align_Right);
    ui->formatFill->setDefaultAction(ui->action_Align_Fill);
    ui->editCut->setDefaultAction(ui->action_Cut);
    ui->editCopy->setDefaultAction(ui->action_Copy);
    ui->editPaste->setDefaultAction(ui->action_Paste);
    ui->editUndo->setDefaultAction(ui->action_Undo);
    ui->editRedo->setDefaultAction(ui->action_Redo);
    ui->insertLink->setDefaultAction(ui->action_Insert_Link);
    ui->insertImage->setDefaultAction(ui->action_Insert_Image);
    ui->insertTable->setDefaultAction(ui->action_Insert_Table);
    ui->formatListOrdered->setDefaultAction(ui->action_Format_Ordered_List);
    ui->formatListUnordered->setDefaultAction(ui->action_Format_Unordered_List);
    ui->mergeCells->setDefaultAction(ui->action_Merge_Cells);
    ui->splitCells->setDefaultAction(ui->action_Split_Cells);
    ui->rowDelete->setDefaultAction(ui->action_Delete_Row);
    ui->columnDelete->setDefaultAction(ui->action_Delete_Column);
    ui->rowAdd->setDefaultAction(ui->action_Add_Row);
    ui->columnAdd->setDefaultAction(ui->action_Add_Column);
    ui->columnSize->setDefaultAction(ui->action_Size_Columns);
}

void TextEditor::setupUi()
{
    ui->action_Undo->setEnabled(ui->textEdit->document()->isUndoAvailable());
    ui->action_Redo->setEnabled(ui->textEdit->document()->isRedoAvailable());

    // default font
    ui->textEdit->setFont(QApplication::font());

    alignmentChanged();
    colorChanged();
    fontChanged();
    styleChanged();
    clipboardDataChanged();

    Util::SetFocus(ui->textEdit);

    ui->textEdit->setTextInteractionFlags(ui->textEdit->textInteractionFlags() | Qt::LinksAccessibleByMouse);
    enableActions();
}

void TextEditor::setupMenus()
{
    setContextMenuPolicy(Qt::CustomContextMenu);

    // Cut and paste
    m_editorMenu.addAction(ui->action_Cut);
    m_editorMenu.addAction(ui->action_Copy);
    m_editorMenu.addAction(ui->action_Copy_Html);
    m_editorMenu.addAction(ui->action_Paste);
    m_editorMenu.addAction(ui->action_Paste_Html);
    m_editorMenu.addSeparator();

    // Undo and redo
    m_editorMenu.addAction(ui->action_Undo);
    m_editorMenu.addAction(ui->action_Redo);
    m_editorMenu.addSeparator();

    // Sub-menus
    QMenu* formatMenu = m_editorMenu.addMenu(tr("Character Format"));
    QMenu* alignMenu  = m_editorMenu.addMenu(tr("Alignment"));
    QMenu* insertMenu = m_editorMenu.addMenu(tr("Insert"));
    QMenu* tableMenu  = m_editorMenu.addMenu(tr("Table"));

    // Format menu
    formatMenu->addAction(ui->action_Bold);
    formatMenu->addAction(ui->action_Italic);
    formatMenu->addAction(ui->action_Underline);
    formatMenu->addAction(ui->action_Text_Color);

    // Align menu
    alignMenu->addAction(ui->action_Align_Left);
    alignMenu->addAction(ui->action_Align_Center);
    alignMenu->addAction(ui->action_Align_Right);
    alignMenu->addAction(ui->action_Align_Fill);
    alignMenu->addSeparator();
    alignMenu->addAction(ui->action_Format_Ordered_List);
    alignMenu->addAction(ui->action_Format_Unordered_List);

    // Insert menu
    insertMenu->addAction(ui->action_Insert_Link);
    insertMenu->addAction(ui->action_Insert_Image);
    insertMenu->addAction(ui->action_Insert_Table);

    // Table menu
    tableMenu->addAction(ui->action_Size_Columns);
    tableMenu->addSeparator();
    tableMenu->addAction(ui->action_Add_Row);
    tableMenu->addAction(ui->action_Add_Column);
    tableMenu->addAction(ui->action_Delete_Row);
    tableMenu->addAction(ui->action_Delete_Column);
    tableMenu->addSeparator();
    tableMenu->addAction(ui->action_Merge_Cells);
    tableMenu->addAction(ui->action_Split_Cells);
}

void TextEditor::setupTimers()
{
    m_focusTimer.setSingleShot(true);
    connect(&m_focusTimer, &QTimer::timeout, this, &TextEditor::focusEditor);
}

void TextEditor::focusEditor()
{
    // for some reason, initial focus doesn't work, so we set the focus on a signal
    QTextCursor beginning = cursor();
    beginning.movePosition(QTextCursor::End);
    ui->textEdit->setTextCursor(beginning);
    Util::SetFocus(ui->textEdit);
}

void TextEditor::showEvent(QShowEvent* event)
{
    QWidget::showEvent(event);

    using namespace std::chrono_literals;
    m_focusTimer.start(1ms);     // for some reason this doesn't work directly.
}

QTextCursor TextEditor::cursor() const
{
    return ui->textEdit->textCursor();
}

std::tuple<int,int,int,int> TextEditor::selectedCells() const
{
    int firstRow = -1, numRows = -1, firstColumn = -1, numColumns = -1;

    if (QTextTable* table = cursor().currentTable(); table != nullptr) {
        cursor().selectedTableCells(&firstRow, &numRows, &firstColumn, &numColumns);

        if (firstRow == -1 && firstColumn == -1) { // if none selected:
            const QTextTableCell cell = table->cellAt(cursor());
            return std::make_tuple(cell.row(), 1, cell.column(), 1);
        }
    }

    return std::make_tuple(firstRow, numRows, firstColumn, numColumns);
}

void TextEditor::enableActions()
{
    QTextTable* table = cursor().currentTable();
    const bool inTable = (table != nullptr);
    const bool tableMultiSelect = cursor().hasComplexSelection();
    const bool hasSelection = cursor().hasSelection();
    const bool canPaste = ui->textEdit->canPaste();
    bool tableMergedCell = false;

    if (table != nullptr) {
        const QTextTableCell cell = table->cellAt(cursor());
        tableMergedCell = cell.rowSpan() > 1 || cell.columnSpan() > 1;
    }

    // Set proper icon for editing or inserting tables
    ui->action_Insert_Table->setIcon(Icons::get(inTable ? "table" : "insert-table"));

    ui->action_Merge_Cells->setEnabled(tableMultiSelect);
    ui->action_Split_Cells->setEnabled(tableMergedCell && !tableMultiSelect);
    ui->action_Add_Row->setEnabled(inTable);
    ui->action_Add_Column->setEnabled(inTable);
    ui->action_Delete_Row->setEnabled(inTable);
    ui->action_Delete_Column->setEnabled(inTable);
    ui->action_Size_Columns->setEnabled(inTable);
    ui->action_Copy->setEnabled(hasSelection);
    ui->action_Cut->setEnabled(hasSelection);
    ui->action_Paste->setEnabled(canPaste);
    ui->action_Paste_Html->setEnabled(canPaste);
}

void TextEditor::setupFontSizes()
{
    const QList<int> standardSizes = QFontDatabase::standardSizes();
    for (int size : standardSizes)
        ui->fontSize->addItem(QString::number(size));

    ui->fontSize->setCurrentIndex(standardSizes.indexOf(QApplication::font().pointSize()));
}

void TextEditor::showContextMenu(const QPoint& pos)
{
    m_editorMenu.exec(mapToGlobal(pos));
}

void TextEditor::setHtml(const QString& s)
{
    if (ui != nullptr)
        ui->textEdit->setHtml(s);

    m_original = s;

    enableActions();
}

QString TextEditor::toHtml() const
{
    if (ui == nullptr)
        return { };

    return ui->textEdit->toHtml();
}

QTextEdit* TextEditor::editor()
{
    return (ui != nullptr) ? ui->textEdit : nullptr;
}

const QTextEdit* TextEditor::editor() const
{
    return (ui != nullptr) ? ui->textEdit : nullptr;
}

void TextEditor::setupButtonBox(QDialogButtonBox* buttonBox) const
{
    if (buttonBox == nullptr)
        return;

    // Reset
    if (QPushButton* resetButton = buttonBox->button(QDialogButtonBox::Reset); resetButton != nullptr)
        connect(resetButton, &QPushButton::clicked, this, &TextEditor::resetText);
}

void TextEditor::cursorPositionChanged()
{
    alignmentChanged();
    styleChanged();
    enableActions();
}

void TextEditor::selectionChanged()
{
    enableActions();
}

void TextEditor::clipboardDataChanged()
{
    if (const QMimeData* md = QApplication::clipboard()->mimeData())
        ui->action_Paste->setEnabled(md->hasText());
}

void TextEditor::textFamily(int index)
{
    QTextCharFormat fmt;
    fmt.setFontFamily(ui->fontFamily->itemText(index));
    mergeFormatOnSelection(fmt);
}

void TextEditor::textSize(int index)
{
    const qreal pointSize = qreal(ui->fontSize->itemText(index).toFloat());

    if (pointSize > 0.0f) {
        QTextCharFormat fmt;
        fmt.setFontPointSize(pointSize);
        mergeFormatOnSelection(fmt);
    }
}

void TextEditor::textColor()
{
    const QColor col = QColorDialog::getColor(ui->textEdit->textColor(), this);
    if (!col.isValid())
        return;
    QTextCharFormat fmt;
    fmt.setForeground(col);
    mergeFormatOnSelection(fmt);
    colorChanged();
}

void TextEditor::resetText()
{
    const QMessageBox::StandardButton result =
            QMessageBox::warning(this, tr("Reset?"),
                                 tr("Reset text?  This will lose recent changes."),
                                 QMessageBox::Ok | QMessageBox::Cancel,
                                 QMessageBox::Cancel);

    if (result != QMessageBox::Ok)
        return;

    ui->textEdit->setHtml(m_original);
}

void TextEditor::alignmentChanged()
{
    const Qt::Alignment a = ui->textEdit->alignment();
    ui->action_Align_Left->setChecked(bool(a & Qt::AlignLeft));
    ui->action_Align_Center->setChecked(bool(a & Qt::AlignHCenter));
    ui->action_Align_Right->setChecked(bool(a & Qt::AlignRight));
    ui->action_Align_Fill->setChecked(bool(a & Qt::AlignJustify));
}

void TextEditor::colorChanged()
{
    const QColor& color = ui->textEdit->textColor();

    QPixmap p(ui->textColor->size());
    p.fill(color);
    ui->action_Text_Color->setIcon(p);
}

void TextEditor::fontChanged()
{
    const QFont& font = ui->textEdit->font();
    ui->fontFamily->setCurrentIndex(ui->fontFamily->findText(QFontInfo(font).family()));
    ui->fontSize->setCurrentIndex(ui->fontSize->findText(QString::number(font.pointSize())));
}

void TextEditor::styleChanged()
{
    if (QTextList* list = cursor().currentList(); list != nullptr) {
        bool ordered;
        switch (list->format().style()) {
        case QTextListFormat::ListDisc:
        case QTextListFormat::ListCircle:
        case QTextListFormat::ListSquare:
            ordered = false;
            break;
        default:
            ordered = true;
            break;
        }

        ui->action_Format_Ordered_List->setChecked(ordered);
        ui->action_Format_Unordered_List->setChecked(!ordered);
    } else {
        ui->action_Format_Ordered_List->setChecked(false);
        ui->action_Format_Unordered_List->setChecked(false);
    }
}

void TextEditor::currentCharFormatChanged(const QTextCharFormat& format)
{
    ui->action_Bold->setChecked(format.fontWeight() == QFont::Bold);
    ui->action_Italic->setChecked(format.fontItalic());
    ui->action_Underline->setChecked(format.fontUnderline());
    colorChanged();
}

void TextEditor::setListFormat(bool apply, QTextListFormat::Style style)
{
    if (apply) {
        cursor().beginEditBlock();

        QTextBlockFormat blockFmt = cursor().blockFormat();
        QTextListFormat listFmt;

        if (cursor().currentList() != nullptr) {
            listFmt = cursor().currentList()->format();
        } else {
            listFmt.setIndent(blockFmt.indent() + 1);
            blockFmt.setIndent(0);
            cursor().setBlockFormat(blockFmt);
        }

        listFmt.setStyle(style);
        cursor().createList(listFmt);
        cursor().endEditBlock();
    } else {
        if (QTextList* list = cursor().currentList(); list != nullptr) {
            list->remove(cursor().block());
            QTextBlockFormat blockFmt = cursor().blockFormat();
            blockFmt.setIndent(0);
            cursor().setBlockFormat(blockFmt);
        }
    }

    styleChanged();
}

void TextEditor::mergeFormatOnSelection(const QTextCharFormat& format)
{
    cursor().mergeCharFormat(format);
    ui->textEdit->mergeCurrentCharFormat(format);
}

void TextEditor::on_action_Bold_triggered()
{
    QTextCharFormat fmt;
    fmt.setFontWeight(ui->action_Bold->isChecked() ? QFont::Bold : QFont::Normal);
    mergeFormatOnSelection(fmt);
}

void TextEditor::on_action_Italic_triggered()
{
    QTextCharFormat fmt;
    fmt.setFontItalic(ui->action_Italic->isChecked());
    mergeFormatOnSelection(fmt);
}

void TextEditor::on_action_Underline_triggered()
{
    QTextCharFormat fmt;
    fmt.setFontUnderline(ui->action_Underline->isChecked());
    mergeFormatOnSelection(fmt);
}

void TextEditor::on_action_Text_Color_triggered()
{
    textColor();
}

void TextEditor::on_action_Align_Left_triggered()
{
   ui->textEdit->setAlignment(Qt::AlignLeft | Qt::AlignAbsolute);
   alignmentChanged();
}

void TextEditor::on_action_Align_Right_triggered()
{
    ui->textEdit->setAlignment(Qt::AlignRight | Qt::AlignAbsolute);
    alignmentChanged();
}

void TextEditor::on_action_Align_Center_triggered()
{
    ui->textEdit->setAlignment(Qt::AlignHCenter);
    alignmentChanged();
}

void TextEditor::on_action_Align_Fill_triggered()
{
    ui->textEdit->setAlignment(Qt::AlignJustify);
    alignmentChanged();
}

void TextEditor::on_action_Cut_triggered()
{
    ui->textEdit->cut();
}

void TextEditor::on_action_Copy_triggered()
{
    ui->textEdit->copy();
}

void TextEditor::on_action_Copy_Html_triggered()
{
    QApplication::clipboard()->setText(ui->textEdit->toHtml());
}

void TextEditor::on_action_Paste_Html_triggered()
{
    ui->textEdit->setHtml(QApplication::clipboard()->text());
}

void TextEditor::on_action_Paste_triggered()
{
    ui->textEdit->paste();
}

void TextEditor::on_action_Undo_triggered()
{
    ui->textEdit->undo();
}

void TextEditor::on_action_Redo_triggered()
{
    ui->textEdit->redo();
}

void TextEditor::on_action_Insert_Link_triggered()
{
    m_linkDialog.setText(cursor().selectedText());
    m_linkDialog.setFocus();

    if (m_linkDialog.exec() != QDialog::Accepted || !isVisible())
        return;

    const QString link = "<a href=\"" + m_linkDialog.url() + "\">" + m_linkDialog.text() + "</a> ";
    cursor().insertHtml(link);
}

void TextEditor::on_action_Insert_Table_triggered()
{
    QTextTable* table = cursor().currentTable();

    TextEditorTableFormat tableFormatDialog(table, this);

    if (tableFormatDialog.exec() != QDialog::Accepted || !isVisible())
        return;

    if (table == nullptr) {
        // Insert new table. We don't need the return value here: commented for clarity.
        /* table = */ cursor().insertTable(tableFormatDialog.rows(),
                                           tableFormatDialog.columns(),
                                           tableFormatDialog.format());
    } else {
        // Edit table
        table->resize(tableFormatDialog.rows(), tableFormatDialog.columns());
        table->setFormat(tableFormatDialog.format());
    }
}

void TextEditor::on_action_Size_Columns_triggered()
{
    QTextTable* table = cursor().currentTable();
    if (table == nullptr)
        return;

    QTextTableFormat format = table->format();
    [[maybe_unused]] const auto [firstRow, numRows, firstColumn, numColumns] = selectedCells();
    Util::maybe_unused(firstRow, numRows); // see comment in maybe_unused.h

    if (firstColumn == -1)
        return;

    auto widths = format.columnWidthConstraints();
    widths.resize(table->columns());
    assert((firstColumn + numColumns- 1) < widths.size());

    TextEditorColumnSize columnSizeDialog(widths.at(firstColumn), this);

    if (columnSizeDialog.exec() != QDialog::Accepted)
        return;

    const QTextLength length = columnSizeDialog.length();

    for (int c = 0; c < numColumns; ++c)
        widths[firstColumn + c] = length;

    format.setColumnWidthConstraints(widths);
    table->setFormat(format);
}

void TextEditor::on_action_Insert_Image_triggered()
{
    QMessageBox::warning(this, "Unimplemented",
                         "This feature is currently unimplemented.  Check back later!",
                         QMessageBox::Ok);
    return;

    m_imageLinkDialog.setFocus();

    if (m_imageLinkDialog.exec() != QDialog::Accepted)
        return;

    cursor().insertImage(m_linkDialog.url());
}

void TextEditor::on_action_Format_Ordered_List_triggered(bool checked)
{
    setListFormat(checked, QTextListFormat::ListDecimal);
}

void TextEditor::on_action_Format_Unordered_List_triggered(bool checked)
{
    setListFormat(checked, QTextListFormat::ListDisc);
}

void TextEditor::on_action_Merge_Cells_triggered()
{
    if (QTextTable* table = cursor().currentTable(); table != nullptr)
        table->mergeCells(cursor());
}

void TextEditor::on_action_Split_Cells_triggered()
{
    if (QTextTable* table = cursor().currentTable(); table != nullptr) {
        const QTextTableCell cell = table->cellAt(cursor());
        table->splitCell(cell.row(), cell.column(), cell.rowSpan(), cell.columnSpan());
    }
}

void TextEditor::on_action_Add_Row_triggered()
{
    [[maybe_unused]] const auto [firstRow, numRows, firstColumn, numColumns] = selectedCells();
    Util::maybe_unused(numRows, firstColumn, numColumns); // see comment in maybe_unused.h


    if (QTextTable* table = cursor().currentTable(); table != nullptr)
        table->insertRows(firstRow, 1);
}

void TextEditor::on_action_Add_Column_triggered()
{
    [[maybe_unused]] const auto [firstRow, numRows, firstColumn, numColumns] = selectedCells();
    Util::maybe_unused(firstRow, numRows, numColumns); // see comment in maybe_unused.h

    if (QTextTable* table = cursor().currentTable(); table != nullptr)
        table->insertColumns(firstColumn, 1);
}

void TextEditor::on_action_Delete_Row_triggered()
{
    [[maybe_unused]] const auto [firstRow, numRows, firstColumn, numColumns] = selectedCells();
    Util::maybe_unused(firstColumn, numColumns); // see comment in maybe_unused.h

    if (QTextTable* table = cursor().currentTable(); table != nullptr)
        table->removeRows(firstRow, numRows);
}

void TextEditor::on_action_Delete_Column_triggered()
{
    [[maybe_unused]] const auto [firstRow, numRows, firstColumn, numColumns] = selectedCells();
    Util::maybe_unused(firstRow, numRows); // see comment in maybe_unused.h

    if (QTextTable* table = cursor().currentTable(); table != nullptr)
        table->removeColumns(firstColumn, numColumns);
}

