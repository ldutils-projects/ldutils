/*
    Copyright 2019-2022 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <QColorDialog>
#include <QAbstractItemModel>
#include <QPainter>

#include "colordelegate.h"

// Avoid parenting this to two things
ColorDelegate::ColorDelegate(QObject* /*parent*/, bool showAlpha,
                             const QString& winTitle, bool winBorders, int role) :
    DelegateBase(nullptr, true, winTitle, winBorders, role),
    pad(0, 0),
    maxSize(-1, -1),
    showAlpha(showAlpha)
{
}

ColorDelegate::~ColorDelegate()
{
}

QWidget* ColorDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem& /*option*/, const QModelIndex& idx) const
{
    auto* editor = new QColorDialog(parent);
    if (editor == nullptr)
        return nullptr;

    if (const auto* model = idx.model(); model != nullptr) {
        editor->setCurrentColor(model->data(idx, role).value<QColor>());
        if (showAlpha)
            editor->setOption(QColorDialog::ShowAlphaChannel);
        setPopup(editor);
    }

    return editor;
}

void ColorDelegate::setModelData(QWidget* editor, QAbstractItemModel* model, const QModelIndex& idx) const
{
    updateResultFromEditor(editor, model, idx, [&](bool& accepted) {
        auto* dialog = dynamic_cast<QColorDialog*>(editor);
        if (dialog == nullptr)
            return QColor::fromRgb(0,0,0);

        accepted = (dialog->result() == QDialog::Accepted);
        return dialog->currentColor();
    });
}


void ColorDelegate::paint(QPainter* painter, const QStyleOptionViewItem& option, const QModelIndex& idx) const
{
    const QAbstractItemModel& model = *idx.model();
    const QVariant data = model.data(idx, Qt::BackgroundRole);

    if (data.type() == QVariant::Color) {
        QRect reqRect = option.rect;

        if (maxSize.width() > 0)
            reqRect.setWidth(std::min(reqRect.width(), maxSize.width()));

        if (maxSize.height() > 0)
            reqRect.setHeight(std::min(reqRect.height(), maxSize.height()));

        const QSize dynPad(std::min(pad.width(), reqRect.width() - 4),
                           std::min(pad.height(), reqRect.height() - 4));

        const QRect rect(reqRect.left() + dynPad.width(),
                         reqRect.top() + dynPad.height(),
                         reqRect.width() - dynPad.width() * 2,
                         reqRect.height() - dynPad.height() * 2);

        painter->fillRect(rect, *static_cast<const QColor*>(data.constData()));

        // Display cell text, if any
        if (const QVariant text = model.data(idx, Qt::DisplayRole); text.type() == QVariant::String) {
            if (const QVariant fg = model.data(idx, Qt::ForegroundRole); fg.type() == QVariant::Color)
                painter->setPen(*static_cast<const QColor*>(fg.constData()));

            if (const QVariant font = model.data(idx, Qt::FontRole); font.type() == QVariant::Font)
                painter->setFont(*static_cast<const QFont*>(font.constData()));

            painter->drawText(rect, Qt::AlignHCenter | Qt::AlignVCenter,
                              *static_cast<const QString*>(text.constData()));
        }

        return;
    }

    return DelegateBase::paint(painter, option, idx);
}

QSize ColorDelegate::sizeHint(const QStyleOptionViewItem& /*option*/, const QModelIndex& /*idx*/) const
{
    return (maxSize.width() > -1) ? maxSize : QSize(25, 25);
}
