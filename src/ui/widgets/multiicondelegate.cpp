/*
    Copyright 2020-2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <QGuiApplication>
#include <QPalette>
#include <QPen>
#include <QPainter>

#include <src/core/cfgdatabase.h>

#include <src/util/ui.h>

#include "multiicondelegate.h"

MultiIconDelegate::MultiIconDelegate(QObject* parent, const QString &winTitle, OutlineMode outlineMode,
                                     bool winBorders, int role) :
    DelegateBase(parent, true, winTitle, winBorders, role),
    m_ellipsisIcon(Util::IsLightTheme() ? ":art/ui/Ellipsis-Light.svg" : ":art/ui/Ellipsis-Dark.svg"),
    m_outlinePen(outlinePen()),
    m_outlineMode(outlineMode)
{
}

void MultiIconDelegate::drawIcon(QPainter* painter, const QIcon& icon, const QSize requestSize,
                                 QRect& rect, int fixedRight, const QStyleOptionViewItem& option,
                                 Qt::Alignment align) const
{
    const QSize size = icon.actualSize(requestSize);
    rect.setRight(std::min(rect.left() + size.width(), fixedRight));

    QRect iconRect = rect;
    iconRect.setSize(size + QSize(1,1));
    iconRect.moveCenter(rect.center());

    icon.paint(painter, iconRect, align,
               bool(option.state & QStyle::State_Selected) ? QIcon::Selected :
               bool(option.state & QStyle::State_Active)   ? QIcon::Active   : QIcon::Normal,
               bool(option.state & QStyle::State_Off)      ? QIcon::Off      : QIcon::On);

    if (m_outlineMode == OutlineMode::Box) {
        painter->setPen(m_outlinePen);
        painter->drawRect(iconRect.adjusted(0, 0, -1, -1));
    }

    rect.setLeft(rect.left() + size.width() + CfgDataBase::iconPad.width());
}

QPen MultiIconDelegate::outlinePen()
{
    QPen outline;
    QColor outlineColor(QGuiApplication::palette().color(QPalette::AlternateBase));

    // Make a subtler outline than the QPalette::Text color.  Hopefully this is reasonable for most
    // color schemes.
    if (Util::IsLightTheme())
        outlineColor = outlineColor.darker(150);
    else
        outlineColor = outlineColor.lighter(220);

    outline.setColor(outlineColor);
    outline.setStyle(Qt::SolidLine);
    outline.setWidth(1);

    return outline;
}

void MultiIconDelegate::paint(QPainter* painter, const QStyleOptionViewItem& option, const QModelIndex& idx) const
{
    const QAbstractItemModel& model = *idx.model();
    const QVariant tagVar = model.data(idx, Util::RawDataRole);
    const QSize iconSize = this->iconSize();
    const int   maxIcons = this->maxIcons();

    if (tagVar.type() == QVariant::StringList) {
        const QStringList& tags = *static_cast<const QStringList*>(tagVar.constData());
        int count = 0;
        QRect rect = option.rect;
        rect.setBottom(rect.bottom() - CfgDataBase::iconPad.height() / 2);
        rect.setTop(rect.top() + CfgDataBase::iconPad.height() / 2);

        const int displayedIconCount = std::min(maxIcons, tags.size());

        const bool useEllipsis = tags.size() > maxIcons;

        const int fixedRight = rect.right();

        const int widthPerIcon = (rect.width() - (CfgDataBase::iconPad.width() * displayedIconCount) -
                                  (useEllipsis ? iconSize.width() / ellipsisWidthRatio : 0)) /
                std::max(displayedIconCount, 1);

        const QSize requestSize  = QSize(std::min(iconSize.width(), widthPerIcon), iconSize.height());
        const QSize ellipsisSize = QSize(iconSize.width() / ellipsisWidthRatio, requestSize.height());

        for (const auto& tag : tags) {
            if (count >= maxIcons) {
                rect.setBottom(rect.bottom() - ellipsisSize.height() / 8);

                drawIcon(painter, m_ellipsisIcon, ellipsisSize, rect, fixedRight, option,
                         Qt::AlignLeft | Qt::AlignBottom);
                break;
            }

            if (const QIcon icon = iconFor(tag); !icon.isNull()) {
                drawIcon(painter, icon, requestSize, rect, fixedRight, option);
                ++count;
            }
        }

        return;
    }

    return DelegateBase::paint(painter, option, idx);
}

QSize MultiIconDelegate::sizeHint(const QStyleOptionViewItem& option, const QModelIndex& idx) const
{
    const QAbstractItemModel& model = *idx.model();
    const QVariant tagVar = model.data(idx, Util::RawDataRole);
    const QSize iconSize = this->iconSize();
    const int   maxIcons = this->maxIcons();

    if (tagVar.type() == QVariant::StringList) {
        const QStringList& tags = *static_cast<const QStringList*>(tagVar.constData());
        int count = 0;
        QSize hint(0, 0); // accumulate aggregate size into here

        for (const auto& tag : tags) {
            if (count >= maxIcons) {
                // Room for ellipsis
                hint.setWidth(hint.width() + iconSize.width() / ellipsisWidthRatio);
                break;
            }

            if (const QIcon icon = iconFor(tag); !icon.isNull()) {
                const QSize size = iconFor(tag).actualSize(iconSize);
                hint = QSize(hint.width() + size.width() + CfgDataBase::iconPad.width(),
                             std::max(hint.height(), size.height() + CfgDataBase::iconPad.height()));
                ++count;
            }
        }

        return hint;
    }

    return DelegateBase::sizeHint(option, idx);
}
