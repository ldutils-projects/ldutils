/*
    Copyright 2019-2022 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef ICONSELECTORDELEGATE_H
#define ICONSELECTORDELEGATE_H

#include <src/ui/widgets/delegatebase.h>
#include <src/ui/dialogs/iconselector.h>

class IconSelectorDelegate final : public DelegateBase
{
    Q_OBJECT

public:
    IconSelectorDelegate(const QStringList& roots,
                         const QString& winTitle = tr("Select icon"),
                         const IconSelector::namefilter_t& = IconSelector::passFn,
                         bool winBorders = true, int role = Qt::DecorationRole,
                         QObject* parent = nullptr);

    QWidget* createEditor(QWidget* parent, const QStyleOptionViewItem&, const QModelIndex&) const override;
    void destroyEditor(QWidget* editor, const QModelIndex&) const override;
    void setModelData(QWidget* editor, QAbstractItemModel*, const QModelIndex&) const override;

private:
    const QStringList                m_roots;
    const IconSelector::namefilter_t m_nameFilter;
};

#endif // ICONSELECTORDELEGATE_H
