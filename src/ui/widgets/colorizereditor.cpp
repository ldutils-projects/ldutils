/*
    Copyright 2019-2022 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "colorizereditor.h"
#include "ui_colorizereditor.h"

#include "src/util/icons.h"
#include "src/util/ui.h"
#include "src/core/colorizermodel.h"

ColorizerEditor::ColorizerEditor(ColorizerModel& model, const QStringList& headerLabels, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ColorizerEditor),
    m_model(model),
    m_headerView(Qt::Horizontal, this),
    m_columnDelegate(this, headerLabels),
    m_colorDelegate(this),
    m_iconDelegate({":art/tags"}),
    m_queryDelegate(model.queryCtx(*this), this)
{
    ui->setupUi(this);

    setupActionIcons();
    setupView();
    setupDelegates();
    setupMenu();
    setupSignals();
    Util::SetupWhatsThis(this);
}

ColorizerEditor::~ColorizerEditor()
{
    delete ui;
}

void ColorizerEditor::setupActionIcons()
{
    Icons::defaultIcon(ui->action_Unset_Icon,             "edit-clear");
    Icons::defaultIcon(ui->action_Unset_Background_Color, "edit-clear");
    Icons::defaultIcon(ui->action_Unset_Foreground_Color, "edit-clear");
    Icons::defaultIcon(ui->action_Remove,                 "edit-delete");
    Icons::defaultIcon(ui->action_Add,                    "list-add");
}

void ColorizerEditor::setupSignals()
{
    // Selection changes
    connect(ui->colorizerView->selectionModel(), &QItemSelectionModel::selectionChanged, this, &ColorizerEditor::enableMenus);
}

void ColorizerEditor::setupView()
{
    QTreeView& view = *ui->colorizerView;

    view.setModel(&m_model);
    view.setHeader(&m_headerView);

    m_headerView.setSectionResizeMode(QHeaderView::Interactive);
    m_headerView.setSectionsMovable(true);

    enableMenus();
}

void ColorizerEditor::defaultColumnWidths()
{
    QTreeView& view = *ui->colorizerView;
    Util::ResizeColumns(view, { 15, 45, 5, 5, 5, 5, 5 });
}

QTreeView* ColorizerEditor::treeView() const
{
    return (ui == nullptr) ? nullptr : ui->colorizerView;
}

void ColorizerEditor::setVisible(bool visible)
{
    QWidget::setVisible(visible);

    if (visible)
        defaultColumnWidths();
}

void ColorizerEditor::setupDelegates()
{
    QTreeView& view = *ui->colorizerView;

    m_columnDelegate.setSelector(view.selectionModel());
    m_colorDelegate.setSelector(view.selectionModel());
    m_iconDelegate.setSelector(view.selectionModel());
    m_queryDelegate.setSelector(view.selectionModel());

    view.setItemDelegateForColumn(ColorizerModel::Column,  &m_columnDelegate);
    view.setItemDelegateForColumn(ColorizerModel::Query,   &m_queryDelegate);
    view.setItemDelegateForColumn(ColorizerModel::Icon,    &m_iconDelegate);
    view.setItemDelegateForColumn(ColorizerModel::FgColor, &m_colorDelegate);
    view.setItemDelegateForColumn(ColorizerModel::BgColor, &m_colorDelegate);
}

void ColorizerEditor::setupMenu()
{
    m_contextMenu.addActions({ ui->action_Add,
                             ui->action_Remove });
    m_contextMenu.addSeparator();
    m_contextMenu.addActions({ ui->action_Unset_Icon,
                             ui->action_Unset_Foreground_Color,
                             ui->action_Unset_Background_Color });

    setContextMenuPolicy(Qt::CustomContextMenu);

    connect(this, &ColorizerEditor::customContextMenuRequested, this, &ColorizerEditor::showContextMenu);
}

void ColorizerEditor::enableMenus()
{
    // Enable menus as appropriate
    const bool hasSelection = ui->colorizerView->selectionModel()->hasSelection();

    ui->action_Remove->setEnabled(m_model.rowCount() > 0);
    ui->action_Unset_Background_Color->setEnabled(hasSelection);
    ui->action_Unset_Foreground_Color->setEnabled(hasSelection);
    ui->action_Unset_Icon->setEnabled(hasSelection);

    ui->buttonRemoveItem->setEnabled(ui->action_Remove->isEnabled());
    ui->buttonResetBg->setEnabled(ui->action_Unset_Background_Color->isEnabled());
    ui->buttonResetFg->setEnabled(ui->action_Unset_Foreground_Color->isEnabled());
    ui->buttonResetIcon->setEnabled(ui->action_Unset_Icon->isEnabled());
}

void ColorizerEditor::showContextMenu(const QPoint& pos)
{
    m_contextMenu.exec(mapToGlobal(pos));
}

void ColorizerEditor::on_action_Unset_Icon_triggered()
{
    m_model.clearData(ui->colorizerView->selectionModel(), ColorizerModel::Icon);
}

void ColorizerEditor::on_action_Unset_Background_Color_triggered()
{
    m_model.clearData(ui->colorizerView->selectionModel(), ColorizerModel::BgColor);
}

void ColorizerEditor::on_action_Unset_Foreground_Color_triggered()
{
    m_model.clearData(ui->colorizerView->selectionModel(), ColorizerModel::FgColor);
}

void ColorizerEditor::on_action_Remove_triggered()
{
    Util::RemoveRows(m_model, ui->colorizerView->selectionModel()->selectedRows());
    enableMenus();
}

void ColorizerEditor::on_action_Add_triggered()
{
    int insertRow = ui->colorizerView->selectionModel()->currentIndex().row();
    if (insertRow < 0)
        insertRow = m_model.rowCount();

    m_model.insertRows(insertRow, 1);
    ui->colorizerView->scrollTo(m_model.index(insertRow, 0));
    enableMenus();
}
