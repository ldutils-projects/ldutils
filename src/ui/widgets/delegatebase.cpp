/*
    Copyright 2019-2023 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <tuple>

#include <QItemSelectionModel>

#include "src/util/util.h"
#include "src/undo/undomgr.h"
#include "src/core/treemodel.h"
#include "delegatebase.h"

DelegateBase::DelegateBase(QObject* parent, bool popupEditor, const QString& winTitle,
                           bool winBorders, int role) :
    QStyledItemDelegate(parent),
    selector(nullptr),
    role(role),
    m_popupEditor(popupEditor),
    m_winTitle(winTitle),
    m_undoMgr(nullptr),
    m_winBorders(winBorders)
{
}

DelegateBase::~DelegateBase()
{
}

void DelegateBase::updateEditorGeometry(QWidget* editor, const QStyleOptionViewItem& option, const QModelIndex& /*idx*/) const
{
    if (m_popupEditor) {
        editor->setGeometry(Util::MapOnScreen(editor, option.rect.center(), editor->size()));
    } else {
        editor->setGeometry(option.rect);
    }
}

QDialog* DelegateBase::setPopup(QDialog* editor) const
{
    editor->setWindowFlags(m_winBorders ? Qt::Window : Qt::Popup);
    editor->setWindowTitle(m_winTitle);
    editor->setModal(true);
    editor->show();

    return editor;
}

auto DelegateBase::undoName(const QModelIndex& idx, int count)
{
    const QAbstractItemModel* model = idx.model();
    const QString columnName = model->headerData(idx.column(), Qt::Horizontal).toString();
    const QString undoText = UndoMgr::genNameX(tr("Set ") + columnName, count);

    return undoText;
}

void DelegateBase::updateResultFromEditor(QWidget* editor, QAbstractItemModel* model, const QModelIndex& idx,
                                          const std::function<QVariant(bool&)>& fetchFromEditor) const
{
    if (editor == nullptr)
        return;

    bool accepted;
    const QVariant value = fetchFromEditor(accepted);

    if (!accepted)
        return;

    // Set all selected rows, if someone set up the selector.
    const QModelIndexList selected =
            Util::MapDown((selector != nullptr)
                          ? selector->selectedRows(idx.column())
                          : QModelIndexList({idx}));

    // Make nicer undo name and group all the changes into a single undo
    const auto undoText = undoName(idx, selected.size());
    const UndoMgr::ScopedUndo undoSet(m_undoMgr, undoText);

    model = Util::MapDown(model);

    if (TreeModel* treeModel = dynamic_cast<TreeModel*>(model); treeModel != nullptr) {
        // TreeModels use multiSet method
        treeModel->multiSet(selected, value, role);
    } else {
        for (const auto& idx : selected)
            model->setData(idx, value, role);
    }
}

void DelegateBase::updateResultFromEditor(QWidget* editor, const QModelIndex& idx,
                                          const std::function<void(const QModelIndex&)>& setFromEditor) const
{
    if (editor == nullptr)
        return;

    // Set all selected rows, if someone set up the selector.
    const QModelIndexList selected = (selector != nullptr) ? selector->selectedRows(idx.column()) : QModelIndexList({idx});

    // Make nicer undo name and group all the changes into a single undo
    const auto undoText = undoName(idx, selected.size());
    const UndoMgr::ScopedUndo undoSet(m_undoMgr, undoText);

    for (const auto& idx : selected)
        setFromEditor(idx);
}
