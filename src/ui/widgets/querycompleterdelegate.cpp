/*
    Copyright 2019-2022 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "querycompleterdelegate.h"

#include <src/ui/misc/querycompleter.h>
#include <src/core/query.h>

QueryCompleterDelegate::QueryCompleterDelegate(const Query::Context& queryCtx, QObject* parent) :
    LineEditDelegate(parent),
    queryCtx(queryCtx)
{
}

QWidget* QueryCompleterDelegate::createEditor(QWidget* parent, const QStyleOptionViewItem& option, const QModelIndex& idx) const
{
    QWidget* editor = LineEditDelegate::createEditor(parent, option, idx);

    if (auto* lineEdit = dynamic_cast<QLineEdit*>(editor); lineEdit != nullptr) {
        auto* completer = new QueryCompleter(queryCtx, lineEdit);
        lineEdit->setCompleter(completer);
    }

    return editor;
}
