/*
    Copyright 2021-2023 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef MODELTEXTEDITORDELEGATE_H
#define MODELTEXTEDITORDELEGATE_H

#include <Qt>  // for Qt::EditRole
#include "texteditordelegate.h"

class TreeModel;
class QWidget;
class TextEditorDialogBase;
class QString;

class ModelTextEditorDelegate : public TextEditorDelegate
{
public:
    explicit ModelTextEditorDelegate(const TreeModel&, QWidget* parent,
                                     bool defaultShowVars = false,
                                     const QString& winTitle = QObject::tr("Edit rich text"),
                                     bool winBorders = true, int role = Qt::EditRole);
    ~ModelTextEditorDelegate() override;

private:
    [[nodiscard]] TextEditorDialogBase* dialogFactory(QWidget* parent) const override;  // create a new text editor dialog

    const TreeModel& m_model;    // model to create the model text editor for
    bool             m_defaultShowVars;
};

#endif // MODELTEXTEDITORDELEGATE_H
