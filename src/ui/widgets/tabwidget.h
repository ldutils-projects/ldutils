/*
    Copyright 2019-2022 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TABWIDGET_H
#define TABWIDGET_H

#include <src/ui/panes/panebase.h>
#include <src/core/settings.h>

#include <QTabWidget>
#include <QMenu>

class QString;
class QWidget;
class QAction;
class MainWindowBase;
class QCloseEvent;

// Add some custom features to the QTabWidget.  This also acts as our secondary top level window class.
class TabWidget final : public QTabWidget, public Settings
{
    Q_OBJECT

public:
    TabWidget(MainWindowBase&);
    ~TabWidget() override;

    int addTab(const QString& name = "New Tab", QWidget* contents = nullptr, int index = -1,
               Qt::Orientation orientation = Qt::Horizontal);

    void deleteTabs(bool later = true);

    [[nodiscard]] PaneBase::Container* currentTab() const;
    [[nodiscard]] PaneBase::Container* currentTabWarn() const;

    [[nodiscard]] QAction* getPaneAction(PaneAction cc) const;

    static void balanceChildren(PaneBase::Container* row);

    // *** begin Settings API
    void save(QSettings&) const override;
    void load(QSettings&) override;
    // *** end Settings API

private slots:
    void showTabContextMenu(const QPoint&);
    void tabChanged(int index);
    void tabClicked(int index);
    void tabMoved(int from, int to);
    void tabClose(int index);
    void tabRenameInteractive();
    void setWindowTitleInteractive();
    void tabCloseInteractive();
    void addTabInteractive();
    void balanceTabInteractive();
    void nextTab();
    void prevTab();
    void setAlwaysOnTop(bool);

private:
    friend class TestLdUtils;
    friend class TestZtgps;

    void balanceTab() const;
    void setupActions();
    void setupTabBar();
    void setupSignals();
    void setupMenus();
    void tabRename(int index);
    void setSecondaryWindowTitle();
    void closeEvent(QCloseEvent *event) override;
    void showEvent(QShowEvent*) override;
    [[nodiscard]] bool alwaysOnTop() const;
    [[nodiscard]] bool isSecondaryWindow() const;

    QMenu           m_tabMenu;            // tab context menu
    QAction*        actionAddTab;
    QAction*        actionRenameTab;
    QAction*        actionSetWindowTitle;
    QAction*        actionCloseTab;
    QAction*        actionBalanceTab;
    QAction*        actionNextTab;
    QAction*        actionPrevTab;
    QAction*        actionAlwaysOnTop;
    bool            addedActionsFlag;

    MainWindowBase& m_mainWindow;
};

#endif // TABWIDGET_H
