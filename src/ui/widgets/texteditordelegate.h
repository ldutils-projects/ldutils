/*
    Copyright 2019-2022 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TEXTEDITORDELEGATE_H
#define TEXTEDITORDELEGATE_H

#include <QByteArray>
#include <src/ui/widgets/delegatebase.h>

class TextEditorDialogBase;

class TextEditorDelegate : public DelegateBase
{
    Q_OBJECT

public:
    TextEditorDelegate(QObject* parent = nullptr, const QString& winTitle = tr("Edit rich text"),
                       bool winBorders = true, int role = Qt::EditRole);

    ~TextEditorDelegate() override;

    QWidget* createEditor(QWidget* parent, const QStyleOptionViewItem& option, const QModelIndex& idx) const override;
    void setModelData(QWidget* editor, QAbstractItemModel* model, const QModelIndex& idx) const override;

    // *** begin Settings API
    void save(QSettings&) const override;
    void load(QSettings&) override;
    // *** end Settings API

protected:
    [[nodiscard]] virtual TextEditorDialogBase* dialogFactory(QWidget* parent) const;  // create a new text editor dialog

private:
    TextEditorDialogBase* loadSettings(TextEditorDialogBase*) const;
    void saveSettings(const TextEditorDialogBase*) const;

    mutable QByteArray m_settings;  // deferred settings save across create/delete of underlying widget
};

#endif // TEXTEDITORDELEGATE_H
