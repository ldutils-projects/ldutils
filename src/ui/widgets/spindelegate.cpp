/*
    Copyright 2019-2022 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <QSpinBox>
#include "spindelegate.h"

SpinDelegate::SpinDelegate(QObject* parent, int min, int max, int step,
                           const QString& prefix,
                           const QString& suffix) :
    DelegateBase(parent, false),
    min(min), max(max), step(step),
    prefix(prefix), suffix(suffix)
{
}

QWidget* SpinDelegate::createEditor(QWidget* parent, const QStyleOptionViewItem& /*option*/, const QModelIndex& idx) const
{
    auto* editor = new QSpinBox(parent);
    if (editor == nullptr)
        return nullptr;

    if (const auto* model = idx.model(); model != nullptr) {
        editor->setRange(min, max);
        editor->setPrefix(prefix);
        editor->setSuffix(suffix);
        editor->setSingleStep(step);
        editor->setValue(model->data(idx, role).toInt());
        editor->setFrame(false);
        editor->setAutoFillBackground(true);  // otherwise it ends up transparent
    }

    return editor;
}

void SpinDelegate::setModelData(QWidget* editor, QAbstractItemModel* model, const QModelIndex& idx) const
{
    updateResultFromEditor(editor, model, idx, [&](bool& accepted) {
        auto* spinBox = dynamic_cast<QSpinBox*>(editor);
        if (spinBox == nullptr)
            return 0;

        accepted = true;
        return spinBox->value();
    });
}

