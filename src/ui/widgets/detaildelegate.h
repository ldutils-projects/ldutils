/*
    Copyright 2019-2022 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef DETAILDELEGATE_H
#define DETAILDELEGATE_H

#include <QStyledItemDelegate>
#include <QModelIndex>

#include <src/util/util.h>

// This is made for use with the DetailFilter.  Annoyingly, setting a row delegate
// for row N sets it for row N of EVERY parent node, which is not at all the behavior
// we want.  This addresses that by only passing the delegate methods through for
// given indexes.  This only works for a static structure, which is the case for the
// DetailFilter and views using it.
template <class DELEGATE>
class DetailDelegate : public QStyledItemDelegate
{
public:
    template <typename... P>

    DetailDelegate(const QModelIndex& node, QObject* parent, const P&... p);
    ~DetailDelegate() override { delete delegate; }

    QWidget* createEditor(QWidget* parent, const QStyleOptionViewItem& option, const QModelIndex& idx) const override;
    void setEditorData(QWidget* editor, const QModelIndex& idx) const override;
    void setModelData(QWidget* editor, QAbstractItemModel* model, const QModelIndex& idx) const override;
    void updateEditorGeometry(QWidget* editor, const QStyleOptionViewItem& option, const QModelIndex& idx) const override;
    void paint(QPainter* painter, const QStyleOptionViewItem& option, const QModelIndex& idx) const override;
    QSize sizeHint(const QStyleOptionViewItem& option, const QModelIndex& idx) const override;

    DELEGATE& operator()() { return *delegate; }

private:
    bool isNode(const QModelIndex& idx) const {
        if (!remapped) {
            if (const QModelIndex mapped = Util::MapDown(node); mapped.isValid()) {
                node = mapped;
                remapped = true;
            }
        }

        return Util::MapDown(idx).column() == node.column();
    }

    DELEGATE*            delegate;
    mutable QModelIndex  node;
    mutable bool         remapped = false;
};

template <class DELEGATE>
template <typename... P>
DetailDelegate<DELEGATE>::DetailDelegate(const QModelIndex& node, QObject* parent, const P&... p) :
    QStyledItemDelegate(parent),
    node(node)
{
    delegate = new DELEGATE(p...);
}

template <class DELEGATE>
QWidget* DetailDelegate<DELEGATE>::createEditor(QWidget* parent, const QStyleOptionViewItem& option, const QModelIndex& idx) const
{
    if (isNode(idx))
        return delegate->createEditor(parent, option, idx);

    return nullptr;
}

template <class DELEGATE>
void DetailDelegate<DELEGATE>::setEditorData(QWidget* editor, const QModelIndex& idx) const
{
    if (isNode(idx))
        delegate->setEditorData(editor, idx);
}

template <class DELEGATE>
void DetailDelegate<DELEGATE>::setModelData(QWidget* editor, QAbstractItemModel* model, const QModelIndex& idx) const
{
    if (isNode(idx))
        delegate->setModelData(editor, model, idx);
}

template <class DELEGATE>
void DetailDelegate<DELEGATE>::updateEditorGeometry(QWidget* editor, const QStyleOptionViewItem& option, const QModelIndex& idx) const
{
    if (isNode(idx))
        delegate->updateEditorGeometry(editor, option, idx);
}

template <class DELEGATE>
void DetailDelegate<DELEGATE>::paint(QPainter* painter, const QStyleOptionViewItem& option, const QModelIndex& idx) const
{
    if (isNode(idx))
        delegate->paint(painter, option, idx);
    else
        QStyledItemDelegate::paint(painter, option, idx);
}

template <class DELEGATE>
QSize DetailDelegate<DELEGATE>::sizeHint(const QStyleOptionViewItem& option, const QModelIndex& idx) const
{
    if (isNode(idx))
        return delegate->sizeHint(option, idx);

    return QStyledItemDelegate::sizeHint(option, idx);
}

#endif // DETAILDELEGATE_H
