/*
    Copyright 2019-2022 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "tabwidget.h"

#include <QTabBar>
#include <QFrame>
#include <QSignalBlocker>
#include <QInputDialog>
#include <QString>
#include <QMessageBox>
#include <QAction>
#include <QCloseEvent>

#include <src/util/util.h>
#include <src/util/ui.h>
#include <src/util/ui.inl.h>
#include <src/util/icons.h>
#include <src/undo/undowincfg.h>
#include <src/ui/windows/mainwindowbase.h>
#include <src/core/cfgdatabase.h>
#include <src/core/appbase.h>

TabWidget::TabWidget(MainWindowBase& mainWindow) :
    QTabWidget(&mainWindow),
    m_tabMenu(tr("Tab Actions")),
    actionAddTab(new QAction(Icons::get("tab-new"), "Add New Tab")),
    actionRenameTab(new QAction(Icons::get("edit-rename"), "Rename Tab...")),
    actionSetWindowTitle(new QAction(Icons::get("edit-rename"), "Set Window Title...")),
    actionCloseTab(new QAction(Icons::get("tab-close"), "Close Tab")),
    actionBalanceTab(new QAction(Icons::get("object-rows"), "Balance Tab Contents")),
    actionNextTab(new QAction(Icons::get("go-next"), "Next Tab")),
    actionPrevTab(new QAction(Icons::get("go-previous"), "Prev Tab")),
    actionAlwaysOnTop(new QAction(Icons::get("layer-top"), "Keep Window on Top")),
    addedActionsFlag(false),
    m_mainWindow(mainWindow)
{
    setupTabBar();
    // Rest of setup happens at showEvent time, after we know if we're a secondary window.

    setMovable(true);  // allow drag-reordering of tabs
}

TabWidget::~TabWidget()
{
    deleteTabs(true);  // orderly cleanup to avoid focus problems on exit
}

void TabWidget::setupActions()
{
    actionAlwaysOnTop->setCheckable(true);
    actionAlwaysOnTop->setChecked(true);

    actionAddTab->setToolTip("&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Adds a new tab to the window's main content area.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;");
    actionRenameTab->setToolTip("&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Rename the currently active tab.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;");
    actionSetWindowTitle->setToolTip("&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Set the window title.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;");
    actionCloseTab->setToolTip("&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Close the currently active tab, removing all of its content panes.  This action will present a warning, if enabled in the program configuration settings.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;");
    actionBalanceTab->setToolTip("&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Balance the contents of this tab, recursively.  This will make each pane the same size as panes belong to the same parent, within size constraints.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;");
    actionNextTab->setToolTip("&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Move to the next tab in the sequence.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;");
    actionPrevTab->setToolTip("&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Move to the previous tab in the sequence.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;");
    actionAlwaysOnTop->setToolTip("&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Keep window on top of main window.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;");

    // Set default icons if theme icons are not found
    Icons::defaultIcon(actionAddTab,         "tab-new");
    Icons::defaultIcon(actionRenameTab,      "edit-rename");
    Icons::defaultIcon(actionSetWindowTitle, "edit-rename");
    Icons::defaultIcon(actionCloseTab,       "tab-close");
    Icons::defaultIcon(actionBalanceTab,     "object-rows");
    Icons::defaultIcon(actionNextTab,        "go-next");
    Icons::defaultIcon(actionNextTab,        "go-previous");
    Icons::defaultIcon(actionAlwaysOnTop,    "layer-top");

    // Set tooltip strings as WhatsThis, also.
    if (!isSecondaryWindow())
        for (auto* a : { actionAddTab, actionRenameTab, actionSetWindowTitle, actionCloseTab, actionBalanceTab, actionNextTab, actionPrevTab })
            addAction(a);

    // TODO: get key sequences from preferences
    actionNextTab->setShortcut(QKeySequence("Ctrl+PgDown"));
    actionPrevTab->setShortcut(QKeySequence("Ctrl+PgUp"));
    actionCloseTab->setShortcut(QKeySequence("Ctrl+W"));
}

void TabWidget::setupTabBar()
{
    const QSignalBlocker bts(this);

    tabBar()->setChangeCurrentOnDrag(true);
    tabBar()->setUsesScrollButtons(true);

    // Add new-tab tab
    const int newTabIdx = QTabWidget::addTab(new QFrame(), Icons::get("tab-new"), "");
    setTabToolTip(newTabIdx, tr("<html><body>Add a new tab to the tab bar.</body></html>"));
}

void TabWidget::setupSignals()
{
    // Tab bar stuff
    connect(this, &TabWidget::tabCloseRequested, this, &TabWidget::tabCloseInteractive);
    connect(this, &TabWidget::tabBarDoubleClicked, this, &TabWidget::tabRenameInteractive);

    setContextMenuPolicy(Qt::CustomContextMenu);
    connect(this, &TabWidget::customContextMenuRequested, this, &TabWidget::showTabContextMenu);
    connect(this, &TabWidget::currentChanged, this, &TabWidget::tabChanged);
    connect(this, &TabWidget::tabBarClicked, this, &TabWidget::tabClicked);
    connect(this->tabBar(), &QTabBar::tabMoved, this, &TabWidget::tabMoved);

    // Actions:
    connect(actionBalanceTab, &QAction::triggered, this, &TabWidget::balanceTabInteractive);
    connect(actionAddTab, &QAction::triggered, this, &TabWidget::addTabInteractive);
    connect(actionRenameTab, &QAction::triggered, this, &TabWidget::tabRenameInteractive);
    connect(actionSetWindowTitle, &QAction::triggered, this, &TabWidget::setWindowTitleInteractive);
    connect(actionCloseTab, &QAction::triggered, this, &TabWidget::tabCloseInteractive);
    connect(actionNextTab, &QAction::triggered, this, &TabWidget::nextTab);
    connect(actionPrevTab, &QAction::triggered, this, &TabWidget::prevTab);
    connect(actionAlwaysOnTop, &QAction::toggled, this, &TabWidget::setAlwaysOnTop);
}

void TabWidget::setupMenus()
{
    if (isSecondaryWindow()) {
        setAlwaysOnTop(alwaysOnTop());  // can't do this at construction time.

        // Tab context menu
        m_tabMenu.addAction(actionAlwaysOnTop);
        m_tabMenu.addSeparator();
    }

    m_tabMenu.addAction(actionAddTab);
    m_tabMenu.addAction(actionRenameTab);
    m_tabMenu.addAction(actionCloseTab);

    if (isSecondaryWindow()) {
        m_tabMenu.addSeparator();
        m_tabMenu.addAction(actionSetWindowTitle);
    }

    m_tabMenu.addSeparator();
    m_tabMenu.addAction(actionBalanceTab);
    m_tabMenu.addSeparator();
    m_tabMenu.addAction(actionNextTab);
    m_tabMenu.addAction(actionPrevTab);
}

void TabWidget::closeEvent(QCloseEvent *event)
{
    if (appBase().cfgData().warnOnClose) {
        if (Util::WarningDialog(m_mainWindow, tr("Close"), tr("Close Window?"), this) == QMessageBox::Cancel) {
            event->ignore();
            return;
        }
    }

    const UndoWinCfg::ScopedUndo undoSet(m_mainWindow, tr("Close Secondary Window"));

    deleteTabs(true);

    event->accept();
}

void TabWidget::showEvent(QShowEvent* event)
{
    QTabWidget::showEvent(event);

    // At construction time, isWindow() always returns false, so we must set up menus which depend
    // upon that at show time instead.  setupMenus protects against multiple invocations.
    if (!addedActionsFlag) {
        addedActionsFlag = true; // do first: setAlwaysOnTop causes another showEvent
        setupActions();
        setupMenus();
        setupSignals();
        Util::SetupWhatsThis(this);
    }
}

void TabWidget::deleteTabs(bool later)
{
    // Remove old tabs. removeTab doesn't delete.  We must be careful here and
    // use deleteLater, or the pane destructors cause trouble on their way out.
    const QSignalBlocker bts(this);
    const QSignalBlocker mws(QGuiApplication::instance());

    // Leave the new-tab tab in place
    while (count() > 1) {
        QWidget* tab = widget(0);
        if (later) tab->deleteLater();
        removeTab(0);
        if (!later) delete tab;
    }

    m_mainWindow.m_prevFocus.clear();
}

void TabWidget::tabClose(int index)
{
    // TODO: use destructor to auto-delete from prevFocus
    m_mainWindow.m_prevFocus.clear();

    // Destructor will detach it from the tabbar
    delete widget(index);

    m_mainWindow.paneRefocus();
}

void TabWidget::tabRename(int index)
{
    if (index < 0)
        return;

    bool ok;
    const QString newName = QInputDialog::getText(this, actionRenameTab->text(),
                                                  tr("New tab name:"), QLineEdit::Normal,
                                                  tabText(index), &ok);
    if (!ok || newName.isEmpty())
        return;

    setTabText(index, newName);
}

void TabWidget::setSecondaryWindowTitle()
{
    bool ok;
    const QString newName = QInputDialog::getText(this, actionRenameTab->text(),
                                                  tr("New window title:"), QLineEdit::Normal,
                                                  windowTitle(), &ok);
    if (!ok || newName.isEmpty())
        return;

    setWindowTitle(newName);
}

void TabWidget::tabClicked(int index)
{
    // add new tab, if the new-tab tab was clicked on.
    if (index == (count() - 1))
        addTabInteractive();
}

void TabWidget::tabChanged(int index)
{
    if (index == (count() - 1))
        if (index > 0)
            setCurrentIndex(index - 1);
}

void TabWidget::tabMoved(int from, int to)
{
    const int last = (count() - 1);

    // Override attempts to move past the new-tab tab
    if (from == last)
        tabBar()->moveTab(to, from);
}

int TabWidget::addTab(const QString& name, QWidget* contents, int index, Qt::Orientation orientation)
{
    // Don't insert after the new-tab tab
    if (index < 0 || index >= (count()-1))
        index = count() - 1;

    auto* topContainer = dynamic_cast<PaneBase::Container*>(contents);
    auto* topPane = dynamic_cast<PaneBase*>(contents);
    int newIndex = -1;

    // Always use a container at the top level, even if given a Pane.
    if (topContainer == nullptr) {
        topContainer = m_mainWindow.containerFactory();
        topContainer->setOrientation(orientation);
        newIndex = insertTab(index, topContainer, name);

        if (topPane == nullptr)
            topPane = static_cast<PaneBase*>(m_mainWindow.paneFactory());

        m_mainWindow.addPane(topPane, topContainer);
    } else {
        newIndex = insertTab(index, topContainer, name);
    }

    if (newIndex >= 0)
        setCurrentIndex(newIndex);

    return newIndex;
}

void TabWidget::showTabContextMenu(const QPoint& pos)
{
    m_tabMenu.exec(mapToGlobal(pos));
}

void TabWidget::tabCloseInteractive()
{
    const QString undoName = actionCloseTab->text() + ": " + tabText(currentIndex());

    if (appBase().cfgData().warnOnClose)
        if (Util::WarningDialog(m_mainWindow, undoName,
                                tr("Closing the current tab will close all of its panes."),
                                this) != QMessageBox::Ok)
            return;

    const UndoWinCfg::ScopedUndo undoSet(m_mainWindow, undoName);

    tabClose(currentIndex());

    m_mainWindow.statusMessage(UiType::Info, tr("Closed tab."));
}

PaneBase::Container* TabWidget::currentTab() const
{
    return dynamic_cast<PaneBase::Container*>(currentWidget());
}

PaneBase::Container* TabWidget::currentTabWarn() const
{
    if (PaneBase::Container* tab = currentTab())
        return tab;

    m_mainWindow.statusMessage(UiType::Info, tr("No current tab.  Please create one and try again."));

    return nullptr;
}

void TabWidget::balanceTab() const
{
    if (PaneBase::Container* tab = currentTabWarn()) {
        QList<PaneBase::Container*> rows = tab->findChildren<PaneBase::Container*>(PaneBase::containerName);
        rows.append(tab); // also do the primary tab container

        for (auto& row : rows)
            balanceChildren(row);
    }
}

void TabWidget::balanceTabInteractive()
{
    const UndoWinCfg::ScopedUndo undoSet(m_mainWindow, actionBalanceTab->text() + ": " + tabText(currentIndex()));

    balanceTab();
}

void TabWidget::balanceChildren(PaneBase::Container* row)
{
    if (row == nullptr)
        return;

    QList<int> sizes;  // no constructor does this...
    sizes.reserve(row->count());

    for (int c = 0; c < row->count(); ++c) {
        sizes.push_back(65536);
        row->setStretchFactor(c, 100);
    }

    row->setSizes(sizes);
}

void TabWidget::tabRenameInteractive()
{
    const UndoWinCfg::ScopedUndo undoSet(m_mainWindow, actionRenameTab->text());

    tabRename(currentIndex());
}

void TabWidget::setWindowTitleInteractive()
{
    const UndoWinCfg::ScopedUndo undoSet(m_mainWindow, actionSetWindowTitle->text());

    setSecondaryWindowTitle();
}

void TabWidget::addTabInteractive()
{
    const UndoWinCfg::ScopedUndo undoSet(m_mainWindow, actionAddTab->text());

    addTab();
}

QAction* TabWidget::getPaneAction(PaneAction cc) const
{
    switch (cc) {
    case PaneAction::PaneBalanceTab:  return actionBalanceTab;
    case PaneAction::PaneAddTab:      return actionAddTab;
    case PaneAction::PaneRenameTab:   return actionRenameTab;
    case PaneAction::PaneCloseTab:    return actionCloseTab;
    default:                          return nullptr;
    }
}

bool TabWidget::isSecondaryWindow() const
{
    return isWindow() && window() != m_mainWindow.window();
}

bool TabWidget::alwaysOnTop() const
{
    return isSecondaryWindow() && actionAlwaysOnTop->isChecked();
}

void TabWidget::setAlwaysOnTop(bool onTop)
{
    if (!isSecondaryWindow())
        return;

    if (onTop) {
        window()->setWindowFlags(windowFlags() | Qt::Dialog);
    } else {
        window()->setWindowFlags((windowFlags() & ~Qt::Dialog) | Qt::Window);
    }

    actionAlwaysOnTop->setChecked(onTop);

    if (!appBase().testing()) // avoid show events under test suite
        show();
}

void TabWidget::save(QSettings& settings) const
{
    // Save tabs
    settings.beginWriteArray("tabs"); {
        for (int tab = 0; tab < count() - 1; ++tab) { // -1 due to new-tab tab.
            settings.setArrayIndex(tab);
            if (auto* topWidget = dynamic_cast<Settings*>(widget(tab))) {
                SL::Save(settings, "tabText", tabText(tab));
                topWidget->save(settings);
            }
        }
    } settings.endArray();

    // Save window position, if we're a top level window
    if (isWindow()) {
        SL::Save(settings, "geometry", saveGeometry());
        SL::Save(settings, "alwaysOnTop", alwaysOnTop());
    }

    // Save window title
    if (isSecondaryWindow()) {
        if (!windowTitle().isEmpty())
            SL::Save(settings, "windowTitle", windowTitle());
    }

    // Current tab index
    SL::Save(settings, "currentTab", currentIndex());
}

void TabWidget::load(QSettings& settings)
{
    const QSignalBlocker bts(this);

    deleteTabs();

    // Restore tabs
    const int tabCount = settings.beginReadArray("tabs"); {
        for (int tab = 0; tab < tabCount; ++tab) {
            settings.setArrayIndex(tab);

            // for backwards compatibility, load as integer
            QWidget* newPane = m_mainWindow.paneFactory(PaneClass_t(SL::Load(settings, "paneClass", -1)));
            if (auto* asSettings = dynamic_cast<Settings*>(newPane))
                asSettings->load(settings);

            addTab(SL::Load<QString>(settings, "tabText", "n/a"), newPane);
        }
    } settings.endArray();

    // Restore window position, if we're a top level window
    if (isWindow()) {
        restoreGeometry(settings.value("geometry").toByteArray());
        setAlwaysOnTop(SL::Load<bool>(settings, "alwaysOnTop", true));
    }

    // Save window title
    if (isSecondaryWindow()) {
        if (const auto title = SL::Load<QString>(settings, "windowTitle"); !title.isEmpty())
            setWindowTitle(title);
    }

    // Current tab index
    setCurrentIndex(SL::Load(settings, "currentTab", 0));
}

void TabWidget::nextTab()
{
    Util::NextTab(this, 1); // 1 reserved slot for the next-tab tab
}

void TabWidget::prevTab()
{
    Util::PrevTab(this);
}

