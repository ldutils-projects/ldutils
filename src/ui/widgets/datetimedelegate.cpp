/*
    Copyright 2019-2022 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <QDateTimeEdit>
#include <QTimeZone>
#include "datetimedelegate.h"

DateTimeDelegate::DateTimeDelegate(QObject* parent, const QString& format) :
    DelegateBase(parent, false),
    m_format(format)
{
}

QWidget* DateTimeDelegate::createEditor(QWidget* parent, const QStyleOptionViewItem& /*option*/, const QModelIndex& idx) const
{
    auto* editor = new QDateTimeEdit(parent);
    if (editor == nullptr)
        return nullptr;

    if (const auto* model = idx.model(); model != nullptr) {
        if (!m_format.isEmpty())
            editor->setDisplayFormat(m_format);

        editor->setDateTime(model->data(idx, role).toDateTime());
        editor->setCalendarPopup(true);
        editor->setFrame(false);
        editor->setAutoFillBackground(true);  // otherwise it ends up transparent
        editor->show();
    }

    return editor;
}

void DateTimeDelegate::setModelData(QWidget* editor, QAbstractItemModel* model, const QModelIndex& idx) const
{
    updateResultFromEditor(editor, model, idx, [&](bool& accepted) {
        auto* dateEditor = dynamic_cast<QDateTimeEdit*>(editor);
        if (dateEditor == nullptr)
            return QDateTime();

        accepted = true;
        return dateEditor->dateTime();
    });
}

