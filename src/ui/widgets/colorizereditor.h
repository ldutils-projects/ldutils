/*
    Copyright 2019-2022 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef COLORIZEREDITOR_H
#define COLORIZEREDITOR_H

#include <QWidget>
#include <QHeaderView>
#include <QMenu>

#include <src/ui/widgets/iconselectordelegate.h>
#include <src/ui/widgets/colordelegate.h>
#include <src/ui/widgets/comboboxdelegate.h>
#include <src/ui/widgets/querycompleterdelegate.h>

namespace Ui {
class ColorizerEditor;
} // namespace Ui

class ColorizerModel;
class QStringList;
class QTreeView;

class ColorizerEditor : public QWidget
{
    Q_OBJECT

public:
    explicit ColorizerEditor(ColorizerModel&, const QStringList&, QWidget *parent = nullptr);
    ~ColorizerEditor() override;

    void defaultColumnWidths();
    QTreeView*   treeView() const;

private slots:
    void showContextMenu(const QPoint&);
    void setVisible(bool visible) override;
    void on_action_Unset_Icon_triggered();
    void on_action_Unset_Background_Color_triggered();
    void on_action_Unset_Foreground_Color_triggered();
    void on_action_Remove_triggered();
    void on_action_Add_triggered();
    void enableMenus();

private:
    void setupActionIcons();
    void setupView();
    void setupDelegates();
    void setupMenu();
    void setupSignals();

    Ui::ColorizerEditor*   ui;
    ColorizerModel&        m_model;

    QHeaderView            m_headerView;
    QMenu                  m_contextMenu;

    ComboBoxDelegate       m_columnDelegate;
    ColorDelegate          m_colorDelegate;
    IconSelectorDelegate   m_iconDelegate;
    QueryCompleterDelegate m_queryDelegate;
};

#endif // COLORIZEREDITOR_H
