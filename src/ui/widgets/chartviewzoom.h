/*
    Copyright 2018-2023 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef CHARTVIEWZOOM_H
#define CHARTVIEWZOOM_H

#include <QPoint>
#include <QChartView>

class QWheelEvent;
class QMouseEvent;

class ChartViewZoom : public QtCharts::QChartView
{
    Q_OBJECT
public:
    ChartViewZoom(QWidget *parent = nullptr);

signals:
    void mousePan(QMouseEvent*, const QPoint& rel);   // LMB pan
    void mouseMove(QMouseEvent*);                     // normal mouse move
    void mouseEndPan();                               // end of panning
    void mousePress(QMouseEvent*);                    // for subclasses
    void mouseRelease(QMouseEvent*);                  // for subclasses

protected:
    void mousePressEvent(QMouseEvent* event) override;
    void mouseReleaseEvent(QMouseEvent* event) override;
    void mouseMoveEvent(QMouseEvent* event) override;
    void contextMenuEvent(QContextMenuEvent* event) override;

    QPoint m_orig;    // begin pan location
    QPoint m_prev;    // for generation of relative motion
    bool   m_pressed; // set in mouse press event, to test for panning
    bool   m_panned;  // true if there were any pan events
};

#endif // CHARTVIEWZOOM_H
