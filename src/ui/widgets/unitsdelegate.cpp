/*
    Copyright 2019-2022 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <QPainter>

#include "unitsdelegate.h"

UnitsDelegate::UnitsDelegate(QObject* parent, const Units& units, bool addDefault, int role) :
    DelegateBase(parent, false, "", true, role),
    units(units),
    addDefault(addDefault)
{
}

UnitsDelegate::~UnitsDelegate()
{
}

QWidget* UnitsDelegate::createEditor(QWidget* parent, const QStyleOptionViewItem& /*option*/, const QModelIndex& idx) const
{
    auto* editor = new QComboBox(parent);
    if (editor == nullptr)
        return nullptr;

    if (const auto* model = idx.model(); model != nullptr) {
        if (addDefault)
            editor->addItem(tr("Default"));

        units.addToComboBox(editor);
        editor->setCurrentText(model->data(idx, role).toString());
        editor->setFrame(false);
        editor->setAutoFillBackground(true);  // otherwise it ends up transparent
    }

    return editor;
}

void UnitsDelegate::setModelData(QWidget* editor, QAbstractItemModel* model, const QModelIndex& idx) const
{
    updateResultFromEditor(editor, model, idx, [&](bool& accepted) {
        auto* comboBox = dynamic_cast<QComboBox*>(editor);
        if (comboBox == nullptr)
            return QString();

        accepted = true;
        if (addDefault && comboBox->currentIndex() == 0)
            return QString(); // means to use the default

        return comboBox->currentText();
    });
}
