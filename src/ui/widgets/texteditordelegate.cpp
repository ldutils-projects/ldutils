/*
    Copyright 2019-2022 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <QAbstractItemModel>
#include <QTemporaryFile>

#include <src/util/util.h>
#include <src/ui/dialogs/texteditordialog.h>
#include "texteditordelegate.h"

TextEditorDelegate::TextEditorDelegate(QObject* /*parent*/,
                                       const QString& winTitle, bool winBorders, int role) :
    DelegateBase(nullptr, true, winTitle, winBorders, role)
{
    // Util::SetupWhatsThis(this); not needed for delegate
}

TextEditorDelegate::~TextEditorDelegate()
{
}

QWidget* TextEditorDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem& /*option*/, const QModelIndex& idx) const
{
    TextEditorDialogBase* editor = loadSettings(dialogFactory(parent));
    if (editor == nullptr)
        return nullptr;

    if (const auto* model = idx.model(); model != nullptr) {
        setPopup(editor);
        editor->setHtml(model->data(idx, role).value<QString>());
    }

    return editor;
}

void TextEditorDelegate::setModelData(QWidget* editor, QAbstractItemModel* model, const QModelIndex& idx) const
{
    updateResultFromEditor(editor, model, idx, [&](bool& accepted) {
        auto* dialog = dynamic_cast<TextEditorDialogBase*>(editor);
        if (dialog == nullptr)
            return QString();

        saveSettings(dialog);
        accepted = (dialog->result() == QDialog::Accepted);
        return dialog->toHtml();
    });
}

TextEditorDialogBase* TextEditorDelegate::dialogFactory(QWidget *parent) const
{
    return new TextEditorDialog(parent);
}

void TextEditorDelegate::saveSettings(const TextEditorDialogBase* dialog) const
{
    // Save some UI state before closing.  Slightly awkward since QSettings is tied to a file,
    // meaning we can't just store one as intance data in memory.
    QTemporaryFile tmpSettingsFile;
    if (!tmpSettingsFile.open())
        return;

    {
        // In scope so QSettings flushes on destruction
        QSettings tmpSettings(tmpSettingsFile.fileName(), QSettings::IniFormat);
        dialog->save(tmpSettings);
    }

    m_settings = Util::ReadFile<QByteArray>(tmpSettingsFile.fileName());
}

TextEditorDialogBase* TextEditorDelegate::loadSettings(TextEditorDialogBase* dialog) const
{
    if (dialog == nullptr)
        return nullptr;

    // Load some UI state before closing.  Slightly awkward since QSettings is tied to a file,
    // meaning we can't just store one as intance data in memory.
    QTemporaryFile tmpSettingsFile;
    if (!tmpSettingsFile.open())
        return dialog;

    tmpSettingsFile.write(m_settings);
    tmpSettingsFile.close();

    QSettings tmpSettings(tmpSettingsFile.fileName(), QSettings::IniFormat);
    dialog->load(tmpSettings); // load deferred settings

    return dialog;
}

void TextEditorDelegate::save(QSettings& settings) const
{
    SL::Save(settings, "dialogSettings", m_settings);
}

void TextEditorDelegate::load(QSettings& settings)
{
    SL::Load(settings, "dialogSettings", m_settings);
}
