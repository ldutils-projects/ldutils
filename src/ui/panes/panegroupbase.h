/*
    Copyright 2019-2022 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef PANEGROUPBASE_H
#define PANEGROUPBASE_H

#include <QSplitter>
#include <src/core/settings.h>
#include <src/fwddeclbase.h>

class MainWindowBase;

class PaneGroupBase : public QSplitter, public Settings
{
public:
    PaneGroupBase(MainWindowBase& mainWindow, QWidget* parent = nullptr) :
        QSplitter(parent),
        mainWindow(mainWindow)
    { }

    PaneGroupBase(MainWindowBase& mainWindow, Qt::Orientation orientation, QWidget* parent = nullptr) :
        QSplitter(orientation, parent),
        mainWindow(mainWindow)
    { }

protected:
    // These are templates, because the loader needs a static type to get to the factory, which
    // is derivation-dependent since it depends on classes of the consumer of the library.

    // *** begin Settings API
    void saveUiConfig(QSettings&, PaneClass_t groupPaneClass) const;
    void loadUiConfig(QSettings&, PaneClass_t defaultPaneClass);
    // *** end Settings API

    MainWindowBase& mainWindow;

private:
    // just to provide an out-of-line virtual method, or the vtable gets generated
    // in every module.
    virtual void dummy() const;
};

#endif // PANEGROUPBASE_H
