/*
    Copyright 2021-2023 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef DATACOLUMNPANEBASE_INL_H
#define DATACOLUMNPANEBASE_INL_H

#include "datacolumnpanebase.h"

template <class MD> void DataColumnPaneBase::setupShowColumnComboBox(const DefColumns& defColumns)
{
    if (m_treeView == nullptr || m_showColumn == nullptr)
        return;

    // Populate combobox for column selection
    m_showColumnModel.appendRow(new QStandardItem(QObject::tr("Show Columns")));

    ModelMetaData::setupComboBox<MD>(*m_showColumn, m_showColumnModel, nullptr,
                                     [this, &defColumns](ModelType mt, QStandardItem* item) {
        const bool show = defColumns.defaultShown(mt);
        item->setFlags(Qt::ItemIsUserCheckable | Qt::ItemIsEnabled);
        item->setCheckState(show ? Qt::Checked : Qt::Unchecked);
        m_treeView->setColumnHidden(mt, !show);  // since we haven't connected the controls yet
        return item;
    });

    // connect column select behavior
    connect(&m_showColumnModel, &QStandardItemModel::itemChanged, this, &DataColumnPaneBase::toggleColumnInteractive);
}

template <class MD> void DataColumnPaneBase::setupFilterColumn()
{
    if (m_filterColumn == nullptr)
        return;

    // Populate combobox for column selection
    m_filterColumnModel.appendRow(new QStandardItem(filterAllHeader));

    ModelMetaData::setupComboBox<MD>(*m_filterColumn, m_filterColumnModel);

    connect(m_filterColumn, static_cast<void (QComboBox::*)(int)> (&QComboBox::currentIndexChanged),
            this, &DataColumnPaneBase::setFilterColumn);

    setFilterColumn(0);
}

template <class MD> void DataColumnPaneBase::setWidgets(const DefColumns& defColumns,
                                                        QLineEdit *ft, QComboBox* fc, QComboBox* sc,
                                                        QWidget* fb, QLabel* fv)
{
    m_filterText    = ft;
    m_filterColumn  = fc;
    m_showColumn    = sc;
    paneFilterBar   = fb;
    m_filterValid   = fv;

    setupLineEdit();
    setupCompleter();
    setupFilterColumn<MD>();
    showDefaultColumns(defColumns);
    setupShowColumnComboBox<MD>(defColumns);
    setupFilterStatusIcons();
    showFilterStatusIcon();

    if (m_showColumn != nullptr) {
        m_showColumn->setToolTip(QObject::tr("<html><head/><body>Select display columns.</body></html>"));
        m_showColumn->setWhatsThis(m_showColumn->toolTip());
    }

    if (m_filterColumn != nullptr) {
        m_filterColumn->setToolTip(QObject::tr("<html><head/><body>Default search column, if not specified in query.</body></html>"));
        m_filterColumn->setWhatsThis(m_filterColumn->toolTip());
    }

    // Resize widgets to some sane defaults
    if (auto* splitter = dynamic_cast<QSplitter*>(paneFilterBar); splitter != nullptr) {
        for (int child = 0; child < splitter->count(); ++child) {
            const QWidget* widget = splitter->widget(child);
            if (dynamic_cast<const QLineEdit*>(widget) != nullptr ||
                widget->findChild<QLineEdit*>() != nullptr)
                splitter->setStretchFactor(child, 80);
            else if (dynamic_cast<const QComboBox*>(widget) != nullptr)
                splitter->setStretchFactor(child, 10);
            else if (dynamic_cast<const QToolButton*>(widget) != nullptr)
                splitter->setStretchFactor(child, 1);
        }
    }
}

template <class P> auto DataColumnPaneBase::DisableSort(const QObject& mainWindow)
{
    QList<SortDisabler> ds;
    for (auto& pane : mainWindow.findChildren<P*>())
        ds.append(pane);
    return ds;
}

#endif // DATACOLUMNPANEBASE_INL_H
