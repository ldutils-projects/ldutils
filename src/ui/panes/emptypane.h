/*
    Copyright 2019-2022 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef EMPTYPANE_H
#define EMPTYPANE_H

#include "panebase.h"

namespace Ui {
class EmptyPane;
} // namespace Ui

class EmptyPane final : public PaneBase
{
    Q_OBJECT

public:
    EmptyPane(MainWindowBase& mainWindow, PaneClass_t paneClass, QWidget* parent = nullptr);
    ~EmptyPane() override;

    [[nodiscard]] QString name() const override;

private slots:
    void on_EmptyPane_toggled(bool checked) { paneToggled(checked); }

private:
    Ui::EmptyPane *ui;
};

#endif // EMPTYPANE_H
