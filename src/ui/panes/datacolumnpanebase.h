/*
    Copyright 2019-2023 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef DATACOLUMNPANEBASE_H
#define DATACOLUMNPANEBASE_H

#include <tuple>
#include <QHeaderView>
#include <QMenu>
#include <QStandardItemModel>
#include <QTreeView>
#include <QSet>
#include <QList>
#include <QPoint>
#include <QTimer>
#include <QToolButton>
#include <QPair>

#include <src/core/modelmetadata.h>
#include <src/core/modelmetadata.inl.h>
#include <src/ui/filters/flattenfilter.h>
#include <src/ui/misc/querybar.h>
#include "panebase.h"

// Intermediate class for panes which show columns of data.  Provides methods to
// hide/sort/etc columns.  Has no UI of its own.

class QItemSelectionModel;
class QStandardItem;
class QLineEdit;
class QComboBox;
class QLabel;
class MainWindowBase;
class DelegateBase;
class IconSelector;

class DataColumnPaneBase :
        public PaneBase,
        public QueryBar
{
    Q_OBJECT

public:
    DataColumnPaneBase(MainWindowBase&, PaneClass_t paneClass, QWidget* parent = nullptr, bool m_useFlattener = true);
    ~DataColumnPaneBase() override;

    [[nodiscard]] bool hasSelection() const override;
    [[nodiscard]] bool isSelected(const QModelIndex& idx) const override;
    [[nodiscard]] bool hasItems() const override;
    using PaneBase::getSelections;
    [[nodiscard]] QModelIndexList getSelections() const override;
    [[nodiscard]] virtual QModelIndexList getAllIndexes(int column) const;
    [[nodiscard]] QItemSelectionModel* selectionModel() const override;
    void sort() override;
    virtual void select(const QModelIndexList&, QItemSelectionModel::SelectionFlags =
            QItemSelectionModel::SelectCurrent | QItemSelectionModel::Rows);
    virtual bool select(const QModelIndex& modelIdx, QItemSelectionModel::SelectionFlags =
            QItemSelectionModel::SelectCurrent | QItemSelectionModel::Rows);

    // Copy selection from an foreign QItemSelectionModel, by mapping indexes down and back up
    virtual void select(const QItemSelectionModel&, QItemSelectionModel::SelectionFlags = QItemSelectionModel::ClearAndSelect);

    // Put this on the stack to disable sorting, and re-enable on its destruction.
    class SortDisabler {
    public:
        SortDisabler(DataColumnPaneBase* dcp) : dcp(dcp), sorted(dcp->m_treeView->isSortingEnabled()) { }
        ~SortDisabler() { dcp->m_treeView->setSortingEnabled(sorted); }
    private:
        DataColumnPaneBase* dcp;
        bool                sorted;
    };

    template <class P> static auto DisableSort(const QObject&);

    void showAll() override;
    void expandAll() override;
    void collapseAll() override;
    void selectAll() override;
    void selectNone() override;
    void resizeToFit(int defer = -1) override;
    void copySelected() const override;
    void newConfig() override;
    void viewAsTree(bool) override;
    bool viewIsTree() const override;
    void setQuery(const QString& query) override;
    [[nodiscard]] QString getQuery() const override;

    virtual void showAllColumns();
    virtual void refreshFilter();

protected:
    friend class TestZtgps;

    // *** begin Settings API
    void save(QSettings&) const override;
    void load(QSettings&) override;
    // *** end Settings API

    virtual void setIcon(ModelType mt);   // prompt for icon and set the IconNameRole
    virtual void clearIcon(ModelType mt); // clear existing icon

    void postLoadHook() override; // things we can only do after the model data is loaded.

protected slots:
    virtual void setFilterColumn(int);
    virtual void filterTextChanged(const QString&);
    virtual void toggleColumnInteractive(QStandardItem*);
    virtual void hideHeaderColumnInteractive();
    virtual void hideOtherColumnsInteractive();
    virtual void showDefaultColumnsInteractive();
    virtual void showHeaderContextMenu(const QPoint&);
    virtual void showAllColumnsInteractive();
    virtual void handleRowsInserted(const QModelIndex &parent, int first, int last);

protected:
    // This object can be pushed on the stack to remember column widths across some operation
    // that will otherwise change them, such as a model reset.
    class SaveColumnWidths {
    public:
        SaveColumnWidths(DataColumnPaneBase* dp);
        ~SaveColumnWidths();
    private:
        DataColumnPaneBase* dp;
        QByteArray state;
    };

    // Map columns to an order, and shown state. This must be a vector, because order matters.
    struct DefColumns : public QVector<QPair<ModelType, bool>> {
        DefColumns(std::initializer_list<QPair<ModelType, bool>> args);

        int  findData(ModelType) const; // returns -1 if not found, else position.
        bool defaultShown(ModelType) const;
    };

    virtual const DefColumns& defColumnView() const = 0;   // return default column visual order
    void setupView(QTreeView* view, QAbstractItemModel*);
    template <class MD> void setWidgets(const DefColumns&, QLineEdit* ft,
                                        QComboBox* fc, QComboBox* sc, QWidget* fb = nullptr,
                                        QLabel* fv = nullptr);

    void initDelegates(const QVector<std::tuple<ModelType, DelegateBase*>>&);

    void setupSignals(); // not virtual: called from constructors where virt dispatch can't work
    void reenableChildren() override;

    virtual int  columnCount() const;
    virtual void setColumnHidden(int column, bool hidden);
    virtual void setColumnHiddenInteractive(int column, bool hidden);  // creates undo pt
    virtual void hideOtherColumns();
    virtual void showDefaultColumns();
    virtual void moveSection(int from, int to); // see c++ comment
    virtual void setSort(int logicalIndex, Qt::SortOrder); // see c++ comment
    virtual int  visualIndex(int logicalIndex) const;
    virtual void insertColumnOnLoad(uint32_t addedInVersion, int col, int count); // see c++ comment

    // We share icon selectors between panes, since they are expensive to create and only
    // one is needed at a time since they are modal.
    virtual const QStringList& iconSelectorPaths()   const; // subclasses can override
    virtual IconSelector*      iconSelectorFactory() const; // make a new one: no testing needed
    virtual IconSelector*      iconSelector();              // return old or create new from factory

    // Ease of access to column name
    virtual QString columnName(int column) const {
        return m_baseModel->headerData(column, Qt::Horizontal).toString();
    }

    void refreshHiddenColumns();
    [[nodiscard]] QModelIndex clickPosIndex(const QPoint& pos) const;

    void setSourceModel(QAbstractItemModel*);
    void setFlattenPredicate(const FlattenFilter::PredFn& pred);

    static const QString filterAllHeader;   // text for filter-all

    QTreeView*          m_treeView;         // primary treeview

private slots:
    void handleSectionMoved(int logicalIndex, int oldVisualIndex, int newVisualIndex);
    void handleSortChanged(int logicalIndex, Qt::SortOrder);

private:
    // Smaller hammer than the Qt one.
    struct SignalBlocker {
        SignalBlocker(DataColumnPaneBase& p) : p(p) {
            if (nest++ == 0) {
                p.setupSectionMoveSignal(false);
                p.setupSectionSortSignal(false);
            }
        }
        ~SignalBlocker() {
            if (--nest == 0) {
                p.setupSectionMoveSignal(true);
                p.setupSectionSortSignal(true);
            }
        }
        DataColumnPaneBase& p;
        static int nest;
    };

    friend class UndoPaneSectionMove;
    friend class UndoPaneSetColumnHidden;
    friend class UndoPaneSort;

    void setupTimers();
    void setupHeaderMenus();
    void setupLineEdit();
    void setupCompleter();
    void setupSectionMoveSignal(bool install);
    void setupSectionSortSignal(bool install);
    void resizeDeferredHook();
    void dataAddedDeferredHook();
    void showDefaultColumns(const DefColumns&);  // initialize default columns from given set
    void saveSortData();
    void updateFilter(const QString&) override;  // update filter, but don't pass to subclasses
    [[nodiscard]] bool validHeaderIndex(int pos) const;

    template <class MD> void setupShowColumnComboBox(const DefColumns&);
    template <class MD> void setupFilterColumn();

    FlattenFilter       m_flattenFilter;     // flatten tree views
    QHeaderView         m_headerView;        // header view
    QLineEdit*          m_filterText;        // filter text gadget, or nullptr if none
    QComboBox*          m_filterColumn;      // filter column, or nullptr if none
    QComboBox*          m_showColumn;        // shown column combobox, or nullptr if none
    QStandardItemModel  m_showColumnModel;   // for show-column combo box
    QStandardItemModel  m_filterColumnModel; // for fiilter-column combo box
    QAbstractItemModel* m_baseModel;         // underlying model
    QMenu               m_headerMenu;        // header context menu
    int                 m_headerMenuIndex;   // index of header column right-clicked
    QTimer              m_resizeTimer;       // avoid resize spam
    QTimer              m_dataAddedTimer;    // triggers after new data is added to base model
    QTimer              m_queryChangedTimer; // timer for query filter changes
    bool                m_useFlattener;      // false to disable flatten filter
    bool                m_filterDirty;       // need to re-run filter (e.g, on data addition)
    QVector<int>        m_savedCurrentIndex; // for reenabling active item after load
    Qt::SortOrder       m_prevSortOrder;     // for creating undos
    int                 m_prevSortColumn;    // ...
};

#endif // DATACOLUMNPANEBASE_H
