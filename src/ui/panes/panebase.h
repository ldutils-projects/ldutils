/*
    Copyright 2019-2023 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef PANEBASE_H
#define PANEBASE_H

#include <QMenu>
#include <QGroupBox>
#include <QModelIndexList>
#include <QLayout>
#include <QTimer>

#include <src/fwddeclbase.h>
#include <src/ui/panes/panegroupbase.h>
#include <src/core/settings.h>

class QItemSelectionModel;
class MainWindowBase;
class QSplitter;
class QLayout;
class QDragEnterEvent;
class QDropEvent;

enum class PaneAction {
    // commands on data in the pane
    _Begin = 0,
    _BeginData = _Begin,
    ShowAll = _BeginData,
    ExpandAll,
    CollapseAll,
    SelectAll,
    SelectNone,
    ResizeToFit,
    SetFiltersVisible,
    ViewAsTree,
    CopySelected,
    _EndData,

    // commands on the pane itself
    _BeginPane = _EndData,
    PaneAdd    = _BeginPane,
    PaneAddGroup,
    PaneReplace,
    PaneSplitH,
    PaneSplitV,
    PaneClose,
    PaneLeft,
    PaneRight,
    PaneRowUp,
    PaneRowDown,
    PaneBalanceSiblings,
    PaneBalanceTab,
    PaneAddTab,
    PaneRenameTab,
    PaneCloseTab,
    _EndPane,
    _Count = _EndPane
};

// Class for operations provided by UI panels
class PaneBase : public QGroupBox, public Settings
{
    Q_OBJECT

public:
    using Superclass = QGroupBox;
    using Container  = PaneGroupBase;

    PaneBase(MainWindowBase& mainWindow, PaneClass_t paneClass, QWidget* parent);
    ~PaneBase() override;

    // *** begin Pane API
    virtual void showAll() { }
    virtual void expandAll() { }
    virtual void collapseAll() { }
    virtual void selectAll() { }
    virtual void selectNone() { }
    virtual void resizeToFit(int /*defer*/ = -1) { }
    virtual void setFiltersVisible(bool);
    virtual void copySelected() const { }
    virtual void focusIn() { }
    virtual void focusOut() { }
    virtual void setQuery(const QString&) { }
    [[nodiscard]] virtual QString getQuery() const { return ""; }

    virtual void sort() { } // trigger resort, to avoid doing it on every data change

    [[nodiscard]] virtual bool isSelected(const QModelIndex&) const { return false; }
    [[nodiscard]] virtual bool hasSelection() const { return false; }
    [[nodiscard]] virtual bool hasItems() const { return true; }
    // Get all selections
    [[nodiscard]] virtual QModelIndexList getSelections() const;
    // Get selections matching predicate
    [[nodiscard]] virtual QModelIndexList getSelections(const std::function<bool(const QModelIndex&)>&) const;
    [[nodiscard]] virtual QItemSelectionModel* selectionModel() const { return nullptr; }
    virtual void newConfig() { }
    [[nodiscard]] virtual bool supportsAction(PaneAction) const { return true; }
    [[nodiscard]] virtual QString name() const = 0;

    virtual void viewAsTree(bool /*asTree*/) { }
    [[nodiscard]] virtual bool viewIsTree() const { return false; }
    virtual void paneToggled(bool checked);

    virtual PaneBase* clone() const;
    // *** end Pane API

    [[nodiscard]] static bool needsFocus(PaneAction);
    [[nodiscard]] static bool separatorAfter(PaneAction pa);

    static const constexpr char* containerName = "PaneContainer";

    // *** begin Settings API
    void save(QSettings&) const override;
    void load(QSettings&) override;
    // *** end Settings API

    virtual void postLoadHook(); // things we can only do after the model data is loaded.

    PaneId_t    paneId()    const { return m_paneId; }  // query unique Id
    PaneClass_t paneClass() const { return m_thisClass; } // query pane class

protected slots:
    virtual void showPaneContextMenu(const QPoint &pos);
    virtual void replacePane();

protected:
    friend class UndoPaneBase;
    friend class UndoPaneState;

    const MainWindowBase& mainWindow() const { return m_mainWindow; }
    const MainWindowBase& constMainWindow() { return m_mainWindow; }
    MainWindowBase& mainWindow() { return m_mainWindow; }

    void setPaneFilterBar(QWidget* fb) { paneFilterBar = fb; }
    void setPaneFilterBar(QLayout* fb) { m_paneFilterLayout = fb; }
    void setFiltersVisible(bool visible, QLayout*);
    void setFiltersVisible(bool visible, QSplitter*);
    void setFiltersVisible(bool visible, QWidget*);
    void disableToolTipsFor(QObject* child);

    bool eventFilter(QObject* obj, QEvent* event) override;

    virtual void reenableChildren();
    virtual void addContextMenuRange(QMenu* menu, PaneAction begin, PaneAction end);
    virtual void setupPaneHeaderContextMenus();
    virtual void setupActionContextMenu(QMenu& contextMenu);

    void mousePressEvent(QMouseEvent* event) override;
    void dragEnterEvent(QDragEnterEvent*) override;
    void dropEvent(QDropEvent*) override;
    void showEvent(QShowEvent*) override;

    void setupViewContextMenu(QMenu& contextMenu);
    void setupPaneContextMenu(QMenu& contextMenu);

    MainWindowBase&  m_mainWindow;       // main window reference
    QMenu            paneMenu;           // for pane operations (add/remove/etc)
    QWidget*         paneFilterBar;      // pane's filter bar area if widget, or nullptr
    QLayout*         m_paneFilterLayout; // pane's filter bar area if layout, or nullptr
    QObject*         m_contentArea;      // for hiding tooltips
    PaneClass_t      m_thisClass;        // pane class ID
    PaneId_t         m_paneId;           // unique ID
    QTimer           m_replacePaneTimer; // see comment in PaneBase::dropEvent implementation
    PaneClass_t      m_replaceWith;      // for replacing the pane on a QDropEvent
    PaneClass_t      m_addTo;            // for adding new planes on a QDropEvent
    PaneClass_t      m_newWin;           // for adding panes to new windows on a QDropEvent

    template <typename T> void deleteUI(T& ui) {
        delete ui;
        ui = nullptr;
    }

private:
    void setupTimers();

    PaneBase(const PaneBase&) = delete;
    PaneBase& operator=(const PaneBase&) = delete;
};

#endif // PANEBASE_H
