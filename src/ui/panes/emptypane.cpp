/*
    Copyright 2019-2021 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "src/util/ui.h"

#include "emptypane.h"
#include "ui_emptypane.h"

EmptyPane::EmptyPane(MainWindowBase& mainWindow, PaneClass_t paneClass, QWidget* parent) :
    PaneBase(mainWindow, paneClass, parent),
    ui(new Ui::EmptyPane)
{
    ui->setupUi(this);

    setPaneFilterBar(ui->infoText);  // for show/hide
    setupPaneHeaderContextMenus();
    Util::SetupWhatsThis(this);
}

EmptyPane::~EmptyPane()
{
    delete ui;
}

QString EmptyPane::name() const
{
    // Slight kludge: since this lives in ldutils, and pane names belong to derived classes, we simply
    // assume the name. It's just for display purposes anyway.
    return tr("Empty Pane");
}
