/*
    Copyright 2019-2022 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <src/ui/windows/mainwindowbase.h>
#include "panegroupbase.h"

void PaneGroupBase::saveUiConfig(QSettings& settings, const PaneClass_t groupPaneClass) const
{
    SL::Save(settings, "paneClass", PaneId_t::base_type(groupPaneClass)); // for backwards compatibility, save as int

    // Save children
    settings.beginWriteArray("children"); {
        for (int c = 0; c < count(); ++c) {
            settings.setArrayIndex(c);
            if (const auto* child = dynamic_cast<Settings*>(widget(c)))
                child->save(settings);
        }
    } settings.endArray();

    SL::Save(settings, "geometry", saveGeometry());
    SL::Save(settings, "state",    saveState());
}

void PaneGroupBase::loadUiConfig(QSettings& settings, const PaneClass_t defaultPaneClass)
{
    // Load children
    const int childCount = settings.beginReadArray("children"); {
        for (int c = 0; c < childCount; ++c) {
            settings.setArrayIndex(c);

            // for backwards compatibility, load base type
            const PaneClass_t::base_type defaultPaneClassBase(defaultPaneClass);
            const PaneClass_t pc(SL::Load(settings, "paneClass", defaultPaneClassBase));

            if (QWidget* child = mainWindow.paneFactory(pc); child != nullptr) {
                addWidget(child);
                if (auto* setChild = dynamic_cast<Settings*>(child); setChild != nullptr)
                    setChild->load(settings);
            }
        }
    } settings.endArray();

    // TODO: this is a workaround for a problem where the saved geometry is too large after
    // loading.  This needs a real fix.
    setMinimumSize(QSize(150, 150));

    if (settings.contains("geometry"))
        restoreGeometry(settings.value("geometry").toByteArray());
    if (settings.contains("state"))
        restoreState(settings.value("state").toByteArray());
}

void PaneGroupBase::dummy() const
{
}
