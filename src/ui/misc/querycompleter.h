/*
    Copyright 2019-2022 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef QUERYCOMPLETER_H
#define QUERYCOMPLETER_H

#include <QStandardItemModel>
#include <QCompleter>

class QString;
class QStringList;
class QLineEdit;

namespace Query {
class Context;
} // namespace Query

class QueryCompleter : public QCompleter
{
    Q_OBJECT

public:
    QueryCompleter(const Query::Context& m_ctx, QLineEdit* parent);

    using QCompleter::QCompleter;

    [[nodiscard]] QString pathFromIndex(const QModelIndex& index) const override;
    [[nodiscard]] QStringList splitPath(const QString& path) const override;

private:
    const Query::Context& m_ctx;
    QStandardItemModel    m_completionModel;   // for completer
    mutable int           m_completionStart;   // start of completion substring
};

#endif // QUERYCOMPLETER_H
