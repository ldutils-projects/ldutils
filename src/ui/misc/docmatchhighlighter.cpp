/*
    Copyright 2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <QString>

#include "src/core/appbase.h"

#include "docmatchhighlighter.h"

DocMatchHighlighter::DocMatchHighlighter(QTextDocument *parent) :
    QSyntaxHighlighter(parent)
{
    setupFormat();
}

void DocMatchHighlighter::setupFormat()
{
    m_matchFormat.setForeground(appBase().cfgData().uiColor[UiType::Match]);
    m_matchFormat.setFontWeight(QFont::Bold);
}

void DocMatchHighlighter::setPatterns(const PatternList& patterns)
{
    m_patterns = patterns;
}

void DocMatchHighlighter::clearPatterns()
{
    m_patterns.clear();
}

void DocMatchHighlighter::newConfig()
{
    setupFormat(); // reset the highlight format
}

void DocMatchHighlighter::highlightBlock(const QString& text)
{
    for (const auto& re : m_patterns) {
        for (QRegularExpressionMatchIterator it = re.globalMatch(text); it.hasNext(); ) {
            QRegularExpressionMatch match = it.next();

            setFormat(match.capturedStart(), match.capturedLength(), m_matchFormat);
        }
    }
}
