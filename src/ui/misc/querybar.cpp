/*
    Copyright 2021-2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <QLabel>

#include <src/core/appbase.h>

#include "querybar.h"

QueryBase::QueryBase(SubTreeFilter& subtreeFilter) :
    m_subtreeFilter(subtreeFilter),
    m_filterValid(nullptr)
{
}

void QueryBase::setup(QAbstractItemModel* model, bool dynamic, int role)
{
    setSourceModel(model);
    newConfig();
    m_subtreeFilter.setDynamicSortFilter(dynamic);
    m_subtreeFilter.setFilterRole(role);  // from copy-formatted data
}

void QueryBase::setupFilterStatusIcons()
{
    if (m_filterValid != nullptr) {
        const QSize iconSize = m_filterValid->minimumSize();

        m_filterEmptyIcon   = cfgDataBase().filterEmptyIcon.pixmap(iconSize);
        m_filterValidIcon   = cfgDataBase().filterValidIcon.pixmap(iconSize);
        m_filterInvalidIcon = cfgDataBase().filterInvalidIcon.pixmap(iconSize);
    }

    showFilterStatusIcon();
}

bool QueryBase::isEmptyQuery() const
{
    return m_subtreeFilter.isEmpty();
}

bool QueryBase::isValidQuery() const
{
    return m_subtreeFilter.isValid();
}

void QueryBase::showFilterStatusIcon() const
{
    if (m_filterValid != nullptr)
        m_filterValid->setPixmap(isEmptyQuery() ? m_filterEmptyIcon :
                                                  isValidQuery() ? m_filterValidIcon : m_filterInvalidIcon);
}

void QueryBase::newConfig()
{
    setupFilterStatusIcons();
    m_subtreeFilter.setCaseSensitivity(appBase().cfgData().getFilterCaseSensitivity());
    m_subtreeFilter.setSortCaseSensitivity(appBase().cfgData().getSortCaseSensitivity());
}

void QueryBase::setSourceModel(QAbstractItemModel* model)
{
    m_subtreeFilter.setSourceModel(model);
}

void QueryBase::sort()
{
    m_subtreeFilter.sort(m_subtreeFilter.sortColumn(), m_subtreeFilter.sortOrder());
}

void QueryBase::updateFilter(const QString& query)
{
    m_subtreeFilter.setQueryString(query);
    showFilterStatusIcon();
}

void QueryBase::copySelected(QTreeView* treeView, QItemSelectionModel* selectionModel) const
{
    if (treeView == nullptr)
        return;

    m_subtreeFilter.copyToClipboard(*treeView,
                                    m_subtreeFilter.sortedSelection(selectionModel),
                                    appBase().cfgData().rowSeparator, appBase().cfgData().colSeparator,
                                    Util::CopyRole);
}

QueryBar::QueryBar(QObject* parent) :
    QueryBase(m_subtreeFilter),
    m_subtreeFilter(parent)
{
}
