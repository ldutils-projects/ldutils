/*
    Copyright 2021-2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef FILTERBASE_H
#define FILTERBASE_H

#include <QPixmap>

#include <src/ui/filters/subtreefilter.h>

class QLabel;
class QTreeView;

// Common functionality to support text queries and validity icons.
class QueryBase
{
public:
    QueryBase(SubTreeFilter&);

    virtual void newConfig();

protected:
    friend class TestZtgps; // test hook

    virtual QSortFilterProxyModel& topFilter() { return m_subtreeFilter; }
    virtual const QSortFilterProxyModel& topFilter() const { return m_subtreeFilter; }

    void setup(QAbstractItemModel* model, bool dynamic, int role);
    void setupFilterStatusIcons();
    void showFilterStatusIcon() const;
    void setSourceModel(QAbstractItemModel*);
    [[nodiscard]] virtual bool isEmptyQuery() const;
    [[nodiscard]] virtual bool isValidQuery() const;

    virtual void sort();
    virtual void updateFilter(const QString&);
    void copySelected(QTreeView*, QItemSelectionModel*) const;

    SubTreeFilter& m_subtreeFilter;     // filter with subtree searching
    QLabel*        m_filterValid;       // filter validity indicator
    QPixmap        m_filterEmptyIcon;   // emblem for empty filter
    QPixmap        m_filterValidIcon;   // emblem for valid filter
    QPixmap        m_filterInvalidIcon; // emblem for invalid filter
};

class QueryBar : public QueryBase
{
public:
    QueryBar(QObject* parent);

protected:
    SubTreeFilter  m_subtreeFilter;     // filter with subtree searching
};
#endif // FILTERBASE_H
