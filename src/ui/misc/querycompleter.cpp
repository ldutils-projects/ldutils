/*
    Copyright 2019-2022 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "querycompleter.h"

#include <QLineEdit>
#include <QStringList>

#include <src/core/appbase.h>
#include <src/core/query.h>
#include <src/util/qtcompat.h>

QueryCompleter::QueryCompleter(const Query::Context& ctx, QLineEdit* parent) :
    QCompleter(parent),
    m_ctx(ctx),
    m_completionStart(-1)
{
    const auto& headers = ctx.headerNames();
    const int colCount = headers.size();

    // Set up two level hierarchy:
    // -- Column Name
    //  \-- Unit suffix(es)
    for (int col = 0; col < colCount; ++col) {
        auto* colNameItem = new QStandardItem(headers.at(col));

        if (const Units* units = ctx.units(col); units != nullptr && colNameItem != nullptr)
            for (const auto& suffixSet : units->rangeSuffixes<QVector<QVector<QStringRef>>>())
                for (const auto& suffix : suffixSet)
                    colNameItem->appendRow(new QStandardItem(suffix.toString()));

        m_completionModel.appendRow(colNameItem);
    }

    // For convenience, insert comparison operators.
    for (auto cmp : Query::Rel::comparisons())
        m_completionModel.appendRow(new QStandardItem(Query::Rel::cmpToString(cmp)));

    setModel(&m_completionModel);

    setCaseSensitivity(Qt::CaseInsensitive);
    setMaxVisibleItems(cfgDataBase().completionListSize);
    setCompletionMode(cfgDataBase().inlineCompletion ? QCompleter::InlineCompletion :
                                                       QCompleter::PopupCompletion);
}

QString QueryCompleter::pathFromIndex(const QModelIndex &index) const
{
    const QString completionString = index.data(Qt::EditRole).toString();
    auto *line_edit = static_cast<QLineEdit*>(parent());
    const QString str = line_edit->text();

    const int nextSpace = str.indexOf(' ', line_edit->cursorPosition());

    // Graft together the string to the completion start point, the completion text, and the rest of the line.
    return str.left(m_completionStart) + completionString + (nextSpace >= 0 ? str.mid(nextSpace) : "");
}

QStringList QueryCompleter::splitPath(const QString& path) const
{
    const QLineEdit* lineEdit = static_cast<QLineEdit*>(parent());
    const QStringRef beginToCursor(&path, 0, lineEdit->cursorPosition());
    const QVector<QStringRef> words = beginToCursor.split(' ', QtCompat::SplitBehavior::SkipEmptyParts);
    const int wordCount = words.size();

    if (wordCount == 0)
        return { };

    const auto isNumStarter = [](const QChar c) { return c.isDigit() || c == '.' || c == '-'; };

    // Suffix completions on numeric values
    if (wordCount >= 2) {
        const QStringRef& suffixWord = words.at(wordCount - 1);
        if (isNumStarter(suffixWord.at(0)) || (wordCount >= 3 && isNumStarter(words[wordCount - 2].at(0)))) {
            int suffixStart = suffixWord.size();
            while (suffixStart > 0 && !isNumStarter(suffixWord.at(suffixStart - 1)))
                --suffixStart;

            m_completionStart = suffixWord.position() + suffixStart;

            // find prior column, and return column name and suffix prefix
            for (int word = wordCount - 2; word >= 0; --word)
                if (m_ctx.parseColumnName(words[word]) >= 0)
                    return { words[word].toString(),                   // column name
                             suffixWord.mid(suffixStart).toString() }; // suffix
        }
    }

    m_completionStart = words.last().position();

    // We didn't find a completion suffix: just complete the column name or comparison op.
    return { words.last().toString() };
}
