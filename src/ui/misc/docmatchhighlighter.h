/*
    Copyright 2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef DOCMATCHHIGHLIGHTER_H
#define DOCMATCHHIGHLIGHTER_H

#include <QSyntaxHighlighter>
#include <QRegularExpression>
#include <QTextCharFormat>
#include <QVector>

#include "src/core/query.h"

class QString;

// Highlight search matches in a text document, such as DocDialogBase's text browser.
class DocMatchHighlighter final : public QSyntaxHighlighter
{
public:
    using PatternList = Query::Base::PatternList;

    explicit DocMatchHighlighter(QTextDocument *parent = nullptr);

    // Set patterns to highlight. This is a list of QRegularExpressions.
    void setPatterns(const PatternList&);
    // Clear highlight patterns
    void clearPatterns();
    // New color scheme
    void newConfig();

private:
    void setupFormat();

    void highlightBlock(const QString &text) override;

    PatternList      m_patterns;
    QTextCharFormat  m_matchFormat;
};

#endif // DOCMATCHHIGHLIGHTER_H
