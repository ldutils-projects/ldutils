/*
    Copyright 2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <cassert>
#include <chrono>

#include <QTreeView>
#include <QLineEdit>
#include <QLabel>
#include <QAbstractButton>
#include <QGroupBox>
#include <QSignalBlocker>

#include "src/core/appbase.h"
#include "src/util/units.h"
#include "src/util/util.h"
#include "src/util/icons.h"
#include "toclist.h"

TOCList::TOCList() :
    QueryBase(m_tocFilter),
    m_tocFilter(this)
{
    setupTimers();
}

TOCList::~TOCList()
{
}

void TOCList::setupTimers()
{
    m_pageTimer.setSingleShot(true);

    QObject::connect(&m_pageTimer, &QTimer::timeout,
                     &m_pageTimer, [this]() { updateDisplayedPage(); });
}

int TOCList::tocPage(const QModelIndex& tocIdx)
{
    if (!tocIdx.isValid())
        return -1;

    bool ok;
    if (const int page = tocIdx.data(int(TOC::Page)).toInt(&ok); ok && page >= 0)
        return page;

    return -1;
}

void TOCList::newConfig()
{
    QueryBase::newConfig();

    if (m_currentPage.isValid())
        changePage(m_currentPage); // force rehighlight
}

QVariant TOCList::tocResource(const QModelIndex& tocIdx)
{
    return tocIdx.data(int(TOC::Resource));
}

QByteArray TOCList::tocIcon(const QModelIndex& tocIdx)
{
    return tocIdx.data(int(TOC::Icon)).toByteArray();
}

QString TOCList::tocName(const QModelIndex& tocIdx)
{
    return tocIdx.data(int(TOC::Name)).toString();
}

QModelIndex TOCList::index(const QVariant& resource) const
{
    QModelIndex foundIdx;

    // This is a linear search, slow, but we're only dealing with a handful of things
    // and this isn't used from performance sensitive code, so meh.
    Util::Recurse(m_tocFilter, [&resource, &foundIdx](const QModelIndex& idx) {
        if (idx.data(int(TOC::Resource)) == resource) {
            foundIdx = idx;
            return false;
        }
        return true;
    });

    return foundIdx;
}

void TOCList::setupTOC(QTreeView* toc, const QVector<PageInfo>& pageInfo)
{
    assert(toc != nullptr);

    m_tocModel.clear();
    m_tocModel.setHorizontalHeaderLabels({ "Section" });

    QStandardItem* item = nullptr;
    QVector<QStandardItem*> root = { m_tocModel.invisibleRootItem() };
    int prevIndent = 0;
    int pageNumber = 0;  // index into pageInfo vector

    // Build indented model based on indent level in the PageInfo structure
    for (const auto& page : pageInfo) {
        if (page.m_indent > prevIndent)
            root.append(item);
        else if (page.m_indent < prevIndent)
            root.removeLast();

        prevIndent = page.m_indent;

        // If we have an icon, use that in the QStandardItem
        if (!page.m_icon.isEmpty())
            item = new QStandardItem(Icons::get(page.m_icon.constData()), page.m_heading);
        else
            item = new QStandardItem(page.m_heading);

        if (item == nullptr) {
            assert(0 && "unable to allocate TOC item");
            return;
        }

        item->setData(pageNumber++,    int(TOC::Page));

        if (page.m_resource.isValid())
            item->setData(page.m_resource, int(TOC::Resource));

        if (!page.m_icon.isEmpty())
            item->setData(page.m_icon, int(TOC::Icon));

        root.back()->appendRow(item);
    }

    static const Units unitsStr(Format::String);

    m_tocFilter.setSourceModel(&m_tocModel, { &unitsStr } ); // set underlying model for the filter
    m_tocFilter.setCaseSensitivity(Qt::CaseInsensitive); // TODO: get from prefs

    toc->setModel(&m_tocFilter);
    toc->expandAll();

    // Set minimum size to avoid clipping any columns
    static const int pad = 32; // TODO: use vertical scollbar width rather than hardcoded value
    toc->resizeColumnToContents(int(TOC::Name));
    toc->setMinimumWidth(toc->columnWidth(int(TOC::Name)) + pad);
    toc->setMaximumWidth(toc->columnWidth(int(TOC::Name)) * 2 + pad);

    if (!pageInfo.isEmpty())
        changePage(m_tocFilter.index(0, int(TOC::Name)));
}

bool TOCList::filterMatchesData(const QVariant& data) const
{
    return m_tocFilter.filterMatchesData(data);
}

TOCList::PatternList TOCList::patterns() const
{
    return m_tocFilter.patterns();
}

QModelIndex TOCList::firstSearchPage() const
{
    return Util::FirstIndex(m_tocFilter, hasPagePred);
}

QModelIndex TOCList::lastSearchPage() const
{
    return Util::LastIndex(m_tocFilter, hasPagePred);
}

void TOCList::searchToc(const QString& query)
{
    // This is in the model space, not the filter space. If no pages are displayed,
    // we should un-highlight everything, but the filter has no indexes, so we do it
    // in the model index space.
    m_prevPage = Util::MapDown(m_currentPage);

    updateFilter(query); // set the new filter, which may change m_currentPage

    if (appBase().testing()) {
        updateDisplayedPage();  // update immediately for test suite
    } else {
        using namespace std::chrono_literals;
        m_pageTimer.start(100ms); // update the page, but avoid update spam
    }
}

void TOCList::updateDisplayedPage()
{
    if (!hasPage(m_currentPage))
        m_currentPage = firstSearchPage();

    if (m_currentPage.isValid())
        changePage(m_currentPage);
    else
        tocHighlightMatches(m_prevPage, false);  // if no matching pages, unhighlight.
}

void TOCList::changePage(const QModelIndex& tocIdx, QTreeView* toc)
{
    if (m_currentPage != tocIdx) {
        tocHighlightMatches(m_currentPage, false);
        m_currentPage = tocIdx;
    }

    tocHighlightMatches(m_currentPage, !isEmptyQuery());

    {
        // Avoid yet another changePage from any currentChanged signals
        QSignalBlocker block(toc->selectionModel());
        toc->setCurrentIndex(m_currentPage);
    }
}

void TOCList::nextPage()
{
    if (!m_currentPage.isValid())
        m_currentPage = firstSearchPage();
    else
        m_currentPage = Util::NextIndex(m_currentPage, hasPagePred);

    changePage(m_currentPage);
}

void TOCList::prevPage()
{
    if (!m_currentPage.isValid())
        m_currentPage = lastSearchPage();
    else
        m_currentPage = Util::PrevIndex(m_currentPage, hasPagePred);

    changePage(m_currentPage);
}

TOCList::TOCFilter::TOCFilter(TOCList* tocList) :
    m_tocList(tocList)
{
}

bool TOCList::TOCFilter::match(int row, const QModelIndex& parent) const
{
    if (m_tocList == nullptr)
        return true;

    const QModelIndex idx = m_tocList->m_tocModel.index(row, 0, parent);

    // The call to isEmpty is to short circuit the expensive call to
    // tocSearchStrings in the event the filter is empty.
    return m_tocList->m_tocFilter.isEmpty() ||
           filterMatchesData(m_tocList->tocSearchStrings(idx));
}
