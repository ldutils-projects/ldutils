/*
    Copyright 2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TOCLIST_H
#define TOCLIST_H

#include <functional>

#include <QStandardItemModel>
#include <QPersistentModelIndex>
#include <QVector>
#include <QVariant>
#include <QString>
#include <QStringList>
#include <QByteArray>
#include <QTimer>

#include "src/ui/filters/subtreefilter.h"
#include "src/ui/misc/querybar.h"

class QTreeView;
class QLineEdit;
class QAbstractButton;
class QGroupBox;

// Class that provides table-of-contents (TOC) list functionality.
// E.g, for the DocDialog, or config settings.
class TOCList :
        public QueryBase
{
public:
    TOCList();
    virtual ~TOCList();

    void newConfig() override;

protected slots:
    virtual void changePage(const QModelIndex&) = 0; // change to page for given index
    virtual void searchToc(const QString&);          // search for given string
    virtual void nextPage();                         // Move to next page
    virtual void prevPage();                         // Move to prev page

    void updateDisplayedPage();

protected:
    using PatternList = SubTreeFilter::PatternList;

    // This structure describes the pages: indent level, heading, etc.
    struct PageInfo {
        int              m_indent;
        const QString    m_heading;
        const QVariant   m_resource = QVariant();
        const QByteArray m_icon     = QByteArray();
    };

    // subclass hook to call setupTOC with vector of PageInfo structs
    virtual void setupTOC() = 0;

    // Return QStringList for data contained in page, for searching purposes
    [[nodiscard]] virtual QStringList tocSearchStrings(const QModelIndex&) const = 0;
    // Test for presence of page at given index
    [[nodiscard]] virtual bool hasPage(const QModelIndex&) const = 0;

    // Highlight/unhighlight matches for ease of visual identification
    virtual void tocHighlightMatches(const QModelIndex&, bool highlight) = 0;

    void changePage(const QModelIndex&, QTreeView*); // change to page for given index

    // Return data about TOC entries
    [[nodiscard]] static int        tocPage(const QModelIndex&);
    [[nodiscard]] static QVariant   tocResource(const QModelIndex&);
    [[nodiscard]] static QByteArray tocIcon(const QModelIndex&);
    [[nodiscard]] static QString    tocName(const QModelIndex&);

    // Find index for given resource, or invalid QModelIndex if not found. NOTE: Linear!
    [[nodiscard]] QModelIndex index(const QVariant& resource) const;

    [[nodiscard]] bool isSetup() const { return m_tocModel.rowCount() != 0; }

    // Set up the TOC data from subclass-provided vector of PageInfos
    void setupTOC(QTreeView* toc, const QVector<PageInfo>&);

    // Return true if filter matches given data
    [[nodiscard]] bool filterMatchesData(const QVariant&) const;

    // Return list of parsed patterns from the query
    [[nodiscard]] PatternList patterns() const;

private:
    friend class TestLdUtils; // test hook

    TOCList(const TOCList&) = delete;
    TOCList& operator=(const TOCList&) = delete;

    // Predicate for only accepting valid TOC pages according to hasPage()
    const std::function<bool(const QModelIndex&)> hasPagePred =
            [this](const QModelIndex& idx) { return hasPage(idx); };

    enum TOC {
        Name     = 0,
        Page     = Qt::UserRole,
        Resource,
        Icon,
    };

    [[nodiscard]] QModelIndex firstSearchPage() const;
    [[nodiscard]] QModelIndex lastSearchPage() const;

    // Steer the acceptance function to our virtual one provided by subclasses.
    class TOCFilter : public SubTreeFilter {
    public:
        TOCFilter(TOCList*);
        bool match(int row, const QModelIndex& parent) const override;
    private:
        TOCList* m_tocList;
    };

    void setupTimers();

    QStandardItemModel    m_tocModel;    // model for the TOC entries
    TOCFilter             m_tocFilter;   // for search subsetting
    QTimer                m_pageTimer;   // to avoid page-change spam
    QPersistentModelIndex m_currentPage; // current page to display during searches
    QModelIndex           m_prevPage;    // prior displayed page during searches
};

#endif // TOCLIST_H
