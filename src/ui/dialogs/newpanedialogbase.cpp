/*
    Copyright 2021-2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <src/util/util.h>
#include <src/util/ui.h>
#include <src/util/icons.h>
#include <src/core/appbase.h>
#include <src/ui/windows/mainwindowbase.h>

#include "newpanedialogbase.h"
#include "ui_newpanedialog.h"

NewPaneDialogBase::NewPaneDialogBase(MainWindowBase& mainWindow) :
    QDialog(&mainWindow),
    QueryBar(this),
    ui(nullptr),
    m_mainWindow(mainWindow),
    m_headerView(Qt::Horizontal, this),
    m_firstShow(true)
{
    setupUi(); // Do this here rather than in showEvent, so that save/load works before we display.
}

NewPaneDialogBase::~NewPaneDialogBase()
{
    delete ui;
}

void NewPaneDialogBase::setupUi()
{
    ui = new Ui::NewPaneDialog;
    ui->setupUi(this);

    ui->paneView->setHeader(&m_headerView);
    ui->paneView->sortByColumn(NewPaneModel::Name, Qt::AscendingOrder);
}

void NewPaneDialogBase::showEvent(QShowEvent* event)
{
    onShow();  // tasks to perform just before showing

    // Can't do this on load: model isn't populated yet, so do it on show.
    updateFilter(ui->filterQuery->text());

    QDialog::showEvent(event);
}

void NewPaneDialogBase::onShow()
{
    if (!m_firstShow) {
        updateActions();
        return;
    }

    m_firstShow = false;

    setupModel();
    setupActions();
    setupSignals();
    setupView();
    setupFilterStatusIcons();
    updateActions();

    // Since we weren't tracking the focus all along, set this in advance of focus events coming in.
    newFocus(m_mainWindow.findPane<PaneBase>());

    // Mouse icon
    const int mouseSize = std::max(font().pointSize() * 4, 16);
    ui->mouseIcon->setPixmap(Icons::get("Mouse").pixmap(QSize(mouseSize, mouseSize)));

    Util::SetupWhatsThis(this);
    Util::ResizeViewForData(*ui->paneView);
}

void NewPaneDialogBase::setupView()
{
    const int iconSize = font().pointSize() * 3;

    Util::SetFocus(ui->paneView);
    ui->paneView->setIconSize(QSize(iconSize, iconSize));

    m_headerView.setStretchLastSection(true);

    m_filterValid = ui->queryValid;
}

void NewPaneDialogBase::setupActions()
{
    struct ActionInfo {
        QAction*     action;
        QPushButton* button;
        const char*  icon;
    };

    const QVector<ActionInfo> actionInfo = {
        { ui->action_Add_New_Pane,       ui->btnAdd,       "window-new" },
        { ui->action_Replace_Pane,       ui->btnReplace,   "window-duplicate" },
        { ui->action_Open_in_New_Window, ui->btnNewWindow, "window-new" },
    };

    for (const auto& info : actionInfo) {
        Icons::defaultIcon(info.action, info.icon);
        Icons::defaultIcon(info.button, info.icon);

        info.button->setToolTip(info.action->toolTip());
        info.button->setWhatsThis(info.action->toolTip());
        info.action->setWhatsThis(info.action->toolTip());
    }

    struct BtnInfo {
        QToolButton* button;
        PaneAction   action;
    };

    const QVector<BtnInfo> btnInfo = {
        { ui->btnLeft,    PaneAction::PaneLeft },
        { ui->btnRight,   PaneAction::PaneRight },
        { ui->btnSplitH,  PaneAction::PaneSplitH },
        { ui->btnSplitV,  PaneAction::PaneSplitV },
        { ui->btnClose,   PaneAction::PaneClose },
        { ui->btnRowUp,   PaneAction::PaneRowUp },
        { ui->btnRowDown, PaneAction::PaneRowDown },
    };

    for (const auto& info : btnInfo)
        info.button->setDefaultAction(m_mainWindow.getPaneAction(info.action));
}

void NewPaneDialogBase::setupSignals()
{
    if (ui == nullptr)
        return;

    // Filter text
    connect(ui->filterQuery, &QLineEdit::textChanged, this, &NewPaneDialogBase::filterTextChanged);

    // React to font size changes
    connect(&m_mainWindow, &MainWindowBase::fontSizeChanged, &appBase().newPaneModel(), &NewPaneModel::setupFont);

    // Doubleclicking
    connect(ui->paneView, &QTreeView::doubleClicked, this, &NewPaneDialogBase::doubleClicked);

    // Update actions when selections change
    connect(ui->paneView->selectionModel(), &QItemSelectionModel::currentRowChanged, this, &NewPaneDialogBase::updateActions);

    // Update on focus changes
    connect(&m_mainWindow, &MainWindowBase::paneFocusChanged, this, &NewPaneDialogBase::newFocus);
}

void NewPaneDialogBase::newConfig()
{
    QueryBase::newConfig();

    if (ui != nullptr) {
        ui->paneView->reset();    // force resize of images
        ui->paneView->update();
        Util::ResizeViewForData(*ui->paneView);
    }
}

void NewPaneDialogBase::updateActions()
{
    if (ui == nullptr)
        return;

    const bool hasSelection = selectedPaneClass(false).has_value();

    ui->btnAdd->setEnabled(hasSelection);
    ui->btnReplace->setEnabled(hasSelection);
    ui->btnNewWindow->setEnabled(hasSelection);

    ui->action_Add_New_Pane->setEnabled(hasSelection);
    ui->action_Replace_Pane->setEnabled(hasSelection);
    ui->action_Open_in_New_Window->setEnabled(hasSelection);
}

void NewPaneDialogBase::doubleClicked(const QModelIndex& idx)
{
    if (!idx.isValid())
        return;

    if (bool(appBase().keyboardModifiers() & Qt::ShiftModifier)) {
        ui->action_Open_in_New_Window->trigger();
    } else if (bool(appBase().keyboardModifiers() & Qt::ControlModifier)) {
        ui->action_Add_New_Pane->trigger();
    } else {
        ui->action_Replace_Pane->trigger();
    }
}

void NewPaneDialogBase::filterTextChanged(const QString& query)
{
    updateFilter(query);
}

std::optional<PaneClass_t> NewPaneDialogBase::selectedPaneClass(bool warnIfNone) const
{
    const QModelIndex idx = Util::MapDown(ui->paneView->currentIndex());
    if (!idx.isValid()) {
        if (warnIfNone)
            m_mainWindow.statusMessage(UiType::Warning, tr("No pane type selected."));
        return std::nullopt;
    }

    return PaneClass_t(idx.row());
}

void NewPaneDialogBase::closeIfNoShift()
{
    if (!(appBase().keyboardModifiers() & Qt::ShiftModifier))
        close();
}

void NewPaneDialogBase::on_action_Add_New_Pane_triggered()
{
    if (const auto pc = selectedPaneClass(); pc.has_value()) {
        m_mainWindow.addPaneAction(pc.value());
        closeIfNoShift();
    }
}

void NewPaneDialogBase::on_action_Replace_Pane_triggered()
{
    if (const auto pc = selectedPaneClass(); pc.has_value()) {
        m_mainWindow.replacePaneAction(pc.value());
        closeIfNoShift();
    }
}

void NewPaneDialogBase::on_action_Open_in_New_Window_triggered()
{
    if (const auto pc = selectedPaneClass(); pc.has_value()) {
        m_mainWindow.paneInWindowAction(pc.value());
        closeIfNoShift();
    }
}

void NewPaneDialogBase::newFocus(PaneBase* pane)
{
    if (ui == nullptr || pane == nullptr)
        return;

    ui->focusedPaneInfo->setText(QString("<i>") + pane->name() + "</i>");
}

void NewPaneDialogBase::setupModel()
{
    if (ui == nullptr) // don't do this if we're not displayed yet
        return;

    NewPaneModel& model = appBase().newPaneModel();

    model.removeRows(0, model.rowCount()); // remove any current contents

    QueryBase::setup(&model, true, Qt::DisplayRole);

    ui->paneView->setModel(&m_subtreeFilter);

    Util::ResizeViewForData(*ui->paneView);
}

void NewPaneDialogBase::save(QSettings& settings) const
{
    if (ui != nullptr) {
        SL::Save(settings, "headerView", m_headerView);
        SL::Save(settings, "filterText", ui->filterQuery);
    }
}

void NewPaneDialogBase::load(QSettings& settings)
{
    if (ui != nullptr) {
        SL::Load(settings, "headerView", m_headerView);
        SL::Load(settings, "filterText", ui->filterQuery);
    }
}

