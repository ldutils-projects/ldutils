/*
    Copyright 2019-2022 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <QDirIterator>
#include <QIcon>
#include <QFileDialog>
#include <QImageReader>

#include "src/util/util.h"
#include "src/util/ui.h"
#include "iconselector.h"
#include "ui_iconselector.h"

decltype(IconSelector::m_iconSelector) IconSelector::m_iconSelector;

decltype(IconSelector::passFn) IconSelector::passFn = [](const QString&) { return true; };
decltype(IconSelector::rejectFn) IconSelector::rejectFn = [](const QString&) { return false; };

IconSelector::IconSelector(const QStringList& roots, QWidget *parent,
                           const std::function<bool(const QString&)>& nameFilter) :
    QDialog(parent),
    ui(new Ui::IconSelector),
    m_iconSortModel(this),
    m_roots(roots),
    m_nameFilter(nameFilter),
    m_setup(false)
{
    ui->setupUi(this);

    Util::SetupWhatsThis(this);
}

IconSelector::~IconSelector()
{
    delete ui;
}

void IconSelector::setupModel()
{
    if (m_setup)
        return;

    for (const auto& root : m_roots)
        setupModel(root);

    m_iconSortModel.setSourceModel(&m_iconFs);
    ui->iconView->sortByColumn(IconAndName, Qt::AscendingOrder);

    ui->iconView->setModel(&m_iconSortModel);
    ui->iconView->setWindowTitle(tr("Select Icon"));
    ui->iconView->setColumnHidden(Path, true);  // hide pathnames

    m_setup = true;
}

void IconSelector::openToCurrentPath()
{
    Util::OpenToMatch(m_iconSortModel, *ui->iconView, [this](const QModelIndex& idx) {
        return idx.model() != nullptr &&
                idx.model()->data(idx.model()->sibling(idx.row(), Path, idx), Qt::DisplayRole).toString() == m_currentPath;
    });
}


void IconSelector::setCurrentPath(const QString& path)
{
    m_currentPath = path;
    m_icon = QIcon(path);
}

void IconSelector::setupModel(const QString& root)
{
    // QFilesystemModel doesn't work on resources: https://bugreports.qt.io/browse/QTBUG-7010
    // Instead, we build a simple model ourselves.

    QStandardItem* parent = m_iconFs.invisibleRootItem();

    // Create fake top level name if given [Name] at beginning of path
    int start = 0;
    if (!root.isEmpty() && root.at(0) == '[') {
        start = root.indexOf(']') + 1;
        auto* dir = new QStandardItem(root.mid(1, start - 2));
        parent->appendRow(dir);
        parent = dir;
    }

    QDirIterator it(root.mid(start), QDirIterator::Subdirectories);

    m_iconFs.setColumnCount(_Count);

    int depth = -1;

    while (it.hasNext()) {
        const QString file = it.next();
        const int newDepth = file.count('/');

        while (depth >= 0 && newDepth < depth) {
            parent = parent->parent() == nullptr ? m_iconFs.invisibleRootItem() : parent->parent();
            --depth;
        }

        if (it.fileInfo().isDir()) {
            auto* dir = new QStandardItem(QFileInfo(file).fileName());
            parent->appendRow(dir);
            parent = dir;
        }

        if (it.fileInfo().isFile()) {
            const QString fileName = QFileInfo(file).baseName();
            const bool displayFileName = m_nameFilter(fileName);

            parent->appendRow({ new QStandardItem(QIcon(file), displayFileName ? fileName : ""),
                                new QStandardItem(file)
                              });

            const QStandardItem* iconItem = parent->child(parent->rowCount() - 1, IconAndName);

            // TODO: we should get the size from somewhere.
            static const QSize iconPad(5, 5);
            const QSize querySize = ui->iconView->iconSize();
            const QSize iconSize = iconItem->data(Qt::DecorationRole).value<QIcon>().actualSize(querySize);

            parent->child(parent->rowCount() - 1, IconAndName)->setData(iconSize + iconPad, Qt::SizeHintRole);
        }

        depth = newDepth;
    }
}

void IconSelector::showEvent(QShowEvent* event)
{
    setupModel();
    openToCurrentPath();

    QDialog::showEvent(event);
    m_icon = QIcon();
    m_iconFile = QString();
}

void IconSelector::on_fromFile_pressed()
{
   const auto formats = QImageReader::supportedImageFormats();

   QString filter = "Icons (";
   for (const auto& f : formats)
       filter.append("*." + f + " ");
   filter.append(");;All (*)");

   // TODO: get import dir from main window or somewhere
   const QString file =
           QFileDialog::getOpenFileName(this, tr("Select icon file"), "~", filter,
                                        nullptr, QFileDialog::ReadOnly);

   if (file.isEmpty())
       return;

   m_iconFile = file;
   m_icon = QIcon(file);
   accept();
}

QIcon IconSelector::icon() const
{
    if (!m_icon.isNull()) // from file selection
        return m_icon;

    if (const QModelIndex idx = ui->iconView->currentIndex(); idx.isValid())
        return m_iconSortModel.data(idx, Qt::DecorationRole).value<QIcon>();
    return { };
}

QString IconSelector::iconFile() const
{
    if (!m_iconFile.isNull()) // from file selection
        return m_iconFile;

    if (const QModelIndex idx = ui->iconView->currentIndex(); idx.isValid())
        return m_iconSortModel.data(m_iconSortModel.sibling(idx.row(), Path, idx), Qt::DisplayRole).value<QString>();

    return { };
}

IconSelector* IconSelector::iconSelector(const QStringList& paths, const std::function<IconSelector*()>& factory)
{
    if (!m_iconSelector.contains(paths))
        m_iconSelector.insert(paths, factory());

    return m_iconSelector.value(paths);
}

IconSelector* IconSelector::iconSelector(const QStringList& paths, const IconSelector::namefilter_t& filterFn,
                                         QWidget* parent)
{
    return iconSelector(paths, [&]() { return new IconSelector(paths, parent, filterFn); });
}
