/*
    Copyright 2021 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "texteditordialog.h"
#include "ui_texteditordialog.h"

#include <src/ui/widgets/texteditor.h>
#include <src/util/ui.h>

TextEditorDialog::TextEditorDialog(QWidget* parent) :
    TextEditorDialogBase(parent),
    ui(new Ui::TextEditorDialog)
{
    ui->setupUi(this);
    ui->verticalLayout->insertWidget(0, adoptEditor());

    editor()->setupButtonBox(ui->buttonBox);

    Util::SetupWhatsThis(this);
}

TextEditorDialog::~TextEditorDialog()
{
    delete ui;
}
