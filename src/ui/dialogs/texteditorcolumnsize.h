/*
    Copyright 2021-2022 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TEXTEDITORCOLUMNSIZE_H
#define TEXTEDITORCOLUMNSIZE_H

#include <QDialog>
#include <QTextLength>

namespace Ui {
class TextEditorColumnSize;
} // namespace Ui

class TextEditorColumnSize : public QDialog
{
    Q_OBJECT

public:
    explicit TextEditorColumnSize(QTextLength, QWidget* parent = nullptr);
    ~TextEditorColumnSize() override;

    QTextLength length() const;

private slots:
    void on_variable_toggled(bool checked);
    void on_fixed_toggled(bool checked);
    void on_percent_toggled(bool checked);

private:
    void setup(QTextLength);

    Ui::TextEditorColumnSize *ui;
    QTextLength m_initialLength;
};

#endif // TEXTEDITORCOLUMNSIZE_H
