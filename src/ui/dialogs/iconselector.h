/*
    Copyright 2019-2022 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef ICONSELECTOR_H
#define ICONSELECTOR_H

#include <functional>
#include <QDialog>
#include <QMap>
#include <QStandardItemModel>
#include <QSortFilterProxyModel>
#include <QStringList>

namespace Ui {
class IconSelector;
} // namespace Ui

class IconSelector final : public QDialog
{
    Q_OBJECT

public:
    using namefilter_t = const std::function<bool(const QString&)>;

    explicit IconSelector(const QStringList& roots,
                          QWidget *parent = nullptr,
                          const namefilter_t& = passFn);
    ~IconSelector() override;

    void setCurrentPath(const QString& path); // initially selected item

    [[nodiscard]] QIcon   icon()     const;
    [[nodiscard]] QString iconFile() const;

    static const namefilter_t passFn;
    static const namefilter_t rejectFn;

    // For sharing modal icon selectors, since they're expensive to set up.
    [[nodiscard]] static IconSelector* iconSelector(const QStringList& paths,
                                                    const std::function<IconSelector*()>& factory);

    [[nodiscard]] static IconSelector* iconSelector(const QStringList& paths, const namefilter_t& = passFn,
                                                    QWidget* parent = nullptr);

private slots:
    void on_fromFile_pressed();

private:
    enum {
        IconAndName = 0,
        Path,
        _Count,
    };

    void setupModel();
    void setupModel(const QString& root);
    void openToCurrentPath();
    void showEvent(QShowEvent*) override;

    // QFilesystemModel doesn't work on resources: https://bugreports.qt.io/browse/QTBUG-7010
    Ui::IconSelector*     ui;
    QStandardItemModel    m_iconFs;
    QSortFilterProxyModel m_iconSortModel;
    QStringList           m_roots;
    QIcon                 m_icon;
    QString               m_iconFile;
    QString               m_currentPath;
    namefilter_t          m_nameFilter;
    bool                  m_setup;  // true after we've populated the model

    // for sharing per-path icon selectors
    static QMap<QStringList, IconSelector*> m_iconSelector;
};

#endif // ICONSELECTOR_H
