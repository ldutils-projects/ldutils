/*
    Copyright 2021-2022 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef MODELTEXTEDITDIALOG_H
#define MODELTEXTEDITDIALOG_H

#include <QStandardItemModel>
#include <QMenu>

#include <src/core/modelmetadata.h>

#include "texteditordialogbase.h"

namespace Ui {
class ModelTextEditDialog;
} // namespace Ui

class QModelIndex;
class TreeModel;

// The HTML produced by this class can be variable-expanded using ModelVarExpander
class ModelTextEditDialog : public TextEditorDialogBase
{
    Q_OBJECT

public:
    explicit ModelTextEditDialog(const TreeModel&, bool defaultShowVars, QWidget* parent = nullptr);
    ~ModelTextEditDialog() override;

    // *** begin Settings API
    void save(QSettings&) const override;
    void load(QSettings&) override;
    // *** end Settings API

signals:
    void textApplied();

protected slots:
    virtual void insertColumnVariable(const QString& prefix, const QModelIndex&, const QString& suffix);
    virtual void insertColumnVariable(const QModelIndexList&);

private slots:
    void on_action_Show_Column_Variables_triggered();
    void on_action_Hide_Column_Variables_triggered();
    void on_action_Insert_Variable_triggered();
    void updateActions();
    void showContextMenu(const QPoint&);

private:
    void showEvent(QShowEvent*) override;

    void setupUi();
    void setupSignals();
    void setupColumnModel(const TreeModel&);
    void setupActionIcons();
    void setupButtonActions();
    void setupMenus();
    void insertSelected();

    Ui::ModelTextEditDialog*  ui;
    QStandardItemModel        m_columnModel;  // for displaying columns
    QMenu                     m_columnMenu;   // context menu for column display
};

#endif // MODELTEXTEDITDIALOG_H
