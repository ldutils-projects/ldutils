/*
    Copyright 2021-2022 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef ABSTRACTTEXTEDITORDIALOG_H
#define ABSTRACTTEXTEDITORDIALOG_H

#include <QDialog>

#include <src/core/settings.h>

class TextEditor;

class TextEditorDialogBase :
        public QDialog,
        public Settings
{
public:
    explicit TextEditorDialogBase(QWidget *parent = nullptr);
    ~TextEditorDialogBase() override;

    virtual void setHtml(const QString&);
    [[nodiscard]] virtual QString toHtml() const;

    // *** begin Settings API
    void save(QSettings&) const override;
    void load(QSettings&) override;
    // *** end Settings API

protected:
    TextEditor*  editor() { return m_textEditor; }
    TextEditor*  adoptEditor();   // obtain editor to adopt into UI

private:
    TextEditor*  m_textEditor;    // our editor
    bool         m_editorAdopted; // true when it's adopted by UI
};

#endif // ABSTRACTTEXTEDITORDIALOG_H
