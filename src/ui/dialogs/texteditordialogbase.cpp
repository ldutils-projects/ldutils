/*
    Copyright 2021-2022 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "texteditordialogbase.h"

#include <src/ui/widgets/texteditor.h>

TextEditorDialogBase::TextEditorDialogBase(QWidget* parent) :
    QDialog(parent),
    m_textEditor(new TextEditor()),
    m_editorAdopted(false)
{
}

TextEditorDialogBase::~TextEditorDialogBase()
{
    // only delete the TextEditor if it wasn't adopted by a UI
    if (!m_editorAdopted)
        delete m_textEditor;
}

TextEditor* TextEditorDialogBase::adoptEditor()
{
    m_editorAdopted = true;
    return m_textEditor;
}

void TextEditorDialogBase::setHtml(const QString& s)
{
    if (m_textEditor != nullptr)
        m_textEditor->setHtml(s);
}

QString TextEditorDialogBase::toHtml() const
{
    if (m_textEditor != nullptr)
        return m_textEditor->toHtml();

    return { };
}

void TextEditorDialogBase::save(QSettings& settings) const
{
    if (m_textEditor != nullptr)
        m_textEditor->save(settings);
}

void TextEditorDialogBase::load(QSettings& settings)
{
    if (m_textEditor != nullptr)
        m_textEditor->load(settings);
}
