/*
    Copyright 2018-2022 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <QUrl>
#include <QDir>
#include <QMessageBox>
#include <QStandardPaths>
#include <QToolTip>
#include <QCursor>
#include <QTextDocumentFragment>
#include <QPushButton>

#include <src/ui/widgets/textbrowser.h>
#include <src/util/ui.h>
#include <src/util/icons.h>

#include "docdialogbase.h"
#include "ui_docdialogbase.h"

DocDialogBase::DocDialogBase(QWidget *parent) :
    QDialog(parent),
    ui(nullptr),
    m_textBrowser(nullptr),
    m_searchError(false),
    m_pageChange(0)
{
    // Defer most of setup until first show()
}

DocDialogBase::~DocDialogBase()
{
    delete ui;
}

void DocDialogBase::newConfig()
{
    TOCList::newConfig();
    m_docHighlighter.newConfig();
}

void DocDialogBase::searchToc(const QString& queryString)
{
    TOCList::searchToc(queryString);
    ui->toc->expandAll();
}

void DocDialogBase::setup()
{
    if (ui != nullptr)
        return;

    ui = new Ui::DocDialogBase;
    ui->setupUi(this);

    setupTextBrowser();
    setupSearchPaths();
    setupTOC();
    setupActions();
    setupActionIcons();
    setupQueryBase();
    setupFilterStatusIcons();

    Util::SetupWhatsThis(this);

    updateActions();
}

void DocDialogBase::setupTextBrowser()
{
    if (m_textBrowser != nullptr)
        return;

    m_textBrowser = new TextBrowser(this);
    m_textBrowser->setOpenExternalLinks(true);

    // For search term highlighting
    m_docHighlighter.setDocument(m_textBrowser->document());

    ui->tocBrowserLayout->addWidget(m_textBrowser);

    // These can only be set once we've added the m_textBrowser
    ui->tocBrowserLayout->setStretchFactor(ui->toc, 15);
    ui->tocBrowserLayout->setStretchFactor(m_textBrowser, 85);

    Util::SetFocus(ui->toc);
}

void DocDialogBase::setupActions()
{
    addAction(ui->action_Refresh);

    // Prevent closing on return button. We use that for next-page in searches.
    if (QPushButton* close = ui->buttonBox->button(QDialogButtonBox::Close); close != nullptr) {
        close->setDefault(false);
        close->setAutoDefault(false);
    }
}

// Doc search locations.
void DocDialogBase::setupSearchPaths()
{
    QStringList paths;

    const QString docsTheme = Util::IsLightTheme() ? "docs/light" : "docs/dark";

    for (const QString& dir : { docsTheme, QString("docs") }) {
        paths.append(QStandardPaths::locateAll(QStandardPaths::AppDataLocation, dir, QStandardPaths::LocateDirectory));

        // Also search application directory
        if (const QDir appDir(QApplication::applicationDirPath() + QDir::separator() + dir); appDir.exists())
            paths.append(appDir.path());
    }

    m_textBrowser->setSearchPaths(paths);
}

void DocDialogBase::setupSignals()
{
    // Page changes
    connect(ui->toc->selectionModel(), &QItemSelectionModel::currentChanged, this, &DocDialogBase::changePage);
    connect(ui->toc, &QTreeView::clicked, this, &DocDialogBase::changePage);

    // Refresh UI enable states on page changes
    connect(m_textBrowser, &QTextBrowser::sourceChanged, this, &DocDialogBase::updateActions);
    connect(m_textBrowser, static_cast<void(QTextBrowser::*)(const QUrl&)>(&QTextBrowser::highlighted),
            this, &DocDialogBase::hoverUrl);

    // Search TOC
    connect(ui->tocQuery, &QLineEdit::textChanged, this, &DocDialogBase::searchToc);

    // Next and previous
    connect(ui->tocQuery, &QLineEdit::returnPressed, this, &DocDialogBase::nextPage);
}

void DocDialogBase::setupActionIcons()
{
    Icons::defaultIcon(ui->btnNext, "go-next");
    Icons::defaultIcon(ui->btnPrev, "go-previous");
    Icons::defaultIcon(ui->action_Refresh, "view-refresh");
}

void DocDialogBase::setupQueryBase()
{
    m_filterValid = ui->filterValid;
}

QStringList DocDialogBase::tocSearchStrings(const QModelIndex& tocIdx) const
{
    QStringList texts;

    texts.reserve(2);

    // Add TOC entry name
    texts.append(tocName(tocIdx));

    // Add HTML, and extract it as plain text for searching. Use the text browser search path
    // to find the html.
    for (const auto& path : m_textBrowser->searchPaths()) {
        if (QFile docFile(path + QDir::separator() + tocResource(tocIdx).toByteArray()); docFile.exists()) {
            if (docFile.open(QFile::ReadOnly)) {
                texts.append(QTextDocumentFragment::fromHtml(docFile.readAll()).toPlainText());
                break;
            }
        }
    }

    return texts;
}

void DocDialogBase::showEvent(QShowEvent* event)
{
    setup();
    QDialog::showEvent(event);
}

bool DocDialogBase::hasPage(const QByteArray& resource) const
{
    return m_textBrowser != nullptr && !resource.isEmpty();
}

bool DocDialogBase::hasPage(const QModelIndex& tocIdx) const
{
    return tocIdx.isValid() && hasPage(tocResource(tocIdx).toByteArray());
}

void DocDialogBase::tocHighlightMatches(const QModelIndex&, bool highlight)
{
    if (highlight) {
        m_docHighlighter.setPatterns(patterns());
    } else {
        m_docHighlighter.clearPatterns();
    }

    m_docHighlighter.rehighlight();
}

void DocDialogBase::changePage(const QModelIndex& tocIdx)
{
    if (m_searchError || !tocIdx.isValid()) // don't spam the error message.
        return;

    TOCList::changePage(tocIdx, ui->toc);

    if (const QByteArray resource = tocResource(tocIdx).toByteArray(); hasPage(resource)) {
        m_textBrowser->setSource(QUrl(resource));

        if (m_textBrowser->document()->isEmpty()) {
            QMessageBox::critical(this, tr("Document Error"),
                                  tr("Unable to find documentation files.  Please verify the installation "
                                     "and restart the program.  The current documentation search path is:\n") +
                                  "\n   " +
                                  QStandardPaths::standardLocations(QStandardPaths::AppDataLocation).join("\n   "),
                                  QMessageBox::Ok);

            m_searchError = true;
        }

        // For some reason, the QTextBrowser::sourceChanged isn't sufficient the first time
        if (++m_pageChange < 4)
            updateActions();
    }
}

void DocDialogBase::hoverUrl(const QUrl& url)
{
    if (!url.isEmpty()) {
        if (!url.isRelative()) {
            QToolTip::showText(QCursor::pos(), url.toString(), this);
        } else {
            QToolTip::showText(QCursor::pos(), tr("Documentation Link"), this);
        }

        setCursor(Qt::PointingHandCursor);
    } else {
        setCursor(Qt::ArrowCursor);
        QToolTip::hideText();
    }
}

void DocDialogBase::updateActions()
{
    ui->btnPrev->setEnabled(m_textBrowser->isBackwardAvailable());
    ui->btnNext->setEnabled(m_textBrowser->isForwardAvailable());

    ui->btnPrev->setStatusTip(tr("Previous page: ") + m_textBrowser->historyTitle(-1));
    ui->btnNext->setStatusTip(tr("Next page: ") + m_textBrowser->historyTitle(1));

    ui->btnPrev->setToolTip(ui->btnPrev->statusTip());
    ui->btnNext->setToolTip(ui->btnNext->statusTip());
}

void DocDialogBase::setupTOC(const QVector<PageInfo>& pageInfo)
{
    setupTOC(ui->toc, pageInfo); // give our TOC QTreeView to the TOCList
    setupSignals();
}

void DocDialogBase::on_btnNext_clicked()
{
    m_textBrowser->forward();
}

void DocDialogBase::on_btnPrev_clicked()
{
    m_textBrowser->backward();
}

void DocDialogBase::on_action_Refresh_triggered()
{
    m_textBrowser->reload();
}
