/*
    Copyright 2021-2022 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <algorithm>
#include <QRegularExpression>
#include <QGridLayout>
#include <QStandardItem>
#include <QFontMetrics>
#include <QTextEdit>
#include <QSplitter>
#include <QPushButton>

#include <src/util/icons.h>
#include <src/util/ui.h>
#include <src/ui/widgets/texteditor.h>
#include <src/core/treemodel.h>
#include <src/core/query.inl.h>   // for CanonicalizeColumnName

#include "modeltexteditdialog.h"
#include "ui_modeltexteditdialog.h"

ModelTextEditDialog::ModelTextEditDialog(const TreeModel& model, bool defaultShowVars, QWidget* parent) :
    TextEditorDialogBase(parent),
    ui(new Ui::ModelTextEditDialog)
{
    ui->setupUi(this);

    setupUi();
    setupColumnModel(model);
    setupSignals();
    setupActionIcons();
    setupButtonActions();
    setupMenus();
    Util::SetupWhatsThis(this);

    if (!defaultShowVars)
        on_action_Hide_Column_Variables_triggered(); // start in hidden state if asked
}

ModelTextEditDialog::~ModelTextEditDialog()
{
    // TextEditor is adopted by the UI, so we don't delete it ourselves.
    delete ui;
}

void ModelTextEditDialog::setupUi()
{
    ui->verticalLayout->insertWidget(0, adoptEditor());

    editor()->setupButtonBox(ui->buttonBox);
}

void ModelTextEditDialog::setupSignals()
{
    connect(ui->modelColumns, &QTreeView::doubleClicked, this, &ModelTextEditDialog::insertSelected);

    // Update UI on splitter config or selection change
    connect(ui->splitter, &QSplitter::splitterMoved, this, &ModelTextEditDialog::updateActions);
    connect(ui->modelColumns->selectionModel(), &QItemSelectionModel::selectionChanged,
            this, &ModelTextEditDialog::updateActions);

    if (QPushButton* applyButton = ui->buttonBox->button(QDialogButtonBox::Apply); applyButton != nullptr)
        connect(applyButton, &QPushButton::released, this, [this]() { emit textApplied(); } );

    if (QPushButton* okButton = ui->buttonBox->button(QDialogButtonBox::Ok); okButton != nullptr)
        connect(okButton, &QPushButton::released, this, [this]() { emit textApplied(); } );
}

void ModelTextEditDialog::setupColumnModel(const TreeModel& model)
{
    int maxWidth = 0;

    // Kludge: remove the editable info (set in TrackModel::mdTooltip) since it doesn't apply here.
    // Rather, we should have a way to request the tooltip without this info.
    static const QRegularExpression editable("<p><b><u>Editable:</u></b></p>[^</]*", QRegularExpression::DotMatchesEverythingOption);
    static const QRegularExpression colInfo("<p><b><u>Column Information:</u></b></p>");

    for (ModelType c = 0; c < model.columnCount(); ++c) {
        const QString canonicalName = Query::CanonicalizeColumnName(model.headerData(c, Qt::Horizontal).toString());
        auto* nameItem = new QStandardItem(canonicalName);

        QString modelTooltip = model.headerData(c, Qt::Horizontal, Qt::ToolTipRole).toString();

        const QString tooltip = QString("<p><i><u>Double-click to insert:</u></i></p>") +
                                modelTooltip.replace(editable, "").replace(colInfo, "");

        nameItem->setToolTip(tooltip);

        m_columnModel.appendRow({ nameItem });
        maxWidth = std::max(maxWidth, fontMetrics().boundingRect(nameItem->text()).width());
    }

    m_columnModel.setHeaderData(0, Qt::Horizontal, "Data Column");

    const int contentWidth = maxWidth + fontMetrics().boundingRect('M').width() * 4;

    ui->modelColumns->setModel(&m_columnModel);
    ui->modelColumns->setMinimumWidth(contentWidth);
    ui->modelColumns->setMaximumWidth(contentWidth * 3 / 2);
}

void ModelTextEditDialog::setupActionIcons()
{
    Icons::defaultIcon(ui->btnInsert,  "list-add");
    Icons::defaultIcon(ui->action_Insert_Variable, "list-add");
    Icons::defaultIcon(ui->action_Hide_Column_Variables, "arrow-right");
    Icons::defaultIcon(ui->action_Show_Column_Variables, "arrow-left");
}

void ModelTextEditDialog::setupButtonActions()
{
    ui->btnInsert->setDefaultAction(ui->action_Insert_Variable);
}

void ModelTextEditDialog::setupMenus()
{
    m_columnMenu.addActions({ ui->action_Insert_Variable });
    m_columnMenu.addSeparator();
    m_columnMenu.addActions({ ui->action_Show_Column_Variables,
                              ui->action_Hide_Column_Variables });

    ui->columnVariables->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(ui->columnVariables, &QWidget::customContextMenuRequested, this, &ModelTextEditDialog::showContextMenu);
}

void ModelTextEditDialog::updateActions()
{
    const bool varsVisible  = ui->splitter->sizes().at(1) > 0;
    const bool hasSelection = ui->modelColumns->selectionModel()->hasSelection();

    ui->action_Insert_Variable->setEnabled(hasSelection);
    ui->action_Hide_Column_Variables->setEnabled(varsVisible);
    ui->action_Show_Column_Variables->setEnabled(!varsVisible);

    // We must remove the old action, or setDefaultAction() accumulates them and
    // leaves an additional-action icon in the button.
    for (auto* action : ui->btnShowHideCols->actions())
        ui->btnShowHideCols->removeAction(action);

    ui->btnShowHideCols->setDefaultAction(varsVisible ? ui->action_Hide_Column_Variables
                                                      : ui->action_Show_Column_Variables);
}

void ModelTextEditDialog::showContextMenu(const QPoint& pos)
{
    updateActions();
    m_columnMenu.exec(ui->columnVariables->mapToGlobal(pos));
}

void ModelTextEditDialog::insertColumnVariable(const QString& prefix, const QModelIndex& idx, const QString& suffix)
{
    if (!idx.isValid() || editor() == nullptr)
        return;

    // The string to insert, which can be expanded later to the contents from the model
    const QString varStr = prefix + "${" + m_columnModel.data(idx).toString() + "}" + suffix;

    editor()->editor()->insertPlainText(varStr);
}

void ModelTextEditDialog::insertColumnVariable(const QModelIndexList& indexes)
{
    for (const auto& idx : indexes)
        insertColumnVariable("", idx, idx == indexes.back() ? "" : " ");
}

void ModelTextEditDialog::insertSelected()
{
    insertColumnVariable(ui->modelColumns->selectionModel()->selectedIndexes());
}

void ModelTextEditDialog::save(QSettings& settings) const
{
    SL::Save(settings, "splitterState", ui->splitter->saveState());
}

void ModelTextEditDialog::load(QSettings& settings)
{
    if (settings.contains("splitterState"))
        ui->splitter->restoreState(settings.value("splitterState").toByteArray());
}

void ModelTextEditDialog::on_action_Show_Column_Variables_triggered()
{
    ui->splitter->setSizes({75, 25});
    updateActions();
}

void ModelTextEditDialog::on_action_Hide_Column_Variables_triggered()
{
    ui->splitter->setSizes({100, 0});
    updateActions();
}

void ModelTextEditDialog::showEvent(QShowEvent* event)
{
    TextEditorDialogBase::showEvent(event);
    updateActions();
}

void ModelTextEditDialog::on_action_Insert_Variable_triggered()
{
    insertSelected();
    updateActions();
}

