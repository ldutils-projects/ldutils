/*
    Copyright 2018-2022 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef DOCDIALOGBASE_H
#define DOCDIALOGBASE_H

#include <initializer_list>

#include <QDialog>
#include <QVector>
#include <QAction>
#include <QStandardItemModel>

#include "src/ui/misc/toclist.h"
#include "src/ui/misc/docmatchhighlighter.h"

namespace Ui {
class DocDialogBase;
} // namespace Ui

class QModelIndex;
class QUrl;
class TextBrowser;

class DocDialogBase :
        public QDialog,
        public TOCList
{
    Q_OBJECT

public:
    explicit DocDialogBase(QWidget *parent = nullptr);
    ~DocDialogBase() override;

    void newConfig() override;

protected slots:
    virtual void hoverUrl(const QUrl&);

protected:
    Ui::DocDialogBase* ui;

    TextBrowser*       m_textBrowser;
    bool               m_searchError; // true if we failed to find the documentation
    int                m_pageChange;  // work around apparent defect in QTextBrowser

private slots:
    void on_btnNext_clicked();
    void on_btnPrev_clicked();
    void on_action_Refresh_triggered();
    void updateActions();

    // *** begin TOCList API
    void changePage(const QModelIndex&) override;
    void searchToc(const QString& queryString) override;
    void nextPage() override { TOCList::nextPage(); }
    void prevPage() override { TOCList::prevPage(); }
    // *** end TOCList API

protected:
    // *** begin TOCList API
    using TOCList::setupTOC;
    void setupTOC(const QVector<PageInfo>&);
    // *** end TOCList API

private:
    friend class TestZtgps;  // for test hooks

    void setup();
    void setupSignals();
    void setupSearchPaths();
    void setupTextBrowser();
    void setupActions(); // key stuff
    void setupActionIcons();
    void setupQueryBase();

    // *** begin TOCList API
    QStringList tocSearchStrings(const QModelIndex&) const override;
    bool hasPage(const QModelIndex&) const override;
    bool hasPage(const QByteArray&) const;
    void tocHighlightMatches(const QModelIndex&, bool highlight) override;
    // *** end TOCList API

    void showEvent(QShowEvent*) override;

    DocMatchHighlighter m_docHighlighter;
};

#endif // DOCDIALOGBASE_H
