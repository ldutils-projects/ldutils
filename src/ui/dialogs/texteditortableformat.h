/*
    Copyright 2021-2022 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TEXTEDITORTABLEFORMAT_H
#define TEXTEDITORTABLEFORMAT_H

#include <QDialog>
#include <QTextTableFormat>

namespace Ui {
class TextEditorTableFormat;
} // namespace Ui

class QTextTable;
class QAbstractButton;

class TextEditorTableFormat : public QDialog
{
    Q_OBJECT

public:
    explicit TextEditorTableFormat(const QTextTable*, QWidget *parent = nullptr);
    ~TextEditorTableFormat() override;

    int rows() const;
    int columns() const;
    QTextTableFormat format() const;

private slots:
    void on_buttonBox_clicked(QAbstractButton*);

private:
    friend class TestLdUtils;  // test hook

    void setupParams(const QTextTable*);        // set UI from text table
    void setupParams(const QTextTableFormat&);  // ...
    void reset();                               // reset to original values
    void defaults();                            // reset to defaults
    QTextTableFormat defaultFormat() const;     // return the default format to use

    QTextTableFormat           m_origFormat;    // to avoid clobbering what we don't set
    int                        m_origRows;
    int                        m_origColumns;
    Ui::TextEditorTableFormat *ui;
};

#endif // TEXTEDITORTABLEFORMAT_H
