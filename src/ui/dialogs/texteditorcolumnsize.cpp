/*
    Copyright 2021 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "texteditorcolumnsize.h"
#include "ui_texteditorcolumnsize.h"

TextEditorColumnSize::TextEditorColumnSize(QTextLength length, QWidget* parent) :
    QDialog(parent),
    ui(new Ui::TextEditorColumnSize),
    m_initialLength(length)
{
    ui->setupUi(this);

    setup(length);
}

TextEditorColumnSize::~TextEditorColumnSize()
{
    delete ui;
}

void TextEditorColumnSize::setup(QTextLength length)
{
    ui->width->setValue(length.rawValue());
    ui->variable->setChecked(length.type() == QTextLength::VariableLength);
    ui->fixed->setChecked(length.type()    == QTextLength::FixedLength);
    ui->percent->setChecked(length.type()  == QTextLength::PercentageLength);
}

QTextLength TextEditorColumnSize::length() const
{
    QTextLength::Type type;

    if (ui->fixed->isChecked())         type = QTextLength::FixedLength;
    else if (ui->percent->isChecked())  type = QTextLength::PercentageLength;
    else                                type = QTextLength::VariableLength;

    return QTextLength(type, qreal(ui->width->value()));
}


void TextEditorColumnSize::on_variable_toggled(bool checked)
{
    if (checked) {
        ui->width->setRange(0, 0);
        ui->width->setValue(0);
        ui->width->setSuffix(tr(""));
        ui->width->setEnabled(false);
    }
}

void TextEditorColumnSize::on_fixed_toggled(bool checked)
{
    if (checked) {
        ui->width->setRange(1, 4096);
        ui->width->setDecimals(0);
        if (m_initialLength.type() == QTextLength::Type::FixedLength)
            ui->width->setValue(m_initialLength.rawValue());
        else
            ui->width->setValue(50.0);
        ui->width->setSuffix(tr(" px"));
        ui->width->setEnabled(true);
    }
}

void TextEditorColumnSize::on_percent_toggled(bool checked)
{
    if (checked) {
        ui->width->setRange(1, 100);
        ui->width->setDecimals(2);
        if (m_initialLength.type() == QTextLength::Type::PercentageLength)
            ui->width->setValue(m_initialLength.rawValue());
        else
            ui->width->setValue(25.0);
        ui->width->setSuffix(tr(" %"));
        ui->width->setEnabled(true);
    }
}
