/*
    Copyright 2021-2022 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <QTextTable>
#include <QTextTableFormat>
#include <QAbstractButton>
#include <QPushButton>

#include "src/util/ui.h"

#include "texteditortableformat.h"
#include "ui_texteditortableformat.h"

TextEditorTableFormat::TextEditorTableFormat(const QTextTable* table, QWidget *parent) :
    QDialog(parent),
    m_origRows(table == nullptr ? 3 : table->rows()),
    m_origColumns(table == nullptr ? 3 : table->columns()),
    ui(new Ui::TextEditorTableFormat)
{
    ui->setupUi(this);

    setupParams(table);

    Util::SetupWhatsThis(this);
}

TextEditorTableFormat::~TextEditorTableFormat()
{
    delete ui;
}

int TextEditorTableFormat::rows() const
{
    return ui->rows->value();
}

int TextEditorTableFormat::columns() const
{
    return ui->columns->value();
}

QTextTableFormat TextEditorTableFormat::format() const
{
    QTextTableFormat format = m_origFormat;

    format.setCellSpacing(ui->cellSpacing->value());
    format.setCellPadding(ui->cellPadding->value());
    format.setBorder(ui->borderWidth->value());

    format.setAlignment(Qt::AlignmentFlag((0x0001 << ui->hAlignment->currentIndex()) |
                                          (0x0020 << ui->vAlignment->currentIndex())));

    format.setBorderStyle(QTextFrameFormat::BorderStyle(ui->borderStyle->currentIndex()));
    format.setPosition(QTextFrameFormat::Position(ui->position->currentIndex()));

    format.setLeftMargin(ui->marginL->value());
    format.setRightMargin(ui->marginR->value());
    format.setTopMargin(ui->marginT->value());
    format.setBottomMargin(ui->marginB->value());

    return format;
}

void TextEditorTableFormat::setupParams(const QTextTable* table)
{
    if (table == nullptr) {
        setupParams(defaultFormat());
        return;
    }

    ui->rows->setValue(table->rows());
    ui->columns->setValue(table->columns());

    setupParams(table->format());
}

void TextEditorTableFormat::setupParams(const QTextTableFormat& format)
{
    m_origFormat = format;

    const Qt::Alignment hAlignBits = (format.alignment() & Qt::AlignHorizontal_Mask);
    const Qt::Alignment vAlignBits = (format.alignment() & Qt::AlignVertical_Mask);

    const int hAlignIndex =
            bool(hAlignBits & Qt::AlignLeft)    ? 0 :
            bool(hAlignBits & Qt::AlignRight)   ? 1 :
            bool(hAlignBits & Qt::AlignHCenter) ? 2 : 3;

    const int VAlignIndex =
            bool(vAlignBits & Qt::AlignTop)     ? 0 :
            bool(vAlignBits & Qt::AlignBottom)  ? 1 :
            bool(vAlignBits & Qt::AlignVCenter) ? 2 : 3;

    ui->cellPadding->setValue(format.cellPadding());
    ui->cellSpacing->setValue(format.cellSpacing());
    ui->borderWidth->setValue(format.border());
    ui->hAlignment->setCurrentIndex(hAlignIndex);
    ui->vAlignment->setCurrentIndex(VAlignIndex);
    ui->borderStyle->setCurrentIndex(int(format.borderStyle()));
    ui->position->setCurrentIndex(int(format.position()));
    ui->marginL->setValue(format.leftMargin());
    ui->marginR->setValue(format.rightMargin());
    ui->marginT->setValue(format.topMargin());
    ui->marginB->setValue(format.bottomMargin());
}

void TextEditorTableFormat::reset()
{
    setupParams(m_origFormat);
    ui->rows->setValue(m_origRows);
    ui->columns->setValue(m_origColumns);
}

void TextEditorTableFormat::defaults()
{
    setupParams(defaultFormat());
}

QTextTableFormat TextEditorTableFormat::defaultFormat() const
{
    QTextTableFormat defaults;

    defaults.setCellPadding(2.0);
    defaults.setCellSpacing(0.0);
    defaults.setMargin(2.0);
    defaults.setColumnWidthConstraints(m_origFormat.columnWidthConstraints());
    defaults.setBorderStyle(QTextTableFormat::BorderStyle_Outset);
    defaults.setPosition(QTextTableFormat::InFlow);

    return defaults;
}

void TextEditorTableFormat::on_buttonBox_clicked(QAbstractButton* button)
{
    if (button == ui->buttonBox->button(QDialogButtonBox::StandardButton::RestoreDefaults)) {
        defaults();
    } else if (button == ui->buttonBox->button(QDialogButtonBox::StandardButton::Reset)) {
        reset();
    }
}
