/*
    Copyright 2021-2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef NEWPANEDIALOGBASE_H
#define NEWPANEDIALOGBASE_H

#include <optional>
#include <QDialog>
#include <QHeaderView>

#include <src/fwddeclbase.h>
#include <src/core/settings.h>
#include <src/ui/filters/subtreefilter.h>
#include <src/ui/misc/querybar.h>

namespace Ui {
class NewPaneDialog;
} // namespace Ui

class MainWindowBase;
class PaneBase;

class NewPaneDialogBase :
        public QDialog,
        public Settings,
        public QueryBar
{
    Q_OBJECT

public:
    NewPaneDialogBase(MainWindowBase&);
    ~NewPaneDialogBase() override;

    // *** begin Settings API
    void save(QSettings&) const override;
    void load(QSettings&) override;
    // *** end Settings API

    void newConfig();

public slots:
    void updateActions();

protected slots:
    void doubleClicked(const QModelIndex&);
    void filterTextChanged(const QString&);
    void on_action_Add_New_Pane_triggered();
    void on_action_Replace_Pane_triggered();
    void on_action_Open_in_New_Window_triggered();
    void newFocus(PaneBase*);

protected:
    virtual void setupModel();
    void showEvent(QShowEvent*) override;
    void onShow(); // possibly create and populate UI
    void setupUi();
    void setupActions();
    void setupSignals();
    void setupView();
    void closeIfNoShift();  // close ourselves unless shift modifier is held
    std::optional<PaneClass_t> selectedPaneClass(bool warnIfNone = true) const;

    Ui::NewPaneDialog* ui;
    MainWindowBase&    m_mainWindow;
    QHeaderView        m_headerView;    // header view
    bool               m_firstShow;
};

#endif // NEWPANEDIALOGBASE_H
