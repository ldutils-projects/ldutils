/*
    Copyright 2019-2022 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef MAINWINDOWBASE_H
#define MAINWINDOWBASE_H

#include <functional>
#include <utility>

#include <QMainWindow>
#include <QErrorMessage>
#include <QProgressBar>
#include <QCursor>
#include <QTimer>
#include <QString>
#include <QStringList>
#include <QApplication>
#include <QSet>
#include <QScopedPointer>
#include <src/fwddeclbase.h>
#include <src/core/settings.h>
#include <src/core/uicolormodel.h>
#include <src/ui/panes/panebase.h>
#include <src/ui/widgets/tabwidget.h>
#include <src/util/reverseadapter.h>

class QCloseEvent;
class QSettings;
class QLockFile;
class ChangeTrackingModel;
class UndoMgr;
class CmdLineBase;

class MainWindowBase : public QMainWindow, public Settings
{
    Q_OBJECT

public:
    explicit MainWindowBase(const char* m_apptitle, const CmdLineBase&, QWidget *parent = nullptr);
    ~MainWindowBase() override;

    using actions_t = decltype(std::declval<QWidget>().actions()); // Never change, c++ :) ...

    const actions_t& paneClassAddActions() const { return m_paneAddActions; }
    const actions_t& paneClassGrpActions() const { return m_paneGrpActions; }
    const actions_t& paneClassRepActions() const { return m_paneRepActions; }
    const actions_t& paneClassWinActions() const { return m_paneWinActions; }
    actions_t& paneClassAddActions() { return m_paneAddActions; }
    actions_t& paneClassGrpActions() { return m_paneGrpActions; }
    actions_t& paneClassRepActions() { return m_paneRepActions; }
    actions_t& paneClassWinActions() { return m_paneWinActions; }

    virtual QAction* getPaneAction(PaneAction cc) const = 0;

    // Find pane matching given class P, and the predicate if supplied.
    template <typename P> P* findPane(const std::function<bool(P*)>& = [](P*) { return true; }) const; // search prior focus list first, then all.
    // Find pane given its pane Id
    template <typename P> P* findPane(PaneId_t paneId) const;

    // Factory for UI panes
    virtual QWidget* paneFactory(PaneClass_t paneClass = PaneClass_t(-1)) const = 0;
    virtual PaneBase::Container* containerFactory() const = 0;

    // Push this on the stack to save and restore the cursor.
    class SaveCursor {
    public:
        SaveCursor(MainWindowBase* mainWindow, Qt::CursorShape shape) :
            mainWindow(mainWindow)
        {
            if (mainWindow != nullptr) {
                oldCursor = mainWindow->cursor();
                mainWindow->setCursor(shape);
            }
        }
        ~SaveCursor() {
            if (mainWindow != nullptr)
                mainWindow->setCursor(oldCursor);
        }
    public:
        MainWindowBase* mainWindow;
        QCursor         oldCursor;
    };

    QString currentSettingsDirectory() const;    // directory of settings file in use
    QString currentSettingsFile() const;         // current settings file in use
    void setCurrentSettingsFile(const QString&); // update the settings file
    void clearCurrentSettingsFile() { setCurrentSettingsFile(QString()); }

    // We can't focus widgets during UI construction, so we defer it to a post-load hook.
    void setPostLoadFocus(QWidget* w) { m_postLoadFocus = w; }

    // run on all panels if className == nullptr
    template <typename P = PaneBase, typename FN = void> void runOnPanels(const FN& fn) const;

signals:
    void fontSizeChanged(int newSize);
    void paneFocusChanged(PaneBase*);

public slots:
    virtual void newConfig(bool newValues = true) = 0;
    virtual void selectionChanged(const QItemSelectionModel*,
                                  const QItemSelection& selected, const QItemSelection& deselected) = 0;
    virtual void currentChanged(const QModelIndex &current) = 0;
    virtual void statusMessage(UiType, const QString&) const; // status bar message
    virtual void addPaneAction(QAction*);
    virtual void addPaneAction(PaneClass_t);
    virtual void addGroupAction(QAction*);
    virtual void addGroupAction(PaneClass_t);
    virtual void replacePaneAction(QAction*);
    virtual void replacePaneAction(PaneClass_t);
    virtual void paneInWindowAction(QAction*);
    virtual void paneInWindowAction(PaneClass_t);
    virtual void dirtyStateChanged(bool) { }  // sent by UndoMgr
    virtual void updateActions(); // update action enable/disable state

protected slots:
    void loadRecentSession(QAction*);
    virtual void newFocus(QObject*);
    virtual void postLoadHook(); // see comment in C++

protected:
    friend class UndoWinCfg;
    friend class TabWidget; // TODO: Temporary
    friend class PaneBase;
    friend class UndoCfgData;

    template <typename... P>
    void runOnFocusPane(void (PaneBase::*method)(P...), const P&... p);
    void runOnFocusPane(const std::function<void(PaneBase*)>& fn);

    // *** begin Settings API
    bool saveUiConfig(QSettings& settings) const;
    bool loadUiConfig(QSettings& settings);
    // *** end Settings API

    virtual bool settingsSaver(QSettings&) const { return true; }   // Save derived class settings data
    virtual bool settingsLoader(QSettings&) { return true; }        // Load derived class settings data

    void      splitPaneInteractive(PaneBase* pane, Qt::Orientation orientation);

    static PaneBase::Container* paneParent(QWidget*);
    static const PaneBase::Container* paneParent(const QWidget*);

    static PaneBase::Container* addPane(PaneBase::Container* toAdd, PaneBase::Container* row, int before = 0);
    static PaneBase* addPane(PaneBase* toAdd, PaneBase::Container* parent, bool before = false,
                             PaneBase* refPane = nullptr);

    PaneBase* addGroupSibling(PaneBase* toAdd, PaneBase::Container* parent,
                              bool before = false, PaneBase* refPane = nullptr) const;

    void      setupSignals() const;      // focus changes, etc
    void      setupDefaults();           // dialog sizes
    void      setupTimers();             // post load timers
    void      setupDefaultSettings();
    void      setupAppConfig();          // setup app title, etc
    void      setupActionTooltips();     // set WhatsThis to match ToolTip
    void      cleanup();                 // things to do before deleting the UI
    void      changeFontSize(float mul); // multiply font size by a factor
    void      setFontSize(int newSize);  // set font size to new size
    void      closeEvent(QCloseEvent*) override;
    void      showEvent(QShowEvent*) override;
    void      error(const QString& message, const QString& type) const;
    void      unimplemented();
    bool      uiSave(const QString& file);
    bool      uiLoad(const QString& file, bool restoring = false);
    void      saveSettingsAs();
    void      saveSettings();
    void      openSettings();
    void      revertSettings();
    int       warningDialog(const QString& winTitle, const QString& msg);
    void      setLoadedSettings(const QString& file);  // current settings file changed on load/save
    [[nodiscard]] bool tryLockSettings(const QString& file);    // attempt to lock new settings; true on success

    // load config data from these settings
    virtual void loadCfgData(QSettings&) = 0;

    // If force is true, then reset all the UndoableObjects's dirty states as well, and
    // force-set the window title no matter its old state (used on save/load).
    virtual void markModified(bool dirty, bool force = false);

    // Helper to avoid duplication of this effort in derived clases
    virtual void updateUndoActions(const UndoMgr*, QAction* undo, QAction* redo, QAction* clear = nullptr);

    void      changeStatusBarColor(const QColor& color) const;
    void      changeStatusBarColor(QPalette::ColorRole) const;

    [[nodiscard]] PaneBase* focusedPane(QObject* focused = nullptr) const;
    [[nodiscard]] PaneBase* focusedPane(PaneBase* pane) const;
    [[nodiscard]] PaneBase* focusedPaneWarn(QObject* focused = nullptr) const;
    [[nodiscard]] PaneBase* focusedPaneWarn(PaneBase* pane) const;

    PaneBase* trackFocus(PaneBase*);  // track newly focused pane in the prevFocus deque
    void      paneRefocus(PaneBase* deletedPane = nullptr, PaneBase* newFocus = nullptr);
    PaneBase* replacePane(PaneBase* newPane, PaneBase* oldPane);
    void      removePane(PaneBase* pane);
    void      movePane(QWidget* pane, int relPos); // relpos can be - or +
    void      newWindowInteractive(PaneBase* pane = nullptr);
    TabWidget* newWindow();
    void      balanceSiblingsInteractive();

    static void closeSecondaryWindows();

    void      cleanStructure();
    static void moveChildren(PaneBase::Container* moveTo, PaneBase::Container* moveFrom);
    void      movePaneParent(PaneBase* pane, int relPos); // relpos can be - or +

    virtual bool backupFile(const QString& fname, int count) const;

    virtual void resizeColumnsAllPanes();

    virtual bool sessionSave();
    virtual void sessionRestore();
    virtual void newSession();
    virtual void removeSession(const QString& file); // remove session file
    virtual void firstRunHook();

    void recentSessionsChanged() { }
    void recentSessionsChanged(QMenu*);
    void restoreRecentSessions();
    void addSession(const QString& file); // add settings file to recent session list

    const TabWidget* mainWindowTabs() const;


    [[nodiscard]] TabWidget*     mainWindowTabs();
    [[nodiscard]] bool           hasSettingsFile() const { return !currentSettingsFile().isEmpty(); }
    [[nodiscard]] int            nextPaneId() { return m_nextPaneId++; }

    [[nodiscard]] static QString appConfigDir();
    [[nodiscard]] static QString appDataDir();
    [[nodiscard]] static QString recentSessionsFile();

    static const constexpr char* confFileFilter  = "Configuration (*.ini *.conf);;Text (*.txt);;Backups(*.conf.~*~);;All (*)";

    const char*        m_apptitle;         // application title

    bool               m_saveOnExit;       // true to autosave configuration on exit
    bool               firstExecution;     // whether to auto-show intro dialog
    int                m_startupFontSize;  // for resetting font

    QProgressBar       m_progressBar;      // bottom progress bar
    QStringList        recentSessions;     // recent sessions list
    bool               m_privateSession;   // don't save to sessions file

private:
    bool saveWinConfig(QSettings&) const;
    bool loadWinConfig(QSettings&);
    void saveRecentSessions();

    mutable QSet<QString> m_backedupFile;     // to limit save backups to one per file per session

    TabWidget*         m_uiTabs;              // main window tab widget
    QWidget*           m_postLoadFocus;       // we can't focus during load, because the UI doesn't exist yet.

    mutable QErrorMessage m_errorDialog;      // Report severe errors. Mutable, to use from const methods.

    actions_t          m_paneAddActions;      // pane add menu (dynamic)
    actions_t          m_paneGrpActions;      // add group sibling menu (dynamic)
    actions_t          m_paneRepActions;      // pane replace menu (dynamic)
    actions_t          m_paneWinActions;      // open pane in new window

    QScopedPointer<QLockFile> m_settingsFileLock; // lock for settings file
    QString            m_currentLockedFile;   // file locked by m_settingsFileLock
    QString            m_currentSettingsFile; // last file saved or loaded
    QString            defaultSettingsDir;    // default save location

    QList<PaneBase*>   m_prevFocus;           // deque of prior focused panels
    QTimer             m_postLoadTimer;       // see comment in C++

    int                m_nextPaneId;          // next unique paneId
    bool               m_modFlag;             // modification flag
    bool               m_loadError;           // extra checking on top of QSettings::status()
};

template <typename P, typename FN>
void MainWindowBase::runOnPanels(const FN& fn) const
{
    for (auto* window : QApplication::topLevelWidgets())
        for (auto& obj : window->findChildren<P*>())
            fn(static_cast<P*>(obj));
}

// search prior focus list first, then all.
template <typename P> P* MainWindowBase::findPane(const std::function<bool(P*)>& pred) const
{
    // Search through prior focus list first, most recent first
    for (auto* p : Util::reverse_adapter(m_prevFocus))
        if (P* pane = dynamic_cast<P*>(p); pane != nullptr)
            if (pred(pane))
                return pane;

    // It was never focused.  Last ditch attempt: dig through everything visible.
    for (auto* window : QApplication::topLevelWidgets())
        for (auto* tabs : window->findChildren<TabWidget*>())
            if (auto* tab = tabs->currentTab())
                for (P* pane : tab->findChildren<P*>())
                    if (pred(pane))
                        return pane;

    return nullptr;
}

template <typename P> P* MainWindowBase::findPane(PaneId_t paneId) const
{
    return findPane<P>([paneId](P* p) { return p->paneId() == paneId; });
}

template <typename... P>
void MainWindowBase::runOnFocusPane(void (PaneBase::*method)(P...), const P&... p)
{
    if (PaneBase* pane = focusedPaneWarn())
        (pane->*method)(p...);
}

#endif // MAINWINDOWBASE_H
