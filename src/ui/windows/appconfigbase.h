/*
    Copyright 2020-2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef APPCONFIGBASE_H
#define APPCONFIGBASE_H

#include <QDialog>

class QSettings;
class QAbstractButton;
class QDialogButtonBox;
class MainWindowBase;
class CfgDataBase;

class AppConfigBase : public QDialog
{
    Q_OBJECT

public:
    explicit AppConfigBase(MainWindowBase*);
    ~AppConfigBase() override;

    virtual void resetDefault(bool newConfig = true);
    virtual void resetPrevious(bool newConfig = true);
    virtual void applyCurrent(bool newConfig = true);
    virtual void load(QSettings&); // load active config from these settings

public slots:
    void accept() override;
    void reject() override;

protected:
    void showEvent(QShowEvent*) override;

    virtual void resetDefaultInteractive();     // interactive reset to default with dialog
    virtual void resetPreviousInteractive();    // interactive reset to previous with dialog
    virtual void acceptInteractive();           // interactive accept with dialog
    virtual void saveInitial();                 // save configuration for later restore
    virtual void onShow();                      // possibly create UI, and save initial cfgData
    virtual void appCfgButtons(QDialogButtonBox*, QAbstractButton*);

    virtual void setup()           = 0;         // populate UI
    virtual void updateUIFromCfg() = 0;         // make UI match the cfgData()
    virtual void updateCfgFromUI() = 0;         // make cfgData() match the UI

    virtual const CfgDataBase& prevCfgData() const = 0;
    virtual CfgDataBase& prevCfgData() = 0;

    MainWindowBase& m_mainWindow;
};

#endif // APPCONFIGBASE_H
