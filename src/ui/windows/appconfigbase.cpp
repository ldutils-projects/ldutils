/*
    Copyright 2020-2021 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <QMessageBox>
#include <QWhatsThis>
#include <QAbstractButton>
#include <QDialogButtonBox>
#include <QPushButton>

#include <src/undo/undocfgdata.h>

#include "src/ui/windows/mainwindowbase.h"
#include "src/core/appbase.h"
#include "appconfigbase.h"

AppConfigBase::AppConfigBase(MainWindowBase* mainWindow) :
    QDialog(mainWindow),
    m_mainWindow(*mainWindow)
{
}

AppConfigBase::~AppConfigBase()
{
}

void AppConfigBase::resetDefault(bool newConfig)
{
    cfgDataBaseWritable().reset();
    updateUIFromCfg();
    m_mainWindow.newConfig(newConfig);
}

void AppConfigBase::resetPrevious(bool newConfig)
{
    cfgDataBaseWritable() = prevCfgData(); // restore original values
    updateUIFromCfg();
    m_mainWindow.newConfig(newConfig);
}

void AppConfigBase::applyCurrent(bool newConfig)
{
    updateCfgFromUI();
    m_mainWindow.newConfig(newConfig);
}

void AppConfigBase::load(QSettings& settings)
{
    cfgDataBaseWritable().load(settings);
    updateUIFromCfg();
    m_mainWindow.newConfig();
}

void AppConfigBase::resetDefaultInteractive()
{
    QMessageBox msg(QMessageBox::Warning,
                    tr("Reset to Defaults?"),
                    tr("Reset to defaults?  This will reset your current configuration."),
                    QMessageBox::NoButton, this);

    msg.addButton(tr("&Reset"), QMessageBox::AcceptRole);
    msg.addButton(tr("&Cancel"), QMessageBox::RejectRole);

    if (msg.exec() == QMessageBox::AcceptRole)
        resetDefault(false);
}

void AppConfigBase::resetPreviousInteractive()
{
    QMessageBox msg(QMessageBox::Warning,
                    tr("Reset to Previous?"),
                    tr("Reset to previous configuration?  This will erase your current configuration."),
                    QMessageBox::NoButton, this);

    msg.addButton(tr("&Reset"), QMessageBox::AcceptRole);
    msg.addButton(tr("&Cancel"), QMessageBox::RejectRole);

    if (msg.exec() == QMessageBox::AcceptRole)
        resetPrevious(false);
}

void AppConfigBase::acceptInteractive()
{
    accept();
}

void AppConfigBase::saveInitial()
{
    updateUIFromCfg();
    prevCfgData() = cfgDataBase(); // save original values
}

void AppConfigBase::onShow()
{
    setup();        // create UI on first show
    saveInitial();  // save pre-edit settings
}

void AppConfigBase::showEvent(QShowEvent* event)
{
    onShow();  // tasks to perform just before showing
    QDialog::showEvent(event);
}

void AppConfigBase::accept()
{
    updateCfgFromUI();
    const UndoCfgData::ScopedUndo undoSet(m_mainWindow, undoMgr(), tr("Set Configuration"), prevCfgData(), cfgDataBase());
    if (undoSet.hasDiffs())
        m_mainWindow.newConfig(true);
    QDialog::accept();
}

void AppConfigBase::reject()
{
    cfgDataBaseWritable() = prevCfgData(); // restore original values
    m_mainWindow.newConfig(false);
    QDialog::reject();
}

void AppConfigBase::appCfgButtons(QDialogButtonBox* appCfgButtons, QAbstractButton* button)
{
    if (button == appCfgButtons->button(QDialogButtonBox::StandardButton::RestoreDefaults))
        resetDefaultInteractive();

    if (button == appCfgButtons->button(QDialogButtonBox::StandardButton::Reset))
        resetPreviousInteractive();

    if (appCfgButtons->buttonRole(button) == QDialogButtonBox::RejectRole)
        reject();

    // Rename track tags, if needed
    if (appCfgButtons->buttonRole(button) == QDialogButtonBox::AcceptRole)
        acceptInteractive();

    if (appCfgButtons->buttonRole(button) == QDialogButtonBox::ApplyRole)
        applyCurrent(false);

    if (appCfgButtons->buttonRole(button) == QDialogButtonBox::HelpRole)
        QWhatsThis::enterWhatsThisMode();
}
