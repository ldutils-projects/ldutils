/*
    Copyright 2019-2022 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <cassert>
#include <chrono>

#include <QInputDialog>
#include <QStatusBar>
#include <QDir>
#include <QFileInfo>
#include <QFile>
#include <QCloseEvent>
#include <QMessageBox>
#include <QFileDialog>
#include <QStandardPaths>
#include <QLockFile>

#include <src/util/util.h>
#include <src/util/ui.h>
#include <src/util/icons.h>
#include <src/util/cmdlinebase.h>
#include <src/undo/undowincfg.h>
#include <src/ui/panes/panebase.h>
#include <src/ui/widgets/tabwidget.h>
#include <src/core/cfgdatabase.h>
#include <src/core/appbase.h>

#include "mainwindowbase.h"

MainWindowBase::MainWindowBase(const char* apptitle, const CmdLineBase& cmdLineBase, QWidget *parent) :
    QMainWindow(parent),
    m_apptitle(apptitle),
    m_saveOnExit(true),
    firstExecution(!cmdLineBase.m_disableFirstRun),
    m_startupFontSize(QApplication::font().pointSize()),
    m_privateSession(cmdLineBase.m_privateSession),
    m_uiTabs(nullptr),
    m_postLoadFocus(nullptr),
    m_errorDialog(this),
    m_currentSettingsFile(cmdLineBase.m_initialSettingsFile),
    defaultSettingsDir(QDir::home().absolutePath()),
    m_postLoadTimer(this),
    m_nextPaneId(0),
    m_modFlag(false),
    m_loadError(false)
{
    setupDefaults();
    setupTimers();
    restoreRecentSessions();  // load recent sessions, if available
}

MainWindowBase::~MainWindowBase()
{
}

void MainWindowBase::cleanup()
{
    // Avoid attempting to reset focus on the way out.
    disconnect(reinterpret_cast<QGuiApplication*>(QGuiApplication::instance()),
               &QGuiApplication::focusObjectChanged,
               this, &MainWindowBase::newFocus);

    // Delete the tabs BEFORE we tear down the MainWindow, so destructors can use some of
    // our data on their way out.  Otherwise, the QWidget does this, by which time we're gone.
    if (auto* tabs = mainWindowTabs())
        tabs->deleteTabs(false);
}

void MainWindowBase::setupSignals() const
{
    // notify of focus changes
    connect(reinterpret_cast<QGuiApplication*>(QGuiApplication::instance()),
            &QGuiApplication::focusObjectChanged,
            this, &MainWindowBase::newFocus);
}

void MainWindowBase::setupDefaults()
{
    m_errorDialog.resize(500, 250);  // default size for error dialog
}

void MainWindowBase::setupAppConfig()
{
    QApplication::setApplicationDisplayName(m_apptitle);

    m_errorDialog.setWindowTitle("Error");
}

void MainWindowBase::setupActionTooltips()
{
    Util::SetupWhatsThis(this);
}

void MainWindowBase::setupTimers()
{
    m_postLoadTimer.setSingleShot(true);
    connect(&m_postLoadTimer, &QTimer::timeout, this, &MainWindowBase::postLoadHook, Qt::QueuedConnection);
}

void MainWindowBase::firstRunHook()
{
    firstExecution = false;
}

// The UI is not displayed yet on first load.  This is a workaround: a hook to be
// called after it is, to set up e.g, menu check states depending on some UI visibility.
void MainWindowBase::postLoadHook()
{
    if (!isVisible())  // in some environments, this happens before the main window is loaded
        return;

    if (firstExecution)
        firstRunHook();  // potentially show first-execution intro

    // Let panes run their own post load hook
    runOnPanels([](PaneBase* pane) { pane->postLoadHook(); });

    cfgDataBaseWritable().postLoadHook();

    // Focus can't be set during UI construction.
    if (m_postLoadFocus != nullptr)
        Util::SetFocus(m_postLoadFocus);

    if (m_privateSession)
        statusMessage(UiType::Warning, tr("Private session: Saves disabled."));

    if (!undoMgr().undoing()) // if we're loading for a UI undo, don't clear the undo.
        undoMgr().clear();
}

// track newly focused pane in the prevFocus deque
PaneBase* MainWindowBase::trackFocus(PaneBase* newFocus)
{
    static const int maxFocusDequeSize = 8;

    if (newFocus == nullptr)
        return nullptr;

    // Push it on the focus deque, unless it's already at the end.
    if (m_prevFocus.empty() || m_prevFocus.back() != newFocus)
        m_prevFocus.push_back(newFocus);

    while (m_prevFocus.size() > maxFocusDequeSize)
        m_prevFocus.pop_front();

    emit paneFocusChanged(newFocus);  // signal others

    return newFocus;
}

PaneBase* MainWindowBase::focusedPane(QObject* focused) const
{
    // If not given an explicit object, use the prior focus
    if (focused == nullptr && !m_prevFocus.empty())
        return m_prevFocus.back();

    // look up the chain until we run out, or find a class we understand.
    while (focused != nullptr) {
        auto* panel = qobject_cast<PaneBase*>(focused);
        if (panel != nullptr)
            return panel;

        focused = focused->parent();
    }

    return nullptr;
}

PaneBase* MainWindowBase::focusedPane(PaneBase* pane) const
{
    if (pane != nullptr)
        return pane;

    return focusedPane();
}

PaneBase* MainWindowBase::focusedPaneWarn(QObject *focused) const
{
    if (PaneBase* pane = focusedPane(focused))
        return pane;

    statusMessage(UiType::Info, tr("No container pane focused. Please click on one and try again."));

    return nullptr;
}

PaneBase* MainWindowBase::focusedPaneWarn(PaneBase* pane) const
{
    if (pane != nullptr)
        return pane;

    return focusedPaneWarn();
}

void MainWindowBase::setFontSize(int newSize)
{
    QFont font = QApplication::font();

    font.setPointSize(Util::Clamp(newSize, 5, 35));
    QApplication::setFont(font);

    // TODO: This is a workaround for an unknown problem where the main window won't
    // repaint the font changes unless something triggers a relayout(?).
    if (!appBase().testing())  // avoid show events under test suite
        for (auto* window : QApplication::topLevelWidgets())
            QFrame(window).show();

    // Fiddle with columns so they aren't truncated.
    resizeColumnsAllPanes();

    emit fontSizeChanged(newSize); // send signal for anything else which cares
}

void MainWindowBase::changeFontSize(float mul)
{
    QFont font = QApplication::font();

    int newSize = int(float(QApplication::font().pointSizeF()) * mul);

    if (newSize == font.pointSize())
        newSize += (mul > 1.0f) ? 1 : -1;

    setFontSize(newSize);
}

// We use the TabWidget as our secondary top level window class
TabWidget* MainWindowBase::newWindow()
{
    auto* tabBar = new TabWidget(*this);
    tabBar->setWindowFlags(Qt::Window);
    tabBar->setAttribute(Qt::WA_DeleteOnClose);

    if (!appBase().testing()) // avoid show events under test suite
        tabBar->show();

    Util::CopyActions(tabBar, this); // apply MainWindow's actions to new window

    return tabBar;
}

// For interactive (menu) use: create new window and give it default contents.
void MainWindowBase::newWindowInteractive(PaneBase* pane)
{
    TabWidget* tabBar = newWindow();

    if (pane == nullptr)
        pane = dynamic_cast<PaneBase*>(paneFactory());

    if (pane != nullptr) {
        tabBar->addTab("Main", pane);
        Util::SetFocus(pane);
    }

    tabBar->resize(640, 500);
}

// Clean up UI pane structure (remove empty groups, etc)
void MainWindowBase::cleanStructure()
{
    for (auto& row : findChildren<PaneBase::Container*>(PaneBase::containerName)) {
        if (row->count() == 0) {  // Remove empty groups
            row->deleteLater();
            break;
        }
        if (row->count() == 1) {
            QWidget* child = row->widget(0);
            if (child == nullptr)
                continue;

            // If a group has one child, replace group in its own parent with that child.
            if (PaneBase::Container* gp = paneParent(row); gp != nullptr) {
                if (const int index = gp->indexOf(row); index >= 0) {
                    gp->replaceWidget(index, child);
                    child->show();
                    row->deleteLater();
                }
            } else if (auto* cc = dynamic_cast<PaneBase::Container*>(child); cc != nullptr) {
                // special case: if this is already the root node, there's no more parent,
                // so add child's children to ourselves and remove that child.
                cc->deleteLater();       // remove ourselves
                cc->setParent(nullptr); // remove us from parent group (for size preservation)
                moveChildren(row, cc);
            }
        }
    }
}

// Move child items of a pane container to a differnent container, cloning the properties
// of the original (sizes and orientation).
void MainWindowBase::moveChildren(PaneBase::Container* moveTo, PaneBase::Container* moveFrom)
{
    const auto oldSizes = moveFrom->sizes(); // grab sizes before removing children

    // Move childen to new container, preserving order.
    while (moveFrom->count() > 0) {
        if (QWidget* child = moveFrom->widget(0); child != nullptr) {
            child->setParent(moveTo);
            child->show();
        }
    }

    moveTo->setOrientation(moveFrom->orientation()); // clone orientation of parent
    moveTo->setSizes(oldSizes);
}

TabWidget* MainWindowBase::mainWindowTabs()
{
    if (m_uiTabs != nullptr)  // return cached one
        return m_uiTabs;

    if (auto tabs = findChildren<TabWidget*>(); !tabs.isEmpty())
        return m_uiTabs = tabs.front();

    return nullptr;
}

const TabWidget* MainWindowBase::mainWindowTabs() const
{
    return const_cast<const TabWidget*>(const_cast<MainWindowBase*>(this)->mainWindowTabs());
}

// Update focus on pane removal.
void MainWindowBase::paneRefocus(PaneBase* deletedPane, PaneBase* newFocus)
{
    m_prevFocus.removeAll(deletedPane); // remove from prior focus

    // If no explicit new focus, get from prev focus list, if possible.
    if (newFocus == nullptr)
        newFocus = findPane<PaneBase>();

    // Focus the new pane
    if (newFocus != nullptr)
        Util::SetFocus(newFocus);

    // If we landed on the new-tab tab, pick another one.
    if (auto* tabs = mainWindowTabs()) {
        const int currentTab = tabs->currentIndex();
        if (currentTab == (tabs->count()-1))
            if (currentTab > 0)
                tabs->setCurrentIndex(currentTab - 1);
    }
}

PaneBase* MainWindowBase::replacePane(PaneBase* newPane, PaneBase* oldPane)
{
    if (oldPane == nullptr) {
        delete newPane;
        return nullptr;
    }

    PaneBase::Container* row = paneParent(oldPane);

    if (row == nullptr) {
        delete newPane;
        return nullptr;
    }

    const int colIndex = row->indexOf(oldPane);

    row->replaceWidget(colIndex, newPane);

    paneRefocus(oldPane, newPane);

    oldPane->deleteLater();

    statusMessage(UiType::Info, tr("Replaced pane: ") + oldPane->objectName() + " -> " +
                  newPane->objectName());

    return newPane;
}

void MainWindowBase::removePane(PaneBase* pane)
{
    if (pane == nullptr)
        return;

    paneRefocus(pane);

    pane->setParent(nullptr);
    pane->deleteLater();

    statusMessage(UiType::Info, tr("Removed pane: ") + pane->objectName());

    cleanStructure();
}

// This can move either panes or containers, so it accepts a QWidget
PaneBase::Container* MainWindowBase::paneParent(QWidget* pane)
{
    if (pane == nullptr)
        return nullptr;
    return dynamic_cast<PaneBase::Container*>(pane->parent());
}

const PaneBase::Container* MainWindowBase::paneParent(const QWidget* pane)
{
    if (pane == nullptr)
        return nullptr;
    return dynamic_cast<const PaneBase::Container*>(pane->parent());
}

// This can move either panes or containers, so it accepts a QWidget
void MainWindowBase::movePane(QWidget* pane, int relPos)
{
    if (pane == nullptr || relPos == 0)
        return;

    PaneBase::Container* row = paneParent(pane);

    if (row == nullptr)
        return;

    const int idx = row->indexOf(pane);

    // Can't move off ends
    if (idx < 0 || (idx == 0 && relPos < 0) || (idx >= (row->count()-1) && relPos > 0))
        return;

    pane->setParent(nullptr);       // remove from where it was

    // New index
    row->insertWidget(idx + relPos, pane); // add to where it goes

    // Refocus it iff it was a pane (it became unfocused on removal)
    Util::SetFocus(dynamic_cast<PaneBase*>(pane));

    updateActions();
}

void MainWindowBase::movePaneParent(PaneBase* pane, int relPos)
{
    if (pane == nullptr)
        return;

    PaneBase::Container* row = paneParent(pane);
    if (row == nullptr)
        return;

    movePane(row, relPos);
}

void MainWindowBase::resizeColumnsAllPanes()
{
    runOnPanels([](PaneBase* pane) {
        pane->resizeToFit();
    });
}

void MainWindowBase::changeStatusBarColor(const QColor &color) const
{
    Util::SetWidgetStyle(statusBar(), color);
}

void MainWindowBase::changeStatusBarColor(QPalette::ColorRole role) const
{
    changeStatusBarColor(QGuiApplication::palette().color(role));
}

void MainWindowBase::balanceSiblingsInteractive()
{
    const UndoWinCfg::ScopedUndo undoSet(*this, tr("Balance Siblings"));

    if (PaneBase* focused = focusedPaneWarn())
        if (auto* row = dynamic_cast<PaneBase::Container*>(focused->parent()))
            TabWidget::balanceChildren(row);
}

void MainWindowBase::newFocus(QObject* focus)
{
    if (PaneBase* focusPane = focusedPane(focus); focusPane != nullptr) {
        if (!m_prevFocus.empty()) {
            if (focusPane == m_prevFocus.back())  // spam prevention.
                return;

            m_prevFocus.back()->focusOut();
        }

        focusPane->focusIn();

        // Need to change code just below if Pane::Superclass changes
        // Damnit, we want a static_assert here, but typeid is not constexpr.
        assert(typeid(PaneBase::Superclass) == typeid(QGroupBox));

        // Set previous focus item title to non-bold
        if (!m_prevFocus.empty())
            Util::SetWidgetStyle(m_prevFocus.back(), QColor(), "", "normal");

        // Set current focus item title to bold
        if (focus != nullptr)
            Util::SetWidgetStyle(focusPane, QColor(), "", "bold");

        trackFocus(focusPane);

        if (focusPane->selectionModel() != nullptr) {
            currentChanged(focusPane->selectionModel()->currentIndex());
            selectionChanged(focusPane->selectionModel(),
                             focusPane->selectionModel()->selection(), QItemSelection());
        }
    } else {
        currentChanged(QModelIndex());
        selectionChanged(nullptr, QItemSelection(), QItemSelection());
    }

    updateActions();
}

// Backup file in fname.  Returns true on success.  Keeps "count" numbered backups as [1..count]
bool MainWindowBase::backupFile(const QString& fname, int count) const
{
    const auto backup = [&fname](int n) {
        return (n == 0) ? QFile(fname) : QFile(fname + ".~" + QString::number(n) + "~");
    };

    if (count == 0 || !backup(0).exists() || m_backedupFile.contains(fname))
        return true;

    m_backedupFile.insert(fname); // only back up each file once per session

    // If there's an open slot, use it (fill out the count)
    for (int i = 1; i <= count; ++i)
        if (!backup(i).exists())
            return backup(0).rename(backup(i).fileName());

    // Too many: we have to remove one.
    // Keep N numbered backups as [1..N], with most recent as biggest N.
    // Move old [2..N] to be new [1..N-1].
    for (int i = 1; i < count; ++i) {
        if (backup(i+1).exists()) {
            if (backup(i+0).exists() && !backup(i+0).remove())
                return false;
            if (!backup(i+1).rename(backup(i+0).fileName()))
                return false;
        }
    }

    return backup(0).rename(backup(count).fileName());
}

QString MainWindowBase::currentSettingsDirectory() const
{
    return QFileInfo(currentSettingsFile()).dir().path();
}

QString MainWindowBase::currentSettingsFile() const
{
    return m_currentSettingsFile;
}

void MainWindowBase::setCurrentSettingsFile(const QString& path)
{
    m_currentSettingsFile = path;

    if (m_currentSettingsFile.isEmpty())
        m_settingsFileLock.reset();
}

QString MainWindowBase::appConfigDir()
{
    return QStandardPaths::writableLocation(QStandardPaths::AppConfigLocation);
}

QString MainWindowBase::appDataDir()
{
    return QStandardPaths::writableLocation(QStandardPaths::AppDataLocation);
}

QString MainWindowBase::recentSessionsFile()
{
    return appConfigDir() + QDir::separator() + "RecentSessions.conf";
}

void MainWindowBase::restoreRecentSessions()
{
    // Restore list
    QSettings settings(recentSessionsFile(), QSettings::IniFormat, this);
    MemberLoad(settings, recentSessions);

    // If no settings supplied externally, use most recent one from recentSessions
    if (!hasSettingsFile() && !recentSessions.isEmpty())
        setCurrentSettingsFile(recentSessions.front());

    // If none from recentSessions, use default as last resort
    if (!hasSettingsFile())
        setCurrentSettingsFile(appDataDir() + QDir::separator() + QApplication::applicationName() + ".conf");

    addSession(currentSettingsFile());

    recentSessionsChanged();  // let subclasses update
}

void MainWindowBase::loadRecentSession(QAction* action)
{
    // Short-circuit if no session change to avoid reload.
    if (currentSettingsFile() == action->iconText())
        return;

    uiLoad(action->iconText());
}

void MainWindowBase::saveRecentSessions()
{
    QSettings settings(recentSessionsFile(), QSettings::IniFormat, this);
    MemberSave(settings, recentSessions);

    settings.sync();

    if (settings.status() != QSettings::NoError)
        error(tr("Unable to save recent sesssions"), tr("Save Sessions"));
}

void MainWindowBase::recentSessionsChanged(QMenu* recentSessionsMenu)
{
    if (recentSessionsMenu == nullptr)
        return;

    recentSessionsMenu->clear();

    // Only add entries that exist on the filesystem
    for (const auto& session : recentSessions)
        if (QFile(session).exists())
            if (QAction* action = recentSessionsMenu->addAction(Icons::get("document-open"), session); action != nullptr)
                action->setIconText(session);
}

void MainWindowBase::addSession(const QString& file)
{
    static const int recentSessionCount = 5;

    defaultSettingsDir = QFileInfo(file).path();
    setCurrentSettingsFile(QDir::toNativeSeparators(file));

    if (m_privateSession)  // don't update session file if asked not to
        return;

    if (int index = recentSessions.indexOf(file); index >= 0) {
        // If recentSessions already contains the session file, bump it to the top.
        if (index == 0)  // list wouldn't change
            return;

        recentSessions.move(index, 0);
    } else {
        // Add new item at beginning, and trim list size.
        recentSessions.prepend(file);

        while (recentSessions.size() > recentSessionCount)
            recentSessions.pop_back();
    }

    saveRecentSessions();
    recentSessionsChanged(); // let subclasses update
}

void MainWindowBase::removeSession(const QString& file)
{
    if (currentSettingsFile() == file)
        clearCurrentSettingsFile();  // clear

    if (recentSessions.removeAll(file) == 0)
        return;

    saveRecentSessions();
    recentSessionsChanged();
}

bool MainWindowBase::sessionSave()
{
    if (!hasSettingsFile() || m_privateSession)
        return true;

    // Ensure directory containing lastSettingsFile exists.
    const QString dirname = currentSettingsDirectory();

    if (!QDir::root().mkpath(dirname)) {
        QMessageBox::critical(this, tr("Save Error"),
                              tr("Error creating directory for settings file: ") + dirname,
                              QMessageBox::Ok);

        return false;
    }

    // Backup old one
    backupFile(currentSettingsFile(), appBase().cfgData().backupUICount);

    QSettings settings(currentSettingsFile(), QSettings::IniFormat, this);
    save(settings);
    settings.sync();

    if (settings.status() != QSettings::NoError) {
        QMessageBox::critical(this, tr("Save Error"),
                              tr("Error saving settings: ") + currentSettingsFile(),
                              QMessageBox::Ok);
        return false;
    }

    return true;
}

void MainWindowBase::sessionRestore()
{
    // Don't use uiLoad for this: we don't want an error dialog if it fails
    uiLoad(currentSettingsFile(), true);
}

void MainWindowBase::newSession()
{
    appBase().newSession();
    clearCurrentSettingsFile();
}

void MainWindowBase::closeEvent(QCloseEvent* event)
{
    if (appBase().cfgData().warnOnExit) {
        if (warningDialog(tr("Quit") + m_apptitle, tr("Quit application?")) == QMessageBox::Cancel) {
            event->ignore();
            return;
        }
    }

    if (m_saveOnExit && !m_privateSession) {
        if (!hasSettingsFile()) {
           saveSettingsAs();
        } else if (!sessionSave()) {
            event->ignore();
            return;
        }
    }

    closeSecondaryWindows();
    QApplication::closeAllWindows();

    // For an unknown reason, on certain distros such as OpenSUSE, QGuiApplication::quitOnLastWindowClosed
    // is not causing the app to exit after the last window has been closed.  This forces the issue.
    // TODO: figure out why the normal quitOnLastWindowClosed handling isn't working.
    if (!appBase().testing())
        qApp->quit();
}

void MainWindowBase::showEvent(QShowEvent* event)
{
    QMainWindow::showEvent(event);

    // In some environments, the window isn't displayed even after the call to QMainWindow::showEvent(),
    // so we do this on a small timer so the window is displayed.
    using namespace std::chrono_literals;
    m_postLoadTimer.start(500ms);
}

void MainWindowBase::statusMessage(UiType ml, const QString& txt) const
{
    if (!isVisible())
        return;

    if (ml < UiType::_Begin || ml >= UiType::_Count)
        return;

    int timeout;

    // default timeouts per message level
    switch (ml) {
    case UiType::Warning:  timeout =  5000; break;
    case UiType::Error:    timeout = 10000; break;
    case UiType::Critical: timeout = 15000; break;
    default:               timeout =  3000; break;
    }

    statusBar()->showMessage(txt, timeout);
    changeStatusBarColor(appBase().cfgData().uiColor[ml]);
}

void MainWindowBase::error(const QString& message, const QString& type) const
{
    if (!appBase().testing()) // don't show the dialog under the test suite
        m_errorDialog.showMessage(message, type);

    statusMessage(UiType::Error, message);
}

// Display unimplemented warning
void MainWindowBase::unimplemented()
{
    QMessageBox::warning(this, "Unimplemented",
                         "This feature is currently unimplemented.  Check back later!",
                         QMessageBox::Ok);
}

PaneBase::Container* MainWindowBase::addPane(PaneBase::Container* toAdd, PaneBase::Container* row, int before)
{
    if (toAdd == nullptr || row == nullptr)
        return nullptr;

    row->insertWidget(before, toAdd);

    return toAdd;
}

PaneBase* MainWindowBase::addPane(PaneBase* toAdd, PaneBase::Container* parent, bool before, PaneBase* refPane)
{
    if (toAdd == nullptr)
        return nullptr;

    if (parent == nullptr)
        parent = paneParent(refPane);

    if (parent == nullptr) {
        delete toAdd;  // because we adopt it
        return nullptr;
    }

    int colIndex;

    if (refPane == nullptr) {
        colIndex = before ? 0 : parent->count();
    } else {
        colIndex = parent->indexOf(refPane);
    }

    if (!before)
        ++colIndex;

    parent->insertWidget(colIndex, toAdd);

    return toAdd;
}
void MainWindowBase::splitPaneInteractive(PaneBase* pane, Qt::Orientation orientation)
{
    if (pane == nullptr)
        return;

    PaneBase::Container* parent = paneParent(pane);
    if (parent == nullptr)
        return;

    PaneBase* newPane = pane->clone();
    if (newPane == nullptr)
        return;

    const UndoWinCfg::ScopedUndo undoSet(*this, tr("Split Pane") + " " +
                                         (orientation == Qt::Horizontal ? tr("Horizontally") : tr("Vertically")));

    // Same orientation: Just add to that group
    if (parent->orientation() == orientation) {
        auto* refPane = pane; // rename the value to placate clang-tidy.
        addPane(newPane, parent, true, refPane);
        return;
    }

    // If we get here, we are going to split in the other orientation (H in a V group,
    // or V in an H group).
    auto* newContainer = containerFactory();

    if (newContainer == nullptr) {
        delete newPane;
        return;
    }

    newContainer->setOrientation(orientation);

    const int location = parent->indexOf(pane);

    parent->replaceWidget(location, newContainer);
    addPane(pane, newContainer);
    addPane(newPane, newContainer);

    statusMessage(UiType::Info, tr("Split pane: ") + pane->objectName());
}

PaneBase* MainWindowBase::addGroupSibling(PaneBase* toAdd, PaneBase::Container* parent,
                                          bool before, PaneBase* refPane) const
{
    if (toAdd == nullptr)
        return nullptr;

    if (parent == nullptr)
        parent = paneParent(refPane);

    if (parent == nullptr) {
        delete toAdd;  // because we adopt it
        return nullptr;
    }

    PaneBase::Container* grandparent = paneParent(parent);

    // Grandparent may be a group, or a tab
    if (grandparent == nullptr) {
        // grandparent is tab: change its group to other orientation and add its children to a new group.
        PaneBase::Container* newGroup = containerFactory();
        if (newGroup == nullptr) {
            delete toAdd;
            return nullptr;
        }

        moveChildren(newGroup, parent);

        // Add the cloned group, and the new widget.
        if (before)  parent->addWidget(toAdd);
        parent->addWidget(newGroup);
        if (!before) parent->addWidget(toAdd);

        // Reverse orientation of parent.
        parent->setOrientation(parent->orientation() == Qt::Horizontal ? Qt::Vertical : Qt::Horizontal);
    } else {
        // grandparent is a group: just add to that.
        addPane(toAdd, grandparent, before, refPane);
    }

    return toAdd;
}

void MainWindowBase::addPaneAction(PaneClass_t pc)
{
    PaneBase* refPane = focusedPane();
    PaneBase::Container* row = nullptr;

    if (refPane == nullptr) { // No reference. Find a row to add to.
        row = findChild<PaneBase::Container*>();
        if (row == nullptr) {
            statusMessage(UiType::Warning, tr("No active pane found"));
            return;
        }
    }

    const UndoWinCfg::ScopedUndo undoSet(*this, tr("Add New Pane"));

    // Create and focus a new pane
    Util::SetFocus(addPane(dynamic_cast<PaneBase*>(paneFactory(pc)), row, true, refPane));
}

void MainWindowBase::addPaneAction(QAction *action)
{
    addPaneAction(PaneClass_t(paneClassAddActions().indexOf(action)));
}

void MainWindowBase::addGroupAction(PaneClass_t pc)
{
    PaneBase* refPane = focusedPaneWarn();
    if (refPane == nullptr)
        return;

    PaneBase::Container* row = paneParent(refPane);
    if (row == nullptr)  // unable to find parent container
        return;

    const UndoWinCfg::ScopedUndo undoSet(*this, tr("Add Group Sibling"));

    // Add group sibling and focus it.
    Util::SetFocus(addGroupSibling(dynamic_cast<PaneBase*>(paneFactory(pc)), row, false, refPane));
}

void MainWindowBase::addGroupAction(QAction* action)
{
    addGroupAction(PaneClass_t(paneClassGrpActions().indexOf(action)));
}

void MainWindowBase::replacePaneAction(PaneClass_t pc)
{
    PaneBase* refPane = focusedPaneWarn();
    if (refPane == nullptr)
        return;

    const UndoWinCfg::ScopedUndo undoSet(*this, tr("Replace Pane"));

    // Create and focus a new pane
    Util::SetFocus(replacePane(dynamic_cast<PaneBase*>(paneFactory(pc)), refPane));
}

void MainWindowBase::replacePaneAction(QAction* action)
{
    replacePaneAction(PaneClass_t(paneClassRepActions().indexOf(action)));
}

void MainWindowBase::paneInWindowAction(PaneClass_t pc)
{
    const UndoWinCfg::ScopedUndo undoSet(*this, tr("Open Pane in New Window"));

    newWindowInteractive(dynamic_cast<PaneBase*>(paneFactory(pc)));
}

void MainWindowBase::paneInWindowAction(QAction* action)
{
    paneInWindowAction(PaneClass_t(paneClassWinActions().indexOf(action)));
}

void MainWindowBase::updateActions()
{
    const PaneBase* focused    = focusedPane();
    const PaneBase::Container* paneRow = paneParent(focused);
    const PaneBase::Container* paneGP  = paneParent(paneRow);
    // indexOf isn't available in a const flavor
    const int paneIndex   = (paneRow != nullptr) ? paneRow->indexOf(const_cast<PaneBase*>(focused)) : -1;
    const int paneGPIndex = (paneGP != nullptr) ? paneGP->indexOf(const_cast<PaneBase::Container*>(paneRow)) : -1;

    const bool hasFocused      = (focused != nullptr);
    const bool canMovePaneL    = (paneRow != nullptr && paneIndex > 0);
    const bool canMovePaneR    = (paneRow != nullptr && paneIndex < (paneRow->count()-1));
    const bool canSplitPane    = (paneRow != nullptr && hasFocused);
    const bool canBalancePanes = canSplitPane;
    const bool canMovePaneGPL  = (paneGP != nullptr && paneGPIndex > 0);
    const bool canMovePaneGPR  = (paneGP != nullptr && paneGPIndex < (paneGP->count()-1));

    getPaneAction(PaneAction::PaneClose)->setEnabled(hasFocused);
    getPaneAction(PaneAction::PaneLeft)->setEnabled(canMovePaneL);
    getPaneAction(PaneAction::PaneRight)->setEnabled(canMovePaneR);
    getPaneAction(PaneAction::PaneRowUp)->setEnabled(canMovePaneGPL);
    getPaneAction(PaneAction::PaneRowDown)->setEnabled(canMovePaneGPR);
    getPaneAction(PaneAction::PaneSplitH)->setEnabled(canSplitPane);
    getPaneAction(PaneAction::PaneSplitV)->setEnabled(canSplitPane);
    getPaneAction(PaneAction::PaneBalanceSiblings)->setEnabled(canBalancePanes);

    for (auto* grpAction : paneClassGrpActions())
        grpAction->setEnabled(paneGP != nullptr);

    for (auto* addAction : paneClassAddActions())
        addAction->setEnabled(hasFocused);

    for (auto* repAction : paneClassRepActions())
        repAction->setEnabled(hasFocused);
}

void MainWindowBase::closeSecondaryWindows()
{
    // Close and delete current secondary windows
    for (auto* window : QApplication::topLevelWidgets()) {
        if (auto* tabWidget = dynamic_cast<TabWidget*>(window)) {
            tabWidget->hide();
            tabWidget->deleteLater();
        }
    }
}

bool MainWindowBase::saveWinConfig(QSettings& settings) const
{
    if (const auto* tabs = mainWindowTabs())
        tabs->save(settings);

    // Save secondary windows
    settings.beginWriteArray("windows"); {
        int winId = 0;
        for (const auto* window : QApplication::topLevelWidgets()) {
            if (const auto* tabWidget = dynamic_cast<const TabWidget*>(window)) {
                settings.setArrayIndex(winId++);
                tabWidget->save(settings);
            }
        }
    } settings.endArray();

    return true;
}

bool MainWindowBase::saveUiConfig(QSettings& settings) const
{
    // Clear out old settings
    settings.clear();

    bool ok = true;

    // Save window size/position
    settings.beginGroup("MainWindow"); {
        SL::Save(settings, versionKey, version());
        SL::Save(settings, "geometry", saveGeometry());
        SL::Save(settings, "state",    saveState());
        SL::Save(settings, "cfgData",  appBase().cfgData());
        MemberSave(settings, defaultSettingsDir);
        MemberSave(settings, firstExecution);

        ok &= settingsSaver(settings); // save derived settings if any
        ok &= saveWinConfig(settings); // load tab/window layout

        // Save font size
        SL::Save(settings, "fontSize", QApplication::font().pointSize());
    } settings.endGroup();

    return ok;
}

bool MainWindowBase::loadWinConfig(QSettings& settings)
{
    // We can't foocus during UI construction, so this is deferred to the postLoadHook
    setPostLoadFocus(nullptr);

    if (auto* tabs = mainWindowTabs())
        tabs->load(settings);

    // Load new secondary windows (after deleting any currently open ones)
    closeSecondaryWindows();
    const int winCount = settings.beginReadArray("windows"); {
        for (int winId = 0; winId < winCount; ++winId) {
            settings.setArrayIndex(winId);
            (newWindow())->load(settings);
        }
    } settings.endArray();

    cleanStructure();

    return true;
}

bool MainWindowBase::loadUiConfig(QSettings& settings)
{
    undoMgr().clear(); // undo/redo no longer apply in this new world.

    m_loadError = false;

    // Load window size/position
    settings.beginGroup("MainWindow"); {
        // only load compatible versions
        if (SL::Load(settings, versionKey, 0) == Settings::version()) {
            SL::Load(settings, "cfgData", appBase().cfgData());

            if (settings.contains("geometry"))
                restoreGeometry(settings.value("geometry").toByteArray());
            if (settings.contains("state"))
                restoreState(settings.value("state").toByteArray());

            MemberLoad(settings, defaultSettingsDir);
            MemberLoad(settings, firstExecution);

            m_loadError |= !settingsLoader(settings); // load derived settings, if any
            m_loadError |= !loadWinConfig(settings);  // load tab/window layout

            // Font size
            if (settings.contains("fontSize"))
                setFontSize(SL::Load(settings, "fontSize", 10));
        } else {
            m_loadError = true;
        }
    } settings.endGroup();

    paneRefocus();            // update the focused pane
    newConfig();              // refresh everything from just-loaded config
    undoMgr().clear();        // reset undo manager
    m_postLoadTimer.start(0); // hook to do things after the UI is finally displayed.

    return !m_loadError;
}

void MainWindowBase::runOnFocusPane(const std::function<void(PaneBase*)>& fn)
{
    if (PaneBase* pane = focusedPaneWarn())
        fn(pane);
}

bool MainWindowBase::uiSave(const QString& file)
{
    if (m_privateSession)   // don't save private sessions
        return false;

    // Attempt to lock the new file
    if (!tryLockSettings(file))
        return false;

    addSession(file);

    if (!backupFile(currentSettingsFile(), appBase().cfgData().backupUICount))
        error(tr("Error creating settings file backup:<p>") + currentSettingsFile(), tr("Save UI"));

    QSettings settings(file, QSettings::IniFormat, this);
    save(settings);

    if (settings.status() == QSettings::NoError) {
        statusMessage(UiType::Info, tr("Saved: ") + file);
        setLoadedSettings(currentSettingsFile());
        return true;
    }

    error(tr("Error saving settings:<p>") + file, tr("Save UI"));
    return false;
}

bool MainWindowBase::uiLoad(const QString& file, bool restoring)
{
    // Attempt to lock the new file
    if (!tryLockSettings(file)) {
        if (!firstExecution)
            clearCurrentSettingsFile();
        return false;
    }

    addSession(file);

    QSettings settings(file, QSettings::IniFormat, this);
    load(settings);

    if (settings.status() == QSettings::NoError && !m_loadError) {
        setLoadedSettings(currentSettingsFile());
        statusMessage(UiType::Info, tr("Loaded: ") + file);
        return true;
    }

    if (!restoring) {
        error(tr("Error loading settings:<p>") + file, tr("Load UI"));
        removeSession(file);
    }

    return false;
}

void MainWindowBase::saveSettingsAs()
{
    const QString settingsFile =
            QFileDialog::getSaveFileName(this, tr("Save Configuration"), defaultSettingsDir,
                                         confFileFilter, nullptr);

    if (settingsFile.isEmpty()) {
        statusMessage(UiType::Warning, tr("Canceled"));
        return;
    }

    uiSave(settingsFile);
}

void MainWindowBase::saveSettings()
{
    if (!hasSettingsFile() ||
        (QFileInfo::exists(currentSettingsFile()) && !QFileInfo(currentSettingsFile()).isWritable()))
        return saveSettingsAs();

    uiSave(currentSettingsFile());
}

void MainWindowBase::openSettings()
{
    const QString settingsFile =
            QFileDialog::getOpenFileName(this, tr("Open Configuration"), defaultSettingsDir,
                                         confFileFilter,  nullptr, QFileDialog::ReadOnly);

    if (settingsFile.isEmpty()) {
        statusMessage(UiType::Warning, tr("Canceled"));
        return;
    }

    uiLoad(settingsFile);
}

void MainWindowBase::revertSettings()
{
    if (!hasSettingsFile()) {
        statusMessage(UiType::Error, tr("No settings file to revert from"));
        return;
    }

    if (appBase().cfgData().warnOnRevert)
        if (warningDialog(tr("Revert settings"),
                          tr("Revert settings from file?") + "\n   " + currentSettingsFile()) != QMessageBox::Ok)
            return;

    const QFileInfo stat(currentSettingsFile());

    if (stat.isReadable())
        uiLoad(currentSettingsFile());
    else
        statusMessage(UiType::Error, tr("Unable to read save file"));
}

int MainWindowBase::warningDialog(const QString& winTitle, const QString& msg)
{
    return Util::WarningDialog(*this, winTitle, msg);
}

void MainWindowBase::setLoadedSettings(const QString& file)
{
    const QFileInfo fileInfo(file);

    setWindowTitle(fileInfo.baseName() + "[*]");
}

bool MainWindowBase::tryLockSettings(const QString& file)
{
    if (file.isEmpty()) {
        error(tr("Error: No session file."), tr("Settings"));
        return false;
    }

    // It's OK to relock the same file
    if (m_currentLockedFile == file)
        return true;

    const QString lockFile = file + ".~lock~";

    decltype(m_settingsFileLock) newLock(new QLockFile(lockFile));
    newLock->setStaleLockTime(0);

#   define INDENT "&nbsp;&nbsp;"

    // Try to lock. On failure, ask user what to do.
    while (!newLock->tryLock(1)) {
        if (newLock->error() == QLockFile::LockFailedError) {
            firstExecution = false; // avoid the first run dialog due to lack of settings load

            // Who has it locked?
            qint64 pid;
            QString hostname, appname;
            const bool haveInfo = newLock->getLockInfo(&pid, &hostname, &appname);

            if (QMessageBox::warning(nullptr, tr("File locked"),
                                     tr("<p><b>File is locked by another process.  Do you wish to use it anyway?</b></p>"
                                        "<p><b>Warning:</b> This can cause unpredictable behavior if it is in use by another program.</p>"
                                        "<p>"
                                        INDENT "<b>PID:</b> %1<br/>"
                                        INDENT "<b>Application:</b> %2<br/>"
                                        INDENT "<b>File:</b> %3<br/>"
                                        "</p>")
                                     .arg(haveInfo ? pid : -1)
                                     .arg(haveInfo ? appname : tr("Unknown"), file),
                                     QMessageBox::Yes|QMessageBox::No,
                                     QMessageBox::No) != QMessageBox::Yes) {
                break;
            }

            if (!QFile(lockFile).remove()) {
                QMessageBox::critical(nullptr, tr("Lock error"),
                                      tr("<p>Unable to remove lock for:<br/></p>"
                                         "<p>" INDENT "<b>File:</b> %1<br/></p>")
                                      .arg(file), QMessageBox::Ok);
                break;
            }

            continue;  // try again, now that we've removed the old lock.
        }

        if (newLock->error() == QLockFile::PermissionError) {
            firstExecution = false; // avoid the first run dialog due to lack of settings load

            QMessageBox::critical(nullptr, tr("Lock error"),
                                  tr("<p>No permissions to create lock for:<br/></p>"
                                     "<p>" INDENT "<b>File:</b> %1<br/></p>")
                                  .arg(file), QMessageBox::Ok);
        }

        break;
    }

#   undef INDENT

    // old lock will be deleted when it goes out of scope

    if (newLock->isLocked()) {
        m_settingsFileLock.swap(newLock);
        m_currentLockedFile = file;
        return true;
    }

    statusMessage(UiType::Warning, tr("Canceled: ") + file);
    return false;
}

void MainWindowBase::markModified(bool dirty, bool force)
{
    if (dirty == m_modFlag && !force)
        return;

    m_modFlag = dirty;

    setWindowModified(dirty);

    if (force || appBase().undoMgr().isDirty() != dirty)
        undoMgr().setDirty(dirty);
}

void MainWindowBase::updateUndoActions(const UndoMgr* mgrToUse, QAction* undo, QAction* redo, QAction* clear)
{
    const int undoCount = (mgrToUse != nullptr) ? mgrToUse->undoCount() : 0;
    const int redoCount = (mgrToUse != nullptr) ? mgrToUse->redoCount() : 0;

    const QString topUndoName = (mgrToUse != nullptr) ? mgrToUse->topUndoName() : "";
    const QString topRedoName = (mgrToUse != nullptr) ? mgrToUse->topRedoName() : "";

    const auto undoTxt = [](const QString& type, const QString& name, int count) {
        return type + " " + name +
                (count > 1 ? QString(tr(" (... %1 more)")).arg(count - 1) : tr(""));
    };

    const auto setUndoName = [](QAction* undo, int count, const QString& name) {
        if (undo != nullptr) {
            undo->setEnabled(count > 0);
            undo->setText(name);      // action text
            undo->setToolTip(name);   // for the toolbar
            undo->setWhatsThis(name);
            undo->setStatusTip(name);
        }
    };

    const QString undoName = undoTxt(tr("Undo"), topUndoName, undoCount);
    const QString redoName = undoTxt(tr("Redo"), topRedoName, redoCount);

    if (clear != nullptr)
        clear->setEnabled(undoCount > 0 || redoCount > 0);

    setUndoName(undo, undoCount, undoName);
    setUndoName(redo, redoCount, redoName);
}
