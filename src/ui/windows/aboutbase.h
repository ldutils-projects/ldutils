/*
    Copyright 2019-2022 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef ABOUTBASE_H
#define ABOUTBASE_H

#include <QDialog>

class QTabWidget;
class QAction;

class AboutBase : public QDialog
{
public:
    using QDialog::QDialog;

    // This order must match the UI tab bar order
    enum Tab {
      About,
      Authors,
      Libraries,
      License,
      Privacy,
      Donate,
    };

    void showTab(Tab);

protected slots:
    void prevTab();
    void nextTab();

protected:
    QTabWidget* tabs;

    void setupPrevNext(QTabWidget*, QAction* next, QAction* prev);
    void setupActionIcons(QAction* next, QAction* prev);
};

#endif // ABOUTBASE_H
