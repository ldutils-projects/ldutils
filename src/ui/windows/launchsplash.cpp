/*
    Copyright 2020-2022 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <chrono>

#include <QGuiApplication>
#include <QString>
#include <QPalette>

#include "src/core/appbase.h"
#include "src/util/ui.h"

#include "launchsplash.h"
#include "ui_launchsplash.h"

LaunchSplash::LaunchSplash(int count, const QString& logoHtml) :
    QFrame(nullptr),
    m_current(0),
    ui(new Ui::LaunchSplash)
{
    ui->setupUi(this);
    setWindowFlags(Qt::SplashScreen | Qt::FramelessWindowHint);

    ui->appLogo->setText(logoHtml);

    setupTimer();
    setupTitle();
    Util::SetupWhatsThis(this);

    setBackgroundRole(QPalette::Midlight);

    ui->progressBar->setMaximum(count);

    if (!appBase().testing()) { // show unless running under test suite
        show();
        qApp->processEvents();  // let event loop run to display this window.
    }
}

LaunchSplash::~LaunchSplash()
{
    delete ui;
}

void LaunchSplash::setupTitle()
{
    const QString appTitleText =
            QString("<html><head/><body><p align=center><span style=\" font-size:25pt; font-style:italic;\">") +
            QApplication::applicationDisplayName() + " " + QApplication::applicationVersion() +
            "</span></p></body></html>";

    ui->appTitle->setText(appTitleText);
}

void LaunchSplash::setupTimer()
{
    m_hideTimer.setSingleShot(true);

    connect(&m_hideTimer, &QTimer::timeout, this, [this]() { hide(); });
}

void LaunchSplash::setStatus(const QString& text)
{
    ui->statusText->setText(text);
    ui->progressBar->setValue(++m_current);

    if (!appBase().testing())
        update();
}

void LaunchSplash::finished()
{
    setStatus(tr("Done."));
    ui->progressBar->setValue(ui->progressBar->maximum());

    using namespace std::chrono_literals;
    m_hideTimer.start(750ms);
    // Don't execute processEvents() here. We'll hide when normal event loop runs.
}
