#-----------------------------------------------------------------------
# Copyright 2023 Loopdawg Software
# 
# ldutils is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#-----------------------------------------------------------------------

set(INCLUDE_INSTALL_DIR "${CMAKE_INSTALL_INCLUDEDIR}/${LIBNAME}")
set(LIB_INSTALL_DIR     "${CMAKE_INSTALL_LIBDIR}/${LIBNAME}")

file(GLOB_RECURSE SRC_FILES CONFIGURE_DEPENDS "*.h" "*.cpp" "*.ui")

target_sources(${LIBNAME} PRIVATE "${SRC_FILES}")
target_include_directories(${LIBNAME} PRIVATE ${CMAKE_SOURCE_DIR})
target_link_libraries(${LIBNAME} PUBLIC 
                                 "${Qt}::Core"
                                 "${Qt}::Gui"
                                 "${Qt}::Widgets"
                                 "${Qt}::Charts"
                                 "${Qt}::Svg"
                                 "${Qt}::Xml"
                                 "${Qt}::Concurrent")

# Translations
ldutils_add_translate_target(${SRC_FILES})

# Install headers
install(DIRECTORY "${CMAKE_CURRENT_LIST_DIR}" DESTINATION "${INCLUDE_INSTALL_DIR}" FILES_MATCHING PATTERN "*.h")

# Install link library
install(TARGETS "${LIBNAME}" ARCHIVE CONFIGURATIONS "${CMAKE_BUILD_TYPE}"
        DESTINATION "${LIB_INSTALL_DIR}/${CMAKE_BUILD_TYPE}")

# CMake Package
include(CMakePackageConfigHelpers)

configure_package_config_file("${LIBNAME}Config.cmake.in"
                              "${CMAKE_CURRENT_BINARY_DIR}/${LIBNAME}Config.cmake"
                              INSTALL_DESTINATION "${CMAKE_INSTALL_LIBDIR}/${LIBNAME}"
                              PATH_VARS INCLUDE_INSTALL_DIR LIB_INSTALL_DIR)

write_basic_package_version_file("${CMAKE_CURRENT_BINARY_DIR}/${LIBNAME}ConfigVersion.cmake"
                                VERSION "${PKGVERSION}"
                                COMPATIBILITY ExactVersion)

install(FILES "${CMAKE_CURRENT_BINARY_DIR}/${LIBNAME}Config.cmake"
               ${CMAKE_CURRENT_BINARY_DIR}/${LIBNAME}ConfigVersion.cmake
        DESTINATION "${CMAKE_INSTALL_LIBDIR}/${LIBNAME}")
