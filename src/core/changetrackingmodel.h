/*
    Copyright 2020-2023 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef CHANGETRACKINGMODEL_H
#define CHANGETRACKINGMODEL_H

#include <QVector>
#include <QVariantMap>

#include "src/util/nameditem.h"
#include "src/undo/undoableobject.h"
#include "treemodel.h"

// Provides dirty bits when the model is changed, and undo ability.
// Note that undo tracking (and dirty tracking to a lesser extent) is
// expensive and shouldn't be used if there are high frequency changes.
class ChangeTrackingModel : public TreeModel, public UndoableObject, virtual public NamedItemInterface
{
    Q_OBJECT

public:
    explicit ChangeTrackingModel(TreeItem* root, QObject *parent = nullptr);
    ~ChangeTrackingModel() override;

    // *** begin TreeModel API
    // Because there is no signal for pre data change in the Qt classes, we intercept and emit one ourselves.
    using TreeModel::setData;
    bool setData(const QModelIndex &idx, const QVariant& value, int role) override;

    bool setIcon(const QModelIndex& idx, const QString&) override;
    bool clearIcon(const QModelIndex& idx) override;
    // *** end TreeModel API

    // *** begin Settings API
    void load(QSettings&) override;
    // *** end Settings API

    // *** begin Stream Save API
    QDataStream& load(QDataStream&, const QModelIndex& parent = QModelIndex(), bool append = false) override;
    using TreeModel::save;
    using TreeModel::load;
    // *** end Stream Save API

signals:
    void dataAboutToBeChanged(const QModelIndex&, const QVariant&, int role);
    void dirtyStateChanged(bool);

protected:
    // This is called before undos on a range
    virtual void preUndoHook(const QModelIndex&, int, int) { }
    // This is called before undos on single index
    void preUndoHook(const QModelIndex&);

    // This is called after undos on a range
    virtual void postUndoHook(const QModelIndex&, int, int) { }
    // This is called after undos on single index
    void postUndoHook(const QModelIndex&);

    // Called before and after all undos in a set
    virtual void preUndoSet(QVariantMap&) { }
    virtual void postUndoSet(QVariantMap&) { }

    void emitDirtyStateChanged(bool /*dirty*/) override;
    void emitAboutToChange(const QModelIndex&, const QVariant&, int role);

    bool dragMove(const TreeModel& srcModel, const QVector<QPersistentModelIndex>& srcIndexes,
                  int dstRow, int dstCol, const QModelIndex& dstParent) override;

private slots:
    void modelModified() { setDirty(true); }
    void preInsertRows(const QModelIndex&, int start, int end);
    void preRemoveRows(const QModelIndex&, int start, int end);
    void preInsertCols(const QModelIndex&, int start, int end);
    void preRemoveCols(const QModelIndex&, int start, int end);
    void preDataChanged(const QModelIndex&, const QVariant&, int role);

private:
    friend class UndoModel;
    friend class UndoModelInsDel;
    friend class UndoModelSetData;
    friend class UndoModelData;

    void setupChangeSignals(bool install) override;

    using TreeModel::operator=;
};

#endif // CHANGETRACKINGMODEL_H
