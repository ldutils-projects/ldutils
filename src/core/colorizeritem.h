/*
    Copyright 2019-2022 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef COLORIZERITEM_H
#define COLORIZERITEM_H

#include <QString>
#include <QColor>
#include <QIcon>

#include <src/core/settings.h>
#include <src/core/modelmetadata.h>
#include <src/core/query.h>
#include <src/core/treeitem.h>

class QVariant;
class ColorizerModel;
class CfgDataBase;

// Single colorizer entry, struct.
class ColorizerItem final : public TreeItem
{
public:
    using ItemData = QVector<QVariant>;

    ColorizerItem(ColorizerModel&, const ItemData& data = ItemData());
    ColorizerItem(ColorizerModel&, const ColorizerItem&);
    ColorizerItem(const ColorizerItem&);

    void     clear();
    QVariant data(ModelType, int role) const override;
    bool     setData(ModelType, const QVariant& value, int role, bool& changed) override;
    void     clearData(ModelType);
    bool     setIcon(ModelType mt, const QString&);
    int columnCount() const override;

    bool hasFgColor() const { return m_fgColor.isValid(); }
    bool hasBgColor() const { return m_bgColor.isValid(); }
    bool hasIcon()    const { return !m_iconFile.isEmpty(); }
    QVariant fgColor() const { return hasFgColor() ? QVariant(m_fgColor) : QVariant(); }
    QVariant bgColor() const { return hasBgColor() ? QVariant(m_bgColor) : QVariant(); }
    QVariant icon()    const { return hasIcon() ? QVariant(m_icon) : QVariant(); }

    // *** begin Settings API
    void save(QSettings&) const override;
    void load(QSettings&) override;
    // *** end Settings API

    ModelType m_column;
    QString   m_query;
    QString   m_iconFile;
    QColor    m_fgColor;
    QColor    m_bgColor;
    bool      m_matchCase;
    bool      m_hideText;
    QIcon     m_icon;

    ColorizerItem& operator=(const ColorizerItem&);

    inline bool match(const QVariant& data) const {
        return m_queryRoot->match(data);
    }

    inline bool match(const QModelIndex& idx) const {
        return m_queryRoot->matchIdx(idx);
    }

    inline bool match(const QAbstractItemModel& model, const QModelIndex& parent, int row) const {
        return m_queryRoot->match(model, parent, row);
    }

    static inline bool checkRole(int role); // return true if it's a role we supply data for (eg, Qt::ForegroundRole)

    inline QVariant dataForRole(int role) const;

private:
    ColorizerItem* factory(const ColorizerItem::ItemData& data, TreeItem* parent) override;

    void     updateQuery(const QString& query);
    void     updateQuery();
    Query::query_uniqueptr_t  m_queryRoot;
    ColorizerModel&           m_model;
};

#endif // COLORIZERITEM_H
