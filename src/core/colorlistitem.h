/*
    Copyright 2019-2020 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef COLORLISTITEM_H
#define COLORLISTITEM_H

#include <src/core/treeitem.h>

class ColorListItem : public TreeItem
{
public:
    explicit ColorListItem(const TreeItem::ItemData& data, TreeItem *parent = nullptr);

    explicit ColorListItem(TreeItem *parent = nullptr);

    using TreeItem::data;
    QVariant data(int column, int role) const override;

    using TreeItem::setData;
    bool setData(int column, const QVariant &value, int role, bool& changed) override;

    int columnCount() const override;

    ColorListItem* factory(const ColorListItem::ItemData& data, TreeItem* parent) override;

    friend class UiColorModel;
};

#endif // COLORLISTITEM_H
