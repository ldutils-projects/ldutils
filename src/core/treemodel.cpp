/*
    Copyright 2019-2023 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <cassert>
#include <algorithm>
#include <numeric>

#include <QMutexLocker>
#include <QApplication>
#include <QSortFilterProxyModel>
#include <QIcon>
#include <QMimeData>
#include <QPersistentModelIndex>
#include <QScopedValueRollback>

#include <src/util/versionedstream.h>
#include <src/util/util.h>
#include <src/util/units.h>
#include <src/util/variantcmp.h>
#include <src/undo/undoableobject.h>

#include "treemodel.h"

QIcon TreeModel::brokenIcon;

TreeModel::TreeModel(TreeItem* root, QObject *parent) :
    QAbstractItemModel(parent),
    rootItem(root),
    m_undoing(false),
    m_oldSaveFormat(false)
{
}

TreeModel::~TreeModel()
{
    delete rootItem;
}

int TreeModel::columnCount(const QModelIndex&  /* parent */) const
{
    QMutexLocker lock(&m_mutex);
    return rootItem->columnCount();
}

QVariant TreeModel::data(const QModelIndex& idx, int role) const
{
    if (!idx.isValid())
        return { };

    QMutexLocker lock(&m_mutex);

    return getItem(idx)->data(idx.column(), role);
}

Qt::ItemFlags TreeModel::flags(const QModelIndex& idx) const
{
    if (!idx.isValid())
        return Qt::NoItemFlags;

    QMutexLocker lock(&m_mutex);

    return QAbstractItemModel::flags(idx);
}

bool TreeModel::setData(ModelType column, const QModelIndex &idx, const QVariant &value, int role)
{
    return setData(rowSibling(column, idx), value, role);
}

QVariant TreeModel::data(ModelType column, const QModelIndex &idx, int role) const
{
    return data(rowSibling(column, idx), role);
}

bool TreeModel::moveRow(const QModelIndex& srcParent, int srcRow,
                        const QModelIndex& dstParent, int dstRow)
{
    return moveRows(srcParent, srcRow, 1, dstParent, dstRow);
}

bool TreeModel::moveRows(const QModelIndex& srcParent, int srcRow, int count,
                         const QModelIndex& dstParent, int dstRow)
{
    QMutexLocker lock(&m_mutex);

    if (!beginMoveRows(srcParent, srcRow, srcRow, dstParent, dstRow))
        return false;

    bool rc = true;
    for (int offset = count - 1; offset >= 0 && rc; --offset)
        rc = rc && getItem(srcParent)->moveRow(srcRow + offset, getItem(dstParent), dstRow);

    endMoveRows();

    return rc;
}

TreeItem* TreeModel::getItem(const QModelIndex& idx) const
{
    // No mutex: always called internally
    if (idx.isValid())
        if (auto* item = static_cast<TreeItem*>(idx.internalPointer()))
            return item;

    return rootItem;
}

QVariant TreeModel::headerData(ModelType section, Qt::Orientation orientation, int role) const
{
    QMutexLocker lock(&m_mutex);

    if (orientation == Qt::Horizontal && role == Qt::DisplayRole)
        return rootItem->data(section, role);

    return { };
}

QModelIndex TreeModel::index(int row, int column, const QModelIndex& parent) const
{
    QMutexLocker lock(&m_mutex);

    if (parent.isValid() && parent.column() != 0)
        return { };

    TreeItem* parentItem = getItem(parent);
    TreeItem* childItem  = parentItem->child(row);

    if (childItem != nullptr)
        return createIndex(row, column, childItem);

    return { };
}

bool TreeModel::insertColumns(int position, int columns, const QModelIndex& parent)
{
    QMutexLocker lock(&m_mutex);

    beginInsertColumns(parent, position, position + columns - 1);
    const bool success = rootItem->insertColumns(position, columns);
    endInsertColumns();

    return success;
}

bool TreeModel::insertRows(int position, int rows, const QModelIndex& parent)
{
    QMutexLocker lock(&m_mutex);

    beginInsertRows(parent, position, position + rows - 1);
    const bool success = getItem(parent)->insertChildren(position, rows, columnCount());
    endInsertRows();

    return success;
}

QModelIndex TreeModel::parent(const QModelIndex& index) const
{
    if (!index.isValid())
        return { };

    QMutexLocker lock(&m_mutex);

    TreeItem* childItem = getItem(index);
    TreeItem* parentItem = childItem->parent();

    if (parentItem == rootItem || parentItem == nullptr)
        return { };

    return createIndex(parentItem->childNumber(), 0, parentItem);
}

bool TreeModel::removeColumns(int position, int columns, const QModelIndex& parent)
{
    if (columns == 0)
        return true;

    QMutexLocker lock(&m_mutex);

    if (position < 0 || (position + columns) > rootItem->m_childItems.size())
        return false;

    beginRemoveColumns(parent, position, position + columns - 1);
    const bool success = rootItem->removeColumns(position, columns);
    endRemoveColumns();

    if (rootItem->columnCount() == 0)
        removeRows(0, rowCount());

    return success;
}

bool TreeModel::removeRows(int position, int rows, const QModelIndex& parent)
{
    QMutexLocker lock(&m_mutex);

    TreeItem* parentItem = getItem(parent);

    if (rows == 0)
        return true;

    if (position < 0 || (position + rows) > parentItem->m_childItems.size())
        return false;

    beginRemoveRows(parent, position, position + rows - 1);
    const bool success = parentItem->removeChildren(position, rows);
    endRemoveRows();

    return success;
}

// remove rows matching predicate.  return true if any removed.
void TreeModel::removeRows(const std::function<bool(const QModelIndex&)>& predicate,
                           const QModelIndex& parent, int column)
{
    QMutexLocker lock(&m_mutex);

    Util::RemoveRows(*this, predicate, parent, column);
}

int TreeModel::rowCount(const QModelIndex &parent) const
{
    QMutexLocker lock(&m_mutex);
    return getItem(parent)->childCount();
}

bool TreeModel::setData(const QModelIndex& idx, const QVariant& value, int role)
{
    QMutexLocker lock(&m_mutex);

    bool changed;
    const bool result = getItem(idx)->setData(idx.column(), value, role, changed);

    if (result && changed)
        emit dataChanged(idx, idx);

    return result;
}

bool TreeModel::multiSet(const QModelIndexList& indexes, const QVariant &value, int role)
{
    bool allSet = true;

    for (const auto& idx : indexes)
        allSet = allSet && setData(idx, value, role);

    return allSet;
}

bool TreeModel::clearData(const QModelIndex& idx, int role)
{
    return setData(idx, QVariant(), role);
}

bool TreeModel::clearData(ModelType column, const QModelIndex& idx, int role)
{
    return clearData(rowSibling(column, idx), role);
}

bool TreeModel::setHeaderData(ModelType section, Qt::Orientation orientation, const QVariant &value, int role)
{
    if (role != Qt::DisplayRole || orientation != Qt::Horizontal)
        return false;

    QMutexLocker lock(&m_mutex);

    const bool result = rootItem->setData(section, value, role);

    if (result)
        emit headerDataChanged(orientation, section, section);

    return result;
}

bool TreeModel::appendRow(const QModelIndex& parent)
{
    QMutexLocker lock(&m_mutex);

    const int childCount = rowCount(parent);

    beginInsertRows(parent, childCount, childCount);
    const bool rc = getItem(parent)->insertChildren(childCount, 1, columnCount());
    endInsertRows();

    return rc;
}

bool TreeModel::appendRow(const TreeItem::ItemData& data, const QModelIndex& parent)
{
    QMutexLocker lock(&m_mutex);

    const int childCount = rowCount(parent);

    beginInsertRows(parent, childCount, childCount);
    const bool rc = getItem(parent)->insertChildren(childCount, 1, data);
    endInsertRows();

    return rc;
}

bool TreeModel::insertRow(const TreeModel& srcModel, const QModelIndex& srcIdx,
                          int position, const QModelIndex& parent)
{
    QMutexLocker lock(&m_mutex);

    beginInsertRows(parent, position, position);
    const bool rc = getItem(parent)->insertChildren(position, 1, 0);
    endInsertRows();

    // We don't do this inside begin/EndInsertRows at the moment because that confuses
    // ContentAddrModel's indexing.  TODO: fix, as this is a little kludgy.
    if (rc) {
        const QModelIndex dstIdx = child(position, parent);
        copyItem(dstIdx, srcModel, srcIdx);
        emit dataChanged(dstIdx, dstIdx);
    }

    return rc;
}

bool TreeModel::appendRows(const QVector<TreeItem::ItemData>& data, const QModelIndex& parent)
{
    QMutexLocker lock(&m_mutex);

    const int childCount = rowCount(parent);

    beginInsertRows(parent, childCount, childCount + data.size() - 1);
    const bool rc = getItem(parent)->insertChildren(childCount, data);
    endInsertRows();

    return rc;
}

// Convenience function to set both icon and its name.
bool TreeModel::setIcon(const QModelIndex& idx, const QString& iconFile)
{
    if (iconFile.isEmpty()) {
        return setData(idx, QVariant(), Util::IconNameRole) &&
               setData(idx, QVariant(), Qt::DecorationRole);
    }

    return setData(idx, iconFile, Util::IconNameRole) &&
           setData(idx, QIcon(iconFile), Qt::DecorationRole);
}

bool TreeModel::clearIcon(const QModelIndex& idx)
{
    return setIcon(idx, QString());
}

bool TreeModel::setSiblingIcon(int column, const QModelIndex& idx, const QString& iconFile)
{
    return setIcon(rowSibling(column, idx), iconFile);
}

void TreeModel::setDataFromThread(const QModelIndex& idx, const QVariant& value, int role)
{
    QMutexLocker lock(&m_mutex);

    if (idx.isValid())
        setData(idx, value, role);
}

QModelIndex TreeModel::child(int row, const QModelIndex& parent) const
{
    QMutexLocker lock(&m_mutex);
    return index(row, 0, rowSibling(0, parent));
}

QModelIndex TreeModel::lastChild(const QModelIndex& parent) const
{
    QMutexLocker lock(&m_mutex);

    return child(rowCount(parent)-1, rowSibling(0, parent));
}

void TreeModel::refresh()
{
    emit layoutAboutToBeChanged();
    emit layoutChanged();
}

const Units& TreeModel::units(ModelType column) const
{
    // It's valid to perform this query with row==-1 to query the default unit for a column, so
    // we must use createIndex and not our own index(), which checks the index for validity.
    return units(createIndex(-1, column, nullptr));
}

const Units& TreeModel::units(const QModelIndex&) const
{
    static const Units rawFloat(Format::Float);

    return rawFloat;
}

void TreeModel::setBrokenIcon(const QIcon& icon)
{
    brokenIcon = icon;
}

void TreeModel::setHorizontalHeaderLabels(const QStringList& labels)
{
    QMutexLocker lock(&m_mutex);

    const int insertCount = labels.count() - columnCount();

    if (insertCount > 0)
        insertColumns(columnCount(), insertCount);

    int c = 0;
    for (const auto& label : labels)
        setHeaderData(c++, Qt::Horizontal, label);
}

QStringList TreeModel::headerLabels() const
{
    QStringList headers;

    const int colCount = columnCount();
    headers.reserve(colCount);

    for (int col = 0; col < colCount; ++col)
        headers.append(headerData(col, Qt::Horizontal).toString());

    return headers;
}

int TreeModel::findHeaderColumn(const QString& text) const
{
    const int colCount = columnCount();
    for (int col = 0; col < colCount; ++col)
        if (headerData(col, Qt::Horizontal) == text)
            return col;

    return -1;
}

void TreeModel::apply(const std::function<bool (const QModelIndex&)>& fn, const QModelIndex& parent) const
{
    if (!parent.isValid() || fn(parent)) {
        const int rows = rowCount(parent);

        for (int row = 0; row < rows; ++row)
            apply(fn, index(row, 0, parent));
    }
}

int TreeModel::count(const std::function<bool (const QModelIndex &)>& pred,
                     const QModelIndex &parent) const
{
    int matched = 0;

    apply([&](const QModelIndex& idx) { matched += pred(idx) ? 1 : 0; return true; },
          parent);

    return matched;
}

QModelIndex TreeModel::findRow(const QModelIndex& parent,
                               const std::function<bool(const QModelIndex&)>& predicate,
                               int maxDepth) const
{
    // We've gone and recursed too far.  Re-curses!
    if (maxDepth <= 0)
        return { };

    const int rows = rowCount(parent);

    for (int row = 0; row < rows; ++row) {
        const QModelIndex pos = index(row, 0, parent);

        // Return if the item matches
        if (predicate(pos))
            return pos;

        // search subtree
        const QModelIndex found = findRow(pos, predicate, maxDepth - 1);
        if (found.isValid())
            return found;
    }

    // We failed.
    return { };
}

QModelIndex TreeModel::findRow(const QModelIndex& parent,
                               const QVariant& value, int column, int role,
                               int maxDepth) const
{
    return findRow(parent,
                   [&](const QModelIndex& pos) { return data(column, pos, role) == value; },
                   maxDepth);
}

void TreeModel::copyItem(const QModelIndex& dstIdx,
                         const TreeModel& srcModel, const QModelIndex& srcIdx)
{
    TreeItem* dst       = getItem(dstIdx);
    const TreeItem* src = srcModel.getItem(srcIdx);

    if (dst == src) // short circuit self-copy
        return;

    dst->shallowCopy(src);  // copy data

    dst->removeChildren(0, dst->childCount());
    dst->insertChildren(0, src->childCount(), 0);

    for (int c = 0; c < src->childCount(); ++c)
        copyItem(child(c, dstIdx), srcModel, srcModel.child(c, srcIdx));
}

TreeModel& TreeModel::operator=(const TreeModel& rhs)
{
    if (this == &rhs)
        return *this;

    beginResetModel(); {
        copyItem(QModelIndex(), rhs, QModelIndex());
    } endResetModel();

    return *this;
}

void TreeModel::sort(ModelType column, const QModelIndex& parent, Qt::SortOrder order, int role, bool recursive)
{
    QMutexLocker lock(&m_mutex);

    const int rows = rowCount(parent);

    if (rows <= 1)  // nothing to do
        return;

    static int m_recurseCount = 0;
    if (m_recurseCount++ == 0)  // we're inside a mutex here, so this is safe.
        beginResetModel();

    // Sort within this parent
    QVector<int> ordering(rows);
    std::iota(ordering.begin(), ordering.end(), 0);

    // Determine ordering
    std::sort(ordering.begin(), ordering.end(), [&](int lhs, int rhs) {
        const QVariant lhsData = data(index(lhs, column, parent), role);
        const QVariant rhsData = data(index(rhs, column, parent), role);

        if (order == Qt::AscendingOrder)
            return QtCompat::lt(lhsData, rhsData);

        return QtCompat::gt(lhsData, rhsData);
    });

    getItem(parent)->applyOrdering(ordering);

    // reapply recursively if asked
    if (recursive)
        for (int row = 0; row < rows; ++row)
            sort(column, index(row, 0, parent), order, role, recursive);

    if (--m_recurseCount == 0)
        endResetModel();
}

void TreeModel::clear()
{
    removeRows(0, rowCount());
}

// Return true if 'ancestor' is an ancestor of 'descendent'
bool TreeModel::isAncestor(const QModelIndex& ancestor, QModelIndex descendent) const
{
    while (descendent.isValid()) {
        if (ancestor == descendent)
            return true;
        descendent = parent(descendent);
    }

    return false;
}

QStringList TreeModel::mimeTypes() const
{
    const QStringList types = { internalMoveMimeType };
    return types;
}

bool TreeModel::isStreamMagic(const QMimeData* mimeData) const
{
    // Must be created by mimeData() below
    if (mimeData == nullptr || !mimeData->hasFormat(internalMoveMimeType))
        return false;

    QByteArray encodedData = mimeData->data(internalMoveMimeType);
    QDataStream stream(&encodedData, QIODevice::ReadOnly);

    quint32 magic;
    quint32 version;
    stream >> magic >> version;

    return magic == streamMagic() && version == streamVersionCurrent();
}

QMimeData* TreeModel::mimeData(const QModelIndexList& indexes) const
{
    auto *mimeData = new QMimeData();

    // Indexes
    QByteArray  encodedIndices;
    QDataStream stream(&encodedIndices, QIODevice::WriteOnly);

    // Model data
    stream << streamMagic() << streamVersionCurrent();

    // Save off enough info to reconstruct indices: this is a local move only, so we can stash the pointer.
    stream << quintptr(QApplication::instance()) << quintptr(this) << indexes.size();

    for (const auto& idx : indexes)
        if (idx.column() == 0) // by default, only entire rows
            stream << idx.row() << idx.column() << quintptr(getItem(idx));

    mimeData->setData(internalMoveMimeType, encodedIndices);

    return mimeData;
}

QVector<QPersistentModelIndex> TreeModel::getDropIndices(const QMimeData* data) const
{
    if (!data->hasFormat(internalMoveMimeType))
        return { }; // empty list if we didn't find our MIME type

    int srcRow, srcCol, indexCount;
    quintptr itemPtr, srcTreePtr, appPtr;
    QByteArray encodedData = data->data(internalMoveMimeType);
    QDataStream stream(&encodedData, QIODevice::ReadOnly);
    QVector<QPersistentModelIndex> srcIndexes;
    quint32 magic;
    quint32 version;

    // 0. Read source info
    stream >> magic >> version >> appPtr >> srcTreePtr >> indexCount;
    auto* srcModel = reinterpret_cast<TreeModel*>(srcTreePtr);

    // Since we're packaging pointers, this only works within a single app instance.
    if (QApplication::instance() != reinterpret_cast<QApplication*>(appPtr) || srcModel == nullptr)
        return srcIndexes;

    // 1. Read indexes from the MIME
    srcIndexes.reserve(indexCount);

    while (!stream.atEnd()) {
        stream >> srcRow >> srcCol >> itemPtr; // unpack index values from MIME data

        const QModelIndex srcIdx = createIndex(srcRow, srcCol, reinterpret_cast<TreeItem*>(itemPtr));
        srcIndexes.append(srcIdx);
    }

    return srcIndexes;
}

bool TreeModel::dropMimeData(const QMimeData* data, Qt::DropAction action,
                             int dstRow, int dstCol, const QModelIndex& dstParent)
{
    if (action == Qt::IgnoreAction)
        return true;

    if ((action == Qt::MoveAction || action == Qt::CopyAction)) {
        // Move the source items that were stashed in TreeModel::mimeData;
        auto srcIndexes = getDropIndices(data);

        // Reject copy of parent to anywhere in its own tree.  Qt normally prohibits this, but various
        // filters (e.g, flatten) can bypass Qt's protection.
        srcIndexes.erase(std::remove_if(srcIndexes.begin(), srcIndexes.end(), [this, dstParent](QPersistentModelIndex& idx) {
            return isAncestor(dstParent, idx);
        }), srcIndexes.end());

        if (dstRow == -1) // in Qt-speak, this means append to the parent
            dstRow = rowCount(dstParent);

        if (srcIndexes.isEmpty())
            return false;

        const TreeModel& srcModel = *reinterpret_cast<const TreeModel*>(srcIndexes.front().model());

        return (action == Qt::MoveAction) ? 
            dragMove(srcModel, srcIndexes, dstRow, dstCol, dstParent) :
            dragCopy(srcModel, srcIndexes, dstRow, dstCol, dstParent);
    }

    return false;
}

// Default drag move handling
bool TreeModel::dragMove(const TreeModel& srcModel, const QVector<QPersistentModelIndex>& srcIndexes,
                         int dstRow, int dstCol, const QModelIndex& dstParent)
{
    // Insert new items.
    dragCopy(srcModel, srcIndexes, dstRow, dstCol, dstParent);

    // Remove old items.
    removeRows([&srcIndexes](const QModelIndex& idx) { return srcIndexes.contains(idx); });

    return true;
}

bool TreeModel::dragCopy(const TreeModel& srcModel, const QVector<QPersistentModelIndex>& srcIndexes,
                         int dstRow, int /*dstCol*/, const QModelIndex& dstParent)
{
    const int count = int(std::count_if(srcIndexes.begin(), srcIndexes.end(), [](const QModelIndex& idx) { return idx.column() == 0; }));

    // Insert new items.
    insertRows(dstRow, count, dstParent);

    // Copy from old items.
    int dstPos = 0;
    for (const auto& idx : srcIndexes)
        if (idx.column() == 0)
            copyItem(child(dstRow + dstPos++, dstParent), srcModel, idx);

    return true;
}

void TreeModel::save(QSettings& settings) const
{
    SL::Save(settings, "rootItem", *rootItem);
}

void TreeModel::load(QSettings& settings)
{
    beginResetModel(); {
        SL::Load(settings, "rootItem", *rootItem);
    } endResetModel();
}

void TreeModel::saveItem(const QModelIndex& idx, QDataStream& stream, const TreeModel& model) const
{
    getItem(idx)->save(stream, model);
}

void TreeModel::loadItem(const QModelIndex& idx, QDataStream& stream, TreeModel& model)
{
    getItem(idx)->load(stream, model);
}

// Save data for indicated columns (for undo)
bool TreeModel::saveForUndo(QIODevice& io, const QModelIndex& parent, int first, int count) const
{
    const QScopedValueRollback undoing(m_undoing, true);

    VersionedStream stream;
    if (!stream.openWrite(io, streamMagic(), streamVersionCurrent()))
        return false;

    save(stream, parent, first, count);
    return stream.error() == VersionedStream::NoError;
}

// Load data for just the indicated rows (for undo)
bool TreeModel::loadForUndo(QIODevice& io, const QModelIndex& parent, int first)
{
    const QScopedValueRollback undoing(m_undoing, true);

    VersionedStream stream;
    if (!stream.openRead(io, streamMagic(), streamVersionMin(), streamVersionCurrent()))
        return false;

    int count = 0;

    stream >> count;

    for (int row = first; row < (first + count); ++row)
        loadItem(child(row, parent), stream, *this);

    return stream.error() == VersionedStream::NoError;
}

QDataStream& TreeModel::save(QDataStream& stream, const QModelIndex& parent, int first, int count) const
{
    if (count == -1)
        count = rowCount(parent) - first;

    stream << count;

    for (int row = first; row < (first + count); ++row)
        saveItem(child(row, parent), stream, *this);

    return stream;
}

bool TreeModel::save(const QString& path, const QModelIndex& parent) const
{
    QFile file(path);
    return save(file, parent);
}

bool TreeModel::save(QIODevice& io, const QModelIndex& parent) const
{
    VersionedStream stream;
    if (!stream.openWrite(io, streamMagic(), streamVersionCurrent()))
        return false;
    save(stream, parent);

    m_oldSaveFormat = false;  // mark format as updated to current one

    return stream.error() == VersionedStream::NoError;
}

bool TreeModel::load(const QString& path, const QModelIndex& parent, bool append)
{
    QFile file(path);
    return load(file, parent, append);
}

bool TreeModel::load(QIODevice& io, const QModelIndex& parent, bool append)
{
    VersionedStream stream;
    if (!stream.openRead(io, streamMagic(), streamVersionMin(), streamVersionCurrent()))
        return false;

    load(stream, parent, append);

    // If the save format is not the current one, mark the object as dirty if possible
    m_oldSaveFormat = (stream.getVersion() < streamVersionCurrent());

    return stream.error() == VersionedStream::NoError;
}

QDataStream& TreeModel::load(QDataStream& stream, const QModelIndex& parent, bool append,
                             const std::function<void()>& preHook,
                             const std::function<void()>& postHook)
{
    if (!append)
        removeRows(0, rowCount(parent), parent);

    int newRowCount;
    stream >> newRowCount;

    if (newRowCount > 0) {
        const int oldRowCount = rowCount(parent);

        beginInsertRows(parent, oldRowCount, oldRowCount + newRowCount-1);
        preHook();

        getItem(parent)->insertChildren(oldRowCount, newRowCount, 0);
        for (int row = oldRowCount; row < oldRowCount + newRowCount; ++row)
            getItem(child(row, parent))->load(stream, *this);

        postHook();
        endInsertRows();
    }

    return stream;
}

QDataStream& TreeModel::load(QDataStream& stream, const QModelIndex& parent, bool append)
{
    return load(stream, parent, append, [](){}, [](){});
}
