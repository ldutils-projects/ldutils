/*
    Copyright 2019-2020 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

/*
 * CREDIT: This code based heavily on the technique figured out by Stackoverflow forum
 * user "The Quantum Physicist", in this thread:
 *
 *     https://stackoverflow.com/questions/43125339/creating-qicon-from-svg-contents-in-memory
 *
 * It's unfortunate Qt doesn't provide this natively, but it does not, as of Qt 5.9.
 *
*/

#ifndef SVGICONENGINE_H
#define SVGICONENGINE_H

#include <QIconEngine>
#include <QSvgRenderer>
#include <QByteArray>

class SvgIconEngine : public QIconEngine
{
public:
    explicit SvgIconEngine(const QByteArray& data, bool preserveAspect = true);

    void paint(QPainter* painter, const QRect& rect, QIcon::Mode mode, QIcon::State state) override;
    QIconEngine* clone() const override;
    QPixmap pixmap(const QSize& size, QIcon::Mode mode, QIcon::State state) override;
    const QByteArray& data() const { return m_data; }

private:
    static float findAspect(const QByteArray& data, bool preserveAspect);

    QSvgRenderer m_renderer;
    QByteArray   m_data;
    float        m_aspect; // if known, aspect ratio of original icon
};

#endif // SVGICONENGINE_H
