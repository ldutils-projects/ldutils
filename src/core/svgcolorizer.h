/*
    Copyright 2019-2022 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef SVGCOLORIZER_H
#define SVGCOLORIZER_H

#include <QPair>
#include <QHash>
#include <QRgb>
#include <QColor>
#include <QString>
#include <QIcon>
#include <QTemporaryFile>
#include <QSharedPointer>

class QDomDocument;
class SvgIconEngine;

// This class caches icons and filenames and attempts to change the fill color for SVGs,
// returning a new icon.  This is a little awkward since the QIcon interface only accepts
// filenames, not raw data, and for compability with a bunch of other things, it is very
// convenient to provide QIcons.
class SvgColorizer
{
public:
    SvgColorizer();

    const QIcon& operator()(const QString& name, const QRgb& color, bool colorize = true);
    const QIcon& operator()(const QString& name, const QColor& color, bool colorize = true);
    const QIcon& operator()(const QString& name, const QVariant& color, bool colorize = true);

    QString filename(const QString& name, const QRgb& color, bool colorize = true);
    QString filename(const QString& name, const QColor& color, bool colorize = true);

    void clear(); // clear the cache

private:
    // Data we store per SVG.  The engine is so we can obtain the byte array (the QIcon doesn't
    // provide a method to get to the engine).  The file is so we can create a temp file with the
    // colorized data if asked for.
    struct IconData {
        IconData(const QIcon& icon, const SvgIconEngine* engine, QTemporaryFile* tmpFile = nullptr) :
            icon(icon), engine(engine), file(tmpFile) { }

        QIcon                          icon;   // result icon
        const SvgIconEngine*           engine; // engine used to draw it
        QSharedPointer<QTemporaryFile> file;   // if non-null, ptr to temp file holding colorized data.
    };

    static bool isSvg(const QString& name);
    static bool isColorizable(const QDomDocument& doc);
    static bool setFill(QDomDocument& doc, const QString& val);

    // Map [filename, color] tuple to a [icon, engine, file] tuple.
    QHash<QPair<QString, QRgb>, IconData> iconMap;
    QHash<QString, QIcon> uncoloredIcons;
};

#endif // SVGCOLORIZER_H
