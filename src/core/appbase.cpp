/*
    Copyright 2020-2022 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <cstdlib>
#include <cassert>
#include <QLocale>
#include <QLibraryInfo>
#include <QStandardPaths>
#include <QResource>
#include <QDir>

#include <src/util/versionedstream.h>
#include <src/util/resources.h>
#include "appbase.h"

AppBase::AppBase(int &argc, char **argv, const CmdLineBase& cmdLine) :
    QApplication(argc, argv),
    m_cmdLine(cmdLine),
    m_rc(0),
    m_nextModelId(0)
{
    setupTranslators();
}

void AppBase::setupTranslators()
{
    // Qt translations
    if (m_qtTranslator.load("qt_" + QLocale::system().name(),
                          QLibraryInfo::location(QLibraryInfo::TranslationsPath)))
        installTranslator(&m_qtTranslator);

    loadTranslation(m_ldutilsTranslator, "ldutils");
}

void AppBase::loadResourceFile(const QString& rccFile)
{
    // Search standard paths
    QStringList paths = QStandardPaths::locateAll(QStandardPaths::AppDataLocation, rccFile, QStandardPaths::LocateFile);

    // Search executable location as well.
    paths.append(QApplication::applicationDirPath() + QDir::separator() + rccFile);
    paths.append(QApplication::applicationDirPath() + QDir::separator() + "build" + QDir::separator() +
                 "rcc" + QDir::separator() + rccFile);

    for (const auto& path : paths)
        if (QResource::registerResource(path))
            return;

    qCritical("%s: Unable to find local resource file %s",
            qUtf8Printable(QApplication::applicationName()),
            qUtf8Printable(rccFile));

    throw Exit(5);
}

bool AppBase::loadTranslation(QTranslator& translator, const QString& name)
{
    for (const QString& baseDir : QStandardPaths::standardLocations(QStandardPaths::AppDataLocation)) {
        const QString translationDir = baseDir + QDir::separator() + "translations";

        if (translator.load(QLocale(), name, ".", translationDir, ".qm")) {
            installTranslator(&translator);
            return true;
        }
    }

    return false;
}

void AppBase::newSession()
{
    m_undoMgr.clear();
}

ChangeTrackingModel* AppBase::modelForPersistentId(ModelId_t modelId)
{
    const auto it = m_idToModelMap.find(modelId);
    return (it != m_idToModelMap.end()) ? *it : nullptr;
}

ModelId_t AppBase::persistentIdForModel(const ChangeTrackingModel& model) const
{
    const auto it = m_modelToIdMap.find(&model);
    return (it != m_modelToIdMap.end()) ? *it : ModelId_t(-1);
}

void AppBase::registerModel(ChangeTrackingModel& model)
{
    updatePersistentIdForModel(model, m_nextModelId++);
}

void AppBase::unregisterModel(ChangeTrackingModel& model)
{
    const auto it = m_modelToIdMap.find(&model);

    if (it == m_modelToIdMap.end()) {
        assert(0 && "Expected to find model in m_modelToIdMap");
        return;
    }

    m_idToModelMap.remove(*it);
    m_modelToIdMap.remove(&model);
}

void AppBase::updatePersistentIdForModel(ChangeTrackingModel& model, ModelId_t id)
{
    m_modelToIdMap[&model] = id;
    m_idToModelMap[id] = &model;
}

bool AppBase::testing() const
{
    return property("test").toBool();
}
