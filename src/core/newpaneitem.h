/*
    Copyright 2021 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef NEWPANEITEM_H
#define NEWPANEITEM_H

#include <src/core/modelmetadata.h>

class QVariant;
class QPixmap;

class NewPaneItem final
{
public:
    NewPaneItem() { }
    NewPaneItem(const QString& name, const QString& image, const QString& icon, const QString& tooltip);

    [[nodiscard]] QVariant data(ModelType, int role) const;

private:
    QPixmap getPreviewImage() const;

    QString m_name;
    QString m_icon;
    QString m_image;
    QString m_tooltip;
};

#endif // NEWPANEITEM_H
