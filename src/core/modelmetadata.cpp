/*
    Copyright 2019-2022 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "modelmetadata.h"

const decltype(ModelMetaData::mdIdentityItem) ModelMetaData::mdIdentityItem =
        [](ModelType, QStandardItem* item) { return item; };

const decltype(ModelMetaData::mdAcceptAll) ModelMetaData::mdAcceptAll =
        [](ModelType) { return true; };


// Return true if this data can be placed in a chart
bool ModelMetaData::mdIsChartable(ModelType)
{
    return false;
}

bool ModelMetaData::mdIsRate(ModelType)
{
    return false;
}

const QString& ModelMetaData::mdRateSuffix(ModelType)
{
    static const QString empty("");
    return empty;
}

bool ModelMetaData::mdIgnore(ModelType)
{
    return false;
}

QString ModelMetaData::makeTooltipHeader(const QString& hdr)
{
    return QString("<p><b><u>") + hdr + ":</u></b></p>";
}

QString ModelMetaData::makeTooltipHeader(const QString& hdr0, const QString& hdr1)
{
    return QString("<p><b><u>") + hdr0 + ":</u></b><tt><i> " + hdr1 + "</i></tt><p>";
}

QString ModelMetaData::makeTooltip(const QString& description, bool editable)
{
    QString toolTip = makeTooltipHeader(QObject::tr("Column Information")) + description;

    if (editable) {
        toolTip += makeTooltipHeader(QObject::tr("Editable")) +
                   QObject::tr("You can edit data in this column by double clicking on it "
                               "within a row, or using a context menu.  You can edit multiple "
                               "rows at once by selecting several rows (shift or ctrl click) "
                               "and doubling clicking while holding the shift or ctrl key.");
    }

    return toolTip;
}
