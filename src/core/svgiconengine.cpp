/*
    Copyright 2019-2023 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

/*
 * CREDIT: This code based heavily on the technique figured out by Stackoverflow forum
 * user "The Quantum Physicist", in this thread:
 *
 *     https://stackoverflow.com/questions/43125339/creating-qicon-from-svg-contents-in-memory
 *
 * It's unfortunate Qt doesn't provide this natively, but it does not, as of Qt 5.9.
 *
*/

#include <cstdlib>
#include <cmath>
#include <QPainter>
#include <QDomDocument>
#include <QDomNamedNodeMap>

#include <src/util/qtcompat.h>
#include "svgiconengine.h"

SvgIconEngine::SvgIconEngine(const QByteArray& data, bool preserveAspect) :
    m_renderer(data),
    m_data(data),
    m_aspect(findAspect(data, preserveAspect))
{
}

void SvgIconEngine::paint(QPainter* painter, const QRect& rect, QIcon::Mode, QIcon::State)
{
    QRect drawRect = rect;

    // Attempt to draw with original aspect, if known and requested.
    if (m_aspect > 0.0f) {
        const float requestAspect = float(drawRect.width()) / float(drawRect.height());

        if (requestAspect > m_aspect) {
            // request is wider than our aspect.  Limited by height.
            const int oldWidth = drawRect.width();
            const int newWidth = int(std::trunc(rect.height() * m_aspect));
            if (oldWidth != newWidth) {
                drawRect.setLeft(drawRect.left() + (oldWidth - newWidth) / 2);
                drawRect.setWidth(newWidth);
            }
        } else if (requestAspect < m_aspect) {
            // request is taller than our aspect.  Limited by width.
            const float oldHeight = drawRect.height();
            const float newHeight = std::trunc(drawRect.width() / m_aspect);
            if (oldHeight != newHeight) {
                drawRect.setTop(int(drawRect.top() + (oldHeight - newHeight) / 2));
                drawRect.setHeight(int(newHeight));
            }
        }
    }

    m_renderer.render(painter, drawRect);
}

QIconEngine *SvgIconEngine::clone() const
{
    return new SvgIconEngine(m_data, m_aspect > 0.0f);
}

// Sets up initial empty bitmap to render into.
QPixmap SvgIconEngine::pixmap(const QSize& size, QIcon::Mode mode, QIcon::State state)
{
    if (size.isEmpty())
        return { };

    QImage img(size, QImage::Format_ARGB32);

    img.fill(QRgb(0x00000000));

    QPixmap pix = QPixmap::fromImage(img, Qt::NoFormatConversion);
    QPainter painter(&pix);

    paint(&painter, QRect(QPoint(0, 0), size), mode, state);

    return pix;
}

float SvgIconEngine::findAspect(const QByteArray &data, bool preserveAspect)
{
    if (!preserveAspect)
        return -1.0; // don't mess with it.

    QDomDocument doc;
    doc.setContent(data);

    const QDomNodeList children = doc.childNodes();

    for (int c = 0; c < children.size(); ++c) {
        const QDomNamedNodeMap attrs = children.at(c).attributes();
        if (attrs.contains("viewBox")) {
            const QStringList coords = attrs.namedItem("viewBox").nodeValue().split(" ", QtCompat::SplitBehavior::SkipEmptyParts);

            if (coords.size() == 4) {
                const float width  = coords[2].toFloat() - coords[0].toFloat();
                const float height = coords[3].toFloat() - coords[1].toFloat();

                return width / std::max(height, 1.0f);
            }
        }
    }

    return -1.0; // unknown
}
