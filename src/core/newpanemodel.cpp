/*
    Copyright 2021-2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <cassert>
#include <QApplication>

#include <src/util/units.h>
#include <src/core/appbase.h>
#include <src/core/modelmetadata.inl.h>

#include "newpanemodel.h"

NewPaneModel::NewPaneModel(QObject* parent) :
    TreeModel(nullptr, parent)
{
    setupFont();
}

void NewPaneModel::setupFont()
{
    m_largerFont = QApplication::font();
    m_largerFont.setPointSize(int(QApplication::font().pointSizeF() * 1.2));
}

void NewPaneModel::appendRow(const QString& name, const QString& image, const QString& icon, const QString& tooltip)
{
    beginInsertRows(QModelIndex(), size(), size());
    append(NewPaneItem(name, image, icon, tooltip));
    endInsertRows();
}

bool NewPaneModel::removeRows(int position, int rows, const QModelIndex& parent)
{
    if (rows == 0)
        return true;

    if (position < 0 || (position + rows) > rowCount() || parent.isValid())
        return false;

    beginRemoveRows(parent, position, position + rows - 1);
    remove(position, rows);
    endRemoveRows();

    return true;
}

QString NewPaneModel::mdName(ModelType mt)
{
    switch (mt) {
    case NewPaneModel::Name:   return QObject::tr("Name");
    case NewPaneModel::Image:  return QObject::tr("Image");
    case NewPaneModel::_Count: break;
    }

    assert(0 && "Unknown NewPaneModel value");
    return "";
}

bool NewPaneModel::mdIsEditable(ModelType)
{
    return false;
}

// tooltip for container column header
QString NewPaneModel::mdTooltip(ModelType mt)
{
    switch (mt) {
    case NewPaneModel::Name:   return QObject::tr("<i></i>Pane name.");
    case NewPaneModel::Image:  return QObject::tr("<i></i>Pane preview image.");
    case NewPaneModel::_Count: break;
    }

    assert(0 && "Unknown NewPaneModel value");
    return "";
}

// tooltip for container column header
QString NewPaneModel::mdWhatsthis(ModelType mt)
{
    return mdTooltip(mt);  // just pass through the tooltip
}

Qt::Alignment NewPaneModel::mdAlignment(ModelType)
{
    return Qt::AlignLeft | Qt::AlignVCenter;
}

const Units& NewPaneModel::mdUnits(ModelType)
{
    static const Units rawString(Format::String);
    return rawString;
}

QVariant NewPaneModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (const QVariant val = ModelMetaData::headerData<NewPaneModel>(section, orientation, role); val.isValid())
        return val;

    if (orientation == Qt::Horizontal) {
        if (role == Qt::DisplayRole)
            return mdName(section);
    }

    return { };
}

QVariant NewPaneModel::data(const QModelIndex& idx, int role) const
{
    if (!idx.isValid())
        return { };

    assert(idx.row() < size());

    if (role == Qt::FontRole)
        return m_largerFont;

    return at(idx.row()).data(idx.column(), role);
}

QModelIndex NewPaneModel::index(int row, int column, const QModelIndex&) const
{
    return createIndex(row, column, nullptr);
}

QModelIndex NewPaneModel::parent(const QModelIndex&) const
{
    return { };
}

Qt::ItemFlags NewPaneModel::flags(const QModelIndex& idx) const
{
    if (!idx.isValid())
        return { };

    const Qt::ItemFlags flags = TreeModel::flags(idx) | ModelMetaData::flags<NewPaneModel>(idx) |
          Qt::ItemIsDragEnabled;

    switch (idx.column()) {
    case NewPaneModel::Name:  return flags;
    case NewPaneModel::Image: return flags & ~Qt::ItemIsSelectable;
    default: assert(0); return flags;
    }
}

int NewPaneModel::rowCount(const QModelIndex& parent) const
{
    if (parent.isValid())
        return 0;

    return size();
}

int NewPaneModel::columnCount(const QModelIndex&) const
{
    return NewPaneModel::_Count;
}

const Units& NewPaneModel::units(const QModelIndex& idx) const
{
    static const Units string(Format::String);

    switch (idx.column()) {
    case NewPaneModel::Name: return string;
    }

    return TreeModel::units(idx);
}

quint32 NewPaneModel::streamMagic() const
{
    return AppBase::NativeMagic + quint32(AppBase::Model::NewPanel);
}

quint32 NewPaneModel::streamVersionMin() const
{
    return 0x1;
}

quint32 NewPaneModel::streamVersionCurrent() const
{
    return 0x1;
}
