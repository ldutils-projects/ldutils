/*
    Copyright 2019-2022 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef MODELMETADATA_H
#define MODELMETADATA_H

#include <functional>
#include <QStringList>
#include <QVector>
#include <QVariant>
#include <QModelIndex>

class QStandardItemModel;
class QStandardItem;
class QComboBox;
class CfgData;
class Units;

using ModelType = int;

// Model metadata interface.
class ModelMetaData
{
public:
    [[nodiscard]] static QString        mdName(ModelType);
    [[nodiscard]] static bool           mdIsEditable(ModelType);
    [[nodiscard]] static bool           mdIsChartable(ModelType);
    [[nodiscard]] static QString        mdTooltip(ModelType);
    [[nodiscard]] static QString        mdWhatsthis(ModelType);
    [[nodiscard]] static Qt::Alignment  mdAlignment(ModelType);
    [[nodiscard]] static bool           mdIsRate(ModelType);
    [[nodiscard]] static const QString& mdRateSuffix(ModelType);
    [[nodiscard]] static const Units&   mdUnits(ModelType);
    [[nodiscard]] static bool           mdIgnore(ModelType);

    template <class MD>
    [[nodiscard]] static QVariant headerData(ModelType section, Qt::Orientation orientation,
                    int role = Qt::DisplayRole);

    template <class MD>
    [[nodiscard]] static Qt::ItemFlags flags(const QModelIndex&);

    static const std::function<QStandardItem*(ModelType, QStandardItem*)> mdIdentityItem;
    static const std::function<bool(ModelType)>                           mdAcceptAll;

    template <class MD>
    static void setupComboBox(QComboBox& comboBox,
                              QStandardItemModel& comboModel,
                              QVector<ModelType>* entries = nullptr,
                              const decltype(mdIdentityItem)& = mdIdentityItem,
                              const decltype(mdAcceptAll)&    = mdAcceptAll);

    template <class MD> [[nodiscard]] static QStringList headersList();

protected:
    [[nodiscard]] static QString makeTooltipHeader(const QString& hdr);
    [[nodiscard]] static QString makeTooltipHeader(const QString& hdr0, const QString& hdr1);
    [[nodiscard]] static QString makeTooltip(const QString& description, bool editable);
};

#endif // MODELMETADATA_H
