/*
    Copyright 2019-2022 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TREEITEM_H
#define TREEITEM_H

#include <QVariant>
#include <QVector>
#include <QMap>

#include <src/core/settings.h>

class TreeModel;

class TreeItem : public Settings
{
public:
    using ItemData = QVector<QVariant>;

    explicit TreeItem(const ItemData& data, TreeItem *parent = nullptr, int role = Qt::DisplayRole);
    explicit TreeItem(TreeItem *parent = nullptr);

    ~TreeItem() override;

    // *** begin Settings API
    void save(QSettings&) const override;
    void load(QSettings&) override;
    // *** end Settings API

    // *** begin Stream Save API
    virtual QDataStream& save(QDataStream&, const TreeModel&) const;
    virtual QDataStream& load(QDataStream&, TreeModel&);
    // *** end Stream Save API

protected:
    [[nodiscard]] virtual TreeItem* child(int number);
    [[nodiscard]] virtual const TreeItem* child(int number) const;
    [[nodiscard]] virtual int       childCount() const;
    [[nodiscard]] virtual int       columnCount() const;
    [[nodiscard]] virtual QVariant  data(int column, int role) const;
    virtual bool      insertChildren(int position, int count, const ItemData& data);
    virtual bool      insertChildren(int position, const QVector<ItemData>& data);
    virtual bool      insertChildren(int position, int count, int columns);
    virtual bool      insertColumns(int position, int columns);
    [[nodiscard]] virtual TreeItem* parent() const;
    virtual bool      removeChildren(int position, int count);
    virtual bool      removeColumns(int position, int columns);
    [[nodiscard]] virtual int       childNumber() const;
    virtual bool      setData(int column, const QVariant &value, int role, bool& changed);
    virtual bool      setData(int column, const QVariant &value, int role);
    [[nodiscard]] virtual TreeItem* factory(const ItemData& data, TreeItem* parent);
    virtual bool      saveRole(int role) const { return role != Qt::DecorationRole; } // return true to save role.
    virtual bool      moveRow(int srcRow, TreeItem* dstParent, int dstRow);
    virtual void      applyOrdering(const QVector<int>&);

    virtual void shallowCopy(const TreeItem* src); // copy data, but not structure

    virtual void createIcons(); // setup icons from paths

    inline void loadItemData0x1000_1001(QDataStream&); // m_ItemData loader for save formats 0x1000-0x1001
    inline void loadItemData0x1002(QDataStream&);      // m_ItemData loader for save formats 0x1002-

    friend class TreeModel;
    friend class ContentAddrModel;

    TreeItem& operator=(const TreeItem&) = delete;
    TreeItem(const TreeItem&)            = delete;

    static const constexpr unsigned int guard = 0x9381b9a0;  // binary save/load guard value
    static const constexpr int roleEndMarker = -1;           // mark end of saved roles in binary saves

    QMap<int, ItemData>   m_itemData;    // map of roles to vector of variant data per each
    QVector<TreeItem*>    m_childItems;  // children of this node
    TreeItem*             m_parentItem;  // parent
};

#endif // TREEITEM_H
