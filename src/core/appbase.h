/*
    Copyright 2020-2021 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef APPBASE_H
#define APPBASE_H

#include <QApplication>
#include <QTranslator>
#include <QMap>

#include <src/core/cfgdatabase.h>
#include <src/core/newpanemodel.h>
#include <src/undo/undomgr.h>
#include <src/fwddeclbase.h>

class AppBase;
class CfgDataBase;
class CmdLineBase;
class MainWindowBase;
class ChangeTrackingModel;

class AppBase : public QApplication
{
public:
    enum class Model {
        NewPanel,  // new panel model
    };

    AppBase(int &argc, char **argv, const CmdLineBase&);

    [[nodiscard]] virtual UndoMgr&             undoMgr() { return m_undoMgr; }
    [[nodiscard]] virtual CfgDataBase&         cfgData() = 0;
    [[nodiscard]] virtual const CfgDataBase&   cfgData() const = 0;
    [[nodiscard]] NewPaneModel&                newPaneModel() { return m_newPaneModel; }

    virtual void newSession();   // start a new session

    // Get persistent model ID for the given change tracking model
    virtual ChangeTrackingModel* modelForPersistentId(ModelId_t modelId);
    virtual ModelId_t persistentIdForModel(const ChangeTrackingModel&) const;

    // Register/unregister persistent model IDs
    virtual void registerModel(ChangeTrackingModel&);
    virtual void unregisterModel(ChangeTrackingModel&);
    virtual void updatePersistentIdForModel(ChangeTrackingModel&, ModelId_t id);

    int rc() const { return m_rc; }

    bool testing() const;  // true if running under test suite

    static const constexpr quint32 NativeMagic          = 0x397f41bb;

protected:
    void setupTranslators();
    static bool loadTranslation(QTranslator& translator, const QString& name);
    static void loadResourceFile(const QString& rccFile); // dynamically load a resource file

    const CmdLineBase&                          m_cmdLine;           // ref to command line
    int                                         m_rc;                // return code from creation

private:
    QTranslator                                 m_qtTranslator;      // QT translator
    QTranslator                                 m_ldutilsTranslator; // ldutils translator
    QMap<ModelId_t, ChangeTrackingModel*>       m_idToModelMap;      // map persistent IDs to models
    QMap<const ChangeTrackingModel*, ModelId_t> m_modelToIdMap;      // map models to persistent IDs
    ModelId_t                                   m_nextModelId;       // for doling out IDs
    NewPaneModel                                m_newPaneModel;      // new pane model w/ previews

    UndoMgr                                     m_undoMgr;           // primary undo manager
};

inline AppBase& appBase() { return reinterpret_cast<AppBase&>(*qApp); }
inline UndoMgr& undoMgr() { return appBase().undoMgr(); }
inline const CfgDataBase& cfgDataBase() { return appBase().cfgData(); }
inline CfgDataBase& cfgDataBaseWritable() { return appBase().cfgData(); }

#endif // APPBASE_H
