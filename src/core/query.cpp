/*
    Copyright 2019-2022 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <cassert>
#include <type_traits>
#include <algorithm>

#include <QAbstractItemModel>
#include <QModelIndex>
#include <QMap>

#include <src/core/treemodel.h>
#include <src/util/variantcmp.h>
#include <src/util/util.h>

#include "query.h"
#include "query.inl.h"

namespace Query {

bool Base::isValid() const
{
    return true;
}

Base::PatternList Base::patterns() const
{
    PatternList pList;
    pList.reserve(8);

    return patterns(pList);
}

const decltype(Context::genAll)  Context::genAll  = []() { return query_uniqueptr_t(new All()); };
const decltype(Context::genNone) Context::genNone = []() { return query_uniqueptr_t(new None()); };

// Parse string, return node tree.
Context::Context(int regexRole, int relRole, int strRelRole) :
    m_columnCount(0),
    m_caseSensitivity(Qt::CaseInsensitive),
    m_regexRole(regexRole),
    m_relRole(relRole),
    m_strRelRole(strRelRole),
    m_model(nullptr),
    m_emptyQueryFactory(genAll),
    m_error(false),
    m_tokenPos(0),
    m_tokenLen(0),
    m_parenNest(0)
{
}

Context::Context(const QAbstractItemModel* model, Qt::CaseSensitivity cs, int regexRole, int relRole, int strRelRole) :
    Context(regexRole, relRole, strRelRole)
{
    setModel(model, cs);
}

// Column bounds: [begin, end)
std::pair<int, int> Context::colBounds(int colCount, int column, int colOverride)
{
    if (colOverride != Base::NoColumn)
        column = colOverride;

    return std::make_pair((column == Base::AnyColumn) ? 0 : column,
                          (column == Base::AnyColumn) ? colCount : (column + 1));
}

Context& Context::setModel(const QAbstractItemModel* model, Qt::CaseSensitivity cs)
{
    setCaseSensitivity(cs);

    m_columnCount = 0;
    m_headerMap.clear();
    m_headers.clear();
    m_columnUnits.clear();

    if (model == nullptr)
        return *this;

    m_columnCount = model->columnCount();
    m_columnUnits.reserve(m_columnCount);
    m_headers.reserve(m_columnCount);

    assert(m_columnCount > 0 && m_columnCount < 256);

    m_model = model; // remember model

    const auto* treeModel = qobject_cast<const TreeModel*>(model);

    for (int col = 0; col < m_columnCount; ++col) {
        const QString headerString = CanonicalizeColumnName(model->headerData(col, Qt::Horizontal).toString());

        // Remember header names, for parsing those.
        m_headerMap.insert(headerString.toLower(), col);
        m_headers.append(headerString);

        // Remember per-column unit suffixes, for parsing those.
        if (treeModel != nullptr)
            m_columnUnits.append(&treeModel->units(col));
    }

    return *this;
}

Context& Context::setModel(const QAbstractItemModel* model, const QVector<const Units*>& columnUnits,
                           Qt::CaseSensitivity cs)
{
    return setModel(model, cs)
           .setParseUnits(columnUnits);
}

Context& Context::setParseUnits(const QVector<const Units*>& columnUnits)
{
    m_columnUnits = columnUnits;
    return *this;
}

QString Context::toString(const Base* node) const
{
    if (node == nullptr)
        return { };

    QString string;
    node->toString(*this, string);
    return string;
}

bool Context::match(const query_uniqueptr_t& query, const QModelIndex& parent, int row, int col) const
{
    return query && (m_model != nullptr) && query->match(*m_model, parent, row, col);
}

bool Context::match(const query_uniqueptr_t& query, const QModelIndex& item, int col) const
{
    return query && (m_model != nullptr) && query->match(*m_model, item.parent(), item.row(), col);
}

bool Context::match(const query_uniqueptr_t& query, const QVariant& item) const
{
    return query && !m_columnUnits.isEmpty() && query->match(item);
}

// Simple tokenizer for our wee query language
QStringRef Context::nextToken() const
{
    // Tokenizing regex. Matches our operators, etc. Not easy to comprehend :(
    // Basically this is or-separated sets of smaller expressions each of which matches a token.
    // Since there is ambiguity around operators vs valid regex elements, we require leading or
    // trailing spaces for operators to dismabiguate.
    static const QRegularExpression operatorExp(R"( +[<>=!]=|[<>=!]= +| +=~|=~ +| +\&\&|\&\& +| +\^\^|\^\^ +| +\^ +| +\|\||\|\| +| +,|, +| +\!~|\!~ +| +[|&<>()!:~]|[|&<>()!:~] +|$)");

    const auto match = operatorExp.match(m_text);

    QStringRef token;

    if (!match.hasMatch())              // no match: empty ref into text
        token = m_text.mid(m_text.size());
    else if (match.capturedStart() > 0) // text up to next operator
        token = m_text.left(match.capturedStart()).trimmed();
    else                                // the operator itself
        token = match.capturedRef().trimmed();

    m_tokenPos.set(token.position());
    m_tokenLen.set(token.length());

    return token;
}

const Base* Context::error() const
{
    m_error = true;
    return nullptr;
}

// Eat token, return next one.
inline QStringRef Context::eat() const
{
    m_text = m_text.mid(int(m_tokenPos + m_tokenLen) - m_text.position());
    return nextToken();
}

// Eat given number of characters, return next token.
inline QStringRef Context::eat(int len) const
{
    m_text = m_text.mid(len);
    return nextToken();
}

Seq::Type Context::parseSeqType(const QStringRef& token)
{
    if (token == "&" || token == "&&") return Seq::Type::And;
    if (token == "^" || token == "^^") return Seq::Type::Xor;
    if (token == "|" || token == "||") return Seq::Type::Or;
    if (token == ",") return Seq::Type::Seq;

    return Seq::Type::None;
}

Rel::Cmp Context::parseCompare(const QStringRef& token)
{
    if (token == "==") return Rel::Cmp::EQ;
    if (token == "!=") return Rel::Cmp::NE;
    if (token == "<")  return Rel::Cmp::LT;
    if (token == "<=") return Rel::Cmp::LE;
    if (token == ">")  return Rel::Cmp::GT;
    if (token == ">=") return Rel::Cmp::GE;
    if (token == "=~" || token == ':' || token == '~') return Rel::Cmp::Regex;
    if (token == "!~") return Rel::Cmp::NoRegex;

    return Rel::Cmp::None;
}

int Context::parseColumnName(const QStringRef& token) const
{
    if (token == "*")
        return Base::AnyColumn;

    const QString canonical = CanonicalizeColumnName(token.toString()).toLower();

    const auto hdr = m_headerMap.find(canonical);

    return (hdr == m_headerMap.end()) ? Base::NoColumn : hdr.value();
}

auto Context::parseUnitSuffix(int col) const
{
    auto [beginCol, endCol] = colBounds(m_columnCount, col);

    int maxSizeFound = 0;
    std::tuple<QVariant, Format> result;

    if (beginCol == endCol && m_columnUnits.size() == 1) {
        // If there are no model columns, but we have units, use those to parse.
        beginCol = 0;
        endCol = 1;
    }

    for (int c = beginCol; c < endCol; ++c) {
        // const QString s = m_text.toString();
        if (c < m_columnUnits.size()) {
            if (const auto [value, format, size] = m_columnUnits.at(c)->parse(m_text); size > 0) {
                if (size > maxSizeFound) {
                    maxSizeFound = size;
                    result = std::make_tuple(value, format);
                }
            }
        }
    }

    if (maxSizeFound > 0) {
        eat(maxSizeFound);  // eat however much the units parser thinks we should.  nom nom.
        return result;
    }

    return std::make_tuple(QVariant(), Format::_Invalid);
}

const Base* Context::unwind(const QStringRef& unwindPt, bool hasError) const
{
    if (hasError)
        m_error = true;

    m_text = unwindPt;
    nextToken();
    return nullptr;
}

const Rel* Rel::make(const Context& ctx, const QVariant& pattern, Cmp cmpType, Qt::CaseSensitivity caseSensitivity, int column, int role,
                     Format format)
{
    const Rel* rel = new Rel(ctx, pattern, cmpType, caseSensitivity, column, role, format);
    if (rel != nullptr && rel->isValid())
        return rel;

    delete rel;
    ctx.m_error = true;
    return nullptr;
}

// <column> <cmp> <value> <unit>
const Base* Context::parseColumn() const
{
    bool isNumber;
    const QStringRef unwindPt = m_text;  // for unwinding

    const int column   = parseColumnName(token());
    const Rel::Cmp cmp = parseCompare(eat());

    if (column == Base::NoColumn)
        return unwind(unwindPt, cmp != Rel::Cmp::None);

    if (cmp == Rel::Cmp::None)
        return unwind(unwindPt, false);

    // At this point, we found a column and a comparison.  We're committed.

    if (eat().isEmpty())
        return error();

    const auto [value, fmt] = parseUnitSuffix(column);
    if (!value.isValid())
        return error();

    // Build relative compare node for the data we parsed.
    if (Rel::isRegex(cmp))
        // Regex match.
        return Rel::make(*this, value.toString(), cmp, m_caseSensitivity, column, m_regexRole, fmt);

    if (double dval = value.toDouble(&isNumber); isNumber)
        // Numeric or string comparisons
        return Rel::make(*this, dval, cmp, m_caseSensitivity, column, m_relRole, fmt);
    if (QDateTime date = value.toDateTime(); date.isValid())
        // Date comparisons
        return Rel::make(*this, date, cmp, m_caseSensitivity, column, m_relRole, fmt);
    if (QTime time = value.toTime(); time.isValid())
        // Time comparisons
        return Rel::make(*this, time, cmp, m_caseSensitivity, column, m_relRole, fmt);

    // Relative comparisons
    return Rel::make(*this, value.toString(), cmp, m_caseSensitivity, column, m_strRelRole, fmt);
}

// "!" <pattern>
const Base* Context::parseNeg() const
{
    const QStringRef unwindPt = m_text;  // for unwinding

    if (token() != "!")
        return nullptr;

    eat(); // eat the bang.  nom nom.

    if (const Base* pattern = parsePattern(); pattern != nullptr)
        return new Neg(pattern);

    return unwind(unwindPt, true);
}

// <seq>
// <seq> "|" <seq> ...
// <seq> "&" <seq> ...
// <seq> "," <seq> ...
const Base* Context::parseSeq(Seq::Type prec, const Base* rhs, Seq::Type* nextType) const
{
    const Base* node;
    Seq* seq = nullptr;
    Seq::Type thisType = Seq::Type::None;
    Seq::Type type;
    bool expectNode = false;

    while (true) {
        if (rhs != nullptr) {
            node = rhs;
            rhs = nullptr;
            type = prec;
        } else {
            if (parseSeqType(token()) != Seq::Type::None) {
                delete seq;
                return nullptr;
            }

            if (token().isEmpty()) {
                node = nullptr;
                type = Seq::Type::None;
            } else {
                node = parsePattern();

                if (node == nullptr) {
                    delete seq;
                    return nullptr;
                }

                type = parseSeqType(token());
                if (type != Seq::Type::None) {
                    eat();  // eat the sequence type.  nom nom.
                    expectNode = true;
                } else {
                    expectNode = false;
                }
            }
        }

        if (nextType != nullptr)
            *nextType = type;

        // End of sequence for this precedence
        if (type == Seq::Type::None || type > prec) {
            if (expectNode) {
                delete seq;
                return nullptr;
            }

            return (seq == nullptr) ? node : seq->append(node);
        }

        // First node in the sequence
        if (seq == nullptr) {
            seq = new Seq(thisType = type, node);
            continue;
        }

        if (type < thisType) {
            // Higher priority chains become sub-nodes
            if (const Base* subChain = parseSeq(type, node, &type); subChain != nullptr) {
                seq->append(subChain);
                node = nullptr;
                expectNode = false;
            } else {
                delete seq;
                return nullptr;
            }
        }

        if (type >= thisType && type != Seq::Type::None) {
            seq->append(node); // add to current sequence

            // Lower priority chain becomes super-node
            if (type > thisType) {
                seq = new Seq(thisType = type, seq);
                expectNode = false;
            }
        }
    }
}

const Base* Context::parseRegex() const
{
    const auto [value, fmt] = parseUnitSuffix(Base::AnyColumn);
    if (!value.isValid())
        return error();

    return Rel::make(*this, value.toString(), Rel::Cmp::Regex, m_caseSensitivity, Base::AnyColumn, m_regexRole, fmt);
}

const Base* Context::parseParen() const
{
    if (token() == ")")
        return error();

    if (token() != "(")
        return nullptr;

    ++m_parenNest;

    eat();  // eat the open paren.  nom nom.
    const Base* seq = parseSeq();

    --m_parenNest;
    if (token() != ")" || m_parenNest < 0) {
        delete seq;
        return error();
    }

    eat();   // eat the close paren.  nom nom.
    return seq;
}

const Base* Context::parsePattern() const
{
    if (const Base* node = parseNeg(); node != nullptr && !m_error)
        return node;

    if (const Base* node = parseColumn(); node != nullptr && !m_error)
        return node;

    if (const Base* node = parseParen(); node != nullptr && !m_error)
        return node;

    if (const Base* node = parseRegex(); node != nullptr && !m_error)
        return node;

    return nullptr;
}

query_uniqueptr_t Context::parse(const QString& query) const
{
    if (query.isEmpty())
        return m_emptyQueryFactory();

    m_text  = QStringRef(&query);
    nextToken();
    m_error = false;
    m_parenNest.set(0);

    // Paren nesting must be zero, with no unparsed text left.
    if (query_uniqueptr_t node(parseSeq()); node && m_parenNest == 0 && token().isEmpty())
        return node;

    return query_uniqueptr_t(new Error());
}

bool Context::isValidQuery(const QString& query) const
{
    const auto node(parse(query));

    return node && node->isValid();
}

bool None::match(const QVariant&) const
{
    return false;
}

bool None::match(const QAbstractItemModel&, const QModelIndex&, int, int) const
{
    return false;
}

void None::toString(const Context&, QString& string) const
{
    string = QObject::tr("<error>");
}

Base::PatternList& None::patterns(PatternList& patterns) const
{
    return patterns;
}

bool None::operator==(const Base& rhsBase) const
{
    return dynamic_cast<const None*>(&rhsBase) == this;
}

bool All::match(const QVariant&) const
{
    return true;
}

bool All::match(const QAbstractItemModel&, const QModelIndex&, int, int) const
{
    return true;
}

void All::toString(const Context&, QString& string) const
{
    string.clear();
}

Base::PatternList& All::patterns(PatternList& patterns) const
{
    patterns.append(QRegularExpression(".*"));
    return patterns;
}

bool All::operator==(const Base& rhsBase) const
{
    return dynamic_cast<const All*>(&rhsBase) == this;
}

bool Rel::match(const QStringList& data) const
{
    // Match if any of the strings in the list match
    return std::any_of(data.begin(), data.end(),
                       [this](const QString& string) { return match(string); });
}

bool Rel::match(const QString& data) const
{
    if (m_cmpType == Cmp::Regex)
        return m_regex.match(data).hasMatch();

    if (m_cmpType == Cmp::NoRegex)
        return !m_regex.match(data).hasMatch();

    if (m_pattern.type() != QVariant::String)
        return false;

    // Handle case sensitivity for string compares
    // Use constData for performance: it's lighter than to toString()
    const auto* pattStr = static_cast<const QString*>(m_pattern.constData());

    const int cmp = data.compare(*pattStr, m_caseSensitivity);
    switch (m_cmpType) {
    case Cmp::EQ:      return cmp == 0;
    case Cmp::NE:      return cmp != 0;
    case Cmp::LT:      return cmp < 0;
    case Cmp::LE:      return cmp <= 0;
    case Cmp::GT:      return cmp > 0;
    case Cmp::GE:      return cmp >= 0;
    case Cmp::Regex:   assert(0); break;
    case Cmp::NoRegex: assert(0); break;
    case Cmp::None:    assert(0); break;
    }

    return false;
}

// Relational
bool Rel::match(const QVariant& data) const
{
    // Use constData for performance: it's lighter than to toString()
    if (data.type() == QVariant::String)
        return match(*static_cast<const QString*>(data.constData()));

    // Use constData for performance: it's lighter than to toStringList()
    if (data.type() == QVariant::StringList)
        return match(*static_cast<const QStringList*>(data.constData()));

    if (m_cmpType == Cmp::Regex)
        return m_regex.match(data.toString()).hasMatch();

    if (m_cmpType == Cmp::NoRegex)
        return !m_regex.match(data.toString()).hasMatch();

    // Normal comparisons
    switch (m_cmpType) {
    case Cmp::EQ:      return data == m_pattern;
    case Cmp::NE:      return data != m_pattern;
    case Cmp::LT:      return QtCompat::lt(data, m_pattern);
    case Cmp::LE:      return QtCompat::le(data, m_pattern);
    case Cmp::GT:      return QtCompat::gt(data, m_pattern);
    case Cmp::GE:      return QtCompat::ge(data, m_pattern);
    case Cmp::Regex:   assert(0); break;
    case Cmp::NoRegex: assert(0); break;
    case Cmp::None:    assert(0); break;
    }

    return false;
}

// Relational
bool Rel::match(const QAbstractItemModel& model, const QModelIndex& parent, int row, int col) const
{
    const auto [beginCol, endCol] = Context::colBounds(model.columnCount(parent), m_column, col);

    for (int column = beginCol; column < endCol; ++column) {
        // Skip if the units are not compatible.
        if (column < m_context.m_columnUnits.size())
            if (m_format != Format::_Invalid && !m_context.m_columnUnits.at(column)->validInRange(m_format))
                continue;

        if (match(model.data(model.index(row, column, parent), m_role)))
            return true;
    }

    return false;
}

const QVector<Rel::Cmp>& Rel::comparisons()
{
    static const std::remove_reference<decltype(comparisons())>::type cmpList = {
         Cmp::EQ, Cmp::NE, Cmp::LT, Cmp::LE, Cmp::GT, Cmp::GE, Cmp::Regex, Cmp::NoRegex
    };

    return cmpList;
}

QString Rel::cmpToString(Cmp cmp)
{
    switch (cmp) {
    case Cmp::EQ:      return "==";
    case Cmp::NE:      return "!=";
    case Cmp::LT:      return "<";
    case Cmp::LE:      return "<=";
    case Cmp::GT:      return ">";
    case Cmp::GE:      return ">=";
    case Cmp::Regex:   return "=~";
    case Cmp::NoRegex: return "!~";
    case Cmp::None:    assert(0);
    }

    return "";
}

void Rel::toString(const Context& ctx, QString& string) const
{
    if (m_column >= 0)
        string += ctx.m_headers.at(m_column);
    else if (!isRegex())
        string += "*";

    if (!isRegex() || m_column >= 0) {
        string += " ";
        string += cmpToString(m_cmpType);
        string += " ";
    }

    if (m_column >= 0 && m_column < ctx.m_columnUnits.size())
        string += ctx.m_columnUnits.at(m_column)->toString(m_pattern, m_format);
    else
        string += m_pattern.toString();
}

Base::PatternList& Rel::patterns(PatternList& patterns) const
{
    switch (m_cmpType) {
    case Cmp::NE:      break;
    case Cmp::EQ:
        // add anchored, escaped string
        patterns.append(QRegularExpression(
                            Util::AnchoredPattern(
                                QRegularExpression::escape(m_pattern.toString())),
                            patternOpts()));
        break;
    case Cmp::LT:      [[fallthrough]];
    case Cmp::LE:      [[fallthrough]];
    case Cmp::GT:      [[fallthrough]];
    case Cmp::GE:
        // add begin-anchored, escaped string
        patterns.append(QRegularExpression(
                            Util::AnchoredPattern(
                                QRegularExpression::escape(m_pattern.toString()), true, false),
                            patternOpts()));

        break;
    case Cmp::Regex:   patterns.append(m_regex); break;
    case Cmp::NoRegex: break;
    case Cmp::None:    assert(0); break;
    }

    return patterns;
}

bool Rel::operator==(const Base& rhsBase) const
{
    const Rel* rhs = dynamic_cast<const Rel*>(&rhsBase);

    return rhs                 == this              &&
        rhs->m_pattern         == m_pattern         &&
        rhs->m_regex           == m_regex           &&
        rhs->m_cmpType         == m_cmpType         &&
        rhs->m_caseSensitivity == m_caseSensitivity &&
        rhs->m_column          == m_column          &&
        rhs->m_role            == m_role            &&
        rhs->m_format          == m_format;
}

bool Rel::isValid() const
{
    return !isRegex() || m_regex.isValid();
}

bool Neg::match(const QVariant& data) const
{
    if (node == nullptr)
        return false;

    return !node->match(data);
}

// Negation
bool Neg::match(const QAbstractItemModel& model, const QModelIndex& parent, int row, int col) const
{
    if (node == nullptr)
        return false;

    return !node->match(model, parent, row, col);
}

void Neg::toString(const Context& ctx, QString& string) const
{
    string += "! ( ";
    node->toString(ctx, string);
    string += " )";
}

Base::PatternList& Neg::patterns(PatternList& patterns) const
{
    return patterns;
}

bool Neg::operator==(const Base& rhsBase) const
{
    const Neg* rhs = dynamic_cast<const Neg*>(&rhsBase);

    return rhs == this &&
        *node  == *rhs->node;
}

bool Seq::match(const QVariant& data) const
{
    switch (m_cmpType) {
    case Type::Seq: [[fallthrough]];
    case Type::And: // match all nodes
        for (const auto& node : m_cmpNode)
            if (node != nullptr && !node->match(data))
                return false;
        return true;

    case Type::Xor: // exclusive or of two nodes
        if (m_cmpNode.size() > 1) {
            bool result = (m_cmpNode[0] == nullptr ? false : m_cmpNode[0]->match(data));
            for (int i = 1; i < m_cmpNode.size(); ++i)
                if (m_cmpNode[i] != nullptr)
                    result = (result != (m_cmpNode[i] == nullptr ? false : m_cmpNode[i]->match(data)));

            return result;
        }
        return false;

    case Type::Or: // match any node
        for (const auto& node : m_cmpNode)
            if (node != nullptr && node->match(data))
                return true;
        return false;

    case Type::None:
        assert(0 && "Unknown comparison");
        break;
    }

    return false;
}

// Sequence
bool Seq::match(const QAbstractItemModel& model, const QModelIndex& parent, int row, int col) const
{
    switch (m_cmpType) {
    case Type::And: // match all nodes
        for (const auto& node : m_cmpNode)
            if (node != nullptr && !node->match(model, parent, row, col))
                return false;
        return true;

    case Type::Xor: // exclusive or of two nodes
        if (m_cmpNode.size() > 1) {
            bool result = (m_cmpNode[0] == nullptr ? false : m_cmpNode[0]->match(model, parent, row, col));
            for (int i = 1; i < m_cmpNode.size(); ++i)
                result = (result != (m_cmpNode[i] == nullptr ? false : m_cmpNode[i]->match(model, parent, row, col)));

            return result;
        }
        return false;

    case Type::Or: // match any node
        for (const auto& node : m_cmpNode)
            if (node != nullptr && node->match(model, parent, row, col))
                return true;
        return false;

    case Type::Seq: { // match columns in (non-monotonical) increasing order
        const int colCount = model.columnCount(parent);
        col = std::max(col, 0);
        for (const auto& node : m_cmpNode) {
            while (col < colCount && node != nullptr && !node->match(model, parent, row, col))
                ++col;
            if (col >= colCount)
                return false;
        }

        return true;
    }

    case Type::None:
        assert(0 && "Unknown comparison");
        break;
    }

    return false;
}

void Seq::toString(const Context& ctx, QString& string) const
{
    string += "( ";

    for (const auto& node : m_cmpNode) {
        if (node != nullptr) {
            node->toString(ctx, string);
            if (node != m_cmpNode.back()) {
                switch (m_cmpType) {
                case Type::And: string += " & "; break;
                case Type::Xor: string += " ^ "; break;
                case Type::Or:  string += " | "; break;
                case Type::Seq: string += " , "; break;
                case Type::None: assert(0); break;
                }
            }
        }
    }

    string += " )";
}

Base::PatternList& Seq::patterns(PatternList& patterns) const
{
    for (const auto& node : m_cmpNode)
        if (node != nullptr)
            node->patterns(patterns);

    return patterns;
}

bool Seq::operator==(const Base& rhsBase) const
{
    const Seq* rhs = dynamic_cast<const Seq*>(&rhsBase);

    if (rhs != this ||
        rhs->m_cmpType != m_cmpType ||
        rhs->m_cmpNode.size() != m_cmpNode.size())
        return false;

    for (int i = 0; i < m_cmpNode.size(); ++i)
        if (*m_cmpNode.at(i) != *rhs->m_cmpNode.at(i))
            return false;

    return true;
}

} // namespace Query
