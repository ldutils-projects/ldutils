/*
    Copyright 2021 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef NEWPANEMODEL_H
#define NEWPANEMODEL_H

#include <QVector>
#include <QVariant>
#include <QFont>

#include <src/core/treemodel.h>
#include <src/core/modelmetadata.h>
#include <src/core/newpaneitem.h>

class NewPaneModel final :
        public TreeModel, // for use of TreeModel::units, needed by Query
        private QVector<NewPaneItem>,
        public ModelMetaData
{
    Q_OBJECT

public:
    enum {
        _First,
        Name = _First,
        Icon = Name,   // icon goes in name column
        Image,
        _Count,
    };

    NewPaneModel(QObject* parent = nullptr);

    // *** Begin QAbstractItemModel API
    void appendRow(const QString& name, const QString& image, const QString& icon, const QString& tooltip);

    bool removeRows(int position, int rows, const QModelIndex& parent = QModelIndex()) override;

    [[nodiscard]] QVariant headerData(int section, Qt::Orientation,
                                      int role = Qt::DisplayRole) const override;

    [[nodiscard]] QVariant data(const QModelIndex& idx, int role = Qt::DisplayRole) const  override;

    [[nodiscard]] QModelIndex index(int row, int column,
                                            const QModelIndex& parent = QModelIndex()) const override;

    [[nodiscard]] QModelIndex parent(const QModelIndex&) const override;

    [[nodiscard]] Qt::ItemFlags flags(const QModelIndex&) const override;

    [[nodiscard]] int rowCount(const QModelIndex& parent = QModelIndex()) const override;
    [[nodiscard]] int columnCount(const QModelIndex& parent = QModelIndex()) const override;

    [[nodiscard]] const Units& units(const QModelIndex&) const override;
    // *** End QAbstractItemModel API

    // *** begin ModelMetaData API
    static QString        mdName(ModelType);
    static bool           mdIsEditable(ModelType);
    static QString        mdTooltip(ModelType);
    static QString        mdWhatsthis(ModelType);
    static Qt::Alignment  mdAlignment(ModelType);
    static const Units&   mdUnits(ModelType);
    // *** end ModelMetaData API

    // *** begin Stream Save API
    using TreeModel::save;
    [[nodiscard]] quint32 streamMagic() const override;
    [[nodiscard]] quint32 streamVersionMin() const override;
    [[nodiscard]] quint32 streamVersionCurrent() const override;
    // *** end Stream Save API

public slots:
    void setupFont();

private:
    using TreeModel::setData;       // hide
    using TreeModel::moveRow;       // hide
    using TreeModel::moveRows;      // hide
    using TreeModel::multiSet;      // hide
    using TreeModel::clearData;     // hide
    using TreeModel::setHeaderData; // hide
    using TreeModel::appendRow;     // hide
    using TreeModel::appendRows;    // hide
    using TreeModel::insertRow;     // hide
    using TreeModel::insertRows;    // hide
    using TreeModel::setIcon;       // hide
    using TreeModel::clearIcon;     // hide
    using TreeModel::load;          // hide

    QFont m_largerFont;
};

#endif // NEWPANEMODEL_H
