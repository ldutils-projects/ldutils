/*
    Copyright 2019-2023 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <cassert>
#include <cstdlib>

#include <src/util/util.h>
#include <src/util/varianthash.h>

#include "contentaddrmodel.h"

ContentAddrModel::ContentAddrModel(TreeItem* root, int keyColumn, int keyRole, QObject *parent) :
    TreeModel(root, parent),
    m_keyColumn(keyColumn),
    m_keyRole(keyRole),
    m_dirty(false)
{
    setupSignals();
}

ContentAddrModel::~ContentAddrModel()
{
}

ContentAddrModel& ContentAddrModel::operator=(const ContentAddrModel& other)
{
    m_contentCache.clear();
    m_keyColumn  = other.m_keyColumn;
    m_keyRole    = other.m_keyRole;

    // We must do this after the assignments and cache clearing above, to get proper behavior
    // during the node copies.
    return static_cast<ContentAddrModel&>(TreeModel::operator=(other));
}

// Signals we react to for updating our cache.
void ContentAddrModel::setupSignals()
{
    connect(this, &ContentAddrModel::rowsAboutToBeRemoved, this, &ContentAddrModel::handleRowsAboutToBeRemoved);
    connect(this, &ContentAddrModel::rowsInserted, this, &ContentAddrModel::handleRowsInserted);
    connect(this, &ContentAddrModel::modelReset, this, &ContentAddrModel::handleModelReset);
}

QString ContentAddrModel::uniqueName(const QVariant& name) const
{
    int suffix = 0;

    if (name.type() != QVariant::String) // we only auto-modify strings.
        return { };

    auto oldName = name.value<QString>();

    for (int pos = oldName.length() - 1; pos > 1 && suffix == 0; --pos) {
        if (oldName[pos] == '_') {
            suffix = oldName.midRef(pos+1).toInt();
            oldName = oldName.mid(0, pos);
        }
    }

    for (suffix = std::max(suffix, 1); suffix < 1024; ++suffix) {
        const QString uniqueValue = oldName + "_" + QString::number(suffix);
        if (const auto it = m_contentCache.find(uniqueValue); it == m_contentCache.end())
            return uniqueValue;
    }

    return { };
}

// Content cache based fetch: our whole reason to exist.
QVariant ContentAddrModel::value(const QVariant& key, int column, int role) const
{
    refreshCache();

    // If it's in the cache, use it.
    if (const auto it = m_contentCache.find(key); it != m_contentCache.end())
        return getItem(it.value())->data(column, role);

    // Didn't find it.
    return { };
}

QModelIndex ContentAddrModel::keyIdx(const QVariant& key) const
{
    if (const auto it = m_contentCache.find(key); it != m_contentCache.end())
        return { it.value() };

    return { };
}

bool ContentAddrModel::contains(const QVariant& key) const
{
    return m_contentCache.contains(key);
}

bool ContentAddrModel::setData(const QModelIndex& idx, const QVariant& value, int role)
{
    refreshCache();

    if (idx.column() != m_keyColumn)
        return TreeModel::setData(idx, value, role);

    m_contentCache.remove(data(idx, role));

    const auto setNew = [this](const QModelIndex& idx, const QVariant& v, int role) {
        m_contentCache.insert(v, idx);
        return TreeModel::setData(idx, v, role);
    };

    // If the value isn't in the content cache, insert it, and set the model.
    if (const auto it = m_contentCache.find(value); it == m_contentCache.end())
        return setNew(idx, value, role);

    // Othewise uniquify the new value if possible.
    if (const QString newName = uniqueName(value); !newName.isEmpty())
        return setNew(idx, QVariant(newName), role);

    // If we get here, all attempts have failed.  Put old value back in cache and give up.
    m_contentCache.insert(value, idx);
    return false;
}

void ContentAddrModel::copyItem(const QModelIndex& dstIdx,
                                const TreeModel& srcModel, const QModelIndex& srcIdx)
{
    refreshCache();

    if (dstIdx == srcIdx && dstIdx.isValid()) // short circuit self-copy
        return;

    if (&srcModel == this) {
        // Intra-model copy
        // Fix content cache after copy, else we'd have two items with the same name.

        TreeModel::copyItem(dstIdx, srcModel, srcIdx);

        const QVariant value = data(m_keyColumn, srcIdx, m_keyRole);

        // Rename the source, to avoid conflict with the dst's copy of the value.
        if (const QString newName = uniqueName(value); !newName.isEmpty())
            setData(m_keyColumn, srcIdx, newName, m_keyRole);

        m_contentCache.insert(value, dstIdx);   // update cache to point to new.
    } else {
        const QVariant oldValue = data(m_keyColumn, dstIdx, m_keyRole);
        m_contentCache.remove(oldValue);

        TreeModel::copyItem(dstIdx, srcModel, srcIdx);
        const QVariant newValue = srcModel.data(m_keyColumn, srcIdx, m_keyRole);
        setData(dstIdx, newValue, m_keyRole);
    }
}

void ContentAddrModel::handleRowsAboutToBeRemoved(const QModelIndex& parent, int first, int last)
{
    m_dirty = true;

    for (int row = first; row <= last; ++row)
        m_contentCache.remove(data(index(row, m_keyColumn, parent), m_keyRole));
}

void ContentAddrModel::handleRowsInserted(const QModelIndex&, int, int)
{
    m_dirty = true;
}

void ContentAddrModel::handleModelReset()
{
    m_dirty = true;
    refreshCache();
}

void ContentAddrModel::refreshCache() const
{
    const_cast<ContentAddrModel*>(this)->refreshCache();
}

void ContentAddrModel::refreshCache()
{
    if (!m_dirty)
        return;

    m_dirty = false;

    m_contentCache.clear();

    // Repopulate cache after a model reset or other dirtying event.
    Util::Recurse(*this, [this](const QModelIndex& idx) {
        m_contentCache.insert(data(m_keyColumn, idx, m_keyRole), idx);
        return true;
    });
}
