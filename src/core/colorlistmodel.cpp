/*
    Copyright 2019 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


#include <cassert>

#include "colorlistmodel.h"
#include "colorlistitem.h"

ColorListModel::ColorListModel(QObject *parent) :
    TreeModel(new ColorListItem(), parent)
{
    // Subclasses must call addMissing, because we can't call virtual methods from the constructor.
}

QColor ColorListModel::operator[](int row) const
{
    return data(index(row, ColorListModel::Color), Qt::BackgroundRole).value<QColor>();
}

Qt::ItemFlags ColorListModel::flags(const QModelIndex& index) const
{
    switch (index.column()) {
    case ColorListModel::Name:
        return Qt::ItemIsEnabled | Qt::ItemIsSelectable;
    case ColorListModel::Color:
        return Qt::ItemIsEnabled | Qt::ItemIsEditable;
    default:
        assert(0);
        return TreeModel::flags(index);
    }
}

void ColorListModel::load(QSettings& settings)
{
    TreeModel::load(settings);
    addMissing();
}
