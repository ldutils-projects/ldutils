/*
    Copyright 2019-2022 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef QUERY_H
#define QUERY_H

#include <utility>
#include <tuple>
#include <memory>   // for std::unique_ptr
#include <functional>
#include <QVector>
#include <QStringRef>
#include <QRegularExpression>

#include <src/util/roles.h>
#include <src/util/units.h>
#include <src/util/strongnum.h>

class QAbstractItemModel;
class QModelIndex;

namespace Query {

class Context;

/*******************************************************************************
Pattern BNF:

<pattern> ::=
   <pattern> "|" <pattern> ...   |   ; match any
   <pattern> "&" <pattern> ...   |   ; match all
   <pattern> "," <pattern> ...   |   ; sequence of matches in increasing columns
   "!" <pattern>                 |   ; match negation
   <column> <cmp> <value> <unit> |   ; relational or regex comparison in given column
   <regex> <unit>                    ; PERL regular expression

<column> ::= "*"   |                 ; match in any column
             Name  |                 ; match in given column by name

<regex> ::= Perl Regular Expression from https://perldoc.perl.org/perlre.html

<cmp> ::= "<" | "<=" | ">" | ">=" | "==" | "!=" | "=~" | "!~"

<unit> ::= "" |
           SuffixName                ; unit suffix

Examples:
  Elevation > 1500
  Name : [A-M].*
  foo | bar
  foo & bar & blee
  col1data, col3data, col7data
  Name : foo | bar

*******************************************************************************/

class Base;
using query_uniqueptr_t = std::unique_ptr<const Base>;  // smart pointer to query

// Comparison tree baseclass.  Various node types derive from this.
class Base {
public:
    static const constexpr int AnyColumn = -1;
    static const constexpr int NoColumn = -2;

    using PatternList = QVector<QRegularExpression>;

    virtual ~Base() { }

    // Per-node match
    [[nodiscard]] virtual bool match(const QAbstractItemModel&, const QModelIndex& parent, int row, int col = NoColumn) const = 0;
    [[nodiscard]] virtual bool match(const QVariant&) const = 0;

    // Reconstruct string from parse tree
    virtual void toString(const Context&, QString&) const = 0;

    // Return list of patterns in this tree
    virtual PatternList& patterns(PatternList&) const = 0;
    PatternList patterns() const;

    [[nodiscard]] virtual bool operator==(const Base& rhs) const = 0;
    [[nodiscard]] virtual bool operator!=(const Base& rhs) const { return !(operator==(rhs)); }
    [[nodiscard]] virtual bool isValid() const;

    [[nodiscard]] inline bool matchIdx(const QModelIndex& idx, int col = NoColumn) const {
        return match(*idx.model(), idx.model()->parent(idx), idx.row(), col);
    }

    // Returns true if this is the type of node given in T
    template <typename T> [[nodiscard]] bool is() const { return dynamic_cast<const T*>(this) != nullptr; }
};

// Error pattern: matches nothing
class None : public Base {
public:
    bool match(const QAbstractItemModel&, const QModelIndex& parent, int row, int col = NoColumn) const override;
    bool match(const QVariant&) const override;
    void toString(const Context&, QString&) const override;
    PatternList& patterns(PatternList&) const override;
    bool operator==(const Base& rhs) const override;
};

// Invalid query
class Error : public None {
public:
    bool isValid() const override { return false; }
};

// Matches everything
class All final : public Base {
public:
    bool match(const QAbstractItemModel&, const QModelIndex& parent, int row, int col = NoColumn) const override;
    bool match(const QVariant&) const override;
    void toString(const Context&, QString&) const override;
    PatternList& patterns(PatternList&) const override;
    bool operator==(const Base& rhs) const override;
};

// Single relational comparison node
class Rel final : public Base {
public:
    enum class Cmp {
        EQ,      // ==
        NE,      // !=
        LT,      // <
        LE,      // <=
        GT,      // >
        GE,      // >=
        Regex,   // =~
        NoRegex, // !~
        None, // no comparison
    };

    // Create one, but return nullptr if it was an invalid pattern.
    static const Rel* make(const Context& ctx, const QVariant& pattern, Cmp cmpType, Qt::CaseSensitivity caseSensitivity, int column, int role,
                           Format format);

    bool match(const QAbstractItemModel&, const QModelIndex& parent, int row, int col = NoColumn) const override;
    bool match(const QVariant&) const override;
    bool match(const QStringList&) const;
    bool match(const QString&) const;
    void toString(const Context&, QString&) const override;
    PatternList& patterns(PatternList&) const override;
    bool operator==(const Base& rhs) const override;
    bool isValid() const override;
    [[nodiscard]] bool isRegex() const { return isRegex(m_cmpType); }

    // True if this is a regex comparison
    static bool isRegex(Cmp cmp) { return cmp == Cmp::Regex || cmp == Cmp::NoRegex; }

    static QString cmpToString(Cmp);
    static const QVector<Cmp>& comparisons();

private:
    Rel(const Context& ctx, const QVariant& pattern, Cmp cmpType, Qt::CaseSensitivity caseSensitivity, int column, int role,
        Format format) :
        m_context(ctx),
        m_pattern(pattern),
        m_cmpType(cmpType),
        m_caseSensitivity(caseSensitivity),
        m_column(column),
        m_role(role),
        m_format(format)
    {
        if (isRegex()) {
            m_regex.setPattern(pattern.toString());
            m_regex.setPatternOptions(patternOpts());
            m_regex.optimize();
        }
    }

    QRegularExpression::PatternOptions patternOpts() const {
        return m_caseSensitivity == Qt::CaseInsensitive ?
                                    QRegularExpression::CaseInsensitiveOption :
                                    QRegularExpression::NoPatternOption;
    }

    const Context&      m_context;
    QVariant            m_pattern;
    QRegularExpression  m_regex;
    Cmp                 m_cmpType;
    Qt::CaseSensitivity m_caseSensitivity;
    int                 m_column;
    int                 m_role;      // data role
    Format              m_format;    // unit format, for toString()
};

class Neg final : public Base {
public:
    Neg(const Base* node) :
        node(node)
    { }

    ~Neg() override { delete node; }

    bool match(const QAbstractItemModel&, const QModelIndex& parent, int row, int col = NoColumn) const override;
    bool match(const QVariant&) const override;
    void toString(const Context&, QString&) const override;
    PatternList& patterns(PatternList&) const override;
    bool operator==(const Base& rhs) const override;

private:
    const Base* node;
};

class Seq final : public Base {
public:
    // These are in precision order.
    enum class Type {
        And,
        Xor,
        Or,
        Seq,  // sequence, N matches in non-monotonic increasing column order
        None,
    };

    Seq(Type cmpType, const Base* lhs = nullptr, const Base* rhs = nullptr) :
        m_cmpType(cmpType)
    {
        append(lhs);
        append(rhs);
    }

    Seq* append(const Base* node) {
        if (node != nullptr)
            m_cmpNode.append(node);
        return this;
    }

    ~Seq() override { qDeleteAll(m_cmpNode); }

    bool match(const QAbstractItemModel&, const QModelIndex& parent, int row, int col = NoColumn) const override;
    bool match(const QVariant&) const override;
    void toString(const Context&, QString&) const override;
    PatternList& patterns(PatternList&) const override;
    bool operator==(const Base& rhs) const override;

private:
    QVector<const Base*> m_cmpNode;
    Type                 m_cmpType;
};


// Context data for parsing query language.  This can be set up once for a model and stored,
// to avoid some processing for each and every query parse.
class Context {
public:
    Context(int regexRole = Util::CopyRole,
            int relRole = Util::RawDataRole,
            int strRelRole = Util::CopyRole);

    Context(const QAbstractItemModel* model, Qt::CaseSensitivity cs = Qt::CaseInsensitive,
            int regexRole = Util::CopyRole,
            int relRole = Util::RawDataRole,
            int strRelRole = Util::CopyRole);

    using queryFactory_t = std::function<query_uniqueptr_t()>;

    // parse returns an std::unique_ptr, since QScopedPtrs are not movable
    [[nodiscard]] query_uniqueptr_t parse(const QString& query) const;
    [[nodiscard]] bool isValidQuery(const QString& query) const;
    [[nodiscard]] QString toString(const Base*) const;

    // Passes on to the query, using our model
    [[nodiscard]] bool match(const query_uniqueptr_t& query, const QModelIndex& parent, int row, int col = Base::NoColumn) const;
    [[nodiscard]] bool match(const query_uniqueptr_t& query, const QModelIndex& item, int col = Base::NoColumn) const;
    [[nodiscard]] bool match(const query_uniqueptr_t& query, const QVariant& item) const;

    // This can't be in the constructor: it's done during model setting
    Context& setModel(const QAbstractItemModel*, Qt::CaseSensitivity = Qt::CaseInsensitive);
    // If this isn't a subclass of TreeModel, we can provide units separately.
    // The units pointers must persist for the life of the context.
    Context& setModel(const QAbstractItemModel*, const QVector<const Units*>&,
                      Qt::CaseSensitivity = Qt::CaseInsensitive);
    // Set parse units (e.g, if there is no model)
    Context& setParseUnits(const QVector<const Units*>&);
    // Set case sensitivity
    Context& setCaseSensitivity(Qt::CaseSensitivity cs) { m_caseSensitivity = cs; return *this; }
     // query for empty strings
    Context& setEmptyQueryFactory(const queryFactory_t& f) { m_emptyQueryFactory = f; return *this; }

    static Rel::Cmp parseCompare(const QStringRef& token);
    [[nodiscard]] int parseColumnName(const QStringRef& token) const;
    [[nodiscard]] const QVector<QString>& headerNames() const { return m_headers; }
    [[nodiscard]] const Units* units(int col) const { return m_columnUnits.at(col); }

    static const queryFactory_t genAll;  // generator for Query::All
    static const queryFactory_t genNone; // generator for Query::None

private:
    enum class Status {
        Ok,
        Unrecognized,
        Error,
    };

    friend class Rel;
    friend class Regex;

    [[nodiscard]] const Base* parseColumn() const;
    [[nodiscard]] const Base* parseNeg() const;
    [[nodiscard]] const Base* parseSeq(Seq::Type prec = Seq::Type::None, const Base* rhs = nullptr,
                                       Seq::Type* nextType = nullptr) const;
    [[nodiscard]] const Base* parseRegex() const;
    [[nodiscard]] const Base* parsePattern() const;
    [[nodiscard]] const Base* parseParen() const;

    [[nodiscard]] auto parseUnitSuffix(int col) const;
    [[nodiscard]] static Seq::Type parseSeqType(const QStringRef& token);
    inline QStringRef eat() const;
    inline QStringRef eat(int len) const;
    QStringRef nextToken() const;
    [[nodiscard]] const Base* error() const;
    [[nodiscard]] QStringRef token() const { return { m_text.string(), int(m_tokenPos), int(m_tokenLen) }; }
    [[nodiscard]] const Base* unwind(const QStringRef&, bool hasError) const;

    [[nodiscard]] static std::pair<int, int> colBounds(int colCount, int column, int colOverride = Base::NoColumn);

    int                       m_columnCount;
    QMap<QString, int>        m_headerMap;
    QVector<QString>          m_headers;
    Qt::CaseSensitivity       m_caseSensitivity;
    QVector<const Units*>     m_columnUnits;
    int                       m_regexRole;  // role for regex compares
    int                       m_relRole;    // role for numeric and date relative compares
    int                       m_strRelRole; // role for string relative compares
    const QAbstractItemModel* m_model;
    queryFactory_t            m_emptyQueryFactory;

    mutable QStringRef        m_text;       // parse text
    mutable bool              m_error;      // parse error

    mutable Stronger::int32_t<class TokenKey> m_tokenPos;  // end of next token
    mutable Stronger::int32_t<class TokenKey> m_tokenLen;  // current token length
    mutable Stronger::int32_t<class ParenKey> m_parenNest; // paren nesting
};

} // namespace Query

#endif // QUERY_H
