/*
    Copyright 2019-2022 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef SETTINGS_H
#define SETTINGS_H

#include <type_traits>

#include <QSettings>
#include <QComboBox>
#include <QLineEdit>
#include <QSpinBox>
#include <QDoubleSpinBox>
#include <QHeaderView>
#include <QRadioButton>
#include <QCheckBox>

// Base class for objects which save their settings.
class Settings {
public:
    virtual ~Settings() { }
    virtual void save(QSettings&) const = 0;
    virtual void load(QSettings&) = 0;

protected:
    virtual int version() const; // format version
    static const constexpr char* versionKey = "versionKey";
};

// For declaring that a type can be saved as a variant
template <typename T> struct is_variant_savable : public std::false_type { };
template <typename T> inline constexpr bool is_variant_savable_v = is_variant_savable<T>::value;

namespace {
    // ----- Saving ------
    template <bool isVariant> struct SaveItem {
        template <typename DATA> SaveItem(QSettings& settings, const QString& group, const DATA& data) {
            settings.setValue(group, QVariant::fromValue(data));
        }
    };

    template <> struct SaveItem<false> {
        template <typename DATA> SaveItem(QSettings& settings, const QString& group, const DATA& data) {
            settings.beginGroup(group);
            data.save(settings);
            settings.endGroup();
        }
    };

    template <typename DATA>
    void SaveValue(QSettings& settings, const QString& group, const DATA& data) {
        SaveItem<std::is_assignable_v<QVariant, DATA> ||
                 std::is_enum_v<DATA> ||
                 is_variant_savable_v<DATA>>
                (settings, group, data);
    }

    inline void SaveValue(QSettings& settings, const QString& group, const QHeaderView& data) {
        SaveValue(settings, group, data.saveState());
    }

    inline void SaveValue(QSettings& settings, const QString& group, QComboBox* data) {
        if (data != nullptr)
            SaveValue(settings, group, data->currentText());
    }

    inline void SaveValue(QSettings& settings, const QString& group, QDoubleSpinBox* data) {
        if (data != nullptr)
            SaveValue(settings, group, data->value());
    }

    inline void SaveValue(QSettings& settings, const QString& group, QRadioButton* data) {
        if (data != nullptr)
            SaveValue(settings, group, data->isChecked());
    }

    inline void SaveValue(QSettings& settings, const QString& group, QCheckBox* data) {
        if (data != nullptr)
            SaveValue(settings, group, data->isChecked());
    }

    inline void SaveValue(QSettings& settings, const QString& group, QLineEdit* data) {
        if (data == nullptr)
            return;
        settings.beginGroup(group); {
            settings.setValue("text", data->text());
            settings.setValue("cursorPosition", data->cursorPosition());
        } settings.endGroup();
    }

    inline void SaveValue(QSettings& settings, const QString& group, QSpinBox* data) {
        if (data == nullptr)
            return;
        settings.beginGroup(group); {
            settings.setValue("value", data->value());
        } settings.endGroup();
    }

    template <typename DATA>
    void SaveContainer(QSettings& settings, const QString& group, const DATA& data) {
        settings.beginWriteArray(group); {
            int i = 0;
            for (const auto& item : data) {
                settings.setArrayIndex(i++);
                SaveValue(settings, group, item);
            }
        } settings.endArray();
    }

    // ----- Loading ------
    template <bool isVariant> struct LoadItem {
        template <typename DATA> LoadItem(QSettings& settings, const QString& group, DATA& data) {
            if (settings.contains(group))
                data = settings.value(group).value<DATA>();
        }
    };

    template <> struct LoadItem<false> {
        template <typename DATA> LoadItem(QSettings& settings, const QString& group, DATA& data) {
            settings.beginGroup(group);
            data.load(settings);
            settings.endGroup();
        }
    };

    template <typename DATA>
    void LoadValue(QSettings& settings, const QString& group, DATA& data) {
        LoadItem<std::is_assignable_v<QVariant, DATA> ||
                 std::is_enum_v<DATA> ||
                 is_variant_savable_v<DATA>>(settings, group, data);
    }

    inline void LoadValue(QSettings& settings, const QString& group, QHeaderView& data) {
        if (settings.contains(group))
            data.restoreState(settings.value(group).toByteArray());
    }

    inline void LoadValue(QSettings& settings, const QString& group, QComboBox* data) {
        if (data != nullptr && settings.contains(group))
            data->setCurrentText(settings.value(group).toString());
    }

    inline void LoadValue(QSettings& settings, const QString& group, QDoubleSpinBox* data) {
        if (data != nullptr && settings.contains(group))
            data->setValue(settings.value(group).toDouble());
    }

    inline void LoadValue(QSettings& settings, const QString& group, QRadioButton* data) {
        if (data != nullptr && settings.contains(group))
            data->setChecked(settings.value(group).toBool());
    }

    inline void LoadValue(QSettings& settings, const QString& group, QCheckBox* data) {
        if (data != nullptr && settings.contains(group))
            data->setChecked(settings.value(group).toBool());
    }

    inline void LoadValue(QSettings& settings, const QString& group, QLineEdit* data, const QLineEdit* = nullptr) {
        if (data == nullptr)
            return;
        settings.beginGroup(group); {
            if (settings.contains("text"))
                data->setText(settings.value("text").toString());
            if (settings.contains("cursorPosition"))
                data->setCursorPosition(settings.value("cursorPosition").toInt());
        } settings.endGroup();
    }

    inline void LoadValue(QSettings& settings, const QString& group, QSpinBox* data, const QSpinBox* = nullptr) {
        if (data == nullptr)
            return;
        settings.beginGroup(group); {
            if (settings.contains("value"))
                data->setValue(settings.value("value").toInt());
        } settings.endGroup();
    }

    template <typename DATA>
    void LoadContainer(QSettings& settings, const QString& group, DATA& data,
                       const typename std::remove_reference<decltype(data.front())>::type& def)
    {
        data.clear();

        const int size = settings.beginReadArray(group); {
            data.reserve(size);
            for (int i = 0; i < size; ++i) {
                settings.setArrayIndex(i);
                data.push_back(def);
                LoadValue(settings, group, data.back());
            }
        } settings.endArray();
    }
} // anonymous namespace

// Convenience functions for settings save/load
namespace SL {
    // ----- Saving ------
    template <typename DATA>
    void Save(QSettings& settings, const QString& group, const DATA& data) {
        SaveValue(settings, group, data);
    }

    template <typename DATA>
    void Save(QSettings& settings, const QString& group, const QList<DATA>& data) {
        SaveContainer(settings, group, data);
    }

    template <typename DATA>
    void Save(QSettings& settings, const QString& group, const QVector<DATA>& data) {
        SaveContainer(settings, group, data);
    }

    template <typename DATA1, typename DATA2>
    void Save(QSettings& settings, const QString& group, const QHash<DATA1, DATA2>& data) {
        SaveValue(settings, group, data);
    }

    // ----- Loading ------
    template <typename DATA>
    [[nodiscard]] DATA Load(QSettings& settings, const QString& group, const DATA& def = DATA()) {
        // the inner fromValue needed to handle enum classes
        return settings.value(group, QVariant(QVariant::fromValue(def))).value<DATA>();
    }

    // Load only if exists in prefs
    template <typename DATA>
    void Load(QSettings& settings, const QString& group, DATA& data) {
        LoadValue(settings, group, data);
    }

    template <typename DATA>
    void Load(QSettings& settings, const QString& group, QList<DATA>& data, const DATA& def = DATA()) {
        LoadContainer(settings, group, data, def);
    }

    template <typename DATA>
    void Load(QSettings& settings, const QString& group, QVector<DATA>& data, const DATA& def = DATA()) {
        LoadContainer(settings, group, data, def);
    }

    template <typename DATA1, typename DATA2>
    void Load(QSettings& settings, const QString& group, QHash<DATA1, DATA2>& data) {
        LoadValue(settings, group, data);
    }

// The stringizing operator # can only be used from the preprocessor :(
#define MemberSave(settings, member) SL::Save(settings, #member, member)
#define MemberLoad(settings, member) SL::Load(settings, #member, member)
} // namespace SL

#endif // SETTINGS_H
