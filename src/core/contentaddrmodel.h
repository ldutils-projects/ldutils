/*
    Copyright 2019-2023 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef CONTENTADDRMODEL_H
#define CONTENTADDRMODEL_H

#include <QHash>
#include <QPersistentModelIndex>
#include <src/util/qtcompat.h>
#include "treemodel.h"

// Derives from TreeModel and adds a content addressable fast cache.
class ContentAddrModel : public TreeModel
{
    Q_OBJECT

public:
    explicit ContentAddrModel(TreeItem* root, int m_keyColumn, int m_keyRole, QObject *parent = nullptr);
    ~ContentAddrModel() override;

    ContentAddrModel& operator=(const ContentAddrModel&);

    // Content cached data fetch.  This is why we exist.
    [[nodiscard]] virtual QVariant     value(const QVariant& key, int column, int role = Qt::DisplayRole) const;
    [[nodiscard]] virtual QModelIndex  keyIdx(const QVariant& key) const;

    // Return true if we contain this key.
    [[nodiscard]] virtual bool contains(const QVariant& key) const;

    using TreeModel::setData;
    bool setData(const QModelIndex& idx, const QVariant& value,
                 int role = Qt::DisplayRole) override;

    [[nodiscard]] int keyRole() const { return m_keyRole; }
    [[nodiscard]] int keyColumn() const { return m_keyColumn; }

protected:
    void copyItem(const QModelIndex& dstIdx, const TreeModel& srcModel, const QModelIndex& srcIdx) override;

private slots:
    void handleRowsAboutToBeRemoved(const QModelIndex& parent, int first, int last);
    void handleRowsInserted(const QModelIndex& parent, int first, int last);
    void handleModelReset();

private:
    void setupSignals();
    QString uniqueName(const QVariant& name) const;
    void refreshCache();
    void refreshCache() const;

    using TreeModel::operator=;

    int                                    m_keyColumn;
    int                                    m_keyRole;
    bool                                   m_dirty;
    QHash<QVariant, QPersistentModelIndex> m_contentCache;
};

#endif // CONTENTADDRMODEL_H
