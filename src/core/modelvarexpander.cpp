/*
    Copyright 2021-2022 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <QModelIndex>
#include <QUrl>

#include "modelvarexpander.h"

#include <src/core/treemodel.h>
#include <src/core/query.inl.h>  // for CanonicalizeColumnName

ModelVarExpander::ModelVarExpander(bool fgColorize, bool bgColorize) :
    m_model(nullptr),
    m_fgColorize(fgColorize),
    m_bgColorize(bgColorize)
{
}

void ModelVarExpander::setupCache(const TreeModel* model) const
{
    if (m_model == model) // bypass if already set up, or if no model given
        return;

    m_nameCache.clear();
    m_model = model;

    if (model == nullptr)
        return;

    for (ModelType c = 0; c < model->columnCount(); ++c) {
        const QString canonicalName = Query::CanonicalizeColumnName(model->headerData(c, Qt::Horizontal).toString());
        m_nameCache.insert(canonicalName.toLower(), c); // Cache name for performance:
    }
}

QString ModelVarExpander::expandHtml(const QString& srcHtml, const QModelIndex& idx) const
{
    // Regex to find variable expansions of the form ${Name}
    static const QRegularExpression varRegex(R"REGEX(\$\{([[:alnum:]\s%_-]+)\})REGEX");

    // Defer this in case the model isn't fully set up at our construction time
    setupCache(qobject_cast<const TreeModel*>(idx.model()));

    if (srcHtml.isEmpty())
        return { };

    QString expandedHtml; // we build this: cheaper than many mid-string replacements

    expandedHtml.reserve(srcHtml.size() + 4096); // padding to avoid reallocs upon <span> etc insertions
    int prevEnd = 0;

    QRegularExpressionMatchIterator matches = varRegex.globalMatch(srcHtml);

    while (matches.hasNext()) {
        const QRegularExpressionMatch match = matches.next();

        expandedHtml += srcHtml.midRef(prevEnd, match.capturedStart() - prevEnd);  // insert last match end to here

        const auto it = m_nameCache.find(match.captured(1).toLower());
        if (it == m_nameCache.end()) { // not found?  skip.
            expandedHtml += "<b>N/A</b>";
            prevEnd = match.capturedEnd(0);
            continue;
        }

        const QModelIndex valIdx = idx.sibling(idx.row(), it.value());

        // If we have icon names, insert <img>'s for those icons
        if (const QStringList iconNames = valIdx.data(Util::IconNameRole).toStringList(); !iconNames.isEmpty()) {

            // The Qt HTML <img> scaling is rubbish, so use 36 for the height of everything to minimize that.
            // const QSize iconSize = valIdx.data(Qt::SizeHintRole).toSize();

            for (const auto& icon : iconNames) {
                const QUrl  url = QUrl::fromLocalFile(icon);
                expandedHtml += QString(R"(<img height="%1" src="%2"/>)")
                                // .arg(iconSize.height()  > 0 ? iconSize.height()  : 16)
                                .arg(36)  // see scaling comment above
                                .arg(url.url());
                if (icon != iconNames.back())
                    expandedHtml += "&nbsp;";
            }
        }

        // Add color span if requested and available
        const QColor fgColor = m_fgColorize ? valIdx.data(Qt::ForegroundRole).value<QColor>() : QColor();
        const QColor bgColor = m_bgColorize ? valIdx.data(Qt::BackgroundRole).value<QColor>() : QColor();
        const bool spanOpen = fgColor.isValid() || bgColor.isValid();

        if (spanOpen)           expandedHtml += "<span style=\"";
        if (fgColor.isValid())  expandedHtml += "color:" + fgColor.name() + ";";
        if (bgColor.isValid())  expandedHtml += "background-color:" + bgColor.name() + ";";
        if (spanOpen)           expandedHtml += "\">";

        expandedHtml += valIdx.data(Qt::DisplayRole).toString(); // insert data
        prevEnd = match.capturedEnd(0);

        // Close color span if opened
        if (spanOpen)           expandedHtml += "</span>";
    }

    // Append trailing part after last expansion
    expandedHtml += srcHtml.midRef(prevEnd);  // insert last match end to here

    return expandedHtml;
}

