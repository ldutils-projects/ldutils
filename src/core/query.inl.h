/*
    Copyright 2021 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef QUERY_INL_H
#define QUERY_INL_H

#include <QString>

namespace Query {

[[nodiscard]] inline static QString& CanonicalizeColumnName(QString colName)
{
    return colName.replace(' ', '_');
}

} // namespace Query

#endif // QUERY_INL_H
