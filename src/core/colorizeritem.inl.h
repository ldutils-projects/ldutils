/*
    Copyright 2021-2022 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef COLORIZERITEM_INL_H
#define COLORIZERITEM_INL_H

#include <cassert>
#include <QVariant>

#include "colorizeritem.h"

inline bool ColorizerItem::checkRole(int role)
{
    switch (role) {
    case Qt::ForegroundRole: [[fallthrough]];
    case Qt::BackgroundRole: [[fallthrough]];
    case Qt::DecorationRole: return true;
    default:                 return false;
    }
}

inline QVariant ColorizerItem::dataForRole(int role) const
{
    switch (role) {
    case Qt::ForegroundRole: return fgColor();
    case Qt::BackgroundRole: return bgColor();
    case Qt::DecorationRole: return icon();
    default:                 assert(0); return { };
    }
}

#endif // COLORIZERITEM_INL_H
