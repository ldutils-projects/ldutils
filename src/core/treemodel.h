/*
    Copyright 2019-2023 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TREEMODEL_H
#define TREEMODEL_H

#include <QAbstractItemModel>
#include <QModelIndex>
#include <QVector>
#include <QStringList>
#include <cinttypes>
#include <limits>
#include <tuple>
#include <functional>

#include <src/core/modelmetadata.h>
#include <src/util/roles.h>
#include <src/util/qtcompat.h>
#include <src/core/settings.h>
#include "treeitem.h"

class QSortFilterProxyModel;
class QIcon;
class QMimeData;
class QIODevice;
class Units;

class TreeModel : public QAbstractItemModel, public Settings
{
    Q_OBJECT

public:
    explicit TreeModel(TreeItem* root, QObject *parent = nullptr);
    ~TreeModel() override;

    // Queries
    [[nodiscard]] QVariant data(const QModelIndex&, int role = Qt::DisplayRole) const override;
    [[nodiscard]] QVariant headerData(ModelType, Qt::Orientation orientation,
                                      int role = Qt::DisplayRole) const override;

    [[nodiscard]] QModelIndex index(int row, int column,
                                    const QModelIndex& parent = QModelIndex()) const override;
    [[nodiscard]] QModelIndex parent(const QModelIndex& index) const override;

    [[nodiscard]] int rowCount(const QModelIndex& parent = QModelIndex()) const override;
    [[nodiscard]] int columnCount(const QModelIndex& parent = QModelIndex()) const override;

    [[nodiscard]] Qt::ItemFlags flags(const QModelIndex&) const override;

    // Convenience: index of sibling within same row as index
    [[nodiscard]] QModelIndex rowSibling(ModelType column, const QModelIndex& idx) const {
        return sibling(idx.row(), column, idx);
    }

    // Convenience function: set sibling data within row
    virtual bool setData(ModelType, const QModelIndex&, const QVariant& value,
                         int role = Qt::DisplayRole);

    // Convenience function: get sibling data within row
    [[nodiscard]] virtual QVariant data(ModelType, const QModelIndex&, int role = Qt::DisplayRole) const;

    // Updates
    virtual bool moveRow(const QModelIndex& srcParent, int srcRow,
                         const QModelIndex& dstParent, int dstRow);

    bool moveRows(const QModelIndex &srcParent, int srcRow, int count,
                  const QModelIndex &dstParent, int dstRow) override;

    bool setData(const QModelIndex &idx, const QVariant& value,
                 int role = Qt::DisplayRole) override;

    // Set multiple indexes
    virtual bool multiSet(const QModelIndexList&, const QVariant& value,
                          int role = Qt::DisplayRole);
    virtual bool clearData(const QModelIndex&, int role = Qt::DisplayRole);
    virtual bool clearData(ModelType column, const QModelIndex&, int role = Qt::DisplayRole);
    bool setHeaderData(ModelType section, Qt::Orientation,
                       const QVariant& value, int role = Qt::DisplayRole) override;

    bool insertColumns(int position, int columns,
                       const QModelIndex& parent = QModelIndex()) override;
    bool removeColumns(int position, int columns,
                       const QModelIndex& parent = QModelIndex()) override;
    bool insertRows(int position, int rows,
                    const QModelIndex& parent = QModelIndex()) override;
    bool removeRows(int position, int rows,
                    const QModelIndex& parent = QModelIndex()) override;

    virtual void removeRows(const std::function<bool(const QModelIndex&)>& predicate,
                            const QModelIndex& parent = QModelIndex(), int column = 0);

    virtual bool appendRow(const QModelIndex& parent = QModelIndex());

    virtual bool appendRow(const TreeItem::ItemData&,
                           const QModelIndex& parent = QModelIndex());

    virtual bool appendRows(const QVector<TreeItem::ItemData>&,
                            const QModelIndex& parent = QModelIndex());

    virtual bool insertRow(const TreeModel&, const QModelIndex& srcIdx,
                           int position, const QModelIndex& parent = QModelIndex());

    // Convenience function to set both icon and its name.
    virtual bool setIcon(const QModelIndex&, const QString&);
    virtual bool clearIcon(const QModelIndex&);
    virtual bool setSiblingIcon(int column, const QModelIndex& idx, const QString&);

    void setHorizontalHeaderLabels(const QStringList &labels);
    [[nodiscard]] QStringList headerLabels() const;
    [[nodiscard]] int findHeaderColumn(const QString&) const;

    // Apply some operation to a subtree. Function should return true recurse into subtree.
    virtual void apply(const std::function<bool(const QModelIndex&)>& fn,
                       const QModelIndex& parent = QModelIndex()) const;

    // Drops data into a queue for later update from main render thread
    void setDataFromThread(const QModelIndex&, const QVariant& value,
                           int role = Qt::DisplayRole);

    // Queries
    virtual int count(const std::function<bool(const QModelIndex&)>& predicate,
                      const QModelIndex& parent = QModelIndex()) const;

    [[nodiscard]] virtual QModelIndex findRow(const QModelIndex& parent,
                                              const std::function<bool(const QModelIndex&)>& predicate,
                                              int maxDepth = std::numeric_limits<int>::max()) const;

    [[nodiscard]] virtual QModelIndex findRow(const QModelIndex& parent,
                                              const QVariant& value, int column, int role = Qt::DisplayRole,
                                              int maxDepth = std::numeric_limits<int>::max()) const;

    [[nodiscard]] QModelIndex child(int row, const QModelIndex& parent = QModelIndex()) const;
    [[nodiscard]] QModelIndex lastChild(const QModelIndex& parent = QModelIndex()) const;

    virtual void refresh(); // refresh view from underlying data (e.g, on settings change)

    [[nodiscard]] virtual const Units& units(const QModelIndex&) const;
    [[nodiscard]] const Units& units(ModelType) const;  // default units for column (may be overriden for any given index)

    static void setBrokenIcon(const QIcon&);

    // *** begin Settings API
    void save(QSettings&) const override;
    void load(QSettings&) override;
    // *** end Settings API

    // *** begin Stream Save API
    virtual bool         saveForUndo(QIODevice&, const QModelIndex& parent = QModelIndex(), int first = 0, int count = -1) const;
    virtual bool         loadForUndo(QIODevice&, const QModelIndex& parent, int first);
    virtual QDataStream& save(QDataStream&, const QModelIndex& parent = QModelIndex(), int first = 0, int count = -1) const;
    virtual bool         save(const QString&, const QModelIndex& parent = QModelIndex()) const;
    virtual bool         save(QIODevice&, const QModelIndex& parent = QModelIndex()) const;
    virtual QDataStream& load(QDataStream&, const QModelIndex& parent = QModelIndex(), bool append = false);
    virtual bool         load(const QString&, const QModelIndex& parent = QModelIndex(), bool append = false);
    virtual bool         load(QIODevice&, const QModelIndex& parent = QModelIndex(), bool append = false);
    virtual QDataStream& load(QDataStream&, const QModelIndex& parent, bool append,
                              const std::function<void()>& preHook, const std::function<void()>& postHook);
    virtual quint32      streamMagic() const          { return 0; }
    virtual quint32      streamVersionMin() const     { return 0; }
    virtual quint32      streamVersionCurrent() const { return 0; }
    // *** end Stream Save API

    // Query QMimeData for given model streamMagic
    [[nodiscard]] bool isStreamMagic(const QMimeData*) const;
    // Obtain index list for dropped data
    [[nodiscard]] QVector<QPersistentModelIndex> getDropIndices(const QMimeData*) const;

    // Helper to obtain data as a particular type
    template <typename T> T as(const QModelIndex& idx, ModelType mt, int role = Util::RawDataRole) const {
        return data(mt, idx, role).value<T>();
    }

    // Helper to obtain entire model as a particular type
    template <typename T> T* as() { return dynamic_cast<T*>(this); }
    template <typename T> const T* as() const { return dynamic_cast<const T*>(this); }

    virtual TreeModel& operator=(const TreeModel&);

    // sort items under this parent
    virtual void sort(ModelType,
                      const QModelIndex& parent = QModelIndex(),
                      Qt::SortOrder order = Qt::AscendingOrder,
                      int role = Qt::DisplayRole,
                      bool recursive = true);

    virtual void clear();  // clear entire model contents

    [[nodiscard]] bool undoing() const { return m_undoing; }  // true if the model is save or loading for undo
    [[nodiscard]] bool isOldSaveFormat() const { return m_oldSaveFormat; } // true if the model was loaded from an old save format

    static QIcon brokenIcon;

    static const constexpr char* internalMoveMimeType   = "application/x-tag-model";  // model indices

signals:
    void itemSaved(qint64) const;  // for save progress
    void itemLoaded(qint64) const; // for load progress

protected:
    virtual void saveItem(const QModelIndex&, QDataStream&, const TreeModel&) const;
    virtual void loadItem(const QModelIndex&, QDataStream&, TreeModel&);

    virtual void copyItem(const QModelIndex& dstIdx, const TreeModel& srcModel, const QModelIndex& srcIdx);

    [[nodiscard]] TreeItem* getItem(const QModelIndex& idx) const;

    // For drag and drop
    [[nodiscard]] QStringList mimeTypes() const override;
    [[nodiscard]] QMimeData* mimeData(const QModelIndexList &indexes) const override;
    bool dropMimeData(const QMimeData* data, Qt::DropAction action,
                      int dstRow, int dstCol, const QModelIndex& dstParent) override;

    virtual bool dragMove(const TreeModel& srcModel, const QVector<QPersistentModelIndex>& srcIndexes,
                          int dstRow, int dstCol, const QModelIndex& dstParent);

    virtual bool dragCopy(const TreeModel& srcModel, const QVector<QPersistentModelIndex>& srcIndexes,
                          int dstRow, int dstCol, const QModelIndex& dstParent);

    [[nodiscard]] bool isAncestor(const QModelIndex& ancestor, QModelIndex descendent) const;

    mutable QtCompat::RecursiveMutex m_mutex;

private:
    using QAbstractItemModel::sort; // hide

    TreeItem*    rootItem;
    mutable bool m_undoing;
    mutable bool m_oldSaveFormat;  // true if model was loaded from an old binary save format
};

#endif // TREEMODEL_H
