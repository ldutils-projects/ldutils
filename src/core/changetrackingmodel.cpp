/*
    Copyright 2020-2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "appbase.h"
#include <src/undo/undomgr.h>
#include <src/undo/undomodel.h>
#include "changetrackingmodel.h"

ChangeTrackingModel::ChangeTrackingModel(TreeItem* root, QObject* parent) :
    TreeModel(root, parent),
    UndoableObject(appBase().undoMgr())
{
    ChangeTrackingModel::setupChangeSignals(true);
    appBase().registerModel(*this);
}

ChangeTrackingModel::~ChangeTrackingModel()
{
    appBase().unregisterModel(*this);
}

void ChangeTrackingModel::setupChangeSignals(bool install)
{
    // TODO: row/col moved
    if (install) {
        connect(this, &ChangeTrackingModel::rowsAboutToBeInserted, this, &ChangeTrackingModel::preInsertRows, Qt::UniqueConnection);
//      connect(this, &ChangeTrackingModel::rowsMoved,       this, &ChangeTrackingModel::modelModified, Qt::UniqueConnection);
        connect(this, &ChangeTrackingModel::rowsAboutToBeRemoved, this, &ChangeTrackingModel::preRemoveRows, Qt::UniqueConnection);
        connect(this, &ChangeTrackingModel::columnsAboutToBeInserted, this, &ChangeTrackingModel::preInsertCols, Qt::UniqueConnection);
        connect(this, &ChangeTrackingModel::columnsMoved,    this, &ChangeTrackingModel::modelModified, Qt::UniqueConnection);
//      connect(this, &ChangeTrackingModel::columnsAboutToBeRemoved, this, &ChangeTrackingModel::preRemoveCols, Qt::UniqueConnection);
        connect(this, &ChangeTrackingModel::dataAboutToBeChanged, this, &ChangeTrackingModel::preDataChanged, Qt::UniqueConnection);
    } else {
        disconnect(this, &ChangeTrackingModel::rowsAboutToBeInserted, this, &ChangeTrackingModel::preInsertRows);
//      disconnect(this, &ChangeTrackingModel::rowsMoved,       this, &ChangeTrackingModel::modelModified);
        disconnect(this, &ChangeTrackingModel::rowsAboutToBeRemoved, this, &ChangeTrackingModel::preRemoveRows);
        disconnect(this, &ChangeTrackingModel::columnsAboutToBeInserted, this, &ChangeTrackingModel::preInsertCols);
        disconnect(this, &ChangeTrackingModel::columnsMoved,    this, &ChangeTrackingModel::modelModified);
//      disconnect(this, &ChangeTrackingModel::columnsAboutToBeRemoved, this, &ChangeTrackingModel::preRemoveCols);
        disconnect(this, &ChangeTrackingModel::dataAboutToBeChanged, this, &ChangeTrackingModel::preDataChanged);
    }
}

void ChangeTrackingModel::emitAboutToChange(const QModelIndex& idx, const QVariant& value, int role)
{
    if (data(idx, role) != value) // don't emit if changing to same value
        emit dataAboutToBeChanged(idx, value, role);
}

bool ChangeTrackingModel::dragMove(const TreeModel& srcModel, const QVector<QPersistentModelIndex>& srcIndexes,
                                   int dstRow, int dstCol, const QModelIndex& dstParent)
{
    const UndoMgr::ScopedUndo undoSet(undoMgr(), tr("Reorder item"));
    return TreeModel::dragMove(srcModel, srcIndexes, dstRow, dstCol, dstParent);
}

bool ChangeTrackingModel::setData(const QModelIndex& idx, const QVariant& value, int role)
{
    emitAboutToChange(idx, value, role);
    return TreeModel::setData(idx, value, role);
}

// Only used to create a nicely name undo set
bool ChangeTrackingModel::clearIcon(const QModelIndex& idx)
{
    const UndoMgr::ScopedUndo undoSet(undoMgr(), tr("Clear Icon"));
    return TreeModel::clearIcon(idx);
}

// Only used to create a nicely name undo set
bool ChangeTrackingModel::setIcon(const QModelIndex& idx, const QString& iconFile)
{
    const UndoMgr::ScopedUndo undoSet(undoMgr(), tr("Set Icon"));
    return TreeModel::setIcon(idx, iconFile);
}

// Block our signals during loads
void ChangeTrackingModel::load(QSettings& settings)
{
    SignalBlocker block(*this);
    return TreeModel::load(settings);
}

// Block our signals during loads
QDataStream& ChangeTrackingModel::load(QDataStream& stream, const QModelIndex& parent, bool append)
{
    SignalBlocker block(*this);
    return TreeModel::load(stream, parent, append);
}

void ChangeTrackingModel::emitDirtyStateChanged(bool dirty)
{
    emit dirtyStateChanged(dirty);
}

void ChangeTrackingModel::preInsertRows(const QModelIndex& idx, int start, int end)
{
    const QString undoName = UndoMgr::genName(tr("Insert"), end - start + 1, "Row", "Rows");
    const UndoMgr::ScopedUndo undoSet(undoMgr(), undoName);
    undoMgr().add(new UndoModelInsert(*this, UndoModelInsert::Mode::Row, idx, start, end));
    setDirty(true);
}

void ChangeTrackingModel::preRemoveRows(const QModelIndex& idx, int start, int end)
{
    const QString undoName = UndoMgr::genName(tr("Remove"), end - start + 1, "Row", "Rows");
    const UndoMgr::ScopedUndo undoSet(undoMgr(), undoName);
    undoMgr().add(new UndoModelRemove(*this, UndoModelInsert::Mode::Row, idx, start, end));
    setDirty(true);
}

void ChangeTrackingModel::preInsertCols(const QModelIndex& idx, int start, int end)
{
    const QString undoName = UndoMgr::genName(tr("Insert"), end - start + 1, "Column", "Columns");
    const UndoMgr::ScopedUndo undoSet(undoMgr(), undoName);
    undoMgr().add(new UndoModelInsert(*this, UndoModelInsert::Mode::Col, idx, start, end));
    setDirty(true);
}

void ChangeTrackingModel::preRemoveCols(const QModelIndex& idx, int start, int end)
{
    const QString undoName = UndoMgr::genName(tr("Remove"), end - start + 1, "Column", "Columns");
    const UndoMgr::ScopedUndo undoSet(undoMgr(), undoName);
    undoMgr().add(new UndoModelRemove(*this, UndoModelInsert::Mode::Col, idx, start, end));
    setDirty(true);
}

void ChangeTrackingModel::preDataChanged(const QModelIndex& idx, const QVariant& value, int role)
{
    const UndoMgr::ScopedUndo undoSet(undoMgr(), tr("Set ") + headerData(idx.column(), Qt::Horizontal).toString());
    undoMgr().add(new UndoModelSetData(*this, idx, value, role));
    setDirty(true);
}

void ChangeTrackingModel::preUndoHook(const QModelIndex& idx)
{
    return preUndoHook(idx.parent(), idx.row(), idx.row());
}

void ChangeTrackingModel::postUndoHook(const QModelIndex& idx)
{
    return postUndoHook(idx.parent(), idx.row(), idx.row());
}
