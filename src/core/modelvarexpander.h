/*
    Copyright 2021 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef MODELVAREXPANDER_H
#define MODELVAREXPANDER_H

#include <QHash>

#include <src/core/modelmetadata.h>

class TreeModel;
class QModelIndex;

class ModelVarExpander
{
public:
    ModelVarExpander(bool fgColorize = true, bool bgColorize = true);

    // Expand ${var} style variables in the provided HTML string to the values from the given index.
    [[nodiscard]] virtual QString expandHtml(const QString& srcHtml, const QModelIndex&) const;

private:
    void setupCache(const TreeModel*) const;

    mutable QHash<QString, ModelType> m_nameCache;    // cache name to model type, for perf
    mutable const TreeModel*          m_model;        // the last model we were used with
    bool                              m_fgColorize;   // colorize with Qt::ForegroundRole
    bool                              m_bgColorize;   // colorize with Qt::BackgroundRole
};

#endif // MODELVAREXPANDER_H
