/*
    Copyright 2019-2022 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <QMessageBox>

#include <src/core/uicolormodel.h>
#include <src/util/util.h>
#include <src/util/resources.h>
#include <src/util/ui.h>
#include <src/util/icons.h>

#include "cfgdatabase.h"

CfgDataBase::CfgDataBase(uint32_t version) :
    colSeparator("\\t"),
    rowSeparator("\\n"),
    caseSensitiveFilters(false),
    caseSensitiveSorting(false),
    warnOnClose(true),
    warnOnRemove(true),
    warnOnRevert(true),
    warnOnExit(false),
    inlineCompletion(false),
    completionListSize(7),
    brokenIcon(Util::LocalThemeIcon("status/22/script-error.svg")),
    filterValidIconName(Util::LocalThemeIcon("emblems/8/emblem-success.svg")),
    filterInvalidIconName(Util::LocalThemeIcon("emblems/8/emblem-error.svg")),
    filterEmptyIconName(Util::LocalThemeIcon("emblems/8/emblem-unavailable.svg")),
    backupDataCount(2),
    dataAutosaveInterval(5),
    dataAutosavePath(""),
    backupUICount(2),
    maxUndoCount(25),
    maxUndoSizeMiB(16.0f),
    panePreviewHeight(120),
    cfgDataVersion(version),
    priorCfgDataVersion(0)
{
    CfgDataBase::applyGlobal();
}

CfgDataBase::~CfgDataBase()
{
}

void CfgDataBase::applyGlobal()
{
    QIcon bi(brokenIcon);
    if (bi.actualSize(QSize(24, 24)).width() <= 2)
        bi = Icons::get("emblem-error");

    filterValidIcon   = QIcon(filterValidIconName);
    filterInvalidIcon = QIcon(filterInvalidIconName);
    filterEmptyIcon   = QIcon(filterEmptyIconName);

    TreeModel::setBrokenIcon(bi);
}

void CfgDataBase::save(QSettings& settings) const
{
    MemberSave(settings, colSeparator);
    MemberSave(settings, rowSeparator);

    MemberSave(settings, caseSensitiveFilters);
    MemberSave(settings, caseSensitiveSorting);
    MemberSave(settings, warnOnClose);
    MemberSave(settings, warnOnRemove);
    MemberSave(settings, warnOnRevert);
    MemberSave(settings, warnOnExit);
    MemberSave(settings, inlineCompletion);
    MemberSave(settings, completionListSize);

    MemberSave(settings, brokenIcon);
    MemberSave(settings, filterEmptyIconName);
    MemberSave(settings, filterValidIconName);
    MemberSave(settings, filterInvalidIconName);

    MemberSave(settings, uiColor);

    MemberSave(settings, backupDataCount);
    MemberSave(settings, dataAutosaveInterval);
    MemberSave(settings, dataAutosavePath);

    MemberSave(settings, backupUICount);
    MemberSave(settings, maxUndoCount);
    MemberSave(settings, maxUndoSizeMiB);

    MemberSave(settings, panePreviewHeight);

    MemberSave(settings, cfgDataVersion);
}

void CfgDataBase::load(QSettings& settings)
{
    if (settings.contains("cfgDataVersion")) {
        const decltype(cfgDataVersion) initVersion = cfgDataVersion;
        MemberLoad(settings, cfgDataVersion);
        if (cfgDataVersion > initVersion) {
            QMessageBox(QMessageBox::Critical, QObject::tr("Version error"),
                        QObject::tr("Unable to downgrade from a newer save format."), QMessageBox::Abort).exec();
            throw Exit(5);
        }

    } else {
        cfgDataVersion = 0;
    }

    MemberLoad(settings, colSeparator);
    MemberLoad(settings, rowSeparator);

    MemberLoad(settings, caseSensitiveFilters);
    MemberLoad(settings, caseSensitiveSorting);
    MemberLoad(settings, warnOnClose);
    MemberLoad(settings, warnOnRemove);
    MemberLoad(settings, warnOnRevert);
    MemberLoad(settings, warnOnExit);
    MemberLoad(settings, inlineCompletion);
    MemberLoad(settings, completionListSize);

    MemberLoad(settings, brokenIcon);
    MemberLoad(settings, filterEmptyIconName);
    MemberLoad(settings, filterValidIconName);
    MemberLoad(settings, filterInvalidIconName);

    MemberLoad(settings, uiColor);

    MemberLoad(settings, backupDataCount);
    MemberLoad(settings, dataAutosaveInterval);
    MemberLoad(settings, dataAutosavePath);

    MemberLoad(settings, backupUICount);
    MemberLoad(settings, maxUndoCount);
    MemberLoad(settings, maxUndoSizeMiB);

    MemberLoad(settings, panePreviewHeight);

    applyGlobal();
}
