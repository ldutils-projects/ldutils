/*
    Copyright 2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef REMOVABLEMODEL_H
#define REMOVABLEMODEL_H

#include <QModelIndexList>

#include <src/util/nameditem.h>

// Model supporting removal of entries.
class RemovableModel :
        virtual public NamedItemInterface
{
public:
    RemovableModel();

    // Duplicate given selections. Default method adds to end.
    virtual void remove(const QModelIndexList&);
};

#endif // REMOVABLEMODEL_H
