/*
    Copyright 2021-2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <cassert>
#include <QPixmap>
#include <QStandardPaths>
#include <QDir>
#include <QPainter>
#include <QPalette>
#include <QPen>
#include <QApplication>
#include <QVariant>
#include <QIcon>

#include <src/util/ui.h>
#include <src/util/icons.h>
#include <src/core/appbase.h>

#include "newpanemodel.h"
#include "newpaneitem.h"

NewPaneItem::NewPaneItem(const QString& name, const QString& image, const QString& icon, const QString& tooltip) :
    m_name(name), m_icon(icon), m_image(image), m_tooltip(tooltip)
{
}

QPixmap NewPaneItem::getPreviewImage() const
{
    if (m_image.isEmpty())
        return { };

    const QString docRelDir = Util::IsLightTheme() ? "docs/light" : "docs/dark";
    const QString docRelFile = docRelDir + QDir::separator() + m_image;

    const QString previewFile = QStandardPaths::locate(QStandardPaths::AppDataLocation, docRelFile, QStandardPaths::LocateFile);
    if (previewFile.isEmpty()) {
        assert(0 && "Pane preview not found");
        return { };
    }

    const QPixmap preview(previewFile);

    const float scale = float(std::clamp(cfgDataBase().panePreviewHeight / 320.0, 0.2, 1.0));
    const QRect clipRect(0, 0, cfgDataBase().panePreviewHeight * 2, cfgDataBase().panePreviewHeight);

    // Rescale and clip
    QPixmap rescaled = preview
            .scaled(preview.size() * scale, Qt::IgnoreAspectRatio, Qt::SmoothTransformation)
            .copy(clipRect);

    // Draw a nice outline box around its
    QPainter painter(&rescaled);
    QPen     outline(QApplication::palette().color(QPalette::Link), 2);

    painter.setPen(outline);
    painter.drawRect(rescaled.rect().adjusted(1,1,-1,-1));

    return rescaled;
}

QVariant NewPaneItem::data(ModelType mt, int role) const
{
    if (role == Qt::DecorationRole) {
        switch (mt) {
        case NewPaneModel::Icon:  return Icons::get(m_icon);
        case NewPaneModel::Image: return getPreviewImage();
        default: break;
        }
    }

    if (role == Qt::DisplayRole || role == Util::RawDataRole || role == Util::CopyRole) {
        switch (mt) {
        case NewPaneModel::Name:  return m_name;
        default: break;
        }
    }

    if (role == Qt::ToolTipRole && mt == NewPaneModel::Name) {
        return m_tooltip;
    }

    return { };
}
