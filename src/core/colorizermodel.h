/*
    Copyright 2019-2023 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef COLORIZERMODEL_H
#define COLORIZERMODEL_H

#include <QList>
#include <QVector>
#include <QModelIndex>

#include <src/core/modelmetadata.h>
#include <src/core/treemodel.h>
#include <src/core/colorizeritem.h>
#include <src/core/query.h>

class ColorizerItem;

// Data used to colorize text entries in other model views.
class ColorizerModel final : public TreeModel, public ModelMetaData
{
public:
    enum {
        Column = 0,
        Query,
        FgColor,
        BgColor,
        Icon,
        MatchCase,
        HideText,
        _Count,
    };

    ColorizerModel(const TreeModel* colorizeModel, QObject *parent = nullptr);

    void setModel(const TreeModel* colorizeModel);
    const TreeModel* model() { return m_colorizeModel; }
    const TreeModel* model() const { return m_colorizeModel; }

    // *** begin QAbstractItemModel API
    QVariant data(const QModelIndex& idx, int role = Qt::DisplayRole) const override;
    bool setData(const QModelIndex& idx, const QVariant& value,
                 int role = Qt::DisplayRole) override;
    bool setHeaderData(int section, Qt::Orientation orientation,
                       const QVariant& value, int role = Qt::DisplayRole) override;
    QVariant headerData(int section, Qt::Orientation orientation,
                        int role = Qt::DisplayRole) const override;
    bool insertColumns(int position, int columns,
                       const QModelIndex& parent = QModelIndex()) override;
    bool removeColumns(int position, int columns,
                       const QModelIndex& parent = QModelIndex()) override;
    int columnCount(const QModelIndex& /*parent*/ = QModelIndex()) const override { return _Count; }
    Qt::ItemFlags flags(const QModelIndex&) const override;
    bool insertRows(int position, int rows,
                    const QModelIndex& parent = QModelIndex()) override;
    using TreeModel::removeRows;
    bool removeRows(int position, int rows,
                    const QModelIndex& parent = QModelIndex()) override;
    using QAbstractItemModel::insertRow;
    Qt::DropActions supportedDropActions() const override { return Qt::MoveAction; }
    // *** end QAbstractItemModel API

    using TreeModel::data;
    using TreeModel::setData;
    using TreeModel::clearData;
    void clearData(const QItemSelectionModel*, ModelType);

    // *** begin ModelMetaData API
    static QString        mdName(ModelType);
    static bool           mdIsEditable(ModelType);
    static bool           mdIsChartable(ModelType);
    static QString        mdTooltip(ModelType);
    static QString        mdWhatsthis(ModelType);
    static Qt::Alignment  mdAlignment(ModelType);
    static int            mdDataRole(ModelType);
    static const Units&   mdUnits(ModelType);
    // *** end ModelMetaData API

    // *** begin Settings API
    using TreeModel::save;
    void load(QSettings&) override;
    // *** end Settings API
    virtual ColorizerModel& operator=(const TreeModel&) override;
    virtual ColorizerModel& operator=(const ColorizerModel&);

    template <class T> Query::Context& queryCtx(const T&);

    // Search for colorize data (FG color, BG color, icon) for the given idx in the
    // m_colorizeModel, and return data if found.  Caches hits for performance.
    QVariant colorize(const QModelIndex&, int role) const;

    // Colorize based on a value isntead of a model and index.  No caching!
    QVariant colorize(ModelType, const QVariant&, int role) const;

    const ColorizerItem* find(const QVariant& data, ModelType mt) const;
    const ColorizerItem* find(const QModelIndex& idx) const;

    // Possibly remove text.  Designed to be a fast passthrough if that isn't requested.
    const QVariant& maybeUse(const QVariant&, const QModelIndex& idx, int role) const;

    const TreeModel* colorizedModel() const { return m_colorizeModel; }

    const ColorizerItem* getItem(const QModelIndex &idx) const;

private:
    static inline QVariant dataForRole(const ColorizerItem*, int role);

    void copyItem(const QModelIndex& dstIdx, const TreeModel& srcModel, const QModelIndex& srcIdx) override;

    // Find first applicable ColorizerItem matching value at idx.  Cache result for performance.
    inline const ColorizerItem* firstMatch(const QModelIndex&) const;
    // Find first applicable ColorizerItem matching value.  Non-cached, unlike the above.
    inline const ColorizerItem* firstMatch(ModelType, const QVariant& value) const;

    ColorizerItem* getItem(const QModelIndex &idx);

    void updateCache(bool force = false) const;

    const TreeModel*                               m_colorizeModel; // model we will colorize for
    Query::Context                                 m_queryCtx;      // for query language

    // Items below are mutable because they're performance caches, and don't change the
    // external functional behavior.

    // Per-source-model-column Cache, for performance
    mutable bool                                   m_dirty;
    mutable QVector<QVector<const ColorizerItem*>> m_cache; // per-column cache
    mutable QVector<bool>                          m_canHideText;

    // Performance: we get called a lot with different roles on the same index.
    mutable QModelIndex                            m_lastMatchIdx;
    mutable const ColorizerItem*                   m_lastMatchItem;
};

#endif // COLORIZERMODEL_H
