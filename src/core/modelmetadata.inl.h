/*
    Copyright 2019-2022 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef MODELMETADATA_INL_H
#define MODELMETADATA_INL_H

#include <QComboBox>
#include <QStandardItemModel>
#include <QStandardItem>
#include <QAbstractItemView>

#include <src/ui/widgets/checklistitemdelegate.h>

#include "src/util/roles.h"
#include "modelmetadata.h"

template <class MD>
QStringList ModelMetaData::headersList()
{
    QStringList headers;

    for (int md = MD::_First; md < MD::_Count; ++md)
        headers << MD::mdName(md);

    return headers;
}

template <class MD>
QVariant ModelMetaData::headerData(ModelType section, Qt::Orientation orientation, int role)
{
    if (orientation == Qt::Horizontal) {
        // Text alignment:
        if (role == Qt::TextAlignmentRole)
            return QVariant(MD::mdAlignment(section));

        // Handle column header tooltips.
        if (role == Qt::ToolTipRole)
            return MD::mdTooltip(section);

        if (role == Qt::WhatsThisRole)
            return MD::mdWhatsthis(section);
    }

    return { };
}

template <class MD>
Qt::ItemFlags ModelMetaData::flags(const QModelIndex& idx)
{
    return MD::mdIsEditable(idx.column()) ? Qt::ItemIsEditable : Qt::NoItemFlags;
}

// Set up a combo box & model to display TrackData entries
template <class MD>
void ModelMetaData::setupComboBox(QComboBox& comboBox, QStandardItemModel& comboModel,
                                  QVector<ModelType>* entries,
                                  const decltype(mdIdentityItem)& setupItem,
                                  const decltype(mdAcceptAll)& pred)
{
    if (entries != nullptr)
        entries->reserve(int(MD::_Count));

    for (ModelType td = MD::_First; td < MD::_Count; td++) {
        if (!pred(td) || MD::mdIgnore(td))
            continue;

        if (entries != nullptr)
            entries->push_back(td);

        auto* item = new QStandardItem(MD::mdName(td));

        item->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable);
        item->setData(MD::mdTooltip(td), Qt::ToolTipRole);
        item->setData(MD::mdWhatsthis(td), Qt::WhatsThisRole);
        item->setData(td, Util::ModelTypeRole);

        comboModel.appendRow(setupItem(td, item));
    }

    comboBox.setModel(&comboModel);
    comboBox.setItemDelegate(new ChecklistItemDelegate(&comboBox));
    comboBox.setMaxVisibleItems(15);

    // Ensurce popup is sized to the maximum item width.
    const int minWidth = comboBox.minimumSizeHint().width() + 25; // TODO: kludge to account for checkbox
    comboBox.view()->setMinimumWidth(minWidth);
}

#endif // MODELMETADATA_INL_H
