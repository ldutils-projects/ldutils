/*
    Copyright 2020-2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <QBuffer>
#include <QByteArray>

#include <src/undo/undomgr.h>
#include <src/undo/undomodel.h>
#include <src/core/changetrackingmodel.h>
#include <src/core/appbase.h>

UndoModel::UndoModel(ChangeTrackingModel& model) :
    m_modelId(appBase().persistentIdForModel(model))
{
}

ChangeTrackingModel* UndoModel::findModel() const
{
    return appBase().modelForPersistentId(m_modelId);
}

void UndoModel::saveData(QByteArray& dataZ, const QModelIndex& parent,
                         int start, int end) const
{
    if (!dataZ.isEmpty())
        return;

    // We must save the removed data for restoration.
    ChangeTrackingModel* model = findModel();
    if (model == nullptr)
        return;

    QByteArray rawData;
    QBuffer    rawBuffer(&rawData);

    model->saveForUndo(rawBuffer, parent, start, end - start + 1);

    // store compressed form for space efficiency
    dataZ = qCompress(rawData, 9);
}

void UndoModel::restoreData(const QByteArray& dataZ, const QModelIndex& parent, int start) const
{
    ChangeTrackingModel* model = findModel();
    if (model == nullptr)
        return;

    QByteArray rawData = qUncompress(dataZ);
    QBuffer rawBuffer(&rawData);

    model->loadForUndo(rawBuffer, parent, start);
}

void UndoModel::preUndoSet(QVariantMap& userData) const
{
    if (ChangeTrackingModel* model = findModel(); model != nullptr)
        model->preUndoSet(userData);
}

void UndoModel::postUndoSet(QVariantMap& userData) const
{
    if (ChangeTrackingModel* model = findModel(); model != nullptr)
        model->postUndoSet(userData);
}

UndoModelInsDel::UndoModelInsDel(ChangeTrackingModel& model, Mode mode,
                                 const QModelIndex& parent, int start, int end) :
    UndoModel(model), m_parent(Util::SaveIndex(parent)), m_start(start), m_end(end), m_mode(mode)
{
}

bool UndoModelInsDel::remove() const
{
    const RunHooks hooks(*this, m_parent, m_start, m_end);

    if (!hooks.hasModel())
        return false;

    saveData(hooks.parent());

    if (m_mode == Mode::Row)
        return hooks.model()->removeRows(m_start, m_end - m_start + 1, hooks.parent());

    if (m_mode == Mode::Col)
        return hooks.model()->removeColumns(m_start, m_end - m_start + 1, hooks.parent());

    return false;
}

bool UndoModelInsDel::insert() const
{
    const RunHooks hooks(*this, m_parent, m_start, m_end);

    if (!hooks.hasModel())
        return false;

    bool rc;
    if (m_mode == Mode::Row)
        rc = hooks.model()->insertRows(m_start, m_end - m_start + 1, hooks.parent());
    else
        rc = hooks.model()->insertColumns(m_start, m_end - m_start + 1, hooks.parent());

    if (rc)
        restoreData(hooks.parent());

    return rc;
}

void UndoModelInsDel::saveData(const QModelIndex& parent) const
{
    saveData(m_savedDataZ, parent, m_start, m_end);
}

void UndoModelInsDel::restoreData(const QModelIndex& parent) const
{
    restoreData(m_savedDataZ, parent, m_start);
}

size_t UndoModelInsDel::size() const
{
    return sizeof(*this) + size(m_savedDataZ) + size(m_parent);
}

UndoModelInsert::UndoModelInsert(ChangeTrackingModel& model, Mode mode, const QModelIndex& parent, int start, int end) :
    UndoModelInsDel(model, mode, parent, start, end)
{
}

UndoModelRemove::UndoModelRemove(ChangeTrackingModel& model, Mode mode, const QModelIndex& parent, int start, int end) :
    UndoModelInsDel(model, mode, parent, start, end)
{
    saveData(parent);
}

UndoModelSetData::UndoModelSetData(ChangeTrackingModel& model, const QModelIndex& index, const QVariant& value, int role) :
    UndoModel(model),
    m_index(Util::SaveIndex(index)),
    m_before(model.data(index, role)),
    m_after(value),
    m_role(role)
{
}

bool UndoModelSetData::apply(const QVariant& data) const
{
    const RunHooks hooks(*this, m_index);

    if (hooks.hasModel() && hooks.index().isValid())
        return hooks.model()->setData(hooks.index(), data, m_role);

    return false;
}

size_t UndoModelSetData::size() const
{
    return sizeof(*this) + size(m_index) + size(m_before) + size(m_after);
}

UndoModelData::UndoModelData(ChangeTrackingModel& model, const QModelIndex& parent,
                             int start, int end) :
    UndoModel(model), m_parent(Util::SaveIndex(parent)),
    m_start(start), m_end(end)
{
    // Save current data to apply on undo
    saveData(m_before, parent, m_start, m_end);
}

bool UndoModelData::apply(const QByteArray& dataZ) const
{
    const RunHooks hooks(*this, m_parent, m_start, m_end);

    if (!hooks.hasModel())
        return false;

    // if we didn't already, save post-change data for redo
    saveData(m_after, hooks.parent(), m_start, m_end);
    restoreData(dataZ, hooks.parent(), m_start);

    return true;
}

size_t UndoModelData::size() const
{
    return sizeof(*this) + size(m_before) + size(m_after);
}

UndoModel::RunHooks::RunHooks(ChangeTrackingModel* model, const QModelIndex& parent, int begin, int end) :
    m_block(model), m_model(model), m_parent(parent), m_begin(begin), m_end(end)
{
    if (m_model != nullptr)
        m_model->preUndoHook(m_parent, m_begin, m_end);
}

UndoModel::RunHooks::RunHooks(ChangeTrackingModel* model, const QModelIndex& idx) :
    RunHooks(model, idx.parent(), idx.row(), idx.row())
{
    m_idx = idx;
}

UndoModel::RunHooks::RunHooks(const UndoModel& dm, const Util::SavableIndex& si) :
    RunHooks(dm.findModel(), Util::RestoreIndex(dm.findModel(), si))
{
}

UndoModel::RunHooks::RunHooks(const UndoModel& dm, const Util::SavableIndex& si, int start, int end) :
    RunHooks(dm.findModel(), Util::RestoreIndex(dm.findModel(), si), start, end)
{
}

UndoModel::RunHooks::~RunHooks()
{
    if (m_model != nullptr)
        m_model->postUndoHook(m_parent, m_begin, m_end);
}
