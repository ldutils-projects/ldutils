/*
    Copyright 2020-2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef UNDOBASE_H
#define UNDOBASE_H

#include <QVector>
#include <QVariantMap>
#include <cstddef>
#include <functional>

class QSettings;
class QString;
class QStringList;
class QByteArray;

// Base class for things which can be undone.
class UndoBase {
public:
    virtual ~UndoBase();
    virtual bool undo() const = 0;     // undo this change
    virtual bool redo() const = 0;     // redo this change

    // Hooks run before and after the execution of undo sets. The variant map
    // can be used for userdata to persist across calls.
    virtual void preUndoSet(QVariantMap&) const { }
    virtual void postUndoSet(QVariantMap&) const { }

    [[nodiscard]] virtual size_t size() const = 0;  // query total size in bytes for this change
    [[nodiscard]] virtual const char* className() const = 0;

protected:
    // Estimate byte sizeo of various things.
    [[nodiscard]] static size_t size(const QVariant&);
    [[nodiscard]] static size_t size(const QString&);
    [[nodiscard]] static size_t size(const QStringList&);
    [[nodiscard]] static size_t size(const QByteArray&);
    template <typename T> [[nodiscard]] static size_t size(const QVector<T>&);

    // Turn settings into compressed byte array
    [[nodiscard]] static QByteArray readCfgZ(const std::function<void(QSettings&)>&);  // Return window config as a byte array
    // Apply settings from compressed byte array
    [[nodiscard]] static bool applyCfgZ(const QByteArray& stateZ, const std::function<void(QSettings&)>&);
};

template <typename T> size_t UndoBase::size(const QVector<T>& v)
{
    return sizeof(v) + size_t(v.size()) * sizeof(T);
}

#endif // UNDOBASE_H
