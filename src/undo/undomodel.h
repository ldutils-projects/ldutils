/*
    Copyright 2020-2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef UNDOMODEL_H
#define UNDOMODEL_H

#include <QModelIndex>
#include <QVariantMap>

#include <src/core/changetrackingmodel.h>
#include <src/util/util.h>
#include <src/fwddeclbase.h>

#include "undobase.h"

// Base class for all model undos
class UndoModel : public UndoBase
{
protected:
    UndoModel(ChangeTrackingModel&);

    [[nodiscard]] ChangeTrackingModel* findModel() const;
    template <class M> [[nodiscard]] M* findModel() const;

    ModelId_t m_modelId;

    // Save compressed data to byte arrays
    void saveData(QByteArray&, const QModelIndex& parent, int start, int end) const;
    void restoreData(const QByteArray&, const QModelIndex& parent, int start) const;

    // Methods invoked after and before the execution of undo sets.
    void preUndoSet(QVariantMap&) const override;
    void postUndoSet(QVariantMap&) const override;

    // Run pre and post undo hooks
    class RunHooks {
    public:
        RunHooks(ChangeTrackingModel*, const QModelIndex& parent, int begin, int end);
        RunHooks(ChangeTrackingModel*, const QModelIndex& idx);
        RunHooks(const UndoModel&, const Util::SavableIndex&);
        RunHooks(const UndoModel&, const Util::SavableIndex&, int start, int end);
        ~RunHooks();

        template <class T = ChangeTrackingModel> [[nodiscard]] T* model() const { return dynamic_cast<T*>(m_model); }
        QModelIndex index() const { return m_idx; }
        QModelIndex parent() const { return m_parent; }
        bool hasModel() const { return m_model != nullptr; }

    private:
        ChangeTrackingModel::SignalBlocker m_block;
        ChangeTrackingModel*               m_model = nullptr;
        QModelIndex                        m_parent;
        QModelIndex                        m_idx;
        int                                m_begin = -1;
        int                                m_end   = -1;
    };
};

template <class M> M* UndoModel::findModel() const
{
    return dynamic_cast<M*>(findModel());
}

// Common baseclass for insertion/deletion of rows
class UndoModelInsDel : public UndoModel
{
public:
    enum class Mode { Row, Col };

    UndoModelInsDel(ChangeTrackingModel&, Mode mode, const QModelIndex& parent, int start, int end);

protected:
    bool insert() const;
    bool remove() const;
    using UndoModel::saveData;
    using UndoModel::restoreData;
    void saveData(const QModelIndex& parent) const;
    void restoreData(const QModelIndex& parent) const;

    using UndoModel::size;
    size_t size() const override;

private:
    Util::SavableIndex m_parent;
    int                m_start;
    int                m_end;
    Mode               m_mode;
    mutable QByteArray m_savedDataZ; // compressed pre-removal data
};

// Undo for model row removal: adapt insertion and swap undo/redo.
class UndoModelRemove final : public UndoModelInsDel
{
public:
    UndoModelRemove(ChangeTrackingModel&, Mode mode, const QModelIndex& parent, int start, int end);

protected:
    // Removal is almost the reverse of insertion.
    bool undo() const override { return insert(); }
    bool redo() const override { return remove(); }
    const char* className() const override { return "UndoModelRemove"; }
};

// Undo for model row removal: adapt insertion and swap undo/redo.
class UndoModelInsert final : public UndoModelInsDel
{
public:
    UndoModelInsert(ChangeTrackingModel&, Mode mode, const QModelIndex& parent, int start, int end);

protected:
    // Insertion is almost the reverse of removal.
    bool undo() const override { return remove(); }
    bool redo() const override { return insert(); }
    const char* className() const override { return "UndoModelInsert"; }
};

// Undo for changing a given model index
class UndoModelSetData final : public UndoModel
{
public:
    UndoModelSetData(ChangeTrackingModel&, const QModelIndex&, const QVariant& value, int role);

protected:
    bool apply(const QVariant&) const;

    bool undo() const override { return apply(m_before); }
    bool redo() const override { return apply(m_after); }
    size_t size() const override;
    const char* className() const override { return "UndoModelSetData"; }

    using UndoModel::size;

private:
    Util::SavableIndex m_index;
    QVariant           m_before;
    QVariant           m_after;
    int                m_role;
};

// Undo that saves and restores model data for extra-model-API changes.
class UndoModelData final : public UndoModel
{
public:
    UndoModelData(ChangeTrackingModel&, const QModelIndex&, int start, int end);

protected:
    bool apply(const QByteArray&) const;

    bool undo() const override { return apply(m_before); }
    bool redo() const override { return apply(m_after); }

    using UndoModel::size;
    size_t size() const override;
    const char* className() const override { return "UndoModelData"; }

private:
    Util::SavableIndex m_parent; // parent of all
    int                m_start;
    int                m_end;
    mutable QByteArray m_before; // compressed byte array of model data
    mutable QByteArray m_after;  // ...
};

#endif // UNDOMODEL_H
