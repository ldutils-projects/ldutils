/*
    Copyright 2020-2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <cassert>

#include "undomgr.h"
#include "undoableobject.h"

UndoableObject::UndoableObject(UndoMgr& undoMgr) :
    m_undoMgr(undoMgr),
    m_nestCount(0),
    m_dirty(false)
{
    m_undoMgr.registerUndoableObject(this);
}

UndoableObject::~UndoableObject()
{
    m_undoMgr.unregisterUndoableObject(this);
}

void UndoableObject::setDirty(bool dirty, bool signal)
{
    const bool wasDirty = m_dirty;
    m_dirty = dirty;

    if (wasDirty != m_dirty && signal)
        emitDirtyStateChanged(m_dirty);
}

void UndoableObject::incDontTrack()
{
    if (m_nestCount++ == 0)
        setupChangeSignals(false);
}

void UndoableObject::decDontTrack()
{
    assert(m_nestCount > 0);

    if (--m_nestCount == 0)
        setupChangeSignals(true);
}

UndoableObject::SignalBlocker::SignalBlocker(UndoableObject& object) :
    SignalBlocker(&object)
{
}

UndoableObject::SignalBlocker::SignalBlocker(UndoableObject* object) :
    m_object(object)
{
    if (m_object != nullptr)
        m_object->incDontTrack();
}

UndoableObject::SignalBlocker::~SignalBlocker()
{
    if (m_object != nullptr)
        m_object->decDontTrack();
}
