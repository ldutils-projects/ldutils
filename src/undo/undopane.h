/*
    Copyright 2020 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef UNDOPANE_H
#define UNDOPANE_H

#include <QByteArray>

#include "src/fwddeclbase.h"
#include "undomgr.h"
#include "undobase.h"

class MainWindowBase;
class PaneBase;
class DataColumnPaneBase;

// Base class for pane related undos
class UndoPaneBase : public UndoBase
{
protected:
    UndoPaneBase(PaneBase&);
    ~UndoPaneBase() override; // just to get vtable into cpp

    template <typename T> [[nodiscard]] T* findPane() const;

    MainWindowBase& m_mainWindow;
    PaneId_t        m_paneId;
};

// Save compressed serialized version of pane state for undoing
class UndoPaneState final : public UndoPaneBase
{
public:
    UndoPaneState(PaneBase& pane, const QByteArray& before, const QByteArray& after) :
        UndoPaneBase(pane), m_beforeZ(before), m_afterZ(after)
    { }

    // Our own version of a scoped undo
    class ScopedUndo : public UndoMgr::ScopedUndo {
    public:
        ScopedUndo(PaneBase& pane, const QString& name);

        ~ScopedUndo();
    private:
        PaneBase&       m_pane;
        QByteArray      m_beforeZ;
    };

protected:
    using UndoPaneBase::size;

    bool undo() const override { return apply(m_beforeZ); }
    bool redo() const override { return apply(m_afterZ); }
    size_t size() const override;
    const char* className() const override { return "UndoPaneState"; }

private:
    static QByteArray read(const PaneBase&);
    bool apply(const QByteArray& stateZ) const;

    QByteArray m_beforeZ;
    QByteArray m_afterZ;
};

// Undo moving a header section
class UndoPaneSectionMove final : public UndoPaneBase
{
public:
    UndoPaneSectionMove(DataColumnPaneBase& pane, int from, int to);

    bool undo() const override;
    bool redo() const override;
    size_t size() const override { return sizeof(*this); }
    const char* className() const override { return "UndoPaneSectionMove"; }

private:
    int m_from;
    int m_to;
};

// Undo moving a header section
class UndoPaneSetColumnHidden final : public UndoPaneBase
{
public:
    UndoPaneSetColumnHidden(DataColumnPaneBase&, int column, bool hidden);

    bool undo() const override { return apply(!m_hidden); }
    bool redo() const override { return apply(m_hidden); }
    size_t size() const override { return sizeof(*this); }
    const char* className() const override { return "UndoPaneSetColumnHidden"; }

private:
    bool apply(bool hidden) const;

    int  m_column;
    bool m_hidden;
};

// Undo moving a header section
class UndoPaneSort final : public UndoPaneBase
{
public:
    UndoPaneSort(DataColumnPaneBase&,
                 int fromColumn, Qt::SortOrder fromOrder,
                 int toColumn, Qt::SortOrder toOrder);

    bool undo() const override { return apply(m_fromColumn, m_fromOrder); }
    bool redo() const override { return apply(m_toColumn, m_toOrder); }
    size_t size() const override { return sizeof(*this); }
    const char* className() const override { return "UndoPaneSort"; }

private:
    bool apply(int column, Qt::SortOrder order) const;

    int           m_fromColumn;
    Qt::SortOrder m_fromOrder;
    int           m_toColumn;
    Qt::SortOrder m_toOrder;
};

#endif // UNDOPANE_H
