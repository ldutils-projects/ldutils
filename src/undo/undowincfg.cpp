/*
    Copyright 2020-2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <QSettings>
#include <QFile>

#include "src/core/appbase.h"
#include "src/ui/windows/mainwindowbase.h"
#include "undowincfg.h"

UndoWinCfg::UndoWinCfg(MainWindowBase& mainWindow, const QByteArray& before, const QByteArray& after) :
    m_mainWindow(mainWindow), m_beforeZ(before),  m_afterZ(after)
{
}

UndoWinCfg::ScopedUndo::ScopedUndo(MainWindowBase& mainWindow, const QString& name) :
    UndoMgr::ScopedUndo(appBase().undoMgr(), name),
    m_mainWindow(mainWindow),
    m_before(nesting() == 1 ? read(mainWindow) : QByteArray())
{
}

UndoWinCfg::ScopedUndo::~ScopedUndo()
{
    if (nesting() == 1) { // 1 because we run before parent destructor
        const QByteArray after = read(m_mainWindow);
        if (after != m_before)
            m_undoMgr->add(new (std::nothrow) UndoWinCfg(m_mainWindow, m_before, after));
    }
}

QByteArray UndoWinCfg::read(MainWindowBase& mainWindow)
{
    return readCfgZ([&mainWindow](QSettings& settings) {
        mainWindow.saveWinConfig(settings);
    });
}

bool UndoWinCfg::apply(const QByteArray& stateZ) const
{
    const MainWindowBase::SaveCursor cursor(&m_mainWindow, Qt::WaitCursor);

    return applyCfgZ(stateZ, [this](QSettings& settings) {
        m_mainWindow.loadWinConfig(settings);  // reload
        m_mainWindow.postLoadHook();           // reset active indexes etc.
    });
}

size_t UndoWinCfg::size() const
{
    return sizeof(*this) + size(m_beforeZ) + size(m_afterZ);
}
