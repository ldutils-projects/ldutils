/*
    Copyright 2020-2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <src/core/cfgdatabase.h>
#include <src/ui/windows/mainwindowbase.h>

#include "undocfgdata.h"

UndoCfgData::UndoCfgData(MainWindowBase& mainWindow, const QByteArray& before, const QByteArray& after) :
    m_beforeZ(before), m_afterZ(after), m_mainWindow(mainWindow)
{
}

UndoCfgData::ScopedUndo::ScopedUndo(MainWindowBase& mainWindow, UndoMgr& undoMgr, const QString& name,
                                    const CfgDataBase& before, const CfgDataBase& after) :
    UndoMgr::ScopedUndo(&undoMgr, name),
    m_mainWindow(mainWindow),
    m_beforeZ(nesting() == 1 ? read(before) : QByteArray()),
    m_afterZ(nesting() == 1 ? read(after) : QByteArray())
{
}

UndoCfgData::ScopedUndo::~ScopedUndo()
{
    if (nesting() == 1) { // 1 because we run before parent destructor
        if (hasDiffs())
            m_undoMgr->add(new (std::nothrow) UndoCfgData(m_mainWindow, m_beforeZ, m_afterZ));
    }
}

size_t UndoCfgData::size() const
{
    return sizeof(*this) + size(m_beforeZ) + size(m_afterZ);
}

QByteArray UndoCfgData::read(const CfgDataBase& cfgData)
{
    return readCfgZ([&cfgData](QSettings& settings) {
        cfgData.save(settings);
    });
}

bool UndoCfgData::apply(const QByteArray& stateZ) const
{
    return applyCfgZ(stateZ, [this](QSettings& settings) {
        m_mainWindow.loadCfgData(settings);
    });
}
