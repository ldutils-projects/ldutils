/*
    Copyright 2020-2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "undobase.h"

#include <QTemporaryFile>
#include <QFile>
#include <QSettings>
#include <QByteArray>

UndoBase::~UndoBase()
{
}

size_t UndoBase::size(const QString& s)
{
    return sizeof(s) + size_t(s.toUtf8().size());
}

size_t UndoBase::size(const QByteArray& s)
{
    return sizeof(s) + size_t(s.size());
}

size_t UndoBase::size(const QStringList& sl)
{
    size_t total = sizeof(sl);

    for (const auto& s : sl)
        total += size(s);
    return total;
}

size_t UndoBase::size(const QVariant& v)
{
    switch (v.type()) {
    case QVariant::String:     return sizeof(v) + size(v.toString());
    case QVariant::Int:        return sizeof(v) + sizeof(int);
    case QVariant::Double:     return sizeof(v) + sizeof(double);
    case QVariant::StringList: return sizeof(v) + size(v.toStringList());
    case QVariant::ByteArray:  return sizeof(v) + size(v.toByteArray());
    default:                   return sizeof(v) + 16; // dunno.
    }
}

QByteArray UndoBase::readCfgZ(const std::function<void (QSettings&)>& saveFn)
{
    // Awkward, but it's what we've got. TODO: build a win/tab serialzier to a QDataStream
    QTemporaryFile tempFile;
    tempFile.open();

    QSettings settings(tempFile.fileName(), QSettings::IniFormat);

    saveFn(settings);
    settings.sync();

    tempFile.flush();
    tempFile.seek(0);

    // For some reaosn we cannot read from the temporary file after using
    // it to write, even if we flush, close and re-open it.  Appears to be
    // a Qt bug, but as a workaround, we open the file again with a plain
    // file object.
    QFile f1(tempFile.fileName());
    f1.open(QFile::ReadOnly);

    return qCompress(f1.readAll(), 5);
}

bool UndoBase::applyCfgZ(const QByteArray& stateZ, const std::function<void (QSettings&)>& applyFn)
{
    // Awkward, but it's what we've got. TODO: build a win/tab serialzier to a QDataStream
    QTemporaryFile tempFile;
    tempFile.open();

    // scope for uncompressed data's lifetime
    {
        const QByteArray uncompressed = qUncompress(stateZ);
        if (tempFile.write(uncompressed) != uncompressed.size())
            return false;
    }

    tempFile.close();
    tempFile.open();

    QSettings settings(tempFile.fileName(), QSettings::IniFormat);
    applyFn(settings);

    return true;
}

