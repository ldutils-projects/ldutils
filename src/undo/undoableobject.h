/*
    Copyright 2020-2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef UNDOABLEOBJECT_H
#define UNDOABLEOBJECT_H

#include <QObject>

#include <src/util/strongnum.h>

class UndoMgr;

class UndoableObject
{
public:
    UndoableObject(UndoMgr&);
    virtual ~UndoableObject();

    virtual void setDirty(bool dirty = true, bool signal = true);
    virtual bool isDirty() const { return m_dirty; }
    virtual void incDontTrack();
    virtual void decDontTrack();

    // The QSignalBlocker blocks everything, which is too big of a hammer.
    struct SignalBlocker {
        SignalBlocker(UndoableObject& object);
        SignalBlocker(UndoableObject* object);
        ~SignalBlocker();

    private:
        UndoableObject* m_object;
    };

protected:
    // Install or install signals
    virtual void setupChangeSignals(bool install) = 0;

    // We can't emit signals since Qt doesn't allow inheriting from two Q_OBJECT
    // based classes.  This is a hook for subclasses to do that.
    virtual void emitDirtyStateChanged(bool) { }

    UndoMgr& undoMgr() { return m_undoMgr; }

    UndoMgr&                      m_undoMgr;   // the undo manager
    Stronger::int32_t<class NEST> m_nestCount; // nesting count for signal blockers
    bool                          m_dirty;     // master dirty bit for this model.

private:
    friend struct SignalBlocker;
};

#endif // UNDOABLEOBJECT_H
