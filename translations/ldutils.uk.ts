<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="uk_UA">
<context>
    <name>AppConfigBase</name>
    <message>
        <source>Reset to Defaults?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Reset to defaults?  This will reset your current configuration.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Reset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Reset to Previous?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Reset to previous configuration?  This will erase your current configuration.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Set Configuration</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ChangeTrackingModel</name>
    <message>
        <source>Reorder item</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Clear Icon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Set Icon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Insert</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Remove</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Set </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CmdLineBase</name>
    <message>
        <source>Missing parameter to</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Unknown option</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Usage: %s [args...]

General Options:
   --help            Print usage information and exit.
   --version         Print program version and exit.
   --pubkey          Dump PGP/GPG public key to stdout and exit.
   --conf FILE       Load session from .conf file named FILE.
   --no-first-run    Disable first run handling, even on the first run.
   --private-session Disallow saves for this session.
   --desktop NAME    Sets the XDG_CURRENT_DESKTOP environment variable to NAME. This can
                     be useful when running the program under sudo, for example as in:
                        sudo -H -u user %s --desktop KDE
                     This will allow KDE to use a native theme under sudo.
</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ColorDelegate</name>
    <message>
        <source>Edit color</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ColorizerEditor</name>
    <message>
        <source>Colorization</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;These entries can colorize textual data in list views according to query criteria.&lt;/p&gt;&lt;p&gt;For each row of data, the &lt;span style=&quot; font-weight:600;&quot;&gt;Column&lt;/span&gt; will be given the selected &lt;span style=&quot; font-weight:600;&quot;&gt;FG Color,BG Color&lt;/span&gt;, and &lt;span style=&quot; font-weight:600;&quot;&gt;Icon&lt;/span&gt; if the &lt;span style=&quot; font-weight:600;&quot;&gt;Query&lt;/span&gt; matches that row.&lt;/p&gt;&lt;p&gt;Queries are processed in order, and the first to satisfy its condition will be used.  Queries can be marked as &lt;span style=&quot; font-weight:600;&quot;&gt;Case Sensitive&lt;/span&gt; or &lt;span style=&quot; font-weight:600;&quot;&gt;Case Insensitive&lt;/span&gt;.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Colorization control.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Unset the icon for selected entries.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Unset Icon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Unset the background color for selected entries.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Unset BG</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Unset the foreground color for selected entries.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Unset FG</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Add a new entry to the list.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Add</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Remove selected entries from the list.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Remove</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Unset &lt;span style=&quot; font-weight:600;&quot;&gt;icon&lt;/span&gt; for selected entries.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Unset Background Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Unset &lt;span style=&quot; font-weight:600;&quot;&gt;background color&lt;/span&gt; for selected entries.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Unset Foreground Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Unset &lt;span style=&quot; font-weight:600;&quot;&gt;foreground color&lt;/span&gt; for selected entries.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Remove selected colorization entries.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Add new colorization rule.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DataColumnPaneBase</name>
    <message>
        <source>Column Actions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Show All Columns</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Show Default Columns</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Hide Column</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Hide Other Columns</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Move Header: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Change Sort</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Hide</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Show</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Column</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DelegateBase</name>
    <message>
        <source>Set </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DocDialogBase</name>
    <message>
        <source>Tutorial</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Search in documentation. The page list will be subsetted to those pages matching the search query.&lt;/p&gt;&lt;p&gt;The &lt;span style=&quot; font-weight:600;&quot;&gt;return&lt;/span&gt; key will move to the next matching page.&lt;/p&gt;&lt;p&gt;The standard ZTGPS query functionality is supported, which includes:&lt;/p&gt;&lt;ul style=&quot;margin-top: 0px; margin-bottom: 0px; margin-left: 0px; margin-right: 0px; -qt-list-indent: 1;&quot;&gt;&lt;li style=&quot; margin-top:12px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Boolean expressions&lt;/li&gt;&lt;li style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Parenthical expressions&lt;/li&gt;&lt;li style=&quot; margin-top:0px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Regular expression matching&lt;/li&gt;&lt;/ul&gt;&lt;p&gt;See the documentation for more information on the query language.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Query documentation...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Move back one page in the view history.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Alt+Left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Move forward one page in the view history.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Previous in History</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Next in History</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Alt+Right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Refresh</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Refresh current page.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Ctrl+R</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Document Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Unable to find documentation files.  Please verify the installation and restart the program.  The current documentation search path is:
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Documentation Link</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Previous page: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Next page: </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>EmptyPane</name>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-weight:600; text-decoration: underline;&quot;&gt;Empty (Help) Pane&lt;/span&gt;&lt;/p&gt;&lt;p&gt;Empty panes display a small help message.  It can be unchecked to hide the help text and display a blank background.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>E&amp;mpty Pane</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;This is an empty pane (except for this message!) You can right click on its title area and select &lt;span style=&quot; font-style:italic;&quot;&gt;Replace Pane&lt;/span&gt; from the context menu to turn it into something more interesting.&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:11pt; font-weight:600;&quot;&gt;Active Panes:&lt;/span&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;The pane with a &lt;span style=&quot; font-weight:600;&quot;&gt;Bolded Title&lt;/span&gt; is the &lt;span style=&quot; font-style:italic;&quot;&gt;active pane.&lt;/span&gt; Many operations use the active pane.  Clicking on or in the pane makes it active.&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:11pt; font-weight:600;&quot;&gt;Pane Manipulation:&lt;/span&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;The handles between panes can be dragged to resize them. You can add, re-order, or delete panes with the &lt;span style=&quot; font-style:italic;&quot;&gt;Settings&lt;/span&gt; menu.&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;Panes can be split horizontally or vertically using &lt;span style=&quot; font-style:italic;&quot;&gt;Split Vertical&lt;/span&gt; or &lt;span style=&quot; font-style:italic;&quot;&gt;Split Horizontal&lt;/span&gt; menus.&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;Most things have mouse-hover tooltips, or you can use &lt;span style=&quot; font-style:italic;&quot;&gt;What Is...?&lt;/span&gt; from the &lt;span style=&quot; font-style:italic;&quot;&gt;Help&lt;/span&gt; menu and then click on something.&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;The checkbox next to the pane title area will show or hide some controls.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Empty Pane</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>IconSelector</name>
    <message>
        <source>Select Icon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>From File...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Select icon file</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>IconSelectorDelegate</name>
    <message>
        <source>Select icon</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LaunchSplash</name>
    <message>
        <source>Launch Splash</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:18pt; font-weight:600;&quot;&gt;AppTitle&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;p align=&quot;center&quot;&gt;&lt;img src=&quot;:art/logos/projects/ZTGPS-Large-360x263.jpg&quot;&gt;&lt;/img&gt;&lt;/p&gt;
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Loading...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Done.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LinkDialog</name>
    <message>
        <source>Edit Link</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>URL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Link URL.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Select a filesystem path to use as a URL.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Use filesystem path as a URL.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Link Text</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Text to be displayed for the link.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Text to be displayed for the link.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Select file</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindowBase</name>
    <message>
        <source>Private session: Saves disabled.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>No container pane focused. Please click on one and try again.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Replaced pane: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Removed pane: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Balance Siblings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Unable to save recent sesssions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Save Sessions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Save Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Error creating directory for settings file: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Error saving settings: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Quit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Quit application?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Split Pane</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Horizontally</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Vertically</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Split pane: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>No active pane found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Add New Pane</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Add Group Sibling</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Replace Pane</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Open Pane in New Window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Save UI</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Error creating settings file backup:&lt;p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Saved: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Error saving settings:&lt;p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Loaded: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Error loading settings:&lt;p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Load UI</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Save Configuration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Canceled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Open Configuration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>No settings file to revert from</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Revert settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Revert settings from file?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Unable to read save file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Error: No session file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>File locked</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;p&gt;&lt;b&gt;File is locked by another process.  Do you wish to use it anyway?&lt;/b&gt;&lt;/p&gt;&lt;p&gt;&lt;b&gt;Warning:&lt;/b&gt; This can cause unpredictable behavior if it is in use by another program.&lt;/p&gt;&lt;p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Unknown</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Lock error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;p&gt;Unable to remove lock for:&lt;br/&gt;&lt;/p&gt;&lt;p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;p&gt;No permissions to create lock for:&lt;br/&gt;&lt;/p&gt;&lt;p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Canceled: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source> (... %1 more)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Undo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Redo</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ModelTextEditDialog</name>
    <message>
        <source>Edit Model-Column HTML</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Show or hide the column variable display.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Insert the selected item into the text document.  If multiple items are selected, they will be inserted in order separated by spaces.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Show Column Variables</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Show the column variable list.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Hide Column Variables</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Hide the column variable list.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Insert Variable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Insert variables for one or more selected data columns.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>NewPaneDialog</name>
    <message>
        <source>Create New Pane</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Filter Query...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Add</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Replace</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;A search string can be entered here to filter the list of pane types below. Boolean operators, parentheses, and regular expressions are supported. See the online documentatiion for more information on the query language.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Focused Pane: &lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;These are pane types available for use in the user interface. Panes can be added, removed, replaced, or opened in new windows using the buttons at the bottom of the dialog. Normally this closes the dialog, but if the shift key is held while the button is clicked, the dialog remains open.&lt;/p&gt;&lt;p&gt;The arrow and other buttons will move the active pane, and are identical in operation to the same buttons in the program toolbar or pane menu.&lt;/p&gt;&lt;p&gt;Double clicking an entry will:&lt;/p&gt;&lt;ul style=&quot;margin-top: 0px; margin-bottom: 0px; margin-left: 0px; margin-right: 0px; -qt-list-indent: 1;&quot;&gt;&lt;li style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Replace the active (&lt;span style=&quot; font-weight:600;&quot;&gt;Bolded&lt;/span&gt;) pane.&lt;/li&gt;&lt;li style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Add a new pane if the &lt;span style=&quot; font-style:italic;&quot;&gt;control key&lt;/span&gt; is held down.&lt;/li&gt;&lt;li style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Open the pane in a new window if the &lt;span style=&quot; font-style:italic;&quot;&gt;shift key&lt;/span&gt; is held down.&lt;/li&gt;&lt;/ul&gt;&lt;p&gt;Dragging an entry and dropping it on an existing pane will replace that pane. The above keyboard modifiers are also supported for drag &amp;amp; drop with the effects as above.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Open in Window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Add New Pane</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Add a new pane as a sibling of the currently active (&lt;span style=&quot; font-weight:600;&quot;&gt;Bolded&lt;/span&gt;) pane.&lt;/p&gt;&lt;p&gt;Shift-click to keep the dialog open.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Ctrl+A</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Replace Pane</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Replace the currently active (&lt;span style=&quot; font-weight:600;&quot;&gt;Bolded&lt;/span&gt;) pane with a new pane.&lt;/p&gt;&lt;p&gt;Shift-click to keep the dialog open.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Ctrl+R</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Open in New Window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Open a new pane in a new top level window.&lt;/p&gt;&lt;p&gt;Shift-click to keep the dialog open.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Ctrl+W</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>NewPaneDialogBase</name>
    <message>
        <source>No pane type selected.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>Column</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Query</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>FgColor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>BgColor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Icon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Match Case</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Hide Text</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;i&gt;&lt;/i&gt;Column to colorize.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;i&gt;&lt;/i&gt;Query text.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;i&gt;&lt;/i&gt;Foreground text color, if set.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;i&gt;&lt;/i&gt;Background text color, if set.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;i&gt;&lt;/i&gt;Icon to display in column.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;i&gt;&lt;/i&gt;Enable for case sensitive matching.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;i&gt;&lt;/i&gt;Whether to hide text (to display only icon).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;error&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Normal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Success</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Critical</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cmd Line</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Stdout</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Stderr</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Match</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Pane Control</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Canceled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>sec</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>second</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>seconds</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>m</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>min</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>minute</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>minutes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>h</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>hr</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>hour</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>hours</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>d</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>day</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>days</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>pct</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>B</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>KiB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>MiB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>GiB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>TiB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>PiB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>EiB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>KB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>MB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>GB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>TB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>PB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>EB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>mm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>millimeter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>millimeters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>meter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>meters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Km</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>kilometer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>kilometers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>AU</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>ft</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>foot</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>feet</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>mi</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>mile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>miles</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Smoot</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Smoots</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>m/s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>mps</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>meter/sec</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Km/h</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Kph</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>mi/h</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>mph</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>ft/s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>fps</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>min/km</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>minute/km</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>minutes/km</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>minutes/kilometer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>min/mi</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>minute/mi</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>minutes/mi</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>minutes/mile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>m^2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Km^2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Ft^2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Mi^2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>C</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>DegC</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>K</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>DegK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>F</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>DegF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>°</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>deg</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>degree</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>degrees</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>rad</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>radian</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>radians</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>W</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>watt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>watts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>kW</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>kilowatt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>kilowatts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>hp</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>horsepower</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>kcal/hr</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>kcal/hour</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>kilocalorie/hr</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>kilocalories/hr</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>kilocalorie/hour</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>kilocalories/hour</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>g</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>gram</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>grams</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>kg</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>kilogram</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>kilograms</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>oz</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>ounce</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>ounces</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>lbs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>lb</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>pound</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>pounds</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>stone</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>ton</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>tons</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Wh</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>watt.hour</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>kWh</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>kW.h</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>cal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>calorie</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>calories</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>kcal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>kilocalorie</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>kilocalories</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>J</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>joule</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>joules</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>kJ</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>kilojoule</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>kilojoules</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>MJ</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>megajoule</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>megajoules</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>m/s^2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>m/s/s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>ft/s^2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>ft/s/s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>rps</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>rev/s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>revs/s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>rev/sec</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>revs/sec</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>rev/second</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>revs/second</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>rpm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>rev/m</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>revs/m</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>rev/min</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>revs/min</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>rev/minute</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>revs/minute</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>rph</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>rev/h</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>revs/h</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>rev/hr</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>revs/hr</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>rev/hour</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>revs/hour</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>rev/d</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>revs/d</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>rev/day</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>bps</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>beat/s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>beats/s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>beat/sec</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>beats/sec</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>beat/second</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>beats/second</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>bpm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>beat/m</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>beats/m</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>beat/min</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>beats/min</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>beat/minute</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>beats/minute</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>bph</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>beat/h</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>beats/h</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>beat/hr</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>beats/hr</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>beat/hour</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>beats/hour</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>beats/d</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>beat/d</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>beat/day</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>beats/day</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>S</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>M</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>H</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>D</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>MS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>HMS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>DHMS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Percent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Float %</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Float</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Int</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>String</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Auto Binary</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Auto Decimal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Bytes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>KiB (1024^1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>MiB (1024^2)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>GiB (1024^3)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>TiB (1024^4)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>PiB (1024^5)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>EiB (1024^6)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>KB (1000^1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>MB (1000^2)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>MB (1000^3)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>TB (1000^4)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>PB (1000^5)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>EB (1000^6)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Auto Metric</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Auto Imperial</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>mm (millimeters)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>m (meters)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>km (kilometers)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Astronautical Units</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>ft (feet)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>mi (miles)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>km^2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>ft^2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>mi^2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Celcius</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Kelvin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Fahrenheit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Degrees</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Radians</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Rise/run</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>ddd° mm&apos; ss.s&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Decimal degrees</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>watt hours</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>kilowatt hours</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>physics calories</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>food calories</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>meters / sec^2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>feet / sec^2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Short/Abbrev</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Offset from UTC</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Long</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>IANA Id</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Bad magic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Bad version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>File open error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>File error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Stream error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Unknown error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Show Columns</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;Select display columns.&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;Default search column, if not specified in query.&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Version error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Unable to downgrade from a newer save format.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;i&gt;&lt;/i&gt;Pane name.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;i&gt;&lt;/i&gt;Pane preview image.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Column Information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Editable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>You can edit data in this column by double clicking on it within a row, or using a context menu.  You can edit multiple rows at once by selecting several rows (shift or ctrl click) and doubling clicking while holding the shift or ctrl key.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Edit rich text</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TabWidget</name>
    <message>
        <source>Tab Actions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;body&gt;Add a new tab to the tab bar.&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Close Window?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Close Secondary Window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>New window title:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>New tab name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Closing the current tab will close all of its panes.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Closed tab.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>No current tab.  Please create one and try again.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TextEditor</name>
    <message>
        <source>Note Editor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Set text to &lt;span style=&quot; font-weight:600;&quot;&gt;bold&lt;/span&gt;.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Set text to bold.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Set text to &lt;span style=&quot; font-style:italic;&quot;&gt;italic&lt;/span&gt;.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Set text to italic.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Set text to &lt;span style=&quot; text-decoration: underline;&quot;&gt;italic&lt;/span&gt;.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Set text to underline.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Left justify text.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Left justify text.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Fill text.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Fill text.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Right justify text.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Right justify text.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Center text.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Center text.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Format text as a bullet (unordered) list.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Format text as a bullet (unordered) list.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Format text as a numbered (ordered) list.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Format text as a numbered (ordered) list.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Cut selection.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cut selection.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Copy selection.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Copy selection.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Paste from clipboard.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Paste from clipboard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Undo.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Undo.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Ctrl+Z</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Redo.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Redo.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Ctrl+Y</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Toggle bold text style.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Toggle italic text style.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Toggle underline text style.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Align text left.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Align text right.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Cut selection to clipboard.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Copy selection to clipboard.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Align fill.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Insert Link...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Insert Image...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Insert Table...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Size Columns...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Insert or edit a table.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Insert or edit table.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Insert a hyperlink.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Insert hyperlink.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Insert inline image.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Insert inline image.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Bold</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Ctrl+B</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Italic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Ctrl+I</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Underline</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Ctrl+U</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Align Left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Align Right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Align Center</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cut</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Ctrl+X</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Copy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Ctrl+C</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Paste</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Ctrl+V</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Undo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Redo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Ctrl+Shift+Z, Ctrl+Y</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Align FIll</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Text Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Set current text color.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Insert hyperlink.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Format as Ordered List</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Format as Unordered List</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Insert a new table, or edit an existing table.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Merge Cells</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Merge the selected table cells.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Split Cells</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Split previously merged cells.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Delete Row</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Delete selected table rows.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Delete Column</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Delete selected table columns&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Add Row</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Add a new table row.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Add Column</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Add a new table column.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Set column size from one of:&lt;/p&gt;
&lt;p&gt;&lt;ul&gt;
&lt;li&gt;&lt;b&gt;Variable&lt;/b&gt; - Size floats to contents.&lt;/li&gt;
&lt;li&gt;&lt;b&gt;Fixed&lt;/b&gt; - Fixed width in pixels.&lt;/li&gt;
&lt;li&gt;&lt;b&gt;Percent&lt;/b&gt; - Size as a percent of total width.&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Copy Html</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Copy the editor buffer as HTML source.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Paste Html</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Paste HTML source into the editor. &lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;NOTE: &lt;/span&gt;This will replace the entire current editor contents.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Character Format</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Alignment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Insert</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Table</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Reset?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Reset text?  This will lose recent changes.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TextEditorColumnSize</name>
    <message>
        <source>Set Column Widths</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Variable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Fixed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Percent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source> px</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source> %</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TextEditorDelegate</name>
    <message>
        <source>Edit rich text</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TextEditorDialog</name>
    <message>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TextEditorTableFormat</name>
    <message>
        <source>Table Format</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Number of rows for the table.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Number of columns for the table.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cell Padding</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Table cell padding: the distance between the border of a cell and its contents.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cell Spacing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Sets the vertical alignment for the table.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Table Alignment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source> cols</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;The margin on the table&apos;s &lt;span style=&quot; font-weight:600;&quot;&gt;bottom&lt;/span&gt;.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source> rows</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Flow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;How the table floats within a paragraph it is part of.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>In Flow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Float Left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Float Right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;The margin on the table&apos;s &lt;span style=&quot; font-weight:600;&quot;&gt;left&lt;/span&gt;.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;The margin on the table&apos;s &lt;span style=&quot; font-weight:600;&quot;&gt;right&lt;/span&gt;.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;The margin on the table&apos;s &lt;span style=&quot; font-weight:600;&quot;&gt;top&lt;/span&gt;.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Cell spacing: the distance between adjacent cells.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;The border style used to render cell borders.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>No Border</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Dotted</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Dashed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Solid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Double</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Dot + Dash</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Dot + Dot + Dash</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Groove</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Ridge</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Inset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Outset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Border</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Table Margin (L/R/T/B)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Center</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Sets the horizontal alignment for the table.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Justified</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Border width in pixels for the table cells.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source> px</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Top</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Bottom</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Baseline</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UnitsDelegate</name>
    <message>
        <source>Default</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
