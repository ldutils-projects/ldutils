# Build Requirements

## Debian GNU/Linux 10 (buster)

* Libraries:

        sudo apt install libqt5charts5-dev libqt5svg5-dev libstdc++-8-dev qtbase5-dev 

* Tools:

        sudo apt install build-essential cmake qttools5-dev-tools 

## Debian GNU/Linux 12 (bookworm)

* Libraries:

        sudo apt install libqt5charts5-dev libqt5svg5-dev libstdc++-12-dev qtbase5-dev 

* Tools:

        sudo apt install build-essential cmake qttools5-dev-tools 

## openSUSE Leap 15.4

* Libraries:

        sudo zypper install libQt5Charts5-devel libQt5Core-devel libQt5Gui-devel libqt5-qtsvg-devel libQt5Widgets-devel libQt5Xml-devel libstdc++6-devel-gcc7 

* Tools:

        sudo zypper install pattern:devel_basis cmake-full libqt5-linguist 

## Arch Linux

* Libraries:

        sudo pacman --needed -S gcc qt5-base qt5-charts qt5-svg 

* Tools:

        sudo pacman --needed -S base-devel cmake qt5-tools 

# Building

1. Create and cd to a build tree.  This should be outside the source tree.

        mkdir build-ldutils-Release && cd build-ldutils-Release

2. Create the build system:

        cmake -DCMAKE_BUILD_TYPE=Release /path/to/ldutils

3. Build.  LC_CTYPE may need to be set in some environments to allow filenames with non-ASCII characters.

        LC_CTYPE=UTF-8 cmake --build . -j $(nproc)

4. Install the build.

	 By default, the installation is to a **libldutils** directory as a sibling of the build directory. This can be changed by setting the **DESTDIR** environment (not cmake!) variable.

  * Any cmake version:

        sudo make install

  * cmake 3.15 and later can also use:

        sudo cmake --install .

