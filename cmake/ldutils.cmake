#-----------------------------------------------------------------------
# Copyright 2023 Loopdawg Software
# 
# ldutils is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#-----------------------------------------------------------------------

# Tidy up locations for generated autogen files
macro(ldutils_tidy_autogen)
   foreach(TARGET ${ARGN})
      set_target_properties(${TARGET}
                            PROPERTIES AUTOGEN_BUILD_DIR "${CMAKE_BINARY_DIR}/${BUILD_DIR}/${TARGET}_autogen")

   endforeach()
endmacro()

# Read package version
macro(ldutils_get_package_version OUTVAR)
   file(READ "VERSION" ${OUTVAR})
   string(REPLACE "\"" "" ${OUTVAR} "${${OUTVAR}}")
   string(REPLACE "\n" "" ${OUTVAR} "${${OUTVAR}}")
endmacro()

# Reject in-tree builds
macro(ldutils_reject_in_tree_build)
   if ("${CMAKE_BINARY_DIR}" STREQUAL "${CMAKE_SOURCE_DIR}")
      message(FATAL_ERROR "Please build outside the source tree.")
   endif()
endmacro()

# Reject relative install paths
macro(ldutils_reject_relative_install_path)
   # At install time, verify absolute path.
   install(CODE "if (NOT \"\$ENV{DESTDIR}\${CMAKE_INSTALL_PREFIX}\" MATCHES \"^/.*\")
                     message(FATAL_ERROR \"Install prefix must be an absolute path.\")
                 endif()")
endmacro()

# Reject unknown build types
macro(ldutils_reject_build_types)
   # We only handle Release or Debug
   if (NOT "${CMAKE_BUILD_TYPE}" STREQUAL "Release" AND
       NOT "${CMAKE_BUILD_TYPE}" STREQUAL "Debug")
      message(FATAL_ERROR "CMAKE_BUILD_TYPE must be Release or Debug")
   endif()
endmacro()

# Modify paths within the .desktop file, and install it.
macro(ldutils_install_desktop_file)
   set(oneValueArgs EXENAME ARTIFACT_DIR DESKTOP_NAME ICON_NAME)
   cmake_parse_arguments(IDF "" "${oneValueArgs}" "" ${ARGN})

   if (NOT DEFINED CMAKE_INSTALL_DATAROOTDIR)
      message(FATAL_ERROR "Requires GNUInstallDirs")
   endif()

   # Modify desktop file to point to the actual installed icon location. A bit awkward since we don't
   # know the CMAKE_INSTALL_PREFIX until install time, so we have to use install(CODE...)
   install(CODE
           "set(EXE ${IDF_EXENAME})
            set(ICON \"\${CMAKE_INSTALL_PREFIX}/${DATA_INSTALL_DIR}/${IDF_ICON_NAME}\")
            configure_file(\"${CMAKE_CURRENT_LIST_DIR}/desktop/${IDF_DESKTOP_NAME}\"
                           \"${CMAKE_BINARY_DIR}/${IDF_ARTIFACT_DIR}/${IDF_DESKTOP_NAME}\")")

   # We can now install the desktop file, after fixing its path.
   install(FILES "${CMAKE_BINARY_DIR}/${IDF_ARTIFACT_DIR}/${IDF_DESKTOP_NAME}"
           DESTINATION "${CMAKE_INSTALL_DATAROOTDIR}/applications")

   set_property(DIRECTORY PROPERTY ADDITIONAL_MAKE_CLEAN_FILES "${ARTIFACT_DIR}/${DESKTOP_NAME}")
endmacro()

# Modify paths within the .desktop file, and install it.
macro(ldutils_add_translate_target)
   if (NOT DEFINED Qt OR NOT DEFINED QtVer)
      message(FATAL_ERROR "Requires Qt and QtVer to be set.")
   endif()

   file(GLOB_RECURSE TS_FILES CONFIGURE_DEPENDS "${CMAKE_SOURCE_DIR}/translations/*.ts")
   add_custom_target("translations"
                     COMMAND lupdate ${ARGN} -ts ${TS_FILES}
                     "-locations" "none" "-no-ui-lines" >/dev/null)
endmacro()

macro(ldutils_add_manpage)
   set(oneValueArgs EXENAME)
   cmake_parse_arguments(AM "" "${oneValueArgs}" "" ${ARGN})

   if (NOT DEFINED CMAKE_INSTALL_MANDIR)
      message(FATAL_ERROR "Requires GNUInstallDirs")
   endif()

   add_custom_target("${AM_EXENAME}.1.gz" ALL
                     VERBATIM
                     DEPENDS "${AM_EXENAME}.1"
                     BYPRODUCTS "${AM_EXENAME}.1.gz"
                     COMMAND gzip --stdout "${CMAKE_CURRENT_LIST_DIR}/${AM_EXENAME}.1" > "${AM_EXENAME}.1.gz")

   install(FILES "${CMAKE_CURRENT_BINARY_DIR}/${AM_EXENAME}.1.gz"
           DESTINATION "${CMAKE_INSTALL_MANDIR}/man1")
endmacro()

# Build RCC files. We don't use CMAKE_AUTORCC since we want finer grained option control than provided in CMake 3.13.
macro(ldutils_build_rcc_files)
   file(GLOB QRC_FILES CONFIGURE_DEPENDS "*.qrc")
   foreach(it ${QRC_FILES})
     get_filename_component(RESOURCE ${it} NAME_WE)
     qt5_add_binary_resources(${RESOURCE} "${it}" OPTIONS --compress 6 --threshold 8 --binary)
     add_dependencies(${EXENAME} ${RESOURCE})
   endforeach()
endmacro()

# Standard compile options
macro(ldutils_compile_opts)
   set(Qt           "Qt5")
   set(QtVer        "5.11")
   set(BUILD_DIR    "build")
   set(CMAKE_AUTOMOC ON)
   set(CMAKE_AUTOUIC ON)
   set(ARTIFACT_DIR  "artifacts")
   set(CMAKE_RUNTIME_OUTPUT_DIRECTORY "${ARTIFACT_DIR}")
   set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY "${ARTIFACT_DIR}")

   set(CMAKE_CXX_STANDARD 17)
   set(CMAKE_CXX_STANDARD_REQUIRED ON)

   # Compile and link options for all targets
   add_compile_definitions("QT_DEPRECATED_WARNINGS"
                           "QT_DISABLE_DEPRECATED_BEFORE=0x060000"
                           "$<$<CONFIG:RELEASE>:NDEBUG>")

   add_compile_options("-Wall" 
                       "-pedantic")

   add_link_options("LINKER:--dynamic-list-cpp-typeinfo"
                    "$<$<CONFIG:RELEASE>:LINKER:--strip-all>")

   # Check dependencies
   ldutils_reject_in_tree_build()
   ldutils_reject_build_types()
   ldutils_reject_relative_install_path()

   # Path, for distros that do not have qtchooser
   string(TOLOWER "${Qt}" qt)
   set(ENV{PATH} "$ENV{PATH}:/usr/lib/${qt}/bin:/usr/lib64/${qt}/bin")
endmacro()
