#-----------------------------------------------------------------------
# Copyright 2023 Loopdawg Software
# 
# ZombieTrackerGPS is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#-----------------------------------------------------------------------

# Generic data
set(CPACK_GENERATOR "none")
set(CPACK_SOURCE_GENERATOR "External")

set(CPACK_PACKAGE_NAME "${LIBNAME}")
set(CPACK_PACKAGE_VERSION "${PKGVERSION}")

# Source generation
set(CPACK_SOURCE_PACKAGE_FILE_NAME "${CPACK_PACKAGE_NAME}-${CPACK_PACKAGE_VERSION}")
set(CPACK_SOURCE_IGNORE_FILES "/\\\\.git/" "/.*\\\\.json" "/.*\\\\.user")
set(CPACK_EXTERNAL_PACKAGE_SCRIPT "${CMAKE_CURRENT_SOURCE_DIR}/pack-tar.cmake")

message(STATUS "${CMAKE_CURRENT_SOURCE_DIR}/pack-tar.cmake")

include(CPack)
