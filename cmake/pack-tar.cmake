#-----------------------------------------------------------------------
# Copyright 2023 Loopdawg Software
# 
# ZombieTrackerGPS is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#-----------------------------------------------------------------------

list(GET CPACK_BUILD_SOURCE_DIRS 0 SOURCE_DIR)
list(GET CPACK_BUILD_SOURCE_DIRS 1 BINARY_DIR)

set(OUT_FILE "${CPACK_SOURCE_PACKAGE_FILE_NAME}.tar.xz")
set(OUT_PATH "${BINARY_DIR}/${OUT_FILE}")

execute_process(COMMAND tar --transform "s:^[.]/:./${CPACK_SOURCE_PACKAGE_FILE_NAME}/:"
                              -C "${SOURCE_DIR}" 
                              --exclude *.json 
                              --exclude *.user 
                              --exclude *.qmake.*
                              --exclude art/data
                              --exclude art/source
                              --exclude-vcs
                              --exclude-backups
                              --owner loopdawg
                              --group loopdawg 
                              --xz
                              -cf "${OUT_PATH}" .)
