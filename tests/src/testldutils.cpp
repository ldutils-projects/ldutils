/*
    Copyright 2020-2023 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <experimental/type_traits>
#include <cassert>
#include <set>
#include <unordered_set>
#include <ctgmath>

#include <QObject>
#include <QApplication>
#include <QVariant>
#include <QRegularExpression>
#include <QTextEdit>
#include <QToolButton>
#include <QClipboard>
#include <QSpinBox>
#include <QDoubleSpinBox>

#include <src/util/strongnum.h>
#include <src/util/units.h>
#include <src/core/query.h>
#include <src/ui/widgets/texteditor.h>
#include <src/ui/dialogs/texteditortableformat.h>

#include "testldutils.h"

template <typename SI>
void TestLdUtils::strongnum_single_static_all()
{
    using SI2      = typename SI::template change_key<class KEY2>;
    using SI_uint8 = typename SI::template change_base_type<uint8_t>;
    using SI_int64 = typename SI::template change_base_type<int8_t>;
    using BT       = typename SI::base_type;

    // Verify size
    static_assert(sizeof(SI)  == sizeof(typename SI::base_type));

    // Always copy constructable
    static_assert(std::is_copy_constructible_v<SI>);

    // Always constructable from underlying type
    static_assert(std::is_constructible_v<SI, typename SI::base_type>);

    // Trivially constructable if not using DefaultZeroConstruct
    static_assert(std::is_trivially_default_constructible_v<SI> != SI::is_default_zero_construct());

    // Only constructable from foreign StaticInt if is_allow_foreign_construct
    static_assert(SI::is_foreign_constructable() == std::is_constructible_v<SI, SI2>);

    // Verify assignability from integers: only allowed if not explicit construction
    static_assert(SI::is_explicit_constructable() != std::is_assignable_v<SI, typename SI::base_type>);

    // Can never assign from foreign keys
    static_assert(!std::is_assignable_v<SI, SI2>);

    // Can only convert to underlying type explicitly.
    static_assert(!std::is_assignable_v<typename SI::base_type, SI>);

    // Can only explicitly construct from foreign type if is_foreign_constructable()
    static_assert(SI::is_foreign_constructable() == std::is_constructible_v<SI, SI2>);

    // Verify constructability from same key, different type
    static_assert(std::is_constructible_v<SI, SI_uint8>);
    static_assert(std::is_constructible_v<SI, SI_int64>);
    static_assert(std::is_constructible_v<SI_uint8, SI>);
    static_assert(std::is_constructible_v<SI_int64, SI>);

    // Verify constructability of other numeric types
    static_assert(std::is_constructible_v<int8_t, SI>);
    static_assert(std::is_constructible_v<int16_t, SI>);
    static_assert(std::is_constructible_v<int32_t, SI>);
    static_assert(std::is_constructible_v<int64_t, SI>);
    static_assert(std::is_constructible_v<uint8_t, SI>);
    static_assert(std::is_constructible_v<uint16_t, SI>);
    static_assert(std::is_constructible_v<uint32_t, SI>);
    static_assert(std::is_constructible_v<uint64_t, SI>);
    static_assert(std::is_constructible_v<float, SI>);
    static_assert(std::is_constructible_v<double, SI>);

    // Verify constructability from other numeric types
    static_assert(std::is_constructible_v<SI, int8_t>);
    static_assert(std::is_constructible_v<SI, int16_t>);
    static_assert(std::is_constructible_v<SI, int32_t>);
    static_assert(std::is_constructible_v<SI, int64_t>);
    static_assert(std::is_constructible_v<SI, uint8_t>);
    static_assert(std::is_constructible_v<SI, uint16_t>);
    static_assert(std::is_constructible_v<SI, uint32_t>);
    static_assert(std::is_constructible_v<SI, uint64_t>);
    static_assert(std::is_constructible_v<SI, float>);
    static_assert(std::is_constructible_v<SI, double>);

    // Verify assignability from same key, different type
    static_assert(std::is_assignable_v<SI, SI_uint8>);

    // Test signedness
    static_assert(std::is_signed_v<SI> == std::is_signed_v<typename SI::base_type>);
    static_assert(std::is_unsigned_v<SI> == std::is_unsigned_v<typename SI::base_type>);

    // Test operators
    static_assert(std::experimental::is_detected_v<add_t, SI, SI>);
    static_assert(std::experimental::is_detected_v<sub_t, SI, SI>);
    static_assert(std::experimental::is_detected_v<mul_t, SI, SI>);
    static_assert(std::experimental::is_detected_v<div_t, SI, SI>);
    static_assert(std::experimental::is_detected_v<unary_sub_t, SI>);

    static_assert(SI::is_explicit_constructable() || std::experimental::is_detected_v<add_t, SI, BT>);
    static_assert(SI::is_explicit_constructable() || std::experimental::is_detected_v<sub_t, SI, BT>);
    static_assert(SI::is_explicit_constructable() || std::experimental::is_detected_v<mul_t, SI, BT>);
    static_assert(SI::is_explicit_constructable() || std::experimental::is_detected_v<div_t, SI, BT>);
    static_assert(std::is_invocable_v<std::equal_to<>, SI, SI>);
    static_assert(std::is_invocable_v<std::not_equal_to<>, SI, SI>);
    static_assert(std::is_invocable_v<std::less<>, SI, SI>);
    static_assert(std::is_invocable_v<std::less_equal<>, SI, SI>);
    static_assert(std::is_invocable_v<std::greater<>, SI, SI>);
    static_assert(std::is_invocable_v<std::greater_equal<>, SI, SI>);

    // test result types
//    static_assert(std::is_same_v<std::invoke_result_t<decltype(&SI::operator+), SI, SI>, SI>);
//    static_assert(std::is_same_v<std::invoke_result_t<decltype(&SI::operator-), SI, SI>, SI>);
    static_assert(std::is_same_v<std::invoke_result_t<decltype(&SI::operator*), SI, SI>, SI>);
    static_assert(std::is_same_v<std::invoke_result_t<decltype(&SI::operator/), SI, SI>, SI>);
    static_assert(std::is_same_v<std::invoke_result_t<decltype(&SI::operator+=), SI, SI>, SI&>);
    static_assert(std::is_same_v<std::invoke_result_t<decltype(&SI::operator-=), SI, SI>, SI&>);
    static_assert(std::is_same_v<std::invoke_result_t<decltype(&SI::operator*=), SI, SI>, SI&>);
    static_assert(std::is_same_v<std::invoke_result_t<decltype(&SI::operator/=), SI, SI>, SI&>);

    static_assert(std::is_same_v<std::invoke_result_t<std::equal_to<>, SI, SI>, bool>);
    static_assert(std::is_same_v<std::invoke_result_t<std::not_equal_to<>, SI, SI>, bool>);
    static_assert(std::is_same_v<std::invoke_result_t<std::less<>, SI, SI>, bool>);
    static_assert(std::is_same_v<std::invoke_result_t<std::less_equal<>, SI, SI>, bool>);
    static_assert(std::is_same_v<std::invoke_result_t<std::greater<>, SI, SI>, bool>);
    static_assert(std::is_same_v<std::invoke_result_t<std::greater_equal<>, SI, SI>, bool>);
//    static_assert(std::is_same_v<std::invoke_result_t<std::negate<>, SI, SI>, bool>);

    // Test operators from others keys
    static_assert(!std::is_invocable_v<std::equal_to<>, SI, SI2>);
    static_assert(!std::is_invocable_v<std::not_equal_to<>, SI, SI2>);
    static_assert(!std::is_invocable_v<std::less<>, SI, SI2>);
    static_assert(!std::is_invocable_v<std::less_equal<>, SI, SI2>);
    static_assert(!std::is_invocable_v<std::greater<>, SI, SI2>);
    static_assert(!std::is_invocable_v<std::greater_equal<>, SI, SI2>);

    static_assert(!std::experimental::is_detected_v<add_t, SI, SI2>);
    static_assert(!std::experimental::is_detected_v<sub_t, SI, SI2>);
    static_assert(!std::experimental::is_detected_v<mul_t, SI, SI2>);
    static_assert(!std::experimental::is_detected_v<div_t, SI, SI2>);
    static_assert(!std::is_invocable_v<decltype(&SI::operator+=), SI, SI2>);
    static_assert(!std::is_invocable_v<decltype(&SI::operator-=), SI, SI2>);
    static_assert(!std::is_invocable_v<decltype(&SI::operator*=), SI, SI2>);
    static_assert(!std::is_invocable_v<decltype(&SI::operator/=), SI, SI2>);

    // test result types
//    static_assert(std::is_same_v<SI,   std::invoke_result_t<decltype(&SI::operator+), SI, SI>>);
//    static_assert(std::is_same_v<SI,   std::invoke_result_t<decltype(&SI::operator-), SI, SI>>);
    static_assert(std::is_same_v<SI,   std::invoke_result_t<decltype(&SI::operator*), SI, SI>>);
    static_assert(std::is_same_v<SI,   std::invoke_result_t<decltype(&SI::operator/), SI, SI>>);
    static_assert(std::is_same_v<bool, std::invoke_result_t<decltype(&SI::operator!), SI>>);
//    static_assert(std::is_same_v<SI,   std::invoke_result_t<decltype(&SI::operator-), SI>>);

    // layout and triviality tests
    static_assert(std::is_standard_layout_v<SI>);
    static_assert(std::is_trivially_copyable_v<SI>);
    static_assert(std::is_trivially_copy_constructible_v<SI>);
    static_assert(std::is_trivially_move_constructible_v<SI>);
    static_assert(std::is_trivially_move_assignable_v<SI>);
    static_assert(std::is_trivially_assignable_v<SI, SI>);
    static_assert(std::is_floating_point_v<typename SI::base_type> || std::has_unique_object_representations_v<SI>);
    static_assert(std::is_swappable_v<SI>);
//  static_assert(std::is_swappable_with_v<SI, typename SI::base_type>);
    static_assert(!std::is_polymorphic_v<SI>);
    static_assert(!std::is_floating_point_v<SI>);
    static_assert(!std::is_enum_v<SI>);

    // Verify type changing
    static_assert(std::is_same_v<typename SI::template change_base_type<int8_t>::base_type, int8_t>);
    static_assert(std::is_same_v<typename SI::template change_base_type<uint8_t>::base_type, uint8_t>);

    // Verify key changing
    static_assert(!std::is_same_v<typename SI::template change_key<class K5>, SI>);
    static_assert(std::is_same_v<typename SI::template change_key<class K5>,
                                 typename SI::template change_key<class K5>>);

//    // Test with enum keys
//    enum class Keys {
//        Key1,
//        Key2,
//    };

//    StrongNum<Keys::Key1> ec1;
//    StrongNum<Keys::Key2> ec2;

//    static_assert(!is_assignable_v<decltype(ec1), decltype(ec2)>);

#if THESE_ARE_NOT_SATISFIED  // Note these properties are NOT satisfied!
    static_assert(std::is_integral_v<StrongNum<class K>>);
    static_assert(std::is_pod_v<StrongNum<class K>>);
    static_assert(std::is_trivial_v<StrongNum<class K>>);
    static_assert(!std::is_class_v<StrongNum<class K>>);
#endif // THESE_ARE_NOT_SATISFIED
}

template <typename SI>
void TestLdUtils::strongnum_single_static_f()
{
    strongnum_single_static_all<SI>();
}

template <typename SI>
void TestLdUtils::strongnum_single_static_i()
{
    using SI2 = typename SI::template change_key<class KEY2>;

    strongnum_single_static_all<SI>();

    static_assert(std::experimental::is_detected_v<lshift_t, SI, SI>);
    static_assert(std::experimental::is_detected_v<rshift_t, SI, SI>);
    static_assert(std::experimental::is_detected_v<lshift_asgn_t, SI&, SI>);
    static_assert(std::experimental::is_detected_v<rshift_asgn_t, SI&, SI>);

    static_assert(std::experimental::is_detected_v<and_t, SI, SI>);
    static_assert(std::experimental::is_detected_v<or_t, SI, SI>);
    static_assert(std::experimental::is_detected_v<xor_t, SI, SI>);
    static_assert(std::experimental::is_detected_v<mod_t, SI, SI>);
    static_assert(std::experimental::is_detected_v<and_asgn_t, SI&, SI>);
    static_assert(std::experimental::is_detected_v<or_asgn_t, SI&, SI>);
    static_assert(std::experimental::is_detected_v<mod_asgn_t, SI&, SI>);
    static_assert(std::experimental::is_detected_v<bit_neg_t, SI>);

    static_assert(!std::experimental::is_detected_v<lshift_t, SI, SI2>);
    static_assert(!std::experimental::is_detected_v<rshift_t, SI, SI2>);
    static_assert(!std::experimental::is_detected_v<lshift_asgn_t, SI&, SI2>);
    static_assert(!std::experimental::is_detected_v<rshift_asgn_t, SI&, SI2>);

    static_assert(!std::experimental::is_detected_v<and_t, SI, SI2>);
    static_assert(!std::experimental::is_detected_v<or_t, SI, SI2>);
    static_assert(!std::experimental::is_detected_v<xor_t, SI, SI2>);
    static_assert(!std::experimental::is_detected_v<mod_t, SI, SI2>);
    static_assert(!std::experimental::is_detected_v<and_asgn_t, SI&, SI2>);
    static_assert(!std::experimental::is_detected_v<or_asgn_t, SI&, SI2>);
    static_assert(!std::experimental::is_detected_v<mod_asgn_t, SI&, SI2>);
}

template <Strong::Options opts>
void TestLdUtils::strongnum_all_sizes_static()
{
    static_assert(std::is_same_v<typename Strong::Num<class KEY, int8_t, opts>::base_type, int8_t>);
    static_assert(std::is_same_v<typename Strong::Num<class KEY, int16_t, opts>::base_type, int16_t>);
    static_assert(std::is_same_v<typename Strong::Num<class KEY, int32_t, opts>::base_type, int32_t>);
    static_assert(std::is_same_v<typename Strong::Num<class KEY, int64_t, opts>::base_type, int64_t>);

    static_assert(std::is_same_v<typename Strong::Num<class KEY, uint8_t, opts>::base_type, uint8_t>);
    static_assert(std::is_same_v<typename Strong::Num<class KEY, uint16_t, opts>::base_type, uint16_t>);
    static_assert(std::is_same_v<typename Strong::Num<class KEY, uint32_t, opts>::base_type, uint32_t>);
    static_assert(std::is_same_v<typename Strong::Num<class KEY, uint64_t, opts>::base_type, uint64_t>);

    static_assert(std::is_same_v<typename Strong::Num<class KEY, float, opts>::base_type, float>);
    static_assert(std::is_same_v<typename Strong::Num<class KEY, double, opts>::base_type, double>);

    // Signed types
    strongnum_single_static_i<Strong::Num<class KEY, int8_t,   opts>>();
    strongnum_single_static_i<Strong::Num<class KEY, int16_t,  opts>>();
    strongnum_single_static_i<Strong::Num<class KEY, int32_t,  opts>>();
    strongnum_single_static_i<Strong::Num<class KEY, int16_t,  opts>>();

    // Unsigned types
    strongnum_single_static_i<Strong::Num<class KEY, uint8_t,  opts>>();
    strongnum_single_static_i<Strong::Num<class KEY, uint16_t, opts>>();
    strongnum_single_static_i<Strong::Num<class KEY, uint32_t, opts>>();
    strongnum_single_static_i<Strong::Num<class KEY, uint16_t, opts>>();

    // float types
    strongnum_single_static_f<Strong::Num<class KEY, float,  opts>>();
    strongnum_single_static_f<Strong::Num<class KEY, double, opts>>();
}

template <int opt> void TestLdUtils::strongnum_static_iterate() {
    strongnum_all_sizes_static<Strong::Options(opt)>();
    TestLdUtils::strongnum_static_iterate<opt-1>();
}

template <> void TestLdUtils::strongnum_static_iterate<-1>() { }

void TestLdUtils::strongnum_static()
{
    static_assert(std::is_same_v<Strong::int32_t<class K1>::base_type, int32_t>);
    static_assert(std::is_same_v<Strong::Num<class K1, uint8_t>::base_type, uint8_t>);

    strongnum_static_iterate<int(Strong::Options::ExplicitConstruction) |
                             int(Strong::Options::IsForeignConstructable) |
                             int(Strong::Options::DefaultZeroConstruct)>();
}

template <typename SI>
void TestLdUtils::strongnum_single_all()
{
    using SI4 = typename SI::template change_key<class K42>;
    using BT  = typename SI::base_type;

    // should succeed
    SI k1a = SI(4);
    SI k1b(5);
    SI x1c(k1a);  // 4

    QCOMPARE(k1a, SI(4));
    QCOMPARE(k1b, SI(5));
    QCOMPARE(x1c, SI(4));

    // should succeed
    QCOMPARE(k1a = k1b, SI(5));

    // Unary
    QCOMPARE(-k1a == -BT(5), true);
    QCOMPARE(+k1a == +BT(5), true);

    // comparisons
    QCOMPARE(k1a == BT(5), true);
    QCOMPARE(k1a == BT(4), false);
    QCOMPARE(k1a != BT(5), false);
    QCOMPARE(k1a != BT(4), true);
    QCOMPARE(k1a  < BT(5), false);
    QCOMPARE(k1a  < BT(6), true);
    QCOMPARE(k1a <= BT(5), true);
    QCOMPARE(k1a <= BT(4), false);
    QCOMPARE(k1a  > BT(5), false);
    QCOMPARE(k1a  > BT(4), true);
    QCOMPARE(k1a >= BT(5), true);
    QCOMPARE(k1a >= BT(6), false);
    QCOMPARE(!k1a, false);

    QCOMPARE(BT(5) == k1a, true);
    QCOMPARE(BT(4) == k1a, false);
    QCOMPARE(BT(5) != k1a, false);
    QCOMPARE(BT(4) != k1a, true);
    QCOMPARE(BT(5)  < k1a, false);
    QCOMPARE(BT(4)  < k1a, true);
    QCOMPARE(BT(5) <= k1a, true);
    QCOMPARE(BT(6) <= k1a, false);
    QCOMPARE(BT(5)  > k1a, false);
    QCOMPARE(BT(6)  > k1a, true);
    QCOMPARE(BT(5) >= k1a, true);
    QCOMPARE(BT(4) >= k1a, false);

    QCOMPARE(k1a == k1b, true);
    QCOMPARE(k1a != k1b, false);
    QCOMPARE(k1a  < k1b, false);
    QCOMPARE(k1a <= k1b, true);
    QCOMPARE(k1a  > k1b, false);
    QCOMPARE(k1a >= k1b, true);

    // Arithmetic
    QCOMPARE(-k1a,   SI(-5));
    QCOMPARE(k1a +   BT(2), SI(7));
    QCOMPARE(k1a -   BT(2), SI(3));
    QCOMPARE(k1a *   BT(2), SI(10));
    QCOMPARE(k1a /   BT(2), SI(2.5));
    QCOMPARE(BT(2)  +  k1a, SI(7));
    QCOMPARE(BT(2)  -  k1a, SI(-3));
    QCOMPARE(BT(2)  *  k1a, SI(10));
    QCOMPARE(BT(2)  /  k1a, SI(0.4));
    QCOMPARE(k1a +=  BT(2), SI(7));
    QCOMPARE(k1a -=  BT(2), SI(5));
    QCOMPARE(k1a *=  BT(2), SI(10));
    QCOMPARE(k1a /=  BT(2), SI(5));

    QCOMPARE(k1b =  BT(2),  SI(2));
    QCOMPARE(k1a +  k1b, SI(7));
    QCOMPARE(k1a -  k1b, SI(3));
    QCOMPARE(k1a *  k1b, SI(10));
    QCOMPARE(k1a /  k1b, SI(2.5));
    QCOMPARE(k1a += k1b, SI(7));
    QCOMPARE(k1a -= k1b, SI(5));
    QCOMPARE(k1a *= k1b, SI(10));
    QCOMPARE(k1a /= k1b, SI(5));

    // Post-inc/dec
    QCOMPARE(k1a++, SI(5));
    QCOMPARE(k1a,   SI(6));
    QCOMPARE(k1a--, SI(6));
    QCOMPARE(k1a,   SI(5));

    // Pre-inc/dec
    QCOMPARE(++k1a, SI(6));
    QCOMPARE(k1a,   SI(6));
    QCOMPARE(--k1a, SI(5));
    QCOMPARE(k1a,   SI(5));

    // utility
    QCOMPARE(std::min(k1a, k1b), SI(2));
    QCOMPARE(std::max(k1a, k1b), SI(5));

    // limits
    QCOMPARE(std::numeric_limits<decltype(k1a)>::max(),
             std::numeric_limits<typename SI::base_type>::max());
    QCOMPARE(std::numeric_limits<decltype(k1a)>::min(),
             std::numeric_limits<typename SI::base_type>::min());

    // Swap
    SI k1c(10), k1d(20);
    k1c.swap(k1d);
    QCOMPARE(k1c, SI(20));
    QCOMPARE(k1d, SI(10));
    std::swap(k1c, k1d);
    QCOMPARE(k1c, SI(10));
    QCOMPARE(k1d, SI(20));

    auto bt = BT(30);
    k1d.swap(bt);
    QCOMPARE(k1d, SI(30));
    std::swap(k1d, bt);
    QCOMPARE(k1d, SI(20));

    // containers
    SI4 x4(0);
    SI4(x4+3);

    std::set<SI4> set;
    std::unordered_set<SI4> unorderedSet;

    QCOMPARE(set.insert(x4).second, true);
    QCOMPARE(unorderedSet.insert(x4).second, true);

    QCOMPARE(*set.find(x4), x4);
    QCOMPARE(*unorderedSet.find(x4), x4);

    std::vector<SI4> x4vec = { x4, x4+3 };
    QCOMPARE(x4vec[0], x4);
    QCOMPARE(x4vec[1], x4+3);
}

template <typename SI>
void TestLdUtils::strongnum_single_i()
{
    strongnum_single_all<SI>();  // run all the common tests, which don't do bitwise ops

    using BT = typename SI::base_type;

    // should succeed
    SI k1a = SI(5);
    SI k1b(2);

    QCOMPARE(k1a %   BT(2), SI(1));
    QCOMPARE(k1a,    SI(5));
    QCOMPARE(k1a %=  BT(2), SI(1));
    QCOMPARE(k1a,    SI(1));
    QCOMPARE(k1a =   SI(5), SI(5));

    // Bitwise
    QCOMPARE(k1a &  BT(3),   SI(1));
    QCOMPARE(k1a |  BT(3),   SI(7));
    QCOMPARE(k1a ^  BT(3),   SI(6));
    QCOMPARE(k1a |=  BT(2),  SI(7));
    QCOMPARE(k1a &=  ~BT(2), SI(5));
    QCOMPARE(~k1a,           SI(~5));
    QCOMPARE(k1a << k1b,     SI(20));
    QCOMPARE(k1a >> k1b,     SI(1));
    QCOMPARE(k1a <<= k1b,    SI(20));
    QCOMPARE(k1a,            SI(20));
    QCOMPARE(k1a >>= k1b,    SI(5));
    QCOMPARE(k1a,            SI(5));
}

template <typename SI>
void TestLdUtils::strongnum_single_f()
{
    using BT = typename SI::base_type; // base

    strongnum_single_all<SI>();

    const SI k1f(0.25);
    const SI k1g(0.5);
    const SI k1h(1.5);

    QCOMPARE(std::cos(k1f),                 SI(std::cos(BT(k1f))));
    QCOMPARE(std::sin(k1f),                 SI(std::sin(BT(k1f))));
    QCOMPARE(std::acos(k1f),                SI(std::acos(BT(k1f))));
    QCOMPARE(std::tan(k1f),                 SI(std::tan(BT(k1f))));
    QCOMPARE(std::acos(k1f),                SI(std::acos(BT(k1f))));
    QCOMPARE(std::asin(k1f),                SI(std::asin(BT(k1f))));
    QCOMPARE(std::atan(k1f),                SI(std::atan(BT(k1f))));
    QCOMPARE(std::atan2(k1f, k1g),          SI(std::atan2(BT(k1f), BT(k1g))));
    QCOMPARE(std::cosh(k1f),                SI(std::cosh(BT(k1f))));
    QCOMPARE(std::sinh(k1f),                SI(std::sinh(BT(k1f))));
    QCOMPARE(std::tanh(k1f),                SI(std::tanh(BT(k1f))));
    QCOMPARE(std::acosh(k1h),               SI(std::acosh(BT(k1h))));
    QCOMPARE(std::asinh(k1f),               SI(std::asinh(BT(k1f))));
    QCOMPARE(std::atanh(k1f),               SI(std::atanh(BT(k1f))));
    QCOMPARE(std::exp(k1f),                 SI(std::exp(BT(k1f))));
    QCOMPARE(std::log(k1f),                 SI(std::log(BT(k1f))));
    QCOMPARE(std::log10(k1f),               SI(std::log10(BT(k1f))));
    QCOMPARE(std::exp2(k1f),                SI(std::exp2(BT(k1f))));
    QCOMPARE(std::expm1(k1f),               SI(std::expm1(BT(k1f))));
    QCOMPARE(std::log2(k1f),                SI(std::log2(BT(k1f))));
    QCOMPARE(std::logb(k1f),                SI(std::logb(BT(k1f))));
    QCOMPARE(std::pow(k1f, k1g),            SI(std::pow(BT(k1f), BT(k1g))));
    QCOMPARE(std::sqrt(k1f),                SI(std::sqrt(BT(k1f))));
    QCOMPARE(std::cbrt(k1f),                SI(std::cbrt(BT(k1f))));
    QCOMPARE(std::hypot(k1f, k1g),          SI(std::hypot(BT(k1f), BT(k1g))));
    QCOMPARE(std::erf(k1f),                 SI(std::erf(BT(k1f))));
    QCOMPARE(std::erfc(k1f),                SI(std::erfc(BT(k1f))));
    QCOMPARE(std::tgamma(k1f),              SI(std::tgamma(BT(k1f))));
    QCOMPARE(std::lgamma(k1f),              SI(std::lgamma(BT(k1f))));
    QCOMPARE(std::ceil(k1f),                SI(std::ceil(BT(k1f))));
    QCOMPARE(std::floor(k1f),               SI(std::floor(BT(k1f))));
    QCOMPARE(std::fmod(k1f, k1g),           SI(std::fmod(BT(k1f), BT(k1g))));
    QCOMPARE(std::trunc(k1f),               SI(std::trunc(BT(k1f))));
    QCOMPARE(std::round(k1f),               SI(std::round(BT(k1f))));
    QCOMPARE(std::lround(k1f),              std::lround(BT(k1f)));
    QCOMPARE(std::llround(k1f),             std::llround(BT(k1f)));
    QCOMPARE(std::rint(k1f),                SI(std::rint(BT(k1f))));
    QCOMPARE(std::lrint(k1f),               std::lrint(BT(k1f)));
    QCOMPARE(std::llrint(k1f),              std::llrint(BT(k1f)));
    QCOMPARE(std::nearbyint(k1f),           SI(std::nearbyint(BT(k1f))));
    QCOMPARE(std::remainder(k1f, k1g),      SI(std::remainder(BT(k1f), BT(k1g))));
    QCOMPARE(std::copysign(k1f, k1g),       SI(std::copysign(BT(k1f), BT(k1g))));
    QCOMPARE(std::nextafter(k1f, k1g),      SI(std::nextafter(BT(k1f), BT(k1g))));
    QCOMPARE(std::nexttoward(k1f, k1g),     SI(std::nexttoward(BT(k1f), BT(k1g))));
    QCOMPARE(std::fdim(k1f, k1g),           SI(std::fdim(BT(k1f), BT(k1g))));
    QCOMPARE(std::fmax(k1f, k1g),           SI(std::fmax(BT(k1f), BT(k1g))));
    QCOMPARE(std::fmin(k1f, k1g),           SI(std::fmin(BT(k1f), BT(k1g))));
    QCOMPARE(std::fabs(k1f),                SI(std::fabs(BT(k1f))));
    QCOMPARE(std::abs(k1f),                 SI(std::abs(BT(k1f))));
    QCOMPARE(std::isfinite(k1f),            std::isfinite(BT(k1f)));
    QCOMPARE(std::isinf(k1f),               std::isinf(BT(k1f)));
    QCOMPARE(std::isnan(k1f),               std::isnan(BT(k1f)));
    QCOMPARE(std::isnormal(k1f),            std::isnormal(BT(k1f)));
    QCOMPARE(std::fpclassify(k1f),          std::fpclassify(BT(k1f)));
    QCOMPARE(std::isgreater(k1f, k1g),      std::isgreater(BT(k1f), BT(k1g)));
    QCOMPARE(std::isgreaterequal(k1f, k1g), std::isgreaterequal(BT(k1f), BT(k1g)));
    QCOMPARE(std::isless(k1f, k1g),         std::isless(BT(k1f), BT(k1g)));
    QCOMPARE(std::islessequal(k1f, k1g),    std::islessequal(BT(k1f), BT(k1g)));
    QCOMPARE(std::islessgreater(k1f, k1g),  std::islessgreater(BT(k1f), BT(k1g)));
    QCOMPARE(std::isunordered(k1f, k1g),    std::isunordered(BT(k1f), BT(k1g)));
    QCOMPARE(std::signbit(k1f),             std::signbit(BT(k1f)));
}

// Global, for testing with QVariant
using MyVariantSI = Strong::int32_t<class MyVarKey>;
Q_DECLARE_METATYPE(MyVariantSI)

void TestLdUtils::strongnum()
{
    strongnum_single_i<Strong::Num<class K1, int8_t>>();
    strongnum_single_i<Strong::Num<class K1, int16_t>>();
    strongnum_single_i<Strong::Num<class K1, int32_t>>();
    strongnum_single_i<Strong::Num<class K1, int64_t>>();

    strongnum_single_i<Strong::Num<class K2, uint8_t>>();
    strongnum_single_i<Strong::Num<class K2, uint16_t>>();
    strongnum_single_i<Strong::Num<class K2, uint32_t>>();
    strongnum_single_i<Strong::Num<class K2, uint64_t>>();

    strongnum_single_f<Strong::Num<class K2, float>>();
    strongnum_single_f<Strong::Num<class K2, double>>();

    QVariant siVar;
    siVar.setValue(MyVariantSI(42));
    QCOMPARE(siVar.value<MyVariantSI>(), MyVariantSI(42));
}

// Give Units a default constructor so it can be used with QTest
class UnitsDef : public Units {
public:
    using Units::Units;
    UnitsDef() : Units(Format::String) { }
};

Q_DECLARE_METATYPE(UnitsDef)

void TestLdUtils::units_data()
{
    QTest::addColumn<UnitsDef>("unit");
    QTest::addColumn<double>("value");
    QTest::addColumn<QString>("output");

    QTest::addRow("km")   << UnitsDef(Format::Distkm) << 5000.0 << "5.00 Km";
    QTest::addRow("k")    << UnitsDef(Format::Distm)  << 5000.0 << "5,000.00 m";
    QTest::addRow("ft")   << UnitsDef(Format::Distft) << 5000.0 << "16,404.20 ft";
    QTest::addRow("w")    << UnitsDef(Format::Watt)   << 100.0  << "100.00 W";
    QTest::addRow("hp")   << UnitsDef(Format::hp)     << 100.0  << "0.13 hp";
}

void TestLdUtils::units()
{
    QFETCH(UnitsDef, unit);
    QFETCH(double, value);
    QFETCH(QString, output);

    QCOMPARE(unit(value), output);
}

void TestLdUtils::query_data()
{
    QTest::addColumn<bool>("isValid");
    QTest::addColumn<bool>("isMatch");
    QTest::addColumn<QString>("query");
    QTest::addColumn<QVariant>("data");

    QTest::newRow("err.regex")            << false << false << "foo("                    << QVariant("foo");
    QTest::newRow("err.column")           << false << false << "NotAColumn == x"         << QVariant("foo");
    QTest::newRow("err.op")               << false << false << "NotAColumn == x"         << QVariant("foo");
    QTest::newRow("err.bad_nesting-1")    << false << false << "( * == x"                << QVariant("foo");
    QTest::newRow("err.bad_nesting-2")    << false << false << "* == x )"                << QVariant("foo");
    QTest::newRow("err.bad_nesting-3")    << false << false << "( * == x ) ("            << QVariant("foo");
    QTest::newRow("err.closing-quote")    << false << false << "\"testing"               << QVariant("foo");
    QTest::newRow("err.opening-quote")    << false << false << "testing\""               << QVariant("foo");

    QTest::newRow("match.regex-1")        << true  << true  << "foo.*bar"                << QVariant("foobar");
    QTest::newRow("match.regex-2")        << true  << true  << "foo.*bar"                << QVariant("foozzzbar");
    QTest::newRow("match.regex-3")        << true  << true  << "( foo.*bar )"            << QVariant("foozbar");
    QTest::newRow("match.regex-4")        << true  << true  << "( ^foobar$ )"            << QVariant("foobar");
    QTest::newRow("match.regex-5")        << true  << true  << "* ~ ^foobar$"            << QVariant("foobar");
    QTest::newRow("match.regex-6")        << true  << true  << "* !~ foo.*bar"           << QVariant("fobar");

    QTest::newRow("match.spaces-1")       << true  << true  << R"("t.*t 1 2 3")"         << QVariant("test 1 2 3");
    QTest::newRow("match.spaces-2")       << true  << true  << R"(  "t.*t 1 2 3")"       << QVariant("test 1 2 3");
    QTest::newRow("match.esc-quote")      << true  << true  << R"("t.*t \"1\" 2 3")"     << QVariant("test \"1\" 2 3");

    QTest::newRow("match.op.eq-1")        << true  << true  << "* == foobar"             << QVariant("foobar");
    QTest::newRow("match.op.ne-1")        << true  << true  << "* != foobar"             << QVariant("foobarr");
    QTest::newRow("match.op.lt-1")        << true  << true  << "* < g"                   << QVariant("foo");
    QTest::newRow("match.op.le-1")        << true  << true  << "* <= foo"                << QVariant("foo");
    QTest::newRow("match.op.le-2")        << true  << true  << "* <= g"                  << QVariant("foo");
    QTest::newRow("match.op.gt-1")        << true  << true  << "* > e"                   << QVariant("foo");
    QTest::newRow("match.op.ge-1")        << true  << true  << "* >= e"                  << QVariant("foo");
    QTest::newRow("match.op.ge-2")        << true  << true  << "* >= foo"                << QVariant("foo");

    QTest::newRow("match.expr.and-1")     << true  << true  << "foo && bar"              << QVariant("foobar");
    QTest::newRow("match.expr.and-2")     << true  << true  << "foo && bar"              << QVariant("barfoo");
    QTest::newRow("match.expr.and-3")     << true  << true  << "foo & bar"               << QVariant("foo bar");
    QTest::newRow("match.expr.and-4")     << true  << true  << "foo & bar"               << QVariant("bar foo");
    QTest::newRow("match.expr.and-5")     << true  << true  << "foo & bar & * > e"       << QVariant("foo blee bar");

    QTest::newRow("match.expr.or-1")      << true  << true  << "foo || bar"              << QVariant("foo");
    QTest::newRow("match.expr.or-2")      << true  << true  << "foo || bar"              << QVariant("bar");
    QTest::newRow("match.expr.or-3")      << true  << true  << "foo | bar"               << QVariant("foo");
    QTest::newRow("match.expr.or-4")      << true  << true  << "foo | bar"               << QVariant("bar");
    QTest::newRow("match.expr.or-5")      << true  << true  << "foo || bar | * > e"      << QVariant("z");
    QTest::newRow("match.expr.or-6")      << true  << true  << "foo || bar | * > e"      << QVariant("foo");

    QTest::newRow("match.expr.xor-1")     << true  << true  << "foo ^^ bar"              << QVariant("foo");
    QTest::newRow("match.expr.xor-2")     << true  << true  << "foo ^ bar"               << QVariant("bar");

    QTest::newRow("match.expr.prec-1")    << true  << true  << "foo & bar | * > g"       << QVariant("foo bar");
    QTest::newRow("match.expr.prec-2")    << true  << true  << "foo & bar | * > g"       << QVariant("zzz");
    QTest::newRow("match.expr.prec-3")    << true  << true  << "* > g | foo & bar"       << QVariant("foo bar");
    QTest::newRow("match.expr.prec-4")    << true  << true  << "* > g | foo & bar"       << QVariant("zzz");

    QTest::newRow("match.expr.paren-1")   << true  << true  << "foo & ( bar | * > g )"   << QVariant("foo bar");
    QTest::newRow("match.expr.paren-2")   << true  << true  << "foo & ( bar | * > g )"   << QVariant("z foo");

    QTest::newRow("match.list.1")         << true  << true  << "foo"                     << QVariant(QStringList{ "foo", "bar" });
    QTest::newRow("match.list.2")         << true  << true  << "foo"                     << QVariant(QStringList{ "bar", "foo" });

    QTest::newRow("nomatch.regex-1")      << true  << false << "foo.*bar"                << QVariant("fobar");
    QTest::newRow("nomatch.regex-2")      << true  << false << "foo.*bar"                << QVariant("foozzzba");
    QTest::newRow("nomatch.regex-3")      << true  << false << "( ^foobar$ )"            << QVariant("foobarr");
    QTest::newRow("nomatch.regex-4")      << true  << false << "( ^foobar$ )"            << QVariant("ffoobar");
    QTest::newRow("nomatch.regex-5")      << true  << false << "* !~ ^foobar$"           << QVariant("foobar");
    QTest::newRow("nomatch.regex-6")      << true  << false << "* !~ foo.*bar"           << QVariant("foobar");
    QTest::newRow("nomatch.regex-7")      << true  << false << "* !~ foo.*bar"           << QVariant("foozzzbar");
    QTest::newRow("nomatch.op.eq-1")      << true  << false << "* == foobar"             << QVariant("foobarr");
    QTest::newRow("nomatch.op.eq-2")      << true  << false << "* == foobar"             << QVariant("foobarr");
    QTest::newRow("nomatch.op.eq-3")      << true  << false << "* == foo.*bar"           << QVariant("foobar");
    QTest::newRow("nomatch.op.ne-1")      << true  << false << "* != foobar"             << QVariant("foobar");
    QTest::newRow("nomatch.op.lt-1")      << true  << false << "* < foo"                 << QVariant("g");
    QTest::newRow("nomatch.op.lt-2")      << true  << false << "* < foo"                 << QVariant("foo");
    QTest::newRow("nomatch.op.le-1")      << true  << false << "* <= foo"                << QVariant("g");
    QTest::newRow("nomatch.op.gt-1")      << true  << false << "* > foo"                 << QVariant("e");
    QTest::newRow("nomatch.op.gt-2")      << true  << false << "* > foo"                 << QVariant("foo");
    QTest::newRow("nomatch.op.ge-1")      << true  << false << "* >= foo"                << QVariant("e");

    QTest::newRow("nomatch.expr.and-1")   << true  << false << "foo && bar"              << QVariant("foo");
    QTest::newRow("nomatch.expr.and-2")   << true  << false << "foo && bar"              << QVariant("bar");
    QTest::newRow("nomatch.expr.and-3")   << true  << false << "foo & bar"               << QVariant("foo");
    QTest::newRow("nomatch.expr.and-4")   << true  << false << "foo & bar"               << QVariant("bar");
    QTest::newRow("nomatch.expr.and-5")   << true  << false << "foo & bar & * > g"       << QVariant("foo blee bar");

    QTest::newRow("nomatch.expr.or-1")    << true  << false << "foo || bar"              << QVariant("fo");
    QTest::newRow("nomatch.expr.or-2")    << true  << false << "foo || bar"              << QVariant("ba");
    QTest::newRow("nomatch.expr.or-3")    << true  << false << "foo | bar"               << QVariant("fo");
    QTest::newRow("nomatch.expr.or-4")    << true  << false << "foo | bar"               << QVariant("ba");
    QTest::newRow("nomatch.expr.or-5")    << true  << false << "foo || bar | * > e"      << QVariant("a");

    QTest::newRow("nomatch.expr.xor-1")   << true  << false << "foo ^^ bar"              << QVariant("foo bar");
    QTest::newRow("nomatch.expr.xor-2")   << true  << false << "foo ^ bar"               << QVariant("bar foo");
    QTest::newRow("nomatch.expr.xor-3")   << true  << false << "foo ^ bar"               << QVariant("a");

    QTest::newRow("nomatch.expr.prec-1")  << true  << false << "foo & bar | * > g"       << QVariant("bar");
    QTest::newRow("nomatch.expr.prec-2")  << true  << false << "! ( foo & bar ) | * > g" << QVariant("foo bar");

    QTest::newRow("nomatch.expr.paren-1") << true  << false << "foo & ( bar | * > g )"   << QVariant("bar");
    QTest::newRow("nomatch.expr.paren-2") << true  << false << "foo & ( bar | * > g )"   << QVariant("foo");

    QTest::newRow("nomatch.list.1")       << true  << false << "foo"                     << QVariant(QStringList{ "zzz", "zr" });
    QTest::newRow("nomatch.list.2")       << true  << false << "foo"                     << QVariant(QStringList{ });


}

void TestLdUtils::query()
{
    QFETCH(bool, isValid);
    QFETCH(bool, isMatch);
    QFETCH(QString, query);
    QFETCH(QVariant, data);

    static const Units unitsStr(Format::String);

    Query::query_uniqueptr_t queryRoot =
            Query::Context().setParseUnits({&unitsStr}).parse(query);

    QVERIFY2(queryRoot->isValid() == isValid,
             qUtf8Printable(QString("validity error (expected ") +
                            QString(isValid ? "true" : "false") +
                            "): " + query));

    QVERIFY2(queryRoot->match(data) == isMatch,
             qUtf8Printable(QString("match error (expected ") +
                            QString(isValid ? "true" : "false") +
                            "): " + query));
}

void TestLdUtils::query_patterns_data()
{
    using PL = Query::Base::PatternList;

    QTest::addColumn<QString>("query");
    QTest::addColumn<PL>("patterns");

    QTest::newRow("basic-1") << "foo"       << PL({ QRegularExpression("foo", QRegularExpression::CaseInsensitiveOption ) });
    QTest::newRow("basic-2") << "foo.*bar"  << PL({ QRegularExpression("foo.*bar", QRegularExpression::CaseInsensitiveOption) });

    QTest::newRow("multi-1") << "foo & bar" << PL({ QRegularExpression("foo", QRegularExpression::CaseInsensitiveOption),
                                                    QRegularExpression("bar", QRegularExpression::CaseInsensitiveOption) });

    QTest::newRow("expr-eq-1") << "* == f.*o"  << PL({ QRegularExpression(R"(\Af\.\*o\z)", QRegularExpression::CaseInsensitiveOption) });

    QTest::newRow("expr-eq-2") << "* == f.*o && bar"  << PL({ QRegularExpression(R"(\Af\.\*o\z)", QRegularExpression::CaseInsensitiveOption),
                                                           QRegularExpression("bar", QRegularExpression::CaseInsensitiveOption)} );

    QTest::newRow("expr-ne-1") << "* != f" << PL({ });
    QTest::newRow("expr-lt-1") << "* < f"  << PL({ QRegularExpression(R"(\Af)", QRegularExpression::CaseInsensitiveOption) });
}

void TestLdUtils::query_patterns()
{
    using PL = Query::Base::PatternList;
    QFETCH(QString, query);
    QFETCH(PL, patterns);

    static const Units unitsStr(Format::String);

    Query::query_uniqueptr_t queryRoot =
            Query::Context().setParseUnits({&unitsStr}).parse(query);

    QCOMPARE(queryRoot->patterns(), patterns);
}


void TestLdUtils::text_editor()
{
    static const QRegularExpression passBR("<span.*weight:600.*Bolded.*</span>.*Not_Bolded.*"
                                           "<span.*font-style:italic.*Italic.*</span>.*Not_Italic");
    static const QRegularExpression passPaste("New contents");

    TextEditor editor;

    auto* textEdit     = editor.findChild<QTextEdit*>();
    auto* formatBold   = editor.findChild<QToolButton*>("formatBold");
    auto* formatItalic = editor.findChild<QToolButton*>("formatItalic");
    auto* editCopy     = editor.findChild<QToolButton*>("editCopy");
    auto* editCut      = editor.findChild<QToolButton*>("editCut");
    auto* editPaste    = editor.findChild<QToolButton*>("editPaste");
    auto* editUndo     = editor.findChild<QToolButton*>("editUndo");
    auto* editRedo     = editor.findChild<QToolButton*>("editRedo");
    auto* insertTable  = editor.findChild<QToolButton*>("insertTable");
    auto* mergeCells   = editor.findChild<QToolButton*>("mergeCells");
    auto* splitCells   = editor.findChild<QToolButton*>("splitCells");
    auto* rowDelete    = editor.findChild<QToolButton*>("rowDelete");
    auto* columnDelete = editor.findChild<QToolButton*>("columnDelete");
    auto* rowAdd       = editor.findChild<QToolButton*>("rowAdd");
    auto* columnAdd    = editor.findChild<QToolButton*>("columnAdd");
    auto* columnSize   = editor.findChild<QToolButton*>("columnSize");


    QVERIFY(textEdit      != nullptr);
    QVERIFY(formatBold    != nullptr);
    QVERIFY(formatItalic  != nullptr);
    QVERIFY(editCopy      != nullptr);
    QVERIFY(editCut       != nullptr);
    QVERIFY(editPaste     != nullptr);
    QVERIFY(editUndo      != nullptr);
    QVERIFY(editRedo      != nullptr);
    QVERIFY(insertTable   != nullptr);
    QVERIFY(mergeCells    != nullptr);
    QVERIFY(splitCells    != nullptr);
    QVERIFY(rowDelete     != nullptr);
    QVERIFY(columnDelete  != nullptr);
    QVERIFY(rowAdd        != nullptr);
    QVERIFY(columnAdd     != nullptr);
    QVERIFY(columnSize    != nullptr);

    QString html("html");

    editor.setHtml("<html><head/><body>This is a test</body></html");
    QTest::mouseClick(formatBold, Qt::LeftButton);
    QTest::keyClicks(textEdit, " Bolded ");
    QTest::mouseClick(formatBold, Qt::LeftButton);
    QTest::keyClicks(textEdit, " Not_Bolded ");

    QTest::mouseClick(formatItalic, Qt::LeftButton);
    QTest::keyClicks(textEdit, " Italic ");
    QTest::mouseClick(formatItalic, Qt::LeftButton);
    QTest::keyClicks(textEdit, " Not_Italic ");

    QCOMPARE(passBR.match(editor.toHtml()).hasMatch(), true);

    // Test copy
    textEdit->selectAll();
    QTest::mouseClick(editCopy, Qt::LeftButton);
    QCOMPARE(passBR.match(QGuiApplication::clipboard()->text(html)).hasMatch(), true);

    // Test cut and paste
    QTest::mouseClick(editCut, Qt::LeftButton);
    QCOMPARE(passBR.match(QGuiApplication::clipboard()->text(html)).hasMatch(), true);
    QCOMPARE(passBR.match(editor.toHtml()).hasMatch(), false);
    QGuiApplication::clipboard()->setText("New contents");
    QTest::mouseClick(editPaste, Qt::LeftButton);
    QCOMPARE(passPaste.match(editor.toHtml()).hasMatch(), true);

    // test undo/redo
    QTest::mouseClick(editUndo, Qt::LeftButton);
    QCOMPARE(passPaste.match(editor.toHtml()).hasMatch(), false);
    QTest::mouseClick(editRedo, Qt::LeftButton);
    QCOMPARE(passPaste.match(editor.toHtml()).hasMatch(), true);

//    printf("%s\n", qUtf8Printable(editor.toHtml(html)));

}

void TestLdUtils::text_editor_table_format()
{
    TextEditorTableFormat tableFormat(nullptr);  // nullptr means create a new table format

    auto* rows         = tableFormat.findChild<QSpinBox*>("rows");
    auto* columns      = tableFormat.findChild<QSpinBox*>("columns");
    auto* cellPadding  = tableFormat.findChild<QDoubleSpinBox*>("cellPadding");
    auto* cellSpacing  = tableFormat.findChild<QDoubleSpinBox*>("cellSpacing");
    auto* marginL      = tableFormat.findChild<QDoubleSpinBox*>("marginL");
    auto* marginR      = tableFormat.findChild<QDoubleSpinBox*>("marginR");
    auto* marginT      = tableFormat.findChild<QDoubleSpinBox*>("marginT");
    auto* marginB      = tableFormat.findChild<QDoubleSpinBox*>("marginB");
    auto* position     = tableFormat.findChild<QComboBox*>("position");
    auto* borderWidth  = tableFormat.findChild<QDoubleSpinBox*>("borderWidth");
    auto* hAlignment   = tableFormat.findChild<QComboBox*>("hAlignment");
    auto* vAlignment   = tableFormat.findChild<QComboBox*>("vAlignment");
    auto* borderStyle  = tableFormat.findChild<QComboBox*>("borderStyle");

    QVERIFY(rows         != nullptr);
    QVERIFY(columns      != nullptr);
    QVERIFY(cellPadding  != nullptr);
    QVERIFY(cellSpacing  != nullptr);
    QVERIFY(marginL      != nullptr);
    QVERIFY(marginR      != nullptr);
    QVERIFY(marginT      != nullptr);
    QVERIFY(marginB      != nullptr);
    QVERIFY(position     != nullptr);
    QVERIFY(borderWidth  != nullptr);
    QVERIFY(borderStyle  != nullptr);
    QVERIFY(hAlignment   != nullptr);
    QVERIFY(vAlignment   != nullptr);

    // Test defaults
    QCOMPARE(tableFormat.rows(), 3);
    QCOMPARE(tableFormat.columns(), 3);
    QCOMPARE(tableFormat.format().cellPadding(), 2.0);
    QCOMPARE(tableFormat.format().cellSpacing(), 0.0);
    QCOMPARE(tableFormat.format().leftMargin(), 2.0);
    QCOMPARE(tableFormat.format().rightMargin(), 2.0);
    QCOMPARE(tableFormat.format().topMargin(), 2.0);
    QCOMPARE(tableFormat.format().bottomMargin(), 2.0);
    QCOMPARE(tableFormat.format().border(), 1.0);
    QCOMPARE(tableFormat.format().borderStyle(), QTextTableFormat::BorderStyle_Outset);
    QCOMPARE(tableFormat.format().alignment(), (Qt::AlignJustify | Qt::AlignBaseline));
    QCOMPARE(tableFormat.format().position(), QTextTableFormat::InFlow);

    // Set some values and re-test
    rows->setValue(4);
    columns->setValue(2);
    cellPadding->setValue(0.3);
    cellSpacing->setValue(0.5);
    marginL->setValue(0.1);
    marginR->setValue(0.2);
    marginT->setValue(0.3);
    marginB->setValue(0.4);
    borderWidth->setValue(2.1);
    position->setCurrentIndex(1); // FloatLeft
    borderStyle->setCurrentIndex(2); // Dashed
    hAlignment->setCurrentIndex(0); // Left
    vAlignment->setCurrentIndex(2); // Center

    // Test after UI updates
    QCOMPARE(tableFormat.rows(), 4);
    QCOMPARE(tableFormat.columns(), 2);
    QCOMPARE(tableFormat.format().cellPadding(), 0.3);
    QCOMPARE(tableFormat.format().cellSpacing(), 0.5);
    QCOMPARE(tableFormat.format().leftMargin(), 0.1);
    QCOMPARE(tableFormat.format().rightMargin(), 0.2);
    QCOMPARE(tableFormat.format().topMargin(), 0.3);
    QCOMPARE(tableFormat.format().bottomMargin(), 0.4);
    QCOMPARE(tableFormat.format().border(), 2.1);
    QCOMPARE(tableFormat.format().borderStyle(), QTextTableFormat::BorderStyle_Dashed);
    QCOMPARE(tableFormat.format().alignment(), (Qt::AlignLeft | Qt::AlignVCenter));
    QCOMPARE(tableFormat.format().position(), QTextTableFormat::FloatLeft);

    tableFormat.defaults();  // would like to trigger the button, but it's hard to find from here.

    // Should be back to defaults, except for rows and columns, which stay.
    QCOMPARE(tableFormat.rows(), 4);
    QCOMPARE(tableFormat.columns(), 2);
    QCOMPARE(tableFormat.format().cellPadding(), 2.0);
    QCOMPARE(tableFormat.format().cellSpacing(), 0.0);
    QCOMPARE(tableFormat.format().leftMargin(), 2.0);
    QCOMPARE(tableFormat.format().rightMargin(), 2.0);
    QCOMPARE(tableFormat.format().topMargin(), 2.0);
    QCOMPARE(tableFormat.format().bottomMargin(), 2.0);
    QCOMPARE(tableFormat.format().border(), 1.0);
    QCOMPARE(tableFormat.format().borderStyle(), QTextTableFormat::BorderStyle_Outset);
    QCOMPARE(tableFormat.format().alignment(), (Qt::AlignJustify | Qt::AlignBaseline));
    QCOMPARE(tableFormat.format().position(), QTextTableFormat::InFlow);
}

TocTest::TocTest()
{
    setupTOC();
}

TocTest::~TocTest() { }

void TocTest::setupTOC() {
    setupTOC(&m_treeView, {
                 { 0,    "Intro",           0 },
                 { 0,    "Part 1",         -1 },
                    { 1, "Part 1.A",        1 },
                    { 1, "Part 1.B",        2 },
                       { 2, "Part 1.B.i",   3 },
                       { 2, "Part 1.B.ii",  4 },
                 { 0,    "Part 2",         -1 },
                    { 1, "Part 2.A",        5 },
             });
}

void TestLdUtils::toc_list()
{
    TocTest tocTest;

    const auto hasPageP = [&tocTest](const QModelIndex& idx) {
        return tocTest.hasPage(idx);
    };

    QCOMPARE(Util::RowIndexes(tocTest.m_tocModel).count(), 8);
    QCOMPARE(Util::RowIndexes(tocTest.m_tocModel, hasPageP).count(), 6);

    const QModelIndexList indexes = Util::RowIndexes(tocTest.m_tocModel);

    {
        int i = 0;
        for (QModelIndex idx = Util::FirstIndex(tocTest.m_tocModel); idx.isValid(); idx = Util::NextIndex(idx))
            QCOMPARE(idx, indexes.at(i++));
    }

    {
        int i = indexes.count() - 1;
        for (QModelIndex idx = Util::LastIndex(tocTest.m_tocModel); idx.isValid(); idx = Util::PrevIndex(idx))
            QCOMPARE(idx, indexes.at(i--));
    }
}

int main(int argc, char* argv[])
{
    // Force timezone to GMT to normalize any QDateTime output
    qputenv("TZ", "GMT");

    QApplication::setApplicationName("testldutils");

    QApplication app(argc, argv);

    app.setProperty("test", true);
    QTest::setMainSourcePath(__FILE__, QT_TESTCASE_BUILDDIR);

    TestLdUtils tester;
    return QTest::qExec(&tester, argc, argv);
}
