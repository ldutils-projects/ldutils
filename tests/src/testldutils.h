/*
    Copyright 2022-2023 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TESTLDUTILS_H
#define TESTLDUTILS_H

#include <experimental/type_traits>

#include <QtTest>
#include <QObject>
#include <QTreeView>

#include <src/ui/misc/toclist.h>
#include <src/util/strongnum.h>

class TestLdUtils : public QObject
{
    Q_OBJECT

private slots:
    void strongnum_static();  // static tests
    void strongnum();

    static void units_data();
    static void units();

    static void text_editor();
    static void text_editor_table_format();

    static void query_data();
    static void query();

    static void query_patterns_data();
    static void query_patterns();

    static void toc_list();

private:
    template <int opt>         void strongnum_static_iterate();
    template <typename SI>     void strongnum_single();
    template <typename SI>     void strongnum_single_all();        // float and int common tests
    template <typename SI>     void strongnum_single_i();          // integral types
    template <typename SI>     void strongnum_single_f();          // float types
    template <typename SI>     void strongnum_single_static_all(); // float and int common tests
    template <typename SI>     void strongnum_single_static_i();   // integral types
    template <typename SI>     void strongnum_single_static_f();   // float types
    template <Strong::Options> void strongnum_all_sizes_static();

    // Helper types for std::experimental::is_detected_v
    template <class T, class U> using add_t = decltype(std::declval<T>() + std::declval<U>());
    template <class T, class U> using sub_t = decltype(std::declval<T>() - std::declval<U>());
    template <class T, class U> using mul_t = decltype(std::declval<T>() * std::declval<U>());
    template <class T, class U> using div_t = decltype(std::declval<T>() / std::declval<U>());
    template <class T, class U> using lshift_t = decltype(std::declval<T>() << std::declval<U>());
    template <class T, class U> using rshift_t = decltype(std::declval<T>() >> std::declval<U>());
    template <class T, class U> using lshift_asgn_t = decltype(std::declval<T>() <<= std::declval<U>());
    template <class T, class U> using rshift_asgn_t = decltype(std::declval<T>() >>= std::declval<U>());
    template <class T, class U> using and_t = decltype(std::declval<T>() & std::declval<U>());
    template <class T, class U> using or_t = decltype(std::declval<T>() | std::declval<U>());
    template <class T, class U> using xor_t = decltype(std::declval<T>() | std::declval<U>());
    template <class T, class U> using mod_t = decltype(std::declval<T>() | std::declval<U>());
    template <class T, class U> using and_asgn_t = decltype(std::declval<T>() &= std::declval<U>());
    template <class T, class U> using or_asgn_t = decltype(std::declval<T>() |= std::declval<U>());
    template <class T, class U> using mod_asgn_t = decltype(std::declval<T>() | std::declval<U>());
    template <class T> using bit_neg_t = decltype(~std::declval<T>());
    template <class T> using unary_sub_t = decltype(-std::declval<T>());
};

class TocTest final :
        public QWidget,
        public TOCList
{
    Q_OBJECT
public:
    TocTest();
    ~TocTest() override;

protected slots:
    void changePage(const QModelIndex&) override { }

private:
    friend class TestLdUtils;

    QStringList tocSearchStrings(const QModelIndex&) const override { return { }; }
    bool hasPage(const QModelIndex& idx) const override { return tocResource(idx).toInt() >= 0; }
    void tocHighlightMatches(const QModelIndex&, bool) override { }

    using TOCList::setupTOC;
    void setupTOC() override;

    QTreeView m_treeView;
};

#endif // TESTLDUTILS_H
